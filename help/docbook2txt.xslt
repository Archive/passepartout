<?xml version='1.0'?>
<xsl:stylesheet version="1.0"
	        xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<!-- 
  Docbook-to-plaintext stylesheet
  NOTE: this is tested only on the documentation for Passepartout
-->

  <xsl:output method="text" encoding="iso-8859-1"/>

  <xsl:variable name="uc">ABCDEFGHIJKLMNOPQRSTUVWXYZ</xsl:variable>
  <xsl:variable name="dc">abcdefghijklmnopqrstuvwxyz</xsl:variable>

  <xsl:strip-space elements="*"/>

  <xsl:template match="releaseinfo|footnote"/>

  <xsl:template match="title">
    <xsl:text>

    </xsl:text>
    <xsl:apply-templates/>
    <xsl:text>

    </xsl:text>
  </xsl:template>

  <xsl:template match="command|quote">
    <xsl:text>"</xsl:text>
    <xsl:apply-templates/>
    <xsl:text>"</xsl:text>
  </xsl:template>

  <xsl:template match="para">
    <xsl:apply-templates/>
    <xsl:text>
    </xsl:text>
  </xsl:template>

  <xsl:template match="articleinfo/title">
    <xsl:text>*** </xsl:text>
    <xsl:apply-templates/>
    <xsl:text> ***</xsl:text>
  </xsl:template>

  <xsl:template match="article/section/title//text()">
    <xsl:value-of select="translate(., $dc, $uc)"/>
  </xsl:template>

  <xsl:template match="*">
    <xsl:apply-templates/>
  </xsl:template>

  <xsl:template match="text()">
    <xsl:value-of select="translate(., '&#160;', ' ')"/>
  </xsl:template>

</xsl:stylesheet>
