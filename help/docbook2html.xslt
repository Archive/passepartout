<?xml version='1.0'?>
<xsl:stylesheet version="1.0"
	        xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<!-- 
  Docbook-to-html stylesheet
  NOTE: this is tested only on the documentation for Passepartout
-->

  <xsl:output method="html"
	      encoding="iso-8859-1"
	      version="1.0"
	      indent="yes"/>
  
  <xsl:param name="root"/>

  <xsl:template match="/">
    <html>
      <head>
        <title><xsl:value-of select="//articleinfo/title//text()"/></title>
	<!-- don't set the CSS unless a root is specified -->
	<xsl:if test="$root">
	  <link rel="stylesheet" href="{$root}style.css"/>
	  <link rel="SHORTCUT ICON" href="{$root}favicon.ico"/>
	</xsl:if>
      </head>
      <body>
        <h1><xsl:value-of select="//articleinfo/title//text()"/></h1>
        <!-- don't make ridiculously sort TOC:s -->
        <xsl:if test="count(//section) &gt; 10">
          <p>
            <ul>
              <xsl:for-each select="article/section">
                <xsl:call-template name="tocsection"/>
              </xsl:for-each>
            </ul>
          </p>
        </xsl:if>
        <xsl:apply-templates/>
      </body>
    </html>
  </xsl:template>
  
  <xsl:template match="articleinfo"/>

  <xsl:template name="tocsection">
    <li>
      <a href="#{generate-id(.)}">
        <xsl:apply-templates select="./title/*|./title/text()"/>
      </a>
      <xsl:if test="section">
        <ul>
          <xsl:for-each select="section">
            <xsl:call-template name="tocsection"/>
          </xsl:for-each>
        </ul>
      </xsl:if>
    </li>
  </xsl:template>

  <xsl:template match="section">
    <div id="{generate-id(.)}">
      <xsl:apply-templates/>
    </div>
  </xsl:template>

  <xsl:template match="section/title">
    <h2><xsl:apply-templates/></h2>
  </xsl:template>

  <xsl:template match="section/section/title">
    <h3><xsl:apply-templates/></h3>
  </xsl:template>

  <xsl:template match="para">
    <p>
      <xsl:apply-templates/>
    </p>
  </xsl:template>

  <xsl:template match="ulink">
    <a href="{@url}"><xsl:apply-templates/></a>
  </xsl:template>

  <xsl:template match="graphic">
    <img alt="" src="{$root}{@fileref}"><xsl:apply-templates/></img>
  </xsl:template>

  <xsl:template match="email">
    <a href="mailto:{text()}"><xsl:apply-templates/></a>
  </xsl:template>

  <xsl:template match="programlisting">
    <code>
      <pre>
        <xsl:apply-templates/>
      </pre>
    </code>
  </xsl:template>
  
  <xsl:template match="para/orderedlist">
    <ol>
      <xsl:apply-templates/>
    </ol>
  </xsl:template>

  <xsl:template match="para/itemizedlist">
    <ul>
      <xsl:apply-templates/>
    </ul>
  </xsl:template>

  <xsl:template match="itemizedlist/listitem|orderedlist/listitem">
    <li><xsl:apply-templates/></li>
  </xsl:template>

  <xsl:template match="variablelist">
    <div>
      <h4><xsl:value-of select="./title/text()"/></h4>
      <dl>
        <xsl:apply-templates/>
      </dl>
    </div>
  </xsl:template>

  <xsl:template match="variablelist/title"/>

  <xsl:template match="variablelist/varlistentry/term">
    <dh><dfn><xsl:apply-templates/></dfn></dh>
  </xsl:template>

  <xsl:template match="variablelist/varlistentry/listitem/para">
    <dd><xsl:apply-templates/></dd>
  </xsl:template>

  <xsl:template match="emphasis">
    <em><xsl:apply-templates/></em>
  </xsl:template>

  <xsl:template match="quote">
    <q><xsl:apply-templates/></q>
  </xsl:template>

  <xsl:template match="glossterm">
    <dfn><xsl:apply-templates/></dfn>
  </xsl:template>

  <xsl:template match="filename">
    <strong><xsl:apply-templates/></strong>
  </xsl:template>

  <xsl:template match="varname">
    <strong><xsl:apply-templates/></strong>
  </xsl:template>

  <xsl:template match="command|application|userinput">
    <kbd><xsl:apply-templates/></kbd>
  </xsl:template>

  <xsl:template match="markup|sgmltag">
    <b><xsl:apply-templates/></b>
  </xsl:template>

  <xsl:template match="programlisting/markup">
    <b><xsl:apply-templates/></b>
  </xsl:template>

  <xsl:template match="guilabel|guimenu|guimenuitem|guibutton">
    <span class="gui"><xsl:apply-templates/></span>
  </xsl:template>

  <xsl:template match="acronym">
    <acronym><xsl:apply-templates/></acronym>
  </xsl:template>

  <xsl:template match="footnote"><!-- todo --></xsl:template>

  <xsl:template match="*">
    <xsl:apply-templates/>
  </xsl:template>

</xsl:stylesheet>
