<?xml version="1.0"?>
<document paper_name="A4" doublesided="false" landscape="false" first_page_num="1">
  <text_stream name="stream1" file="../help/C/users_guide.xml" transform="../data/docbook.xslt"/>
  <page>
    <frame name="Image pptout-sv.eps" matrix="0.18 0 0 0.18 250.667 742.66" lock="true" flowaround="true" obstaclemargin="0" type="image" file="../logotype/pptout-sv.eps"/>
    <frame name="Image hline.eps" matrix="1.58667 0 0 1 56 740" lock="true" flowaround="true" obstaclemargin="0" type="image" file="examples/hline.eps"/>
    <frame name="Text " matrix="1 0 0 1 56 109.333" lock="true" flowaround="false" obstaclemargin="0" type="text" num_columns="2" gutter_width="12" stream="stream1" width="476" height="617.334"/>
  </page>
  <page>
    <frame name="Text " matrix="1 0 0 1 58.6667 118.503" lock="false" flowaround="false" obstaclemargin="0" type="text" num_columns="2" gutter_width="12" stream="stream1" width="473.333" height="610.831"/>
    <frame name="Image pptout-sv.eps" matrix="0.18 0 0 0.18 250.667 742.66" lock="true" flowaround="false" obstaclemargin="0" type="image" file="../logotype/pptout-sv.eps"/>
    <frame name="Image hline.eps" matrix="1.58667 0 0 1 56 740" lock="true" flowaround="false" obstaclemargin="0" type="image" file="examples/hline.eps"/>
  </page>
  <page>
    <frame name="Image pptout-sv.eps" matrix="0.18 0 0 0.18 250.667 742.66" lock="true" flowaround="true" obstaclemargin="0" type="image" file="../logotype/pptout-sv.eps"/>
    <frame name="Image hline.eps" matrix="1.58667 0 0 1 56 740" lock="true" flowaround="true" obstaclemargin="0" type="image" file="examples/hline.eps"/>
    <frame name="Text " matrix="1 0 0 1 56 112" lock="true" flowaround="false" obstaclemargin="0" type="text" num_columns="2" gutter_width="12" stream="stream1" width="476" height="614.667"/>
  </page>
</document>
<!-- vim:set sw=2: -->
