<?xml version="1.0"?>
<document paper_name="A4" doublesided="false" landscape="true" first_page_num="1">
	<text_stream name="bread" file="multilingual.html" transform="../../data/xhtml.xslt">
    <parameter name="basesize" value="9.5"/>
    <parameter name="textfont" value="Gentium"/>
    <parameter name="textfontbold" value="Times-Bold"/>
    <parameter name="textfontitalic" value="Gentium-Italic"/>
  </text_stream>
  <text_stream name="head" file="multilingual-head.xml" transform=""/>
  <page>
    <frame name="Text pangram" matrix="1 0 0 1 98.6427 76.5583" lock="false" flowaround="false" obstaclemargin="0" type="text" width="658.791" height="452.259" num_columns="3" gutter_width="12" stream="bread"/>
    <frame name="Text head" matrix="-4.37114e-08 1 -1 -4.37114e-08 93.6176 78.33" lock="false" flowaround="false" obstaclemargin="0" type="text" width="447.486" height="29.1456" num_columns="1" gutter_width="12" stream="head"/>
    <frame name="Text head" matrix="1 0 0 1 317.737 72.5382" lock="false" flowaround="true" obstaclemargin="6" type="text" width="53.2661" height="452.26" num_columns="1" gutter_width="12" stream="head"/>
    <frame name="Text head" matrix="1 0 0 1 536.832 332.839" lock="false" flowaround="true" obstaclemargin="0" type="text" width="226.28" height="49.2461" num_columns="1" gutter_width="12" stream="head"/>
  </page>
</document>
