<?xml version="1.0"?>
<document paper_name="A4" doublesided="true" landscape="false" first_page_num="0">
  <text_stream name="stream1" file="snowflake.xml" transform=""/>
  <text_stream name="stream2" file="example1.html" transform="../../data/xhtml.xslt">
    <parameter name="basesize" value="10pt"/>
    <parameter name="h2size" value="1.2em"/>
  </text_stream>
  <page>
    <frame name="Image gradient.eps" matrix="1.75449 0 0 3.39369 377.659 305.276" lock="false" flowaround="false" obstaclemargin="0" type="image" file="gradient.eps"/>
    <frame name="Text " matrix="0.898794 0.438371 -0.438371 0.898794 138.894 452.115" lock="false" flowaround="false" obstaclemargin="0" type="text" num_columns="1" gutter_width="15" stream="stream2" width="232.472" height="277.355"/>
    <frame name="Image hline.eps" matrix="0.505113 -1.08322 1.42976 0.666706 213.27 872.674" lock="false" flowaround="false" obstaclemargin="0" type="image" file="hline.eps"/>
    <frame name="Text stream2" matrix="-4.37114e-08 1 -1 -4.37114e-08 566.009 66.1262" lock="false" flowaround="false" obstaclemargin="0" type="text" num_columns="1" gutter_width="12" stream="stream2" width="200" height="339.218"/>
    <frame name="Raster hippie.jpg" matrix="0.939693 0.34202 -0.34202 0.939693 -20.7115 -93.0676" lock="false" flowaround="true" obstaclemargin="6" type="raster" file="hippie.jpg" width="288" height="439"/>
    <frame name="Image hline.eps" matrix="1.68667 0 0 1 343.57 283.6" lock="false" flowaround="false" obstaclemargin="0" type="image" file="hline.eps"/>
    <frame name="Image hline.eps" matrix="0.0772781 0.883293 -0.996195 0.0871558 346.699 286.727" lock="false" flowaround="false" obstaclemargin="0" type="image" file="hline.eps"/>
    <frame name="Text stream1" matrix="1 0 0 1 396.237 311.496" lock="false" flowaround="false" obstaclemargin="0" type="text" num_columns="1" gutter_width="12" stream="stream1" width="141.391" height="303.423"/>
    <frame name="Image snowflake.eps" matrix="1.92308 0 0 2.08333 413.782 352.778" lock="false" flowaround="false" obstaclemargin="0" type="image" file="snowflake.eps"/>
    <frame name="Text stream2" matrix="1 0 0 1 125.84 196.165" lock="false" flowaround="true" obstaclemargin="8.50394" type="text" num_columns="1" gutter_width="12" stream="stream2" width="200" height="297.231"/>
  </page>
</document>
