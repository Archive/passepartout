# vim:set ft=automake:

dist_noinst_DATA=\
	test/kameran.html \
	test/kameran.pdf \
	test/kameran.ps \
	test/kameran.x2p \
	$(NULL)

# FIXME: make sure xml2ps aborts on invalid data
# FIXME: make sure the delivered x2p files are valid according to the DTD
check-local::
	@for html in $(filter %.html,$(dist_noinst_DATA)); do \
		x2p=`echo $$html | sed 's/\.html/.x2p/'`; \
		echo "[TEST] $$x2p" >&2; \
		tmpx2p=`mktemp`; \
		$(XSLTPROC) $(top_srcdir)/data/xhtml.xslt $(srcdir)/$$html > $$tmpx2p || exit $$?; \
		diff -u $(srcdir)/$$x2p $$tmpx2p || exit $?; \
		rm -f $$tmpx2p; \
	done
	@for x2p in $(filter %.x2p,$(dist_noinst_DATA)); do \
		for ext in ps pdf; do \
			subject=`echo $$x2p | sed "s/\.x2p/.$$ext/"`; \
			echo "[TEST] $$subject" >&2; \
			tmpsub=`mktemp`; \
			src/xml2ps/xml2ps --output-format=$$ext -p 450x600 < $(srcdir)/$$x2p > $$tmpsub \
				|| exit $?; \
			diff -u $(srcdir)/$$subject $$tmpsub || exit $?; \
			rm -f $$tmpsub; \
		done \
	done

