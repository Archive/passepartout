<?xml version="1.0" encoding="iso-8859-1" ?>
<!DOCTYPE artikel SYSTEM "emission.dtd">
<artikel page="foo">
  <rubrik>SMusK tournee 1997<br/>
    <small>- eller &nbsp; Hur kul �r det, p� en skala?</small></rubrik>
  
  <!-- sidebar 26/6 avf�rd fr�n Teknis<br/>
    27/6 K�penhamn<br/>
    28/6 Ratzeburg<br/>
    29/6 Schlo� Zeilitzheim<br/>
    30/6 Erlangen, jaga<bp/>flickorna<bp/>ur<bp/>husen<br/>
    1/7 Karlsruhe<br/>
    2/7 Luxemburg<br/>
    3/7 Luxemburg<br/>
    4/7 Sch�tzenfest Hannover<br/>
    5/7 Sch�tzenfest Hannover<br/>
    6/7 hem igen -->

  <!--bild href="smusk-a.jpg">
    <bildtext><strong>Partybussen</strong> - ett hem bortifr�n hemma,
      med st�mning ut�ver qaaken.</bildtext>
  </bild-->

  <ingress>Vad �r v�l en turn�?&nbsp; Den kan vara l�ng&nbsp;...&nbsp; och
    varm&nbsp;...&nbsp; och anstr�ngande&nbsp;...&nbsp; och
    alldeles&nbsp;...&nbsp; alldeles underbar!</ingress>
  
  <para><start>Fortfarande op�verkad</start> av mobil<bp/>telefonin h�rskar
    den positiva vanmakten. Det kan finnas b�de ost�dade l�genheter
    och ogjorda jobb d�rhemma, men det �r &mdash; faktiskt &mdash;
    inget att g�ra �t. Man flyter vidare utan att kunna g�ra n�got
    nyttigt, eller f�r den delen ha lust heller.  Om man �r i
    oansvarig st�llning dvs, vilket de flesta �r.</para>
  
  <para>De ansvariga har dock <u>f�rdelen av att ha makt att <em>s�ga �t folk
    vad de skall g�ra</em></u>, s� g�r de det. Det r�cker att upprepa varje
    upplysning tio-tjugo g�nger s� har de flesta h�rt och fattat. Vid
    tillst�llningar som dessa visar sig <u>v�r <em>symbol</em> f�ret mycket
    v�lfunnen</u>. <q>Var �r vi, var �ter vi, var skall vi spela?</q> I
    andanom h�r man b��h:na fr�n skocken.  S� det �r inte mycket som
    sl�r tio dagar p� vuxenskolresa. Jag menar, bara att f� skriva de
    d�r lustiga dubbel-s:en p� vykorten hem g�r det v�rt det.</para>

  <del><rubrik>F�rdmedlet</rubrik>

    <para><u>pX <sup>pX<sub><u>pX</u></sub>pX</sup><sub>pX</sub> Px</u>
      (A<sub>1</sub> * e<sup>x<sup>2<sup>3</sup></sup></sup>)</para>
    
  <para>Den absoluta centralpunkten f�r turn�n �r bussen.</para>

  <para>Kv�llen f�re avf�rd fylls den med allt som �r n�dv�ndigt f�r en
    bra turn�; Vi flyttar p� n�gra s�ten och bygger v�r bussbar
    emellan, vid bakre d�rren, med skinnsoffor bredvid och
    mittemot. Bra med plats f�r en jamgrupp och ett party, eller
    flera. </para>

  <para>Efter avf�rd delas m�ngder av nyttiga saker ut, ett s k Rambo-kit
    med penna, papper, alvedon, godis, kondomer m.m. Turn� t-shirt,
    oideo<bp/>logiskt bl� i �r. Turn�h�fte (ljusr�tt) med
    turist<bp/>information, drinkrecept och ideo<bp/>logiskt korsord. Och, inte
    minst viktigt, nappflaskorna delas ut (optimalt f�r drinkarna, g�r
    inte s�nder, l�tt att skriva namn p�, spiller inte ens om man
    tappar den) d�rmed kan baren ocks� �ppna, festens epicentrum,
    centralpunkten i buss<bp/>partyt, navet kring vilket fr�jdandet
    snurrar. Konsumtionen r�knas om till cl br�nnvinsekvivalent och
    streckas upp p� en lista. Den som druckit mest (bara i baren,
    annat r�knas inte) under dygnet f�r / m�ste n�sta dag b�ra
    barledarv�sten.</para>
  </del>

  <del><rubrik>I am Cornholio!</rubrik>

<!-- table align="right" width="268"><tr><td>
    <a href="smusk-d.jpg"><img src="smusk-d.gif" alt=""></a><br/>
    <strong>Get Happy</strong> - dansande demoner duperar Deuchlandish dunge
</td></tr></table-->

  <para>S� kallade race �r inget nytt, de har funnits i ev�rdeliga tider,
    men d� g�llt Betongblandare (lika delar Bailey's och lime, blandas
    i munnen) eller tequila. Nu har begreppet utvidgats till att g�lla
    allt som g�r att snabbt f� i sig, dvs alla sorters livsmedel plus
    snuff. Det har ocks� f�tt ett nytt utf�rande och tillg�r numera p�
    f�ljande vis; De inblandade l�ser innantill fr�n f�rpackningen, ju
    l�ngre text desto b�ttre, sedan h�ver man snabbt i sig varan varp�
    man drar tr�jan upp �ver nacken och vr�lar "AAAAH", eventuellt
    f�jt av "I am Cornholio!". Har ni sett "Beavis &amp; Butt-head" k�nner
    ni igen er, annars inte (s� varf�r skriver jag det h�r?)</para>
  </del>

  <del>
    <rubrik>Snart hedersmedborgare</rubrik>

    <para>S� g�r det p� tills K�penhamn, huvudstad i Danmark. D�rifr�n
      till resans huvudm�l; Tyskland och Ratzeburg.  En liten trivsam
      stad precis vid gamla �stgr�nsen. Grundades av Henrik Lejonet p�
      1100-talet och ligger p� den gamla saltv�gen. F�rutom ett g�ng
      spelningar �ker vi b�t p� sj�arna (d�r ett av Tysklands b�sta
      roddarlag tr�nar) runtom. Staden f�rs�ker g�ra oss till
      hedersmedborgare, men vi kommer fram till att vi bara varit d�r
      nio g�nger �n. (Ytterligare ett sk�l att vara med p� n�sta
      turn�). Efter en glad dag i solen �ntrar vi �ter bussen. N�r vi
      som brukligt konstaterat att ingen fattas b�r det
      iv�g. Avf�rds<bp/>kommandot triggar omedel<bp/>bart ett massivt bussparty,
      det b�sta p� �ratal. Fonky music d�nar ut �ver de �verlastade
      sofforna vid baren och race f�ljer p� race alltmedan bussen
      susar ner mot Zeilitzheim. D�m om v�r f�rv�ning n�r vi en timme
      efter att vi startat �ter befinner oss vid en mycket bekant
      parkeringsplats. Det tappade armbandsur som �r orsaken till
      �revarvet hittas snabbt och vi antr�der �ter autobahn. Baletten
      �r i farten och vinner dagsligan, och det m�rks inte alls p�
      spelningen dagen efter.</para>

    <para>S� blir den ocks� lite speciell.</para>
  </del>
  <del>
    <rubrik>Na fuu, das war lecker!</rubrik>

    <para>Den gamble batterist Ludde fick f�rra �ret p� turn� en
      f�delsedagspresent.  Ett kassettband med tysk telefonsex.  Med
      en inlevelse som f�r fr�ken Ur att framst� som Traci Lords l�ser
      flickorna titlar som "Anita spielt mit drei B�ller". Detta
      kulturdokument anv�nds nu och framgent under turn�n som
      spr�kkurs med f�rh�r efter varje avsnitt. B�st �r inl�rningen p�
      de sm� utrop som kommer ibland.  Mmm, Jaa, Na-fuu, das war
      lecker! Ett febrilt s�kande inleds efter "drei B�ller", men
      aff�rerna s�ljer bara tv�, fyra eller fem, attans. </para>

    <para>Bad, bus och picknick<bp/>lunch (se upp f�r nedfallande kottar) i
      W�rzburg.  Fram�t eftermiddagen ankommer vi Schlo� Zeilitzheim
      halvv�gs mellan Frankfurt och N�rnberg. Slottsfru �r den mycket
      sympatiska grevinnan Marina von Halem, f�dd von
      Richthofen. Hennes far var kusin med R�de Baronen och i
      slotts<bp/>korridorerna finns det montrar med b�cker om honom, en av
      dem skriven av hans underlydande Hermann G�ring. F�r att g�ra
      n�gon nytta skall vi spela p� torget p� byfesten. Sagt och
      gjort, efter middagen s�tts spelmaskineriet �ter i r�relse.
      Trummorna s�tts upp, notst�ll och s�nganl�ggning kommer p� plats
      och baletten g�r sig klar alltmedan en av svartnande moln
      alltmer kringr�nd sol �nnu str�lar �ver torget. Dirigenten sl�r
      an och de f�rsamlade byborna b�nkar sig
      f�rv�ntans<bp/>fullt&nbsp;...</para>
  </del>

  <del><rubrik>Woosh</rubrik>

    <para>Inga bildbevis finns, men n�gonstans i det f�ljande kaoset,
      rytande vinden och slagregnet �r det flera som sv�r p� att de
      sett ett par balettpojkar springa �ver torget. F�ljande kv�de
      skaldas; <em>Vred var himlen, stormvind riste, ljungande viggar
      och st�rtande regn, s�gos i skymning, nakna k�mpar, l�pande
      trotsigt, torget tv�rs, till goters gamman</em>.</para>

    <para>N�gra timmar senare blir det en spelning i alla fall, inte
      n�mnv�rt p�verkad av en o�mnt distribuerad flaska
      lakritsshots. Vi dras oss tillbaka till v�ra slottsgemak och
      vaknar upp till en ny underbar morgon i detta vackra
      vindistrikt. De h�gade bes�ker en lokal ving�rd, andra letar
      f�ga framg�ngsrikt efter en lunchrestaurang.</para>
  </del>
  <del>
    <rubrik>Burschenschaft Bubenreuter</rubrik>

<!--table align="right" width="194"><tr><td>
    <a href="smusk-b.jpg"><img src="smusk-b.gif" alt=""></a><br/>
    <strong>Framf�r</strong> gamla l�ktaren i Karlsruhe
</td></tr></table -->

    <para>L�ngt nere, i Bayern, ligger Erlangen, en stad som �r
      synonym med Siemens. Vi kvarterar in oss i en studentf�rening,
      ett s. k. Burschen<bp/>schaft (ungef�r br�draskap, Bursche = grabb,
      slyngel, busfr�). Tysk studentf�rening med stort hus,
      studenterna (bara m�n) bor gratis under studietiden och betalar
      sedan n�r de blir stora tyskar med fet Mercedes. H�r kan man
      spela biljard eller f�ktas, nuf�rtiden k�r de med ansiktsskydd,
      fegisarna. V�ggarna i matsalen �r dekorerade med m�lningar fr�n
      den �rofulla och manliga historien. P� kv�llen b�r det ut en
      liten bit till byn Bubenreuter (som studentf�reningen �r
      uppkallad efter) d�r vi bevistar den �rliga
      jaga-flickornaur-husen-festen. Den tillg�r p� s� s�tt att
      studenterna drar runt mellan villorna (vars gr�smattor
      antagligen �r klippta med nagelsax) och h�mtar ut sina flickor
      varvid flickornas f�r�ldrar bjuder p� sm� sm� glas med br�nnvin
      och sm� sm� sm�rg�sar. I en dimmig forntid s�gs det ha g�tt mer
      bryskt till d� ynglingarna stormade in i h�rbrena och rotade
      igenom k�llare och garderober i sin jakt p� ledigt
      kjoltyg.</para>

    <para>Studenterna i sina r�da m�ssor drar omkring p� en tunna med
      �l och ibland rusar de pl�tsligt ett tjugotal meter under
      livliga tjoanden. Allt �r mycket pittoreskt. Vi g�r en bejublad
      spelning p� byfesten och f�r s� mycket �lbongar att vi blir kvar
      ett bra tag.  Det g�r att vi tyv�rr inte kan festa s� mycket i
      det excellenta partyhus som Burschenschaftet utg�r, men man kan
      inte f� allt.</para>
  </del>

  <del><rubrik>Back from IKEA</rubrik>

    <para>Efter ytterligare en morgon utan r� k�ttf�rs till frukost
      drar vi benen efter oss upp till Karlsruhe. H�r bor vi hos en
      festf�rening i en gammal l�ktare som byggts om till bland annat
      gymnastiksal. Som brukligt har de gjort stor aff�r av v�r
      ankomst. Sta'n �r tapetserad med affischer, d�r vi i pratbubblor
      s�ger "Back from IKEA!" och "Super! mit Cocktails!". I �r �r det
      �r som synes cocktailtema och vi har under nerf�rden sytt flugor
      s� att vi �r fina p� spelningen. Vi g�r gratis i baren. En
      m�rklig upplevelse, dricka kalla drinkar, i glas&nbsp;... Immer viel
      Spa�! </para>

    <para>St�rkta av den utm�rkta mat tyskarna nu l�rt sig att laga �r
      det dags att stoppa undan D-marken och i st�llet hala fram sina
      belgiska franc f�r nu skall vi till Luxemburg. (Sanningen att
      s�ga �r det sv�rt att g�ra av med pengar, det mesta ing�r i
      resan.) Men p� autobahn tar det stopp, chauff�ren kan inte gasa
      och k�r av och b�rjar leta efter den trasiga gasvajern.  Efter
      en haltimmes letande uppdagas att det �r avgasbromsen, den
      s. k. <q>retarden</q> som inte slagits av. Resten av resan f�r samme
      chauff�r h�ra v�lf�rtj�nta gliringar som <q>-Retarden, d�r framme
      vid ratten. -vilken av dom menar du?</q>. De krystade, men f�r det
      inte mindre roliga, sk�mten b�rjar inte klinga av f�rr�n i
      </para>
  </del>

  <del><rubrik>Luxemburg</rubrik>

    <para>D�r bes�ker vi en gammal bekant, champagnehuset Bernard
      Massard, som bjuder p� film och rundtur och undf�gnas med musik,
      s�ng och dans.  Publiken best�r av en dansk skol<bp/>orkester i
      varierad �lder.  Efter detta folksf�rbr�drande beger vi oss
      n�gra stenkast till v�rt n�sta slott. Vi dumpar sakerna och �ker
      in och g�r sta'n.  N�gra av oss hittar en kinarestaurang och
      best�ller �tta<bp/>r�tters<bp/>menyn. Tre timmar senare rullar
      vi d�r<bp/>ifr�n, tur att baletten inte skall dansa i
      kv�ll.</para>

    <para>Andra dagen i storfurstend�met p�b�rjas med studie<bp/>bes�k p�
      EU-parlamentet (mot<bp/>prestation f�r att vi f�r bo gratis p� deras
      slott), en svensk tj�nsteman ber�ttar om parlamentet och vilka
      som sitter d�r, i sanning en samling, skojigt att h�ra. Som
      present f�r vi varsin pocketkalkylator. Efter en tur �ver den
      r�da bron som �r ett av stadens landm�rken spelar vi p�
      stortorget, Place d'Armes. D�refter f�ljer veckans mest bisarra
      fram<bp/>tr�dande, p� en sportklubb skall vi spela f�r
      restaurangg�sterna. Men en st�rtskur g�r det om�jligt att spela
      p� gr�splanen.  L�sningen kan besk�das p� en av bilderna;
      Orkestern st�r p� sidan medan baletten dansar p� balkongen
      framf�r restaurangf�nstren (en g�st skymtas genom
      glaset). Intrycket inifr�n �r minst sagt m�rkligt eftersom det
      knappt h�rs n�gonting in. Men g�sterna har kul, och vi
      ocks�. </para>
  </del>

  <del><rubrik>Slutet �r n�ra</rubrik>

<!--table align="right" width="137"><tr><td>
    <a href="smusk-e.jpg"><img src="smusk-e.gif" alt=""></a><br/>
    <strong>Sax</strong> �r sexigt&nbsp;...
</td></tr></table -->

    <para>Bara en stad kvar. Hannover med sin Sch�tzen<bp/>fest, Tysklands
      st�rsta baluns n�st efter Oktoberfest. Fest<bp/>platsen fylls under
      tio dagar av ett tivoli med enorma mar�ngmaskiner. F�r att
      klarg�ra skalan; pariserhjulet �r 60 meter. I detta vimmel av
      glada tyska civilister och skyttegillen i gr� och gr�na
      uniformer finns ocks� vi, i n�got mindre milit�riska, men dock,
      gr�na rockar.  N�sta tysk som fr�gar dumt skall jag sl� i att vi
      �r ett punkskyttegille fr�n K�nigsberg. Spelning som brukligt p�
      restau<bp/>rangen i mitten. L�rdagen avl�per lugnt, efter det
      obligatoriska bes�ket i musikaff�ren �r jag och min assistent p�
      jakt efter �tbara snuskiga saker till pris i korsordst�vlingen,
      tyv�rr d�lig lycka, utbudet var b�ttre f�rr. Inte ens "drei
      B�ller" har de.</para>

<!-- table align="right" width="180"><tr><td>
    <a href="smusk-c.jpg"><img src="smusk-c.gif" alt=""></a><br/>
    <strong>Alla s�tt �r bra.</strong> H�r ses hoppande
    f�nstershoppande hoppor. 
</td></tr></table -->

    <para>Efter kv�llsspelningen �terst�r bara att packa ihop och �ka
      hem. Sedan ett par �r har en oroande trend infunnit sig.  1995
      blev en batterist kvar p� en bensinstation vid Hamburg, men det
      gick bra, han tog sig till K�penhamn och blev h�mtad av sin
      far. I �r somnade tv� saxofonister p� f�rjan och fick ta
      t�get. (Vad som egentligen h�nde blev tema p� en uppsatst�vling
      i bussen, m�nga f�rslag kom in) En extrapolation ger att 1999
      blir n�sta g�ng, och om utvecklingen �r exponentiell blir det
      lite jobbigt om ett tag. S� s�k SMusK, vi beh�ver nytt
      folk&nbsp;... </para>
  </del>
  <signatur>Text &amp; bild: Lillwall</signatur>
</artikel>
