<?xml version='1.0'?>
<xsl:stylesheet version="1.0"
	        xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<!-- 
  Docbook-to-xml2ps stylesheet
  NOTE: this is tested only on the documentation for Passepartout
-->

  <xsl:output method="xml"
	      encoding="iso-8859-1"
	      version="1.0"
	      indent="yes"/>
  
  <xsl:template match="/">
    <block-container font-family="Times-Roman" font-size="10">
      <xsl:apply-templates/>
    </block-container>
  </xsl:template>
  
  <xsl:template match="articleinfo/title|title">
    <para align="center" span-columns="1" font-family="Helvetica" 
	  font-size="24" margin-bottom="20pt">
      <xsl:apply-templates/>
    </para>
  </xsl:template>
  
  <xsl:template match="articleinfo/releaseinfo"/>
  
  <xsl:template match="section/title">
    <para align="left" font-family="Helvetica-Bold" 
	  font-size="12" margin-top="12pt" margin-bottom="2pt">
      <xsl:apply-templates/>
    </para>
  </xsl:template>

  <xsl:template match="section/section/title">
    <para align="left" font-family="Helvetica-Oblique" 
	  font-size="10" margin-top="10pt">
      <xsl:apply-templates/>
    </para>
  </xsl:template>
  
  <xsl:template match="para[position()=1]">
    <para align="justify" line-height="1.3"><xsl:apply-templates/>
    </para>
  </xsl:template>

  <xsl:template match="para">
    <para align="justify" line-height="1.3"><leader width="1em"/><xsl:apply-templates/>
    </para>
  </xsl:template>

  <xsl:template match="programlisting">
    <para align="left" font-family="Helvetica" font-size="80%" line-height="1.2"><xsl:apply-templates/>
    </para>
  </xsl:template>



  <!-- can't have para within para -->
  <xsl:template match="para//para">
    <xsl:apply-templates/>
  </xsl:template>
  
  <xsl:template match="para/orderedlist|para/itemizedlist">
    <xsl:text disable-output-escaping="yes">&lt;/para&gt;</xsl:text>
    <xsl:apply-templates/>
    <xsl:text disable-output-escaping="yes">&lt;para align="justify" line-height="1.3" margin-top="5pt"&gt;</xsl:text>
  </xsl:template>

  <xsl:template match="orderedlist/listitem">
    <para align="left" line-height="1.3"
          margin-top="5pt" margin-left="10pt"><xsl:number/>.&#160;<xsl:apply-templates/></para>
  </xsl:template>

  <xsl:template match="itemizedlist/listitem">
    <para align="left" line-height="1.3" 
          margin-top="5pt" margin-left="10pt">&#x2022;&#160;<xsl:apply-templates/></para>
  </xsl:template>

  <xsl:template match="variablelist//variablelist">
    <block-container margin-left="10pt">
    <xsl:apply-templates/>
    </block-container>
  </xsl:template>

  <xsl:template match="variablelist/title">
    <para align="left" line-height="1.3" font-family="Helvetica"
          margin-top="5pt"><xsl:apply-templates/></para>
  </xsl:template>

  <xsl:template match="variablelist/varlistentry/term">
    <para align="left" line-height="1.3"
          margin-top="5pt"><xsl:apply-templates/></para>
  </xsl:template>

  <xsl:template match="variablelist/varlistentry/listitem/para">
    <para align="left" line-height="1.3"
          margin-left="10pt"><xsl:apply-templates/></para>
  </xsl:template>

  <xsl:template match="glossterm|emphasis">
    <font font-family="Times-Italic"><xsl:apply-templates/></font>
  </xsl:template>

  <xsl:template match="link|varname">
    <font font-family="Times-Bold"><xsl:apply-templates/></font>
  </xsl:template>

  <xsl:template match="command">
    <font font-family="Courier"><xsl:value-of select="translate(string(.),&quot; &quot;,&quot;&#160;&quot;)"/></font>
  </xsl:template>

  <xsl:template match="markup">
    <font font-family="Helvetica"><xsl:apply-templates/></font>
  </xsl:template>

  <xsl:template match="programlisting/markup">
    <font font-family="Helvetica-Bold"><xsl:apply-templates/></font>
  </xsl:template>

  <!-- FIXME: properly use the next template and only append the arrow -->
  <xsl:template match="menuchoice/guimenu">
    <font font-family="Helvetica" font-size="80%"><xsl:value-of select="translate(string(.),&quot; &quot;,&quot;&#160;&quot;)"/>&#160;&#187;&#160;</font>
  </xsl:template>
  <xsl:template match="guilabel|guimenu|guimenuitem|guibutton">
    <font font-family="Helvetica" font-size="80%"><xsl:value-of select="translate(string(.),&quot; &quot;,&quot;&#160;&quot;)"/></font>
  </xsl:template>

  <xsl:template match="acronym">
    <xsl:apply-templates/>
  </xsl:template>

  <xsl:template match="footnote"> (<xsl:apply-templates/>) </xsl:template>

  <xsl:template match="*">
    <xsl:apply-templates/>
  </xsl:template>

</xsl:stylesheet>
<!-- vim:set sw=2: -->
