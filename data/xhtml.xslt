<?xml version='1.0'?>
<!-- A very basic xhtml-to-xml2ps stylesheet. 
     Tables, images and much more are not supported. 
     Fredrik Arnerup, 19 Nov 2003 -->
<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:xhtml="http://www.w3.org/1999/xhtml"> 
  <xsl:output method="xml" indent="yes"/>

  <xsl:variable name="lcletters">aáàâåäãæbcçdðeéèêëfghiîïíìjklmnñoóòôöõøpqrstþuúùûüvwxyýÿz</xsl:variable>
  <xsl:variable name="ucletters">AÁÀÂÅÄÃÆBCÇDÐEÉÈÊËFGHIÎÏíìJKLMNÑOÓÒÔÖÕØPQRSTÞUÚÙÛÜVWXYÝYZ</xsl:variable>

  <xsl:param name="basesize">10pt</xsl:param>
  <xsl:param name="h1size">3em</xsl:param>
  <xsl:param name="h2size">1.5em</xsl:param>

  <xsl:param name="textfont">Times-Roman</xsl:param>
  <xsl:param name="textfontitalic">Times-Italic</xsl:param>
  <xsl:param name="textfontbold">Times-Bold</xsl:param>
  <xsl:param name="textfontbolditalic">Times-BoldItalic</xsl:param>

  <xsl:param name="monospacefont">Courier</xsl:param>
  
  <xsl:param name="headerfont">Helvetica-Bold</xsl:param>

  <xsl:template match="xhtml:html|xhtml:div">
    <block-container font-size="{$basesize}">
      <xsl:attribute name="font-family">
	<xsl:value-of select="$textfont"/>
      </xsl:attribute>
      <xsl:apply-templates/>
    </block-container>
  </xsl:template>

  <!-- Ignore head -->
  <xsl:template match="xhtml:head">
  </xsl:template>

  <!-- headers -->
  <xsl:template match="xhtml:h1">
    <para align="left" span-columns="1" margin-bottom="0.5em"
          font-size="{$h1size}" font-family="{$headerfont}">
      <xsl:apply-templates/>
    </para>
  </xsl:template>

  <xsl:template match="xhtml:h2">
    <para align="left" margin-top="0.8em" margin-bottom="0.2em"
          font-size="{$h2size}" font-family="{$headerfont}">
      <xsl:apply-templates/>
    </para>
  </xsl:template>

  <xsl:template match="xhtml:h3|xhtml:h4|xhtml:h5|xhtml:h6">
    <para align="left" margin-top="0.8em"
          font-size="1.2em" font-family="{$headerfont}">
      <xsl:apply-templates/>
    </para>
  </xsl:template>

  <!-- paragraphs -->
  <xsl:template match="xhtml:p | xhtml:pre | xhtml:address">
    <para align="justify" line-height="1.4">
      <xsl:apply-templates/>
    </para>
  </xsl:template>

  <xsl:template match="xhtml:blockquote">
    <para margin-top="0.5em" margin-bottom="0.5em" margin-left="1em"
          margin-right="1em" align="justify" line-height="1.4">
      <xsl:apply-templates/>
    </para>
  </xsl:template>

  <!-- Unordered lists -->
  <xsl:template match="xhtml:ul">
    <block-container margin-left="2em"> 
      <xsl:apply-templates/>
    </block-container>
  </xsl:template>

  <xsl:template match="xhtml:ul/xhtml:li">
    <para align="left" line-height="1.4">
      <leader width="-0.8em"/><xsl:text>&#x2022;</xsl:text>
      <leader width="0.4em"/>
      <xsl:apply-templates/></para>
  </xsl:template>

  <!-- Ordered lists -->
  <xsl:template match="xhtml:ol">
    <block-container margin-left="2em">
      <xsl:apply-templates/>
    </block-container>
  </xsl:template>

  <xsl:template match="xhtml:ol/xhtml:li">
    <para align="left" line-height="1.4">
      <leader width="-1em"/><xsl:number/><xsl:text>.&#160;</xsl:text>
      <xsl:apply-templates/>
    </para>
  </xsl:template>

  <!-- Definition lists -->
  <xsl:template match="xhtml:dl">
    <block-container>
      <xsl:attribute name="font-family">
	<xsl:value-of select="$textfont"/>
      </xsl:attribute>
      <xsl:apply-templates/>
    </block-container>
  </xsl:template>

  <xsl:template match="xhtml:dl/xhtml:dt">
    <para align="left" line-height="1.4"><xsl:apply-templates/></para>
  </xsl:template>

  <xsl:template match="xhtml:dl/xhtml:dd">
    <para align="left" line-height="1.4"><leader width="1em"/><xsl:apply-templates/></para>
  </xsl:template>

  <!-- underline -->
  <xsl:template match="xhtml:a[@href]">
    <font underline="1"><xsl:apply-templates/></font>
  </xsl:template>
  
  <!-- italic -->
  <xsl:template match="xhtml:em|xhtml:i|xhtml:var">
    <font>
      <xsl:attribute name="font-family">
	<xsl:value-of select="$textfontitalic"/>
      </xsl:attribute>
      <xsl:apply-templates/>
    </font>
  </xsl:template>

  <!-- bold -->
  <xsl:template match="xhtml:b|xhtml:strong">
    <font>
      <xsl:attribute name="font-family">
	<xsl:value-of select="$textfontbold"/>
      </xsl:attribute>
      <xsl:apply-templates/>
    </font>
  </xsl:template>

  <!-- bolditalic -->
  <xsl:template match="xhtml:b/xhtml:em|xhtml:b/xhtml:i|xhtml:strong/xhtml:em|xhtml:strong/xhtml:i|xhtml:em/xhtml:b|xhtml:em/xhtml:strong|xhtml:i/xhtml:b|xhtml:i/xhtml:strong">
    <font>
      <xsl:attribute name="font-family">
	<xsl:value-of select="$textfontbolditalic"/>
      </xsl:attribute>
      <xsl:apply-templates/>
    </font>
  </xsl:template>

  <!-- monospace -->
  <xsl:template match="xhtml:tt|xhtml:code">
    <font>
      <xsl:attribute name="font-family">
	<xsl:value-of select="$monospacefont"/>
      </xsl:attribute>
      <xsl:apply-templates/>
    </font>
  </xsl:template>

  <!-- small caps -->
  <xsl:template match="xhtml:acronym">
    <font font-size="66.667%" letter-spacing="0.2em">
      <xsl:attribute name="font-family">
	<xsl:value-of select="$textfontbold"/>
      </xsl:attribute>
      <xsl:value-of select="translate(substring(., 1),$lcletters,$ucletters)"/>
    </font>
  </xsl:template>

  <!-- small -->
  <xsl:template match="xhtml:small">
    <font font-size="66%"><xsl:apply-templates/></font>
  </xsl:template>

  <!-- big -->
  <xsl:template match="xhtml:big">
    <font font-size="150%"><xsl:apply-templates/></font>
  </xsl:template>

  <!-- sub -->
  <xsl:template match="xhtml:sub">
    <font font-size="66%" baseline="-0.3em"><xsl:apply-templates/></font>
  </xsl:template>

  <!-- sup -->
  <xsl:template match="xhtml:sup">
    <font font-size="66%" baseline="0.5em"><xsl:apply-templates/></font>
  </xsl:template>

  <!-- quoted -->
  <xsl:template match="xhtml:q">
    <xsl:text>&#x201C;</xsl:text>
    <xsl:apply-templates/>
    <xsl:text>&#x201D;</xsl:text>
  </xsl:template>
  
  <xsl:template match="xhtml:q//xhtml:q">
    <xsl:text>&#x2018;</xsl:text>
    <xsl:apply-templates/>
    <xsl:text>&#x2019;</xsl:text>
  </xsl:template>
  
  <!-- line break -->
  <xsl:template match="xhtml:br">
    <linebreak/>
  </xsl:template>

  <!-- stuff to ignore -->
  <xsl:template match="hr|"/>

  <!-- ignore anything else -->
  <xsl:template match="*">
    <xsl:apply-templates/>
  </xsl:template>

</xsl:stylesheet>








