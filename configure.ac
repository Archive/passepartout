# Process this file with autoconf to produce a configure script.
AC_INIT([Passepartout],[0.8-PRE],[http://bugzilla.gnome.org/simple-bug-guide.cgi?product=passepartout])
AC_CONFIG_SRCDIR([src/xml2ps/blockcontainer.cc])

AM_INIT_AUTOMAKE([foreign dist-bzip2 no-define])

AC_CONFIG_HEADERS([src/defines.h])

dnl  ----------------
dnl | compiler flags |
dnl  ----------------

GNOME_COMPILE_WARNINGS([maximum])

DISTCHECK_CONFIGURE_FLAGS="${DISTCHECK_CONFIGURE_FLAGS} --enable-compile-warnings=error"

# Checks for programs.
AC_PROG_CXX
AC_DISABLE_STATIC
AC_PROG_LIBTOOL

# AC_APPLY_RPATH(LIBS)
# --------------------
# Add an -rpath linker option for each -L path in LIBS
# if and only if the environment variable APPLY_RPATH is set
AC_DEFUN([AC_APPLY_RPATH],
[if test "$APPLY_RPATH" != ""; then
  $1=`echo "[$]$1" | [sed -e "s/-L\([^ ]*\)/& -Wl,-rpath,\1/g"]`
fi])
AC_ARG_VAR([APPLY_RPATH], [="true" : specify -rpath options to the linker])

# check for libxml++ 
AC_SUBST([XMLPP_LIBS])
AC_SUBST([XMLPP_CFLAGS])
PKG_CHECK_MODULES([XMLPP], libxml++-2.6 >= 1.0)
AC_APPLY_RPATH([XMLPP_LIBS])

AC_SUBST([GTKMM_LIBS])
AC_SUBST([GTKMM_CFLAGS])
PKG_CHECK_MODULES(GTKMM,[glibmm-2.4 >= 2.16 gtkmm-2.4])
AC_APPLY_RPATH([GTKMM_LIBS])

AC_SUBST([GNOMECANVAS_LIBS])
AC_SUBST([GNOMECANVAS_CFLAGS])
PKG_CHECK_MODULES(GNOMECANVAS, libgnomecanvasmm-2.6)
AC_APPLY_RPATH([GNOMECANVAS_LIBS])

AC_SUBST([GTHREAD_LIBS])
AC_SUBST([GTHREAD_CFLAGS])
PKG_CHECK_MODULES([GTHREAD], [gthread-2.0])
AC_APPLY_RPATH([GTHREAD_LIBS])

AC_SUBST([FREETYPE_LIBS])
AC_SUBST([FREETYPE_CFLAGS])
PKG_CHECK_MODULES([FREETYPE], [freetype2 fontconfig])
AC_APPLY_RPATH([FREETYPE_LIBS])

# gnome is optional
AC_SUBST([GNOME_LIBS])
AC_SUBST([GNOME_CFLAGS])
AC_SUBST([GNOMEVFS_LIBS])
AC_SUBST([GNOMEVFS_CFLAGS])
AC_ARG_WITH([gnome], AS_HELP_STRING([--with-gnome],[Gnome support (default is NO)]),
            [PKG_CHECK_MODULES(GNOME, libgnome-2.0)]
            [PKG_CHECK_MODULES(GNOMEVFS, gnome-vfs-module-2.0)]
            AC_DEFINE([HAVE_GNOME], [1], [Gnome is present]))
AC_APPLY_RPATH([GNOME_LIBS])
AC_APPLY_RPATH([GNOMEVFS_LIBS])

# check for fam
AC_CHECK_LIB([fam], [FAMOpen])

# $(datadir) has to be expanded by make
# path to install docs in
AC_SUBST(docdir, ['"$(datadir)/doc/$(PACKAGE)/"'])
# path to install xml stuff in
AC_SUBST(xmldir, ['"$(datadir)/xml/$(PACKAGE)/"'])

# Checks for header files.
AC_HEADER_DIRENT

# Checks for typedefs, structures, and compiler characteristics.
AC_TYPE_PID_T
AC_TYPE_SIZE_T

# Checks for library functions.
AC_FUNC_FORK
AC_FUNC_STAT
AC_CHECK_FUNCS([realpath strdup mkdtemp])

dnl  -------------------
dnl | freedesktop stuff |
dnl  -------------------

AC_ARG_ENABLE([database-updates],
	      AS_HELP_STRING([--disable-database-updates],
			     [disable database updates (useful for distcheck and distribution packagers]))
AM_CONDITIONAL(DATABASE_UPDATES,[test "$enable_database_updates" != "no"])
DISTCHECK_CONFIGURE_FLAGS="${DISTCHECK_CONFIGURE_FLAGS} --disable-database-updates"

AC_PATH_PROGS(UPDATE_MIME_DATABASE,[update-mime-database],[no])
if test "$UPDATE_MIME_DATABASE" = "no"; then
	AC_MSG_ERROR([update-mime-database not found, please install shared-mime-info from
		      http://www.freedesktop.org/software/shared-mime-info/releases/])
fi

AC_PATH_PROGS(UPDATE_DESKTOP_DATABASE,[update-desktop-database],[no])
if test "${UPDATE_DESKTOP_DATABASE}" = "no"; then
	AC_MSG_ERROR([desktop-file-validate not found, please install desktop-file-utils from
		      http://www.freedesktop.org/software/desktop-file-utils/releases/])
fi

AC_PATH_PROGS(DESKTOP_FILE_VALIDATE,[desktop-file-validate],[no])
if test "${DESKTOP_FILE_VALIDATE}" = "no"; then
	AC_MSG_ERROR([desktop-file-validate not found, please install desktop-file-utils from
		      http://www.freedesktop.org/software/desktop-file-utils/releases/])
fi

dnl  -----------
dnl | xml tests |
dnl  -----------

AC_PATH_PROGS(XMLLINT,[xmllint],[no])
if test "${XMLLINT}" = "no"; then
	AC_MSG_ERROR([xmllint not fount, please install libxml2 from
		      http://xmlsoft.org/downloads.html])
fi

AC_PATH_PROGS(XSLTPROC,[xsltproc],[no])
if test "${XSLTPROC}" = "no"; then
	AC_MSG_ERROR([xsltproc not fount, please install libxslt from
		      http://xmlsoft.org/XSLT/downloads.html])
fi

dnl  ---------------
dnl | documentation |
dnl  ---------------

GNOME_DOC_INIT

AC_CONFIG_FILES([doc/Makefile
		 doc/examples/Makefile
		 help/Makefile
		 help/version.txt
		 ])

dnl  ------
dnl | i18n |
dnl  ------

IT_PROG_INTLTOOL([0.37.1])
dnl intltool 0.40.0 doesn't create the intltool-*.in files anymore
AM_CONDITIONAL(DIST_INTLTOOL,[test "$INTLTOOL_APPLIED_VERSION_AS_INT" -lt 4000])
AM_GLIB_GNU_GETTEXT

GETTEXT_PACKAGE="passepartout"
AC_DEFINE_UNQUOTED(GETTEXT_PACKAGE,"${GETTEXT_PACKAGE}",[the package name for gettext])
AC_SUBST(GETTEXT_PACKAGE)

if test "x${prefix}" = "xNONE"; then
        PACKAGE_LOCALE_DIR="${ac_default_prefix}"
else
        PACKAGE_LOCALE_DIR="${prefix}"
fi
PACKAGE_LOCALE_DIR="${PACKAGE_LOCALE_DIR}/${DATADIRNAME}/locale"
AC_DEFINE_UNQUOTED(PACKAGE_LOCALE_DIR,"${PACKAGE_LOCALE_DIR}",[the package's locale path for gettext])

AC_CONFIG_FILES([po/Makefile.in])

dnl  --------
dnl | output |
dnl  --------

AC_SUBST(DISTCHECK_CONFIGURE_FLAGS)

AC_CONFIG_FILES([Makefile
		 data/Makefile
	         src/Makefile	
                 src/ps/Makefile
                 src/util/Makefile
                 src/testbed/Makefile
		 src/fonts/Makefile
		 src/pptout/Makefile
                 src/pptout/document/Makefile
                 src/pptout/widget/Makefile
                 src/pptout/icons/Makefile
                 src/xml2ps/Makefile
		 logotype/Makefile])
AC_OUTPUT
