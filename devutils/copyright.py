#! /usr/bin/env python

# copyright.py
# Insert copyright information into source code.

# Looks at every C++ file (.cc, .h, .hh) in the current directory and
# recursively in the subdirectories to see if it has two comments
# "///".  If it does, the text between the comments is substituted for
# the text in the file specified on the command line. Does not follow
# symlinks.  Todo: make it insert a copyright message into this file
# as well :-)

import sys
import re
import os
import stat

add=0 # add tags to beginning of file instead of substituting

if len(sys.argv)!=2:
    sys.stderr.write("Usage: copyright.py <message file>\n")
    sys.stderr.write("   or: copyright.py -a\n   to add tags\n")
    sys.exit(2)

def parse_message(message):
    message=re.sub("\n$", "", message) # remove newline on last line
    message=re.sub("(?m)^", "// ", message) # commentify
    message="///\n"+message+"\n///\n" # add begin/end tag
    return message

if sys.argv[1]=="-a": # "add" flag
    add=1
else:
    try:
        message_file=open(sys.argv[1])
        message=message_file.read()
        parsed_message=parse_message(message)
    except IOError:
        sys.stderr.write("Couldn't read "+sys.argv[1]+"\n")
        sys.exit(2)


def substitute(filename, add):
    file=open(filename, "r+")
    sys.stderr.write(filename)
    file_content=file.read() # one big string
    # Check for two lines beginning with "///" and replace everything
    # in between:
    if add:
        file_content="///\n///\n"+file_content
        file.seek(0)
        file.write(file_content)
    else:
        (file_content, num)=re.subn("(?ms)^\/\/\/.*^\/\/\/[^\n]*\n",
                                    parsed_message,
                                    file_content)
        if num==1: # check if a substitution was made
            file.seek(0)
            file.write(file_content)
        else:
            sys.stderr.write(": nothing changed")
    sys.stderr.write("\n")
    file.close()

filetypes=(".cc", ".h", ".hh") # file types to modify

def correct_filetype(filename):
    for filetype in filetypes:
        if filename[-len(filetype):]==filetype:
            return 1
    return 0

from os.path import *
def recursive_substitute(path):
    files=os.listdir(path)
    for file in files:
        file=path+"/"+file
        # Don't follow links!
        if isdir(file) and not islink(file):
            recursive_substitute(file)
        elif isfile(file) and (not islink(file)) and correct_filetype(file):
            # It's a file,
            substitute(file, add)
        else:
            pass

recursive_substitute(".")

