// -*- c++ -*-
///
// Copyright (C) 2002 - 2004, Fredrik Arnerup & Rasmus Kaj, See COPYING
///
#ifndef WINEPS_H 
#define WINEPS_H

#include <string>
#include <fstream>

namespace PS {
  /**
   * Returns true if the bytes in magic match the magic number for
   * "Windows" EPS files.
   * \param magic Must be at least four bytes.
   */
  bool check_windows_magic(const char *magic);

  /** 
   * A filter that extracts the ps code from all the junk in a
   * "Windows" EPS.  Works on ordinary EPS files as well.
   */
  class WinEPSFilter { 
    // Perhaps this should be a subclass of ifstream, but
    // I don't know how to do that. 
  public: WinEPSFilter(const std::string &filename); 
    WinEPSFilter &getline(std::string &line);
    operator bool() const {return in && ok;}
  
  private:
    bool ok, win;
    unsigned long start, length;
    std::ifstream in;
  };
}

/**
 * Similar to getline for istream.
 */
PS::WinEPSFilter &getline(PS::WinEPSFilter &in, std::string &line);

#endif
