// -*- c++ -*-
///
// Copyright (C) 2004, Fredrik Arnerup & Rasmus Kaj, See COPYING
///
#ifndef TYPE42_H
#define TYPE42_H
#include <iostream>

namespace PS {
  /// Convert a TrueType font to a Type42 font
  void truetype2type42(const std::string &infile, std::ostream &out);
};

#endif
