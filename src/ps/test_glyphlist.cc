#include "testbed/testbed.hh"
#include "ps/unicode.h"
#include "util/warning.h"
#include <sstream>

class TestUnicodeOfGlyph : public TestCase {
public:
  TestUnicodeOfGlyph(const std::string& glyph_name, Glib::ustring chars)
    : TestCase("Unicode of glyph " + glyph_name),
      glyph_name_(glyph_name), chars_(chars)
    {}
  
  void test() {
    const Glib::ustring result = PS::Unicode::chars_of_glyph(glyph_name_);
    const std::string reverse = PS::Unicode::glyph_name_of_chars(chars_);
    ASSERT(result.size() > 0);
    verbose << "got \"" << std::string(result) << "\", expecting \"" 
	    << std::string(chars_) << "\", reverser \"" << reverse
	    << "\". ";
    ASSERT(result == chars_);

    // And the other way
    std::istringstream rev(reverse);
    std::string one_reverse;
    while(rev >> one_reverse)
      if(one_reverse == glyph_name_) return;
    const bool failed_to_find_a_correct_value_in_reverse = false;
    ASSERT(failed_to_find_a_correct_value_in_reverse);
  }
  
private:
  const std::string glyph_name_;
  const Glib::ustring chars_;
};

namespace {
  // First some simple ASCII
  TestUnicodeOfGlyph t1("A", "A");
  TestUnicodeOfGlyph t2("a", "a");
  TestUnicodeOfGlyph t3("one", "1");
  TestUnicodeOfGlyph t5("dollar", "$");
  TestUnicodeOfGlyph t6("equal", "=");

  // Then on to some chars that are multibyte in utf8
  TestUnicodeOfGlyph ut1("aring", Glib::ustring(1, gunichar(0xe5)));
  // or: TestUnicodeOfGlyph ut2("aring", "\xc3\xa5");
  // Python comes in handy when converting to UTF-8 escape sequences
  // Start python and type "u'foobar'.encode('UTF-8')"
  // or use unicode escapes: "u'\u05d3'.encode('UTF-8')"

  // Then some multi-character glyphs
  TestUnicodeOfGlyph mut1("daletsheva", "\xd7\x93\xd6\xb0");
}
