///
// Copyright (C) 2002 - 2004, Fredrik Arnerup & Rasmus Kaj, See COPYING
///
#include "pdfparser.h"
#include <fstream>
#include <cassert>		// Todo: other assert!
#include <cstdlib>
#include <iostream>

void usage() {}

/**
 * Note: this program does not yet handle "updated" PDFs (such where some
 * objects have a generation number other than 0).
 */
int main(int argc, char* argv[]) {
  try {
    if(argc < 2 || argc > 4) {
      usage();
      exit(1);
    }
    PDFParser pdf(argv[1]);
    PDF::Dictionary::Ptr trailer = pdf.getTrailer();
    std::cout << "Trailer:" << std::endl
	      << trailer.operator->() << std::endl;
    
    if(PDF::Ref::Ptr ref = trailer->get_entry("Info").dyn_cast<PDF::Ref>()) {
      std::cout << "Info:" << std::endl
		<< pdf.getObject(ref).operator->() << std::endl;
    } else
      std::cout << "This document contains no info dictionary." << std::endl;

    if(PDF::Ref::Ptr ref = trailer->get_entry("Root").dyn_cast<PDF::Ref>()) {
      std::cout << "Root:" << std::endl
		<< pdf.getObject(ref).operator->() << std::endl;
    } else
      std::cout << "This document contains no root dictionary." << std::endl;
    
    
  } catch (const std::exception& err) {
    std::cerr << "Error: " << err.what() << std::endl;
  } catch (...) {
    std::cerr << "Error: " << "Main caught something that "
	      << "wasn't even an exception!"
	      << std::endl;
  }
}

