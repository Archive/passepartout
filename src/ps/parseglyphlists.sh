#!/bin/sh

# Script to build C++ code containing PostScript-glyph-name-to-unicode
# mapping by parsing the glyph lists supplied by Adobe
#
# The mappings can be found at:
# http://partners.adobe.com/asn/developer/type/zapfdingbats.txt
# http://partners.adobe.com/asn/developer/type/glyphlist.txt

function parse_glyphs () {
    awk 'BEGIN {ORS=" "; FS=";"; pl=4; c=0;} /#/ {gsub("#.*", "")} /\w+/ {print "\"" $1 "\","; c+=1; if(c==pl){c=0; print "\n  "} }' $1
}

function parse_codes () {
    awk 'BEGIN {ORS=" "; FS=";"; pl=4; c=0;} /#/ {gsub("#.*", "")} /\w+/ {gsub("[^ ]+", "0x&,", $2); print $2 " 0, "; c+=1; if(c==pl){c=0; print "\n  "} }' $1
}

dingbats="zapfdingbats.txt"
glyphlist="glyphlist.txt"
output="glyphs.cc"

if [ ! -f $glyphlist ]; then
    echo "Can't find $glyphlist" 
    exit 1
fi

if [ ! -f $dingbats ]; then
    echo "Can't find $dingbats" 
    exit 1
fi

echo -n > $output

echo -n "#include \"unicode.h\"
// This file is generated by parseglyphlists.sh
// edit that file instead.

#define u(x) Glib::ustring(Glib::ustring::size_type(1), gunichar(x))

const PS::Unicode::Glyphs PS::Unicode::glyphlist(false), 
  PS::Unicode::dingbats(true);

PS::Unicode::Glyphs::Glyphs(bool dingbats) {
  static const char *const names[] = {" >> $output

parse_glyphs $glyphlist >> $output

echo -n "0
  };

static const int codes[] = {" >> $output

parse_codes $glyphlist >> $output

echo -n "0
  };

static const char* const dingnames[] = {" >> $output

parse_glyphs $dingbats >> $output

echo -n "0
  };

static const int dingcodes[] = {" >> $output

parse_codes $dingbats >> $output

echo -n "0
  };

  const char * const *n = dingbats ? dingnames : names;
  const int *c = dingbats ? dingcodes : codes;
  while(*n) {
    Glib::ustring tmp;
    while(*c) 
      tmp += u(*c++);
    c++;
    namemap[*n] = 0;
    CodeMap::const_iterator i = 
      codemap.insert(std::make_pair(tmp, &(namemap.find(*n)->first)));
    namemap[*n] = &(i->first);
    n++;
  }
}
" >> $output
