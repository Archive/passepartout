// -*- c++ -*-
///
// Copyright (C) 2002 - 2004, Fredrik Arnerup & Rasmus Kaj, See COPYING
///
#ifndef PFB2PFA_H
#define PFB2PFA_H
#include <iostream>
#include <vector>

namespace PS {
  /**
   * convert type 1 pfb font files to pfa format.  Optionally, keep track of
   * the lengths of the parts.
   * \param in stream to read pfb from.
   * \param out stream to write pfa to.
   * \param lengths place to store the block lengths, or 0.
   */
  void pfb2pfa(std::istream &in, std::ostream &out, 
	       std::vector<long> *lengths = 0);
};

#endif
