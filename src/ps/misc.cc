///
// Copyright (C) 2003, 2004, Fredrik Arnerup & Rasmus Kaj, See COPYING
///
#include "misc.h"

namespace PS {
  std::ostream& operator << (std::ostream& out, const Concat& c) {
    return out << '[' << c.matrix << "] concat\n";
  }
}
