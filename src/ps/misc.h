#ifndef PS_MISC_H		// -*- c++ -*-
#define PS_MISC_H
///
// Copyright (C) 2002 - 2004, Fredrik Arnerup & Rasmus Kaj, See COPYING
///
#include <iostream>
#include <util/matrix.h>

namespace PS {
  
  /**
   * Utility class to concat a transformation matrix to PostScript.
   */
  class Concat {
  public:
    explicit Concat(const Matrix& m) : matrix(m) {}
    friend std::ostream& operator << (std::ostream& out, const Concat& c);
  private:
    const Matrix& matrix;
  };
  
  std::ostream& operator << (std::ostream& out, const Concat& c);
}

#endif
