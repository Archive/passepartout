#ifndef PDFPARSER_H		// -*- c++ -*-
#define PDFPARSER_H
///
// Copyright (C) 2004, Fredrik Arnerup & Rasmus Kaj, See COPYING
///
#include "pdf.h"
#include <fstream>

class PDFParser {
public:
  explicit PDFParser(const std::string& filename);
  ~PDFParser();
  
  PDF::Dictionary::Ptr getTrailer() { return trailer; }
  
  /**
   * If ref is a reference, load the referenced object.  Otherwise, just
   * return ref as is.
   */
  PDF::Object::Ptr getObject(PDF::Object::Ptr ref);
  
private:
  std::ifstream source;
  PDF::Dictionary::Ptr trailer;
  int* xrefs;
};

namespace ParsePDF {
  
  void findTrailer(std::istream& in);

  PDF::Object::Ptr readObject(std::istream& in);
  PDF::Dictionary::Ptr readDictionary(std::istream& in);
  
};

#endif
