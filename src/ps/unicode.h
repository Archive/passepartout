// -*-  c++ -*-
///
// Copyright (C) 2002 - 2004, Fredrik Arnerup & Rasmus Kaj, See COPYING
///
#ifndef PSUNICODE
#define PSUNICODE

#include <map>
#include <string>
#include <glibmm/ustring.h>

// Mapping PostScript glyph names to unicode and vice versa.
// I have tried to follow the specs at
// http://partners.adobe.com/asn/developer/type/unicodegn.html
// to the letter.

namespace PS {
  class Unicode {
  public:
    // Get unicode string associated with glyph.  Set dingbats to true
    // if the font is Zapf Dingbats, or a clone thereof.
    // Returns empty string if the glyph name is undefined.
    static Glib::ustring chars_of_glyph(const std::string &glyph_name,
					bool dingbats = false); 

    // Get space-separated list of PostScript glyph names mapping to
    // unicode character string (usually just one character, but
    // PostScript allows for the mapping of one glyph to a sequence of
    // characters).

    // NOTE: this returns an empty string if the chars are not defined
    // in the Adobe glyph list or in the dingbats list.  It does not
    // construct "uni" or "u" forms

    // NOTE ALSO: Glib::ustring's "==" operator is a bit too smart -
    // it considers e.g. "a" and "acircle" or "v" and "fiveroman" to
    // be the same character
    static std::string glyph_name_of_chars(Glib::ustring chars); 

  private:
    static bool get_dingbat(const std::string &glyph, Glib::ustring &answer);
    static bool get_adobe(const std::string &glyph, Glib::ustring &answer);
    static bool get_uniform(const std::string &glyph, Glib::ustring &answer);
    static bool get_uform(const std::string &glyph, Glib::ustring &answer);

    // This is my solution to the problem of having a symmetric map
    // where values also can be used as keys for values of the
    // keys. There are probably better solutions, but this one is
    // mine.
    class Glyphs {
    public:
      Glyphs(bool dingbats);
      typedef std::map<std::string, const Glib::ustring*> NameMap; 
      typedef std::multimap<Glib::ustring, const std::string*> CodeMap; 
      NameMap namemap;
      CodeMap codemap;
    };
    static const Glyphs glyphlist, dingbats;
  };
};

#endif
