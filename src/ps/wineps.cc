///
// Copyright (C) 2003, 2004, Fredrik Arnerup & Rasmus Kaj, See COPYING
///
#include "wineps.h"
#include "util/stringutil.h"   // safe_getline()
#ifdef WINEPS_STANDALONE
#include <iostream>  // debug
#endif


PS::WinEPSFilter &getline(PS::WinEPSFilter &in, std::string &line) {
  return in.getline(line);
}

namespace PS {
  bool check_windows_magic(const char *magic) {
    unsigned long tmp = 0;
    for(int i = 0; i < 4; i++) {
      tmp <<= 8;  tmp |= static_cast<unsigned char>(magic[i]);
    }
    return tmp == 0xC5D0D3C6;  // magic number for "windows" eps
  }

  WinEPSFilter::WinEPSFilter(const std::string &filename) 
    : ok(true), in(filename.c_str())
  {
    if(in) {
      win = (in.peek() == 0xC5);  // first byte of windows eps
      if(win) {
	char tmp;
	// skip magic number
	for(int i = 0; i < 4; i++)
	  in.get(tmp);
	// read start pos of ps section
	start = 0;
	for(int i = 0; i < 4; i++) {
	  in.get(tmp);
	  start |= static_cast<unsigned char>(tmp) << (i * 8);
	}
	// read length of ps section
	length = 0;
	for(int i = 0; i < 4; i++) {
	  in.get(tmp);
	  length |= static_cast<unsigned char>(tmp) << (i * 8);
	}
#ifdef WINEPS_STANDALONE
	std::cerr << "start: " << start
		  << ", length: " << length << std::endl;
#endif
	// go to start
	in.seekg(start);
      }
    }
  }

  WinEPSFilter &WinEPSFilter::getline(std::string &line) {
    unsigned long before = in.tellg();
    unsigned long max = start + length - 1;
    // safe_getline handles different kinds of line breaks:
    if(safe_getline(in, line)) {
      if(win && before + line.length() > max) {
	line = line.substr(0, max + 1 - before);
	ok = false;
      }
    }
    return *this;
  }
}

#ifdef WINEPS_STANDALONE
int main(int argc, char **argv) {
  if(argc != 2) {
    std::cerr << "you must supply a filename" << std::endl;
    return 1;
  }
  PS::WinEPSFilter in(argv[1]);
  std::string tmp;
  while(getline(in, tmp))
    std::cout << tmp << std::endl;
  return 0;
}
#endif
