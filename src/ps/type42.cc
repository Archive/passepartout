///
// Copyright (C) 2004, Fredrik Arnerup & Rasmus Kaj, See COPYING
///
#include "type42.h"
#include <stdexcept>
#include "fonts/freetype.hh"
#include "encode.h"
#include <fstream>

//g++ -DTYPE42_STANDALONE -Wall -g -o type42 -I.. $(pkg-config --libs --cflags glibmm-2.4) type42.cc ../fonts/truetype.cc ../util/warning.cc encode.cc

namespace {
  // starts from 32, goes to 126
  char const *ascii_encoding[] = {
    "space",    "exclam",    "quotedbl",
    "numbersign",    "dollar",    "percent",
    "ampersand",    "quoteright",    "parenleft",
    "parenright",    "asterisk",    "plus",
    "comma",    "hyphen",    "period",
    "slash",    "zero",    "one",
    "two",    "three",    "four",
    "five",    "six",    "seven",
    "eight",    "nine",    "colon",
    "semicolon",    "less",    "equal",
    "greater",    "question",    "at",
    "A",    "B",    "C",
    "D",    "E",    "F",
    "G",    "H",    "I",
    "J",    "K",    "L",
    "M",    "N",    "O",
    "P",    "Q",    "R",
    "S",    "T",    "U",
    "V",    "W",    "X",
    "Y",    "Z",    "bracketleft",
    "backslash",    "bracketright",    "asciicircum",
    "underscore",    "quoteleft",    "a",
    "b",    "c",    "d",
    "e",    "f",    "g",
    "h",    "i",    "j",
    "k",    "l",    "m",
    "n",    "o",    "p",
    "q",    "r",    "s",
    "t",    "u",    "v",
    "w",    "x",    "y",
    "z",    "braceleft",    "bar",
    "braceright",    "asciitilde"
  };
}

void PS::truetype2type42(const std::string &infile, std::ostream &out) {
  std::ifstream in(infile.c_str());
  font::FTMetrics metrics(infile);

  /// \todo get version numbers from truetype file
  out << "%!PS-TrueTypeFont\n"
    // the dict should be initialized to a suitable size, although it
    // isn't critical in level 2 or later
      << "17 dict begin\n" 
      << "/FontName /" << metrics.getPostscriptName() << " def\n"
      << "/PaintType 0 def\n"
      << "/FontMatrix [1 0 0 1 0 0] def\n"
      << "/FontType 42 def\n";

  out << "/FontBBox [";
  for(int i = 0; i < 4; i++)
    out << metrics.getBBox(i) << " ";
  out << "] def\n";
  
  //define encoding
  out << "/Encoding 256 array\n"
      << "0 1 255 {1 index exch /.notdef put} for\n";
  for(int i = 32; i <= 126; i++) // only ascii
    out << "dup " << i << " /" << ascii_encoding[i - 32] << " put\n";
  out << "readonly def\n";

  // glyph name -> glyph index mapping
  using font::FTMetrics;
  out << "/CharStrings " << metrics.numOfGlyphs() << " dict dup begin\n";
  for(unsigned int i = 0; i < metrics.numOfGlyphs(); i++)
    out << "  /" << metrics.nameOfGlyph(i) << " " << i << " def\n";
  out << "end readonly def\n";

  // include the actual truetype data
  /// \todo this needs to be modified if we want to be backwards
  /// compatible with old PostScript interpreters
  in.seekg(0); // not really necessary if FTMetrics gets a filename
               // instead of a stream
  // no exception on eof
  in.exceptions(std::ios_base::badbit);
  out << "/sfnts [<\n";
  ASCIIHexEncodeFilter filter(false);
  filter.begin(&out);
  const int buffer_size = 1024; // rather arbitrary
  char buffer[buffer_size];
  long length = 0;
  while(in) {
    in.read(buffer, buffer_size);
    long count = in.gcount();
    filter.write(reinterpret_cast<unsigned char*>(buffer), count);
    length += count;
    if(length > 64000) { // PostScript strings have a maximum length of 65535
      length = 0;
      filter.write(reinterpret_cast<unsigned char*>(buffer), 0, true); // flush
      out << ">\n<"; // start new string
    }
    filter.end();
  }
  out << ">] def\n";

  // end the dictionary and define the font
  out << "FontName currentdict end definefont pop" << std::endl;
}

#ifdef TYPE42_STANDALONE
int main(int argc, char **argv) {
  try {
    if(argc != 2)
      throw std::runtime_error("Usage: type42 <ttyfile>");
    std::ifstream file(argv[1]);
    PS::truetype2type42(file, std::cout);
  } catch(const std::exception &e) {
    std::cerr << e.what() << std::endl;
  }
}
#endif
