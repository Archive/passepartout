#include "testbed.hh"
#include "util/warning.h"
#include <glibmm/exception.h>	// Stupid glib!
#include <algorithm>
#include <functional>
///
// Copyright (C) 2003, 2004, Fredrik Arnerup & Rasmus Kaj, See COPYING
///

TestCase::CaseSet TestCase::cases;
int TestCase::successes = 0;
int TestCase::failures = 0;

TestCase::TestCase(const std::string& name)
  : name_(name)
{
  cases.insert(this);
}

TestCase::~TestCase() {
  cases.erase(this);
}

bool TestCase::run() {
  try {
    verbose << "Running test " << name_ << " ... ";
    test();
    verbose << "done." << std::endl;
    ++successes;
    return true;
    
  } catch(const std::exception& err) {
    warning << "\nTest \"" << name_ << "\" failed: " << err.what() 
	    << '.' << std::endl;
    ++failures;
    return false;
    
  } catch(const Glib::Exception& err) {
    // Glib::Exception doesn't inherit std::exception.  Bad Glib!
    warning << "\nTest \"" << name_ << "\" failed: " << err.what() 
	    << '.' << std::endl;
    ++failures;
    return false;
  }
}

void do_assert(bool condition, const char* msg) {
  if(!condition)
    throw std::logic_error(msg);
};

int main(int argc, char* argv[]) {
  try {
    verbose.active = true;
    verbose << "Starting test run. " << TestCase::cases.size()
	    << " case(s) to try." << std::endl;
    std::for_each(TestCase::cases.begin(), TestCase::cases.end(),
		  std::mem_fun(&TestCase::run));
    verbose << "Done running " << TestCase::cases.size() << " test case(s). "
	    << TestCase::successes << " succeded, "
	    << TestCase::failures << " failed." << std::endl;
    
    return TestCase::failures == 0? 0: 1;

  } catch(const std::exception& err) {
    warning << "\nERROR: Exception reached main: " << err.what() << std::endl;
    return 1;
  } catch(...) {
    warning << "\nERROR: Caugth non-exception.  How rude!" << std::endl;
    return 1;
  }
}
