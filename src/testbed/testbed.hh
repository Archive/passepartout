#ifndef PPT_TESTBED_H
#define PPT_TESTBED_H
///
// Copyright (C) 2003, 2004, Fredrik Arnerup & Rasmus Kaj, See COPYING
///
#include <stdexcept>
#include <set>

/**
 * Base class for test case classes.  Test classes are self-regiestering in a
 * container, so each instantiaded test case will be run from testmain.
 */
class TestCase {
public:
  TestCase(const std::string& name);
  virtual ~TestCase();
  
  virtual void test() = 0;
  
private:
  bool run();
  std::string name_;
  
  typedef std::set<TestCase*> CaseSet;
  static CaseSet cases;
  static int successes, failures;
  friend int main(int argc, char* argv[]);
  
  // undefined
  TestCase();
  TestCase(const TestCase&);
  TestCase& operator = (const TestCase&);
};

/**
 * A TestCase where the actual testing is done by a simple function.
 */
template<typename Function>
class AnyTestFunction : public TestCase {
public:
  AnyTestFunction(const std::string& name, Function function)
    : TestCase(name), function_(function)
    {}
  void test() {
    function_();
  }
private:
  Function function_;
};

typedef void (*SimpleFunction)();
typedef AnyTestFunction<SimpleFunction> TestFunction;

/** Utility function: extended assertion, throws logic_error on failure */
void do_assert(bool condition, const char* msg);

/** The actual assertion */
#define ASSERT(cond) do_assert(cond, "Assertion failed: " #cond);

#endif
