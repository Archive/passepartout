#ifndef PP_FONTINFO_HH		// -*- c++ -*-
#define PP_FONTINFO_HH
///
// Copyright (C) 2002 - 2004, Fredrik Arnerup & Rasmus Kaj, See COPYING
///
#include <string>
#include <glibmm/ustring.h>
#include <set>

#include "fontmetrics.hh"

namespace font {
  
  struct FontInfo {
    typedef std::string string;
    FontInfo(const std::string& n, float s, float spacing = 0);
    FontInfo(const std::string& n, const font::Metrics& m, float s)
      : name(n), metrics(m), size(s) {}
    
    static FontInfo WidenFont(const FontInfo &orig, const float &widen);
    
    const std::string& getName() const { return name; }
    std::string getRealName() const; // no alias
    const float&  getSize() const { return size; }
    const float& getLetterSpacing() const { return letter_spacing; }

    bool hasGlyph(const std::string &glyphname) const {
      return metrics.hasGlyph(glyphname);
    }
    std::string getGlyphName(const Glib::ustring &chars) const {
      return metrics.getGlyphName(chars);
    }
    float getWidth(const Glib::ustring& s) const;
    float getAscender() const { return metrics.getAscender() * size; }
    float getDescender() const { return metrics.getDescender() * size; }
    float getUnderlinePos() const { return metrics.getUnderlinePos()* size; }
    float getUnderlineThickness() const {
      return metrics.getUnderlineThickness() * size;
    }
    
    const Metrics& getMetrics() const { return metrics; }
  private:
    const std::string name;
    const font::Metrics &metrics;
    float size, letter_spacing;
  };

  typedef std::set<std::string> Fonts;
};

#endif
