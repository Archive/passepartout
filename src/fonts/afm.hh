#ifndef PP_AFM_HH		// -*- c++ -*-
#define PP_AFM_HH
///
// Copyright (C) 2002 - 2004, Fredrik Arnerup & Rasmus Kaj, See COPYING
///
#include <vector>
#include <map>
#include "fontmetrics.hh"

namespace font {
  
  class AFMetrics: public Metrics {
  public:
    AFMetrics(const std::string &filename);
    ~AFMetrics();
    
    std::string getName() const { return name; }
    std::string getPostscriptName() const { return ps_name; }
    float getWidth(const Glib::ustring &str) const;
    float getAscender()  const { return ascender; }
    float getDescender() const { return descender; }
    float getExHeight()  const { return x_height; }
    float getCapHeight() const { return cap_height; }
    float getUnderlinePos() const { return underline_position; }
    float getUnderlineThickness() const { return underline_thickness; }
    float getItalicAngle() const { return italic_angle; }
    
    bool hasGlyph(const std::string &glyphname) const;
    std::string getGlyphName(const Glib::ustring &chars) const;

    int getBBox(int idx) const { /* assert(0<=idx<4); */ return bbox[idx]; }

  private:
    float ascender, descender;
    float underline_position, underline_thickness;
    float cap_height, x_height, italic_angle;
    int bbox[4];
    std::string name, ps_name;
    
    class GlyphData;
    GlyphData* glyphdata;
    
    AFMetrics();
    AFMetrics(const AFMetrics&);
    void operator = (const AFMetrics&);
  };
}

#endif
