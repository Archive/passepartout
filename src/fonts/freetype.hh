#ifndef PP_FREETYPE_HH		// -*- c++ -*-
#define PP_FREETYPE_HH
///
// Copyright (C) 2004, Fredrik Arnerup & Rasmus Kaj, See COPYING
///
#include "fontmetrics.hh"
#include <ft2build.h>
#include FT_FREETYPE_H

namespace font {
  /// Uses FreeType to read font metrics
  class FTMetrics: public Metrics {
  public:
    FTMetrics(const std::string &filename);
    ~FTMetrics();
    
    std::string getName() const;
    std::string getPostscriptName() const;
    float getWidth(const Glib::ustring &str) const;
    float getAscender()  const;
    /// This is positive, for descenders below the baseline
    float getDescender() const;
    float getExHeight()  const;
    float getCapHeight() const;
    float getUnderlinePos() const;
    float getUnderlineThickness() const;
    float getItalicAngle() const;

    bool hasGlyph(const std::string &glyphname) const { return true; } /// \todo
    std::string nameOfGlyph(unsigned int glyph_index) const;
    std::string getGlyphName(const Glib::ustring &chars) const;

    unsigned int get_glyph_index(gunichar c) const;
    void write_CIDToGIDMap(std::ostream &out) const;

    /// return values in millipoints
    int getBBox(int idx) const;


    unsigned int numOfGlyphs() const { return face->num_glyphs; }

  private:
    static FT_Library library;
    FT_Face face;
    float exHeight, capHeight, italicAngle;

    FTMetrics();
    FTMetrics(const FTMetrics&);
    void operator = (const FTMetrics&);
  };
}

#endif
