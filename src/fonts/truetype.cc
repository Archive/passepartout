///
// Copyright (C) 2004, Fredrik Arnerup & Rasmus Kaj, See COPYING
///
#include "truetype.hh"
#ifdef STANDALONE
#include <fstream> // test prog
#include <glibmm.h>
#endif
#include <stdexcept>
#include <iomanip> // ios_base
#include <string>
#include "util/stringutil.h"

// http://developer.apple.com/fonts/TTRefMan/RM06/Chap6.html

// g++ -DSTANDALONE -Wall -g -o truetype -I.. $(pkg-config --libs --cflags glibmm-2.4) truetype.cc ../util/warning.o

namespace {
  /// rub == "read unsigned bigendian"
  unsigned int rub(std::istream &in, int bytes) {
    unsigned int result = 0;
    for(int i = 0; i < bytes; i++) {
      char c;
      in.get(c);
      result <<= 8;
      result |= static_cast<unsigned char>(c);
    }
    return result;
  }

  /// wub16 == "write unsigned bigendian 16-bit"
  std::ostream& wub16(std::ostream &out, unsigned int x) {
    x &= 0xFFFF;
    unsigned char l = x & 0xFF;
    unsigned char h = x >> 8;
    out << h << l;
    return out;
  }

  std::string read_pascal_string(std::istream &in) {
    char c = in.get();
    int length = static_cast<unsigned char>(c);
    /// \todo use std::vector ?
    char *buffer = new char[length + 1];
    try {
      in.get(buffer, length + 1);
    } catch (...) {
      delete[] buffer;
      throw;
    }
    std::string result(buffer);
    delete[] buffer;
    return result;
  }

  std::string read_string(std::istream &in,
                          unsigned int offset,
                          unsigned int length) {
    in.seekg(offset);
    /// \todo use std::vector ?
    char *buffer = new char[length + 1];
    try {
      in.get(buffer, length + 1);
    } catch (...) {
      delete[] buffer;
      throw;
    }
    std::string result(buffer);
    delete[] buffer;
    return result;
  }
  
  /// convert from signed 16.16 fixed point (not tested much)
  double from_fixed(unsigned int a) {
    //    std::cerr << a << std::endl;
    bool sign = (1 << 31) & a;
    // 2's complement
    if(sign) {
      a -= 1;
      a ^= (2 << 31) - 1;
    }
    double result = double(a) / (1 << 16);
    return (sign ? -1 : 1) * result;
  }

  /// convert from signed 16-bit int stored in unsigned int to signed int
  int from_FWord(unsigned int a) {
    bool sign = (1 << 15) & a;
    // 2's complement
    if(sign) {
      a -= 1;
      a ^= (1 << 16) - 1;
    }
    return (sign ? -1 : 1) * a;
  }
}

class MappingTable0: public font::TTYMetrics::MappingTable {
public:
  char glyphIndexArray[256];

  unsigned int get_glyph_index(gunichar c) const {
    // I think 0 is good for undefined chars
    return c < 128 ? static_cast<unsigned char>(glyphIndexArray[c]) : 0;
  }
  
  void write_CIDToGIDMap(std::ostream &out) const {
    for(int i = 0; i < 128; i++) {
      // 16 bit indices, high-order byte first
      wub16(out, get_glyph_index(i));
    }
    out << std::flush;
  }

  void write_CMap(std::ostream &out) const {
    out << "%!PS-Adobe-3.0 Resource-CMap\n"
        << "%%DocumentNeededResources: ProcSet(CIDInit)\n"
        << "%%IncludeResource: ProcSet(CIDInit)\n"
        << "%%BeginResource: CMAP(foo)\n"
        << "%%Title: foo\n"
        << "%%EndComments\n";

    out << "/CIDInit /ProcSet findresource begin\n"
        << "12 dict begin\n"
        << "begincmap\n"
        << "/CIDSystemInfo\n"
        << "3 dict dup begin\n"
        << "/Registry (Adobe) def\n"
        << "/Ordering (Identity) def\n"
        << "/Supplement 0 def\n"
        << "end def\n"
        << "\n"
        << "/CMapName /foo def\n"
        << "/CMapVersion 1 def\n"
        << "/CMapType 1 def\n"
        << "/UIDOffset 0 def\n" /// \todo bogus
        << "/XUID [17 17 4711] def\n" /// \todo bogus
        << "/WMode 0 def\n"
        << "1 begincodespacerange\n"
        << "<00> <80>\n" // ascii
        << "endcodespacerange\n"
      /// \todo notdefrange
        << "\n"
      /// \todo unicode
        << "128 begincidchar\n";
    for(int i = 0; i < 128; i++) {
      out.setf(std::ios_base::hex, std::ios_base::basefield);
      out << "<" << (i < 16 ? "0" : "") << i << "> ";
      out.setf(std::ios_base::dec, std::ios_base::basefield);
      out << int(glyphIndexArray[i]) << "\n";
    }
    out << "endcidchar\n"
        << "endcmap\n"
        << "CMapName currentdict /CMap defineresource pop\n"
        << "end\n"
        << "end\n"
        << "%%EndResource\n"
        << "%%EOF" << std::flush;
  }
};

class MappingTable4: public font::TTYMetrics::MappingTable {
public:
  struct Segment {
    Segment(unsigned int startCode, unsigned int endCode,
            unsigned int idDelta, unsigned int startGID, bool use_StartGID)
      : startCode(startCode), endCode(endCode), size(endCode - startCode + 1),
        idDelta(idDelta), startGID(startGID), use_StartGID(use_StartGID)
    {}
    unsigned int startCode, endCode, size, idDelta, startGID;
    bool use_StartGID;
  };

  typedef std::vector<Segment> Segments;
  Segments segments;

  virtual ~MappingTable4() {}

  unsigned int get_glyph_index(gunichar c) const {
    // this is work in progress

    Segments::const_iterator s = segments.begin();
    s++;
    for(;
        s != segments.end();
        s++) {
      if(c <= s->endCode) {
        if(c >= s->startCode) {
          if(s->use_StartGID)
            return s->startGID + c - s->startCode;
          else
            // idDelta arithmetic is modulo 65536
            return (c + s->idDelta) % 65536;
        } else {
          return 0;
        }
      }
    }
    return 0;

//     for(Segments::const_reverse_iterator s = segments.rbegin();
//         s != segments.rend();
//         s++) {
//       std::cerr << "segment" << std::endl;
//       if(c <= s->endCode && c >= s->startCode) {
//         using namespace std;
//         cerr << "hit" << endl
//              << c << endl
//              << s->idDelta << endl
//              << (c + s->idDelta) % 65536 << endl;
//         if(s->use_StartGID)
//           return s->startGID + c - s->startCode;
//         else
//           // idDelta arithmetic is modulo 65536
//           return (c + s->idDelta) % 65536;
//       }
//     }
//     // Should never reach this in a proper ttf
//     return 0;
  }
  
  void write_CIDToGIDMap(std::ostream &out) const {
    for(int i = 0; i < 65536; i++) {
      // 16 bit indices, high-order byte first
      wub16(out, get_glyph_index(i));
    }
    out << std::flush;
//     // we are assuming the segments are sorted in order of increasing
//     // startCodes
//     Segments::const_iterator s = segments.begin();
//     unsigned int cid = 0;
//     while(s != segments.end() && cid < 65536) {

//       // we have to include all the undefined CIDs as well
//       for(; cid < (*s)->startCode; cid++)
//         wub16(out, 0);

//       if((*s)->glyphIdArray.empty()) {
//         // idDelta arithmetic is modulo 65536
//         for(unsigned int g = (*s)->startCode; g <= (*s)->endCode; g++)
//           wub16(out, (g + (*s)->idDelta) % 65536);
//       } else {
//         for(std::vector<unsigned int>::const_iterator g =
//               (*s)->glyphIdArray.begin();
//             g != (*s)->glyphIdArray.end();
//             g++, cid++) {
//           wub16(out, *g);
//         }
//       }
//       s++;
//     }
//     out << std::flush;
  }

  void write_CMap(std::ostream &out) const { /// \todo
  }
};

font::TTYMetrics::TTYMetrics(std::istream &in, bool debug)
  : out(debug)
{
  // turn on exceptions
  in.exceptions(std::ios_base::badbit
                | std::ios_base::failbit
                | std::ios_base::eofbit);

  // read_font_dir must come first
  read_font_dir(in);
  read_name_table(in);
  read_head_table(in);
  read_character_map_table(in);
  read_postscript_table(in);
  read_horizontal_header_table(in);
  // must run this one after read_horizontal_header_table, because
  // it uses numOfLongHorMetrics
  read_horizontal_metrics_table(in);
}

float font::TTYMetrics::getWidth(const Glib::ustring &str) const {
  long width = 0;
  for(Glib::ustring::const_iterator i = str.begin(); i != str.end(); i++) {
    if(*i < 256)
      width += get_width(mapping_table->get_glyph_index(*i));
  }
  return to_pt(width);
}

bool font::TTYMetrics::hasGlyph(const std::string &glyphname) const {
  return glyphs.find(glyphname) != glyphs.end();
}

std::string font::TTYMetrics::getGlyphName(const Glib::ustring &chars) const {
  if(chars.length() != 1)
    return std::string();
  /// \todo inefficient
  for(Glyphs::const_iterator i = glyphs.begin(); i != glyphs.end(); i++)
    if(i->second == chars[0])
      return i->first;
  return std::string();
}

int font::TTYMetrics::getBBox(int idx) const {
  /// \todo check that these are in the correct order
  int value;
  switch(idx) {
  case 0: value = bbox.xmin; break;
  case 1: value = bbox.ymin; break;
  case 2: value = bbox.xmax; break;
  case 3: value = bbox.ymax; break;
  default: throw std::runtime_error("Invalid BBox index: " + tostr(idx));
  }
  // convert to postscript font units (em/1000)
  return (value * 1000) / int(units_per_em);
}

void font::TTYMetrics::print_info() const {
  using namespace std;
  cout << "full name:       " << full_name << endl;
  cout << "family name:     " << family_name << endl;
  cout << "PostScript name: " << postscript_name << endl;
  cout << "italic angle:    " << italic_angle << " degrees" << endl;
  cout << "underline pos:   " << underline_position << endl;
  cout << "underline thick: " << underline_thickness << endl;
  cout << "ascent:          " << ascent << endl;
  cout << "descent:         " << descent << endl;
  cout << "fixed pitch:     "
       << ((is_fixed_pitch || numOfLongHorMetrics == 1)
           ? std::string("yes")
           : std::string("no"))
       << endl;
  cout << "BBox:            [" << getBBox(0) << ", " << getBBox(1)
       << ", " << getBBox(2) << ", " << getBBox(3) << "]" << endl;
  cout << "Number of glyphs: " << glyphs.size() << endl;
//   for(Glyphs::const_iterator i = glyphs.begin(); i != glyphs.end(); i++)
//     cout << i->first
//          << ", number: " << i->second
//          << ", width: " << get_width(i->second) << endl;
}

unsigned int font::TTYMetrics::get_width(unsigned int index) const {
  try {
    return widths.at(index);
  } catch(...) {
    // assume this is a fixed width font and that there is only one
    // width entry
    return widths.at(0);
  }
}

void font::TTYMetrics::populate_table_types() {
  if(!table_types.empty())
    return;
  for(int i = 0; table_types_data[i]; i += 2)
    table_types[table_types_data[i]] = table_types_data[i + 1];
}

void font::TTYMetrics::read_font_dir(std::istream &in) {
  using namespace std;

  // offset subtable
  unsigned int scalerType = rub(in, 4);
  out << "scaler type:   " << scalerType << endl;
  // "true" or 0x00010000 tells us that the file contains truetype
  // data and not something else
  bool is_truetype = (scalerType == 0x74727565) // "true"
    || (scalerType ==  0x00010000);
  out << (is_truetype
          ? string("This file contains TrueType data")
          : string("This file does not contain TrueType data"))
      << endl;
  unsigned int numTables = rub(in, 2);
  out << "numTables:     " << numTables << endl;
  unsigned int searchRange = rub(in, 2);
  out << "searchRange:   " << searchRange << endl;
  unsigned int entrySelector = rub(in, 2);
  out << "entrySelector: " << entrySelector << endl;
  unsigned int rangeShift = rub(in, 2);
  out << "rangeShift:    " << rangeShift << endl << endl;
  // sanity checks
  bool sc1 = (searchRange % 16) == 0;
  bool sc2 = (searchRange / 16) == (1U << entrySelector);
  bool sc3 = rangeShift == numTables * 16 - searchRange;
  if(!(sc1 && sc2 && sc3))
    throw std::runtime_error("TrueType file failed sanity check");

  read_table_dir(in, numTables);
}

void font::TTYMetrics::read_table_dir(std::istream &in,
                                      unsigned int numTables) {
  populate_table_types();

  using namespace std;
  for(unsigned int i = 0; i < numTables; i++) {
    char tag[5];
    in.get(tag, 5);
    /// \todo make sure tag is ascii
    out << "tag:      " << tag;
    TableTypes::const_iterator type = table_types.find(tag);
    if(type != table_types.end())
      out << " - " << type->second;
    out << "" << endl;
    out << "checkSum: " << rub(in, 4) << endl;
    unsigned int offset = rub(in, 4);
    out << "offset:   " << offset << endl;
    unsigned int length = rub(in, 4);
    out << "length:   " << length << endl << endl;
    offsets[tag] = offset;
  }
}

void font::TTYMetrics::read_head_table(std::istream &in) {
  Offsets::const_iterator i = offsets.find("head");
  if(i == offsets.end())
    throw std::runtime_error("No head table found");
  unsigned int offset = i->second;
  in.seekg(offset);

  using std::endl;
  out << "" << endl << "head" << endl;
  // version should be 1.0
  out << "Version:      " <<  from_fixed(rub(in, 4)) << endl;
  out << "fontRevision: " <<  from_fixed(rub(in, 4)) << endl;
  rub(in, 4); //checkSumAdjustment
  if(rub(in, 4) != 0x5F0F3CF5)
    throw std::runtime_error("Bad magic number in head table");
  rub(in, 2); /// \todo flags, care about this later
  units_per_em = rub(in, 2);
  out << "unitsPerEm:   " << units_per_em << endl;
  rub(in, 8); rub(in, 8); // time stamps
  // bbox
  bbox.xmin = from_FWord(rub(in, 2));
  bbox.ymin = from_FWord(rub(in, 2));
  bbox.xmax = from_FWord(rub(in, 2));
  bbox.ymax = from_FWord(rub(in, 2));
  out << "bounding box: [" << bbox.xmin << ", " << bbox.ymin << ", "
      << bbox.xmax << ", " << bbox.ymax << "]" << endl;
  /// \todo more stuff after this
}

void font::TTYMetrics::read_character_map_table(std::istream &in) {
   Offsets::const_iterator i = offsets.find("cmap");
  if(i == offsets.end())
    throw std::runtime_error("No cmap table found");
  unsigned int offset = i->second;
  in.seekg(offset);

  using std::endl;
  out << "" << endl << "cmap" << endl;
  // version should be 0
  out << "version:         " << rub(in, 2) << endl;
  unsigned int numberSubtables = rub(in, 2);
  out << "numberSubtables: " << numberSubtables << endl;

  // read encoding subtables
  unsigned int macroman_offset = 0, msunicode_offset = 0;
  for(unsigned int i = 0; i < numberSubtables; i++) {
    unsigned int platformID = rub(in, 2);
    unsigned int platformSpecificID = rub(in, 2);
    unsigned int loffset = rub(in, 4);
    out << "" << endl << "encoding" << endl
        << "platformID:         " << platformID << endl
        << "platformSpecificID: " << platformSpecificID << endl
        << "offset:             " << offset << endl;

    std::iostream::pos_type saved_pos = in.tellg();
    // Display character map header
    in.seekg(offset + loffset);
    out << "" << endl << "mapping" << endl;
    // these three fields seem common to most formats, though the
    // "format" field is actually a 32 fixed point in some formats ...
    unsigned int format = rub(in, 2);
    unsigned int length = rub(in, 2);
    unsigned int language = rub(in, 2);
    out << "format:   " << format << endl
	<< "length:   " << length << endl
	<< "language: " << language << endl;
    in.seekg(saved_pos);

    // Use Microsoft Unicode encoding if present, MacRoman otherwise
    if(platformID == 1 && platformSpecificID == 0)
      macroman_offset = loffset;
    else if(platformID == 3 && platformSpecificID == 1)
      msunicode_offset = loffset;
  }
  
  if(macroman_offset == 0 && msunicode_offset == 0)
    throw std::runtime_error("The font contains neither a Microsoft Unicode "
                             "nor a MacRoman encoding.");
  
  // read the actual character map
  /// \todo prefer msunicode, when it is stable
  // in.seekg(offset + (msunicode_offset ? msunicode_offset : macroman_offset));
  in.seekg(offset + (macroman_offset ? macroman_offset : msunicode_offset));
  out << "" << endl << "mapping" << endl;
  // these three fields seem common to most formats, though the
  // "format" field is actually a 32 fixed point in some formats ...
  unsigned int format = rub(in, 2);
  unsigned int length = rub(in, 2);
  unsigned int language = rub(in, 2);
  out << "format:   " << format << endl
      << "length:   " << length << endl
      << "language: " << language << endl;

  switch(format) {
  case 0: { // format 0 is used for 8-bit encodings
    MappingTable0 *mt = new MappingTable0;
    mapping_table.reset(mt);
    in.read(mt->glyphIndexArray, 256);
    for(int i = 0; i < 256; i++)
      out << i << " -> "
          << int(static_cast<unsigned char>(mt->glyphIndexArray[i]))
          << endl;
  }
    break;
  case 4: { // format 4 is for sparse 16-bit encodings
    // twice the number of segments
    MappingTable4 *mt = new MappingTable4;
    mapping_table.reset(mt);
    unsigned int segCountX2 = rub(in, 2);
    unsigned int segCount = segCountX2 / 2;
    out << "segCountX2:   " << segCountX2 << endl
        << "segCount:     " << segCount << endl;
    rub(in, 2); // searchRange
    rub(in, 2); // entrySelector
    std::iostream::pos_type endCode_pos = in.tellg();
    const int array_size = segCountX2;
    const int step = array_size - 1;
    for(unsigned int i = 0; i < segCount; i++) {
      in.seekg(endCode_pos);
      in.ignore(i * 2);
      unsigned int endCode = rub(in, 2);
      in.ignore(step + 1); // skip reservedPad
      unsigned int startCode = rub(in, 2);
      in.ignore(step);
      // idDelta arithmetic is modulo 65536
      unsigned int idDelta = rub(in, 2);
      in.ignore(step);
      unsigned int idRangeOffset = rub(in, 2);
      out << "" << endl
          << "segment " << i << endl
          << "endCode:       " << endCode << endl
          << "startCode:     " << startCode << endl
          << "idDelta:       " << idDelta << endl
          << "idRangeOffset: " << idRangeOffset << endl
          << endl;
      unsigned int startGID = 0;
      if(idRangeOffset != 0) {
        in.ignore(idRangeOffset - 2); // we have already passed the
                                      // two bytes in idRangeOffset

        /// \note This isn't exactly what the specs say, but it seems
        /// to work. Also, the TrueType and OpenType specs seem to
        /// contradict each other. And I really don't understand them
        /// anyway ...
        startGID = rub(in, 2);
      }
      // if idRangeOffset is 0, then there is nothing in glyphIdArray
      // and the glyph indices are determined entirely by startCode
      // and idDelta
      mt->segments.push_back(MappingTable4::Segment(startCode, endCode,
                                                    idDelta, startGID,
                                                    idRangeOffset != 0));
    }
  }
    break;
  default:
    throw std::runtime_error("Character map table format " + tostr(format)
                             + " is not supported");
    break;
  }
}

void font::TTYMetrics::read_postscript_table(std::istream &in) {
  Offsets::const_iterator i = offsets.find("post");
  if(i == offsets.end())
    throw std::runtime_error("No post table found");
  unsigned int offset = i->second;
  in.seekg(offset);

  using std::endl;
  out << "" << endl << "post" << endl;
  double format = from_fixed(rub(in, 4));
  out << "format:             " << format << endl;
  italic_angle = from_fixed(rub(in, 4));
  out << "italicAngle:        " << italic_angle << " degrees" << endl;
  underline_position = from_FWord(rub(in, 2));
  underline_thickness = from_FWord(rub(in, 2));
  out << "underlinePosition:  " << underline_position << endl
      << "underlineThickness: " << underline_thickness << endl;
  is_fixed_pitch = rub(in, 2) == 1;
  out << (is_fixed_pitch
          ? std::string("fixed pitch")
          : std::string("not fixed pitch")) << endl;
  rub(in, 2); // skip reserved data
  rub(in, 4); rub(in, 4); rub(in, 4); rub(in, 4); // skip boring memory crap
    
  /// \todo support other formats
  if(format == 2) {
    unsigned int numberOfGlyphs = rub(in, 2);
    out << "numberOfGlyphs: " << numberOfGlyphs << endl;
    unsigned int numberNewGlyphs = numberOfGlyphs;

    // look for standard order glyphs
    for(unsigned int i = 0; i < numberOfGlyphs; i++) {
      unsigned int glyphNameIndex = rub(in, 2);
      if(glyphNameIndex < 258) { // not a typo, not 256
        glyphs[standard_glyph_names[glyphNameIndex]] = glyphNameIndex;
        //          std::cout << standard_glyph_names[glyphNameIndex] << endl;
        numberNewGlyphs--;
      }
    }

    out << "numberNewGlyphs: " << numberNewGlyphs << endl;

    // read other glyphs
    for(unsigned int i = 258; i < 258 + numberNewGlyphs; i++) {
      std::string name = read_pascal_string(in);
      glyphs[name] = i;
    }
  }
}

void font::TTYMetrics::read_horizontal_header_table(std::istream &in) {
  Offsets::const_iterator i = offsets.find("hhea");
  if(i == offsets.end())
    throw std::runtime_error("No hhea table found");
  unsigned int offset = i->second;
  in.seekg(offset);

  using std::endl;
  out << "" << endl << "hhea table" << endl;
  out << "version: " << from_fixed(rub(in, 4)) << endl;
  ascent = from_FWord(rub(in, 2));
  descent = from_FWord(rub(in, 2));
  out << "ascent:  " << ascent << endl
      << "descent: " << descent << endl;
  rub(in,2); // lineGap
  rub(in,2); // advanceWidthMax
  rub(in,2); // minLeftSideBearing
  rub(in,2); // minRightSideBearing
  rub(in,2); // xMaxExtent
  rub(in,2); // caretSlopeRise
  rub(in,2); // caretSlopeRun
  rub(in,2); // caretOffset
  rub(in,2); // reserved
  rub(in,2); // reserved
  rub(in,2); // reserved
  rub(in,2); // reserved
  rub(in,2); // metricDataFormat

  // this is 1 for fixed width fonts
  numOfLongHorMetrics = rub(in,2);
  out << "numOfLongHorMetrics: " << numOfLongHorMetrics << endl;
}

void font::TTYMetrics::read_horizontal_metrics_table(std::istream &in) {
  Offsets::const_iterator i = offsets.find("hmtx");
  if(i == offsets.end())
    throw std::runtime_error("No hmtx table found");
  unsigned int offset = i->second;
  in.seekg(offset);

  using std::endl;
  for(unsigned int i = 0; i < numOfLongHorMetrics; i++) {
    widths.push_back(rub(in, 2)); // advanceWidth;
    rub(in, 2); // leftSideBearing, don't know what that is
  }
  // if this is a fixed width font then an array of leftSideBearings follow
}

void font::TTYMetrics::read_name_table(std::istream &in) {
  Offsets::const_iterator i = offsets.find("name");
  if(i == offsets.end())
    throw std::runtime_error("No name table found");
  unsigned int offset = i->second;
  in.seekg(offset);

  using std::endl;
  // header
  unsigned int format = rub(in, 2);
  if(format != 0)
    throw std::runtime_error("Name table format is not 0");
  unsigned int count = rub(in, 2);
  out << "count:        " << count << endl;
  unsigned int stringOffset = rub(in, 2);
  out << "stringOffset: " << stringOffset << endl;
  unsigned int gStringOffset = offset + stringOffset;
    
  unsigned int postscriptOffset = 0;
  unsigned int familyOffset = 0;
  unsigned int fullOffset = 0;
  unsigned int postscriptLength = 0;
  unsigned int familyLength = 0;
  unsigned int fullLength = 0;
  for(unsigned int i = 0; i < count; i++) {
    unsigned int platformID, platformSpecificID, languageID, nameID,
      length, offset;
    platformID = rub(in, 2);
    platformSpecificID = rub(in, 2);
    languageID = rub(in, 2);
    nameID = rub(in, 2);
    length = rub(in, 2);
    offset = rub(in, 2);

    /// \todo interpret UTF-16. For now, use the mac encoding
    /// versions (if they exist) and hope they are ASCII
    if(platformID == 1)
      switch(nameID) {
      case 1: familyOffset = offset; familyLength = length; break;
      case 4: fullOffset = offset; fullLength = length; break;
      case 6: postscriptOffset = offset; postscriptLength = length; break;
      default: break;
      }

    out << "" << endl << "platformID:         " << platformID << " ";
    switch(platformID) {
    case 0: out << "(Unicode)"; break;
    case 1: out << "(Macintosh)"; break;
    case 2: out << "(ISO/IEC 10646, deprecated)"; break;
    case 3: out << "(Microsoft)"; break;
    default: out << "(unknown)"; break;
    }
    out << "" << endl;
    out << "platformSpecificID: " << platformSpecificID << endl
        << "languageID:         " << languageID << endl;
    out << "nameID:             " << nameID << " - ";
    if(nameID < 20)
      out << nameID_codes[nameID];
    else if(nameID < 256)
      out << "reserved";
    else
      out << "font-specific";
    out << "" << endl;
    out << "length:             " << length << endl
        << "offset:             " << offset << endl;
  }

  if(fullLength)
    full_name = read_string(in, gStringOffset + fullOffset, fullLength);

  if(familyLength)
    family_name = read_string(in, gStringOffset + familyOffset, familyLength);

  if(postscriptLength)
    postscript_name =
      read_string(in, gStringOffset + postscriptOffset, postscriptLength);

  out << "full name:       " << full_name << endl;
  out << "family name:     " << family_name << endl;
  out << "PostScript name: " << postscript_name << endl;
}

/// the 258 standard glyphs
const char* font::TTYMetrics::standard_glyph_names[] = {
  ".notdef",
  ".null",
  "nonmarkingreturn",
  "space",
  "exclam",
  "quotedbl",
  "numbersign",
  "dollar",
  "percent",
  "ampersand",
  "quotesingle",
  "parenleft",
  "parenright",
  "asterisk",
  "plus",
  "comma",
  "hyphen",
  "period",
  "slash",
  "zero",
  "one",
  "two",
  "three",
  "four",
  "five",
  "six",
  "seven",
  "eight",
  "nine",
  "colon",
  "semicolon",
  "less",
  "equal",
  "greater",
  "question",
  "at",
  "A",
  "B",
  "C",
  "D",
  "E",
  "F",
  "G",
  "H",
  "I",
  "J",
  "K",
  "L",
  "M",
  "N",
  "O",
  "P",
  "Q",
  "R",
  "S",
  "T",
  "U",
  "V",
  "W",
  "X",
  "Y",
  "Z",
  "bracketleft",
  "backslash",
  "bracketright",
  "asciicircum",
  "underscore",
  "grave",
  "a",
  "b",
  "c",
  "d",
  "e",
  "f",
  "g",
  "h",
  "i",
  "j",
  "k",
  "l",
  "m",
  "n",
  "o",
  "p",
  "q",
  "r",
  "s",
  "t",
  "u",
  "v",
  "w",
  "x",
  "y",
  "z",
  "braceleft",
  "bar",
  "braceright",
  "asciitilde",
  "Adieresis",
  "Aring",
  "Ccedilla",
  "Eacute",
  "Ntilde",
  "Odieresis",
  "Udieresis",
  "aacute",
  "agrave",
  "acircumflex",
  "adieresis",
  "atilde",
  "aring",
  "ccedilla",
  "eacute",
  "egrave",
  "ecircumflex",
  "edieresis",
  "iacute",
  "igrave",
  "icircumflex",
  "idieresis",
  "ntilde",
  "oacute",
  "ograve",
  "ocircumflex",
  "odieresis",
  "otilde",
  "uacute",
  "ugrave",
  "ucircumflex",
  "udieresis",
  "dagger",
  "degree",
  "cent",
  "sterling",
  "section",
  "bullet",
  "paragraph",
  "germandbls",
  "registered",
  "copyright",
  "trademark",
  "acute",
  "dieresis",
  "notequal",
  "AE",
  "Oslash",
  "infinity",
  "plusminus",
  "lessequal",
  "greaterequal",
  "yen",
  "mu",
  "partialdiff",
  "summation",
  "product",
  "pi",
  "integral",
  "ordfeminine",
  "ordmasculine",
  "Omega",
  "ae",
  "oslash",
  "questiondown",
  "exclamdown",
  "logicalnot",
  "radical",
  "florin",
  "approxequal",
  "Delta",
  "guillemotleft",
  "guillemotright",
  "ellipsis",
  "nonbreakingspace",
  "Agrave",
  "Atilde",
  "Otilde",
  "OE",
  "oe",
  "endash",
  "emdash",
  "quotedblleft",
  "quotedblright",
  "quoteleft",
  "quoteright",
  "divide",
  "lozenge",
  "ydieresis",
  "Ydieresis",
  "fraction",
  "currency",
  "guilsinglleft",
  "guilsinglright",
  "fi",
  "fl",
  "daggerdbl",
  "periodcentered",
  "quotesinglbase",
  "quotedblbase",
  "perthousand",
  "Acircumflex",
  "Ecircumflex",
  "Aacute",
  "Edieresis",
  "Egrave",
  "Iacute",
  "Icircumflex",
  "Idieresis",
  "Igrave",
  "Oacute",
  "Ocircumflex",
  "apple",
  "Ograve",
  "Uacute",
  "Ucircumflex",
  "Ugrave",
  "dotlessi",
  "circumflex",
  "tilde",
  "macron",
  "breve",
  "dotaccent",
  "ring",
  "cedilla",
  "hungarumlaut",
  "ogonek",
  "caron",
  "Lslash",
  "lslash",
  "Scaron",
  "scaron",
  "Zcaron",
  "zcaron",
  "brokenbar",
  "Eth",
  "eth",
  "Yacute",
  "yacute",
  "Thorn",
  "thorn",
  "minus",
  "multiply",
  "onesuperior",
  "twosuperior",
  "threesuperior",
  "onehalf",
  "onequarter",
  "threequarters",
  "franc",
  "Gbreve",
  "gbreve",
  "Idotaccent",
  "Scedilla",
  "scedilla",
  "Cacute",
  "cacute",
  "Ccaron",
  "ccaron",
  "dcroat"
};

const char* font::TTYMetrics::nameID_codes[] = {
  "copyright notice", "font family", "font subfamily",
  "unique subfamily id", "full name", "name table version",
  "PostScript name", "trademark notice", "manufacturer",
  "designer", "description", "vendor URL",
  "designer URL", "license", "license URL",
  "reserved", "preferred family", "preferred subfamily",
  "compatible full name", "sample text"
};

font::TTYMetrics::TableTypes font::TTYMetrics::table_types;

const char* font::TTYMetrics::table_types_data[] = {
  // from apple specs
  "acnt", "accent attachment",
  "avar", "axis variation",
  "bdat", "bitmap data",
  "bhed", "bitmap font header",
  "bloc", "bitmap location",
  "bsln", "baseline",
  "cmap", "character code mapping",
  "cvar", "CVT variation",
  "cvt ", "control value",
  "EBSC", "embedded bitmap scaling control",
  "fdsc", "font descriptor",
  "feat", "layout feature",
  "fmtx", "font metrics",
  "fpgm", "font program",
  "fvar", "font variation",
  "gasp", "grid-fitting and scan-conversion procedure",
  "glyf", "glyph outline",
  "gvar", "glyph variation",
  "hdmx", "horizontal device metrics",
  "head", "font header",
  "hhea", "horizontal header",
  "hmtx", "horizontal metrics",
  "hsty", "horizontal style",
  "just", "justification",
  "kern", "kerning",
  "lcar", "ligature caret",
  "loca", "glyph location",
  "maxp", "maximum profile",
  "mort", "metamorphosis",
  "morx", "extended metamorphosis",
  "name", "name",
  "opbd", "optical bounds",
  "OS/2", "compatibility",
  "post", "glyph name and PostScript compatibility",
  "prep", "control value program",
  "prop", "properties",
  "trak", "tracking",
  "vhea", "vertical header",
  "vmtx", "vertical metrics",
  "Zapf", "glyph reference",

  // from OpenType Specs
  "BASE", "baseline data",
  "GDEF", "glyph definition data",
  "GPOS", "glyph positioning data",
  "GSUB", "glyph substitution data",
  "JSTF", "justification data",
  "DSIG", "digital signature",
  "LTSH", "linear threshold data",
  "PCLT", "PCL 5 data",
  "VDMX", "vertical device metrics",
  "EBDT", "embedded bitmap data",
  "EBLC", "embedded bitmap location data",
  "EBSC", "embedded bitmap scaling data",
  0
};

#ifdef STANDALONE
void show_glyph_info(const font::TTYMetrics &font, gunichar c) {
  unsigned int i = font.get_mapping_table().get_glyph_index(c);
  std::cout /*<< "'" << Glib::ustring(1, c) */<< "' ("
              << c << "), glyph index " << i
              << ", width " << font.getWidth(Glib::ustring(1, c)) << std::endl;
}

int main(int argc, char **argv) {
  try {
    if(argc != 2)
      throw std::runtime_error("Usage: truetype <fontfile>");
    std::ifstream in(argv[1]);
    if(!in)
      throw std::runtime_error("Could not open \""
                               + std::string(argv[0]) + "\"");
    font::TTYMetrics font(in, true);
    font.print_info();

    show_glyph_info(font, ' ');
    show_glyph_info(font, '!');
    show_glyph_info(font, 'A');
    show_glyph_info(font, 'Z');
    show_glyph_info(font, 'a');
    show_glyph_info(font, 'z');
    show_glyph_info(font, 0x00e5);
  } catch(const std::exception &e) {
    std::cerr << e.what() << std::endl;
    exit(1);
  } catch(const Glib::Error &e) {
    std::cerr << e.what() << std::endl;
    exit(1);
  }
}
#endif
