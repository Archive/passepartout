#ifndef PP_FONTMANAGER_HH	// -*- c++ -*-
#define PP_FONTMANAGER_HH
///
// Copyright (C) 2002 - 2004, Fredrik Arnerup & Rasmus Kaj, See COPYING
///
#include <string>
#include <map>
#include <set>
#include <stdexcept>
#include <glibmm/thread.h>
#include "fontmetrics.hh"

namespace font {

struct FontError: public std::runtime_error {
  FontError(const std::string &what) : std::runtime_error(what) {}
};

using std::string;

/// This is a singleton
class FontManager {
public:
  enum generic_font { serif, sans_serif, cursive, fantasy, monospace };
  typedef std::set<std::string> FontPaths;
  typedef std::set<std::string> FontNames;

  static FontManager& instance();

  /** 
   * A font can have more than one name. Warning: no cyclic
   * references, please! */
  void fontAlias(const string &alias, const string &fontname);

  /**
   * May throw FontError */ 
  const font::Metrics& getFont(const string &name);

  /** get real name of font */
  string unalias(const string &fontname, bool recursive = true) const;

  /** A list of available font names */
  FontNames getAvailableFonts() const;
  void reportFonts(std::ostream &out) const;

  string getMetricsFile(const string &fontname) const {
    try { return getFontFiles(fontname).first; }
    catch(FontError&) { return ""; }
  }

  string getFontFile(const string &fontname) const {
    try { return getFontFiles(fontname).second; }
    catch(FontError&) { return ""; }
  }

  /**
   * Some apps (e.g. ghostscript) don't use fontconfig, so they will
   * need a list of paths to look for fonts in. */
  const FontPaths& getFontPaths() const { return fontpaths; }

private:
  static FontManager *_instance;
  static Glib::StaticMutex mutex; // thread safe initialization
  typedef std::map<string, font::Metrics*> MetricsMap;
  MetricsMap metrics_map;
  typedef std::multimap<string, string> FontAliases;
  FontAliases fontaliases;
  
  /// first is metrics file, second is font file
  typedef std::pair<string, string> FilePair;
  
  /// Map each fontname to a FilePair
  typedef std::map<string, FilePair> FontFiles;
  FontFiles fontfiles;

  FontPaths fontpaths;

  FontManager();

  void loadFont(const string &fontname);
  void checkOutTrueType(const string &filename);
  void checkOutAFM(const string &filename);
  void checkOutType1(const string &filename);
  FilePair getFontFiles(const string &fontname) const;
  FontManager(const FontManager &); // undefined
  FontManager& operator= (const FontManager &);	// undefined
};
}
#endif
