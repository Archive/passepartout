///
// Copyright (C) 2002 - 2004, Fredrik Arnerup & Rasmus Kaj, See COPYING
///
#include "fontinfo.hh"
#include "fontmanager.hh"

std::string 
font::FontInfo::getRealName() const {
  return FontManager::instance().unalias(name);
}

font::FontInfo::FontInfo(const std::string& n, float s, float spacing)
  : name(n), metrics(FontManager::instance().getFont(n)),
    size(s), letter_spacing(spacing)
{}

font::FontInfo
font::FontInfo::WidenFont(const FontInfo& orig, const float& widen) {
  FontInfo result(orig);
  result.letter_spacing += widen;
  return result;
}

float
font::FontInfo::getWidth(const Glib::ustring& s) const {
  return metrics.getWidth(s) * size + letter_spacing * s.length(); 
}
