#ifndef PP_FONTMETRICS_HH	// -*- c++ -*-
#define PP_FONTMETRICS_HH
///
// Copyright (C) 2002 - 2004, Fredrik Arnerup & Rasmus Kaj, See COPYING
///
#include <glibmm/ustring.h>

namespace font {
  /// Interface for font metrics.
  class Metrics {
  public:
    virtual ~Metrics() {}
    /// This is the real name, not the postscript name
    virtual std::string getName() const = 0;
    /// The name to be used in postscript code
    virtual std::string getPostscriptName() const = 0;
    /// Width of string when typset in this font
    virtual float getWidth(const Glib::ustring &str) const = 0;
    /// The distance from the baseline to the top of the heighest glyph
    virtual float getAscender()  const = 0;
    /// The distance from the baseline to the bottom of the lowest glyph
    virtual float getDescender() const = 0;
    /// The height of the letter x
    virtual float getExHeight()  const = 0;
    /// The preferred size of small caps
    virtual float getCapHeight() const = 0;
    /// The preferred distance from the baseline to the underline
    virtual float getUnderlinePos() const = 0;
    /// The preferred thickness of the underline
    virtual float getUnderlineThickness() const = 0;
    /// The slant angle of the font
    virtual float getItalicAngle() const = 0;
    /// True if the font has a particular glyph
    virtual bool hasGlyph(const std::string &glyphname) const = 0;
    /// Return PostScript glyph name corresponding to character or
    /// sequence of characters
    virtual std::string getGlyphName(const Glib::ustring &chars) const = 0;
    /// Return bounding box for font
    virtual int getBBox(int idx) const = 0;
  };
}

#endif
