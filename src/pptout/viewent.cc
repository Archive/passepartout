///
// Copyright (C) 2003, 2004, Fredrik Arnerup & Rasmus Kaj, See COPYING
///
#include "viewent.h"
#include <gdkmm.h>

#include "icons.h"

namespace {
  ArtVpathDash dash_style;
  double dash_lengths[] = {3, 3};
  Glib::RefPtr<Gdk::Pixbuf> broken_pixbuf, missing_pixbuf, wait_pixbuf;
}

Viewent::Viewent(View& the_view, Pagent* pagent)
  : view(the_view)
{
  pagent->props_changed_signal.connect
    (sigc::mem_fun(*this, &Viewent::on_properties_changed));
  pagent->geometry_changed_signal.connect
    (sigc::mem_fun(*this, &Viewent::on_geometry_changed));

  // can't create pixbuf until glib (or whatever) is initialized 
  if(!broken_pixbuf) {
    using namespace Gdk;
    broken_pixbuf = Pixbuf::create_from_xpm_data(broken_xpm);
    missing_pixbuf = Pixbuf::create_from_xpm_data(missing_xpm);
    wait_pixbuf = Pixbuf::create_from_xpm_data(wait_xpm);
  }

  dash_style.dash = dash_lengths;
  dash_style.n_dash = 2;

  group.reset(new Gnome::Canvas::Group(view.get_pagent_group()));
  content_group.reset(new Gnome::Canvas::Group(*group));
  rect.reset(new Gnome::Canvas::Rect(*group));
  rect->property_dash() = &dash_style;
  rect->property_width_pixels() = 1;
  rect->property_x1() = 0; rect->property_y1() = 0;

  state_pixbuf.reset(new Gnome::Canvas::Pixbuf(*group));
  state_pixbuf->property_pixbuf() = broken_pixbuf;
  state_pixbuf->property_width_in_pixels() = true;
  state_pixbuf->property_height_in_pixels() = true;
  state_pixbuf->property_anchor() = Gtk::ANCHOR_CENTER;
  set_state(NORMAL);
}

Viewent::~Viewent()
{}

void Viewent::set_selected(bool selected) {
  rect->property_dash() = selected ? 0 : &dash_style;
}

void Viewent::set_state(State state) {
  switch(state) {
  case BROKEN: state_pixbuf->property_pixbuf() = broken_pixbuf; break;
  case MISSING: state_pixbuf->property_pixbuf() = missing_pixbuf; break;
  case WAIT: state_pixbuf->property_pixbuf() = wait_pixbuf; break;
  default: break;
  }
  if(state == NORMAL)
    state_pixbuf->hide();
  else
    state_pixbuf->show();
}

void Viewent::raise_to_top() { group->raise_to_top(); }

void Viewent::on_geometry_changed() {
  Vector size = get_frame()->get_inherent_size();
  
  state_pixbuf->property_x() = size.x / 2;
  state_pixbuf->property_y() = -size.y / 2;
  
  rect->property_x2() = size.x;
  rect->property_y2() = -size.y;
  
  group->affine_absolute(get_frame()->get_matrix().gaTransform());
}

void Viewent::on_properties_changed() {
  rect->property_outline_color_gdk() = view.get_color(get_frame()->get_lock()
						      ? Color::locked
						      : Color::frame);
}
