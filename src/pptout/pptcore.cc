///
// Copyright (C) 2002 - 2004, Fredrik Arnerup & Rasmus Kaj, See COPYING
///
#include "pptcore.h"
#include "groupmeta.h"
#include "imagemeta.h"
#include "rastermeta.h"
#include "textmeta.h"
#include "pagemeta.h"

PptCore core;

PptCore::PptCore() {
  // Note:  The core lives for as long as the program does, so there is no
  // need to be able to delete these objects.
  meta["group"] = new GroupMeta;
  meta["image"] = new ImageMeta;
  meta["raster"] = new RasterMeta;
  meta["text"] = new TextMeta;
  meta["page"] = new PageMeta;
}

MetaBase* PptCore::getMeta(const std::string& type) {
  MetaMap::const_iterator i = meta.find(type);
  if(i != meta.end())
    return i->second;
  else
    return 0;
}

