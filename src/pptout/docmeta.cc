///
// Copyright (C) 2002 - 2004, Fredrik Arnerup & Rasmus Kaj, See COPYING
///
#include "docmeta.h"
#include "sigc++/sigc++.h"

DocMeta::DocMeta(DocRef _document, const Glib::ustring &filename) 
  : data(new DocData()),
    document(_document)
{
  data->reference();
  data->filename = filename;
}

// DocMeta::DocMeta(const DocMeta &orig)
//   : data(orig.data)
// {
//   std::cerr << "copying" << std::endl;
//   document = orig.document;
// }

sigc::signal<void>& DocMeta::changed_signal() {
  return data->changed_signal;
}

void DocMeta::set_filename(const Glib::ustring &filename) {
  data->filename = filename;
  data->changed_signal();
}
