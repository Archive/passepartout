///
// Copyright (C) 2002 - 2004, Fredrik Arnerup & Rasmus Kaj, See COPYING
///
#include "templatepagedialog.h"
#include "document/page.h"
#include "document/document.h"
#include "widget/wmisc.h"
#include <gtkmm/scrolledwindow.h>
#include <gtkmm/stock.h>
#include <gtkmm/label.h>

namespace {
  class ModelColumns: public Gtk::TreeModel::ColumnRecord {
  public:
    ModelColumns() { add(col_name); add(col_style); }
    Gtk::TreeModelColumn<Glib::ustring> col_name;
    Gtk::TreeModelColumn<int> col_style;
  };
  const ModelColumns columns;
  const Glib::ustring no_template_string("blank page");
}

TemplatePageDialog::TemplatePageDialog(Gtk::Window &parent,
                                       DocumentView &document_view,
                                       int page_num):
  DialogWrap("Add Page", parent),
  document_view(document_view), page_num(page_num)
{
  set_resizable(false);
  set_has_separator(false);

  add_button(Gtk::Stock::CANCEL, Gtk::RESPONSE_CANCEL);
  add_button(Gtk::Stock::ADD, Gtk::RESPONSE_OK);

  Gtk::ScrolledWindow *scroller = manage(new Gtk::ScrolledWindow);
  scroller->set_policy(Gtk::POLICY_AUTOMATIC, Gtk::POLICY_AUTOMATIC);
  scroller->set_shadow_type(Gtk::SHADOW_IN);
  scroller->add(page_list);

  Gtk::Label *template_label =
    manage(new Gtk::Label("Select template page:", 0));

  Gtk::Box *vbox = manage(new Gtk::VBox(false, double_space));
  vbox->set_border_width(border_width);
  Gtk::Box *template_label_box = manage(new Gtk::HBox());
  template_label_box->pack_start(*template_label, Gtk::PACK_SHRINK);
  vbox->pack_start(*template_label_box, Gtk::PACK_SHRINK);
  vbox->pack_start(*scroller, Gtk::PACK_EXPAND_WIDGET);

  get_vbox()->pack_start(*vbox);

  
  Glib::RefPtr<Gtk::ListStore> tree_model = Gtk::ListStore::create(columns);
  page_list.set_model(tree_model);
  Gtk::CellRendererText *text_renderer = manage(new Gtk::CellRendererText());
  int col_count = page_list.append_column("Name", *text_renderer);
  Gtk::TreeViewColumn* column = page_list.get_column(col_count - 1);
  if(column) {
    column->add_attribute(text_renderer->property_text(),
			  columns.col_name);
    column->add_attribute(text_renderer->property_style(),
			  columns.col_style);
  }
  page_list.set_headers_visible(false);
  page_list.property_width_request() = 100;
  page_list.property_height_request() = 100;

  DocRef document = document_view.get_document();
  if(document) { 
    typedef std::list<std::string> Strings;
    Strings tmp = document->get_template_pages();
    for(Strings::iterator i = tmp.begin(); i != tmp.end(); i++) {
      Gtk::TreeModel::Row row = *(tree_model->append());
      row[columns.col_name] = *i;
      row[columns.col_style] = Pango::STYLE_NORMAL;
    }
    Gtk::TreeModel::Row row = *(tree_model->append());
    row[columns.col_name] = no_template_string;
    row[columns.col_style] = Pango::STYLE_ITALIC;
  }
  show_all();
}

void TemplatePageDialog::on_response(int response_id) {
  DocRef document = document_view.get_document();
  if(document
     && response_id == Gtk::RESPONSE_OK) { // assume anything else means CANCEL
    std::string page_name = "";
    Glib::RefPtr<Gtk::TreeSelection> selection = page_list.get_selection();
    Gtk::TreeModel::iterator iter = selection->get_selected();
    if(iter) {
      Gtk::TreeModel::Row row = *iter;
      page_name = Glib::ustring(row[columns.col_name]);
    }
    
    if(!page_name.empty() && page_name != no_template_string
       && document->get_template()) {
      Page *t_page = document->get_template()->get_page(page_name);
      if(!t_page) {
        // There should only be existing template pages in the list
        throw std::logic_error("Template page \""
                               + page_name
                               + "\" not found");
      }

      document->new_page(page_num, t_page); //problem here
    } else {
      document->new_page(page_num);
    }
    
    // go to the new page
    document_view.set_current_page_num(page_num);
  }
  hide();
  delete this;
}
