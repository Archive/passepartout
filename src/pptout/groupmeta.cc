///
// Copyright (C) 2002 - 2004, Fredrik Arnerup & Rasmus Kaj, See COPYING
///
#include "groupmeta.h"
#include "document/group.h"
#include "pptcore.h"
#include "groupviewent.h"

Pagent*
GroupMeta::load(const ElementWrap& xml, Group* parent)
{
  return new Group(xml, parent);
}

Viewent::Ref GroupMeta::create_viewent(View& view, Pagent& node) {
  Viewent::Ref result(new GroupViewent
		      (view, &dynamic_cast<Group&>(node)));
  result->reference();
  return result;
}

PropBase* GroupMeta::getProp() { return 0; }
