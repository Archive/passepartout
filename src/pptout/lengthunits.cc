///
// Copyright (C) 2003, 2004, Fredrik Arnerup & Rasmus Kaj, See COPYING
///
#include "lengthunits.h"

LengthUnits length_units;
AngleUnits angle_units;

LengthUnits::LengthUnits()
  : Units<float, Glib::ustring>("pt") // Adobe points
{
  add_unit("mm", 72 / 25.4); // millimeters
  add_unit("cm", 10 * 72 / 25.4); // centimeters
  add_unit("in", 72); // inches
  add_unit("pica", 12); // picas (Adobe)
}

AngleUnits::AngleUnits()  
  : Units<float, Glib::ustring>(Glib::ustring(1, gunichar(0xb0))) // degrees
{
  add_unit("rad", 180 / 3.14159265358979323846); // radians
}
