#ifndef INSPIRATION_H		// -*- c++ -*-
#define INSPIRATION_H
///
// Copyright (C) 2002 - 2004, Fredrik Arnerup & Rasmus Kaj, See COPYING
///
#include "widget/dialogwrap.h"
#include <memory>

class Game;

/**
 * Inspiration for an empty mind.
 * This is a singleton.
 */
class Inspiration: public DialogWrap {
public:
  static Inspiration &instance();

private:
  static Inspiration *_instance;
  Gtk::Label *score;
  std::auto_ptr<Game> game;

  Inspiration();
  // no default constructors:
  Inspiration(const Inspiration&);
  void operator = (const Inspiration&);
  // no destructor
  ~Inspiration();

  void on_response(int response_id);
  bool on_key_press_event(GdkEventKey*);
  void che_alert();
  void score_change(long score_);
  bool on_delete_event(GdkEventAny*) { che_alert(); return true; }
};

#endif
