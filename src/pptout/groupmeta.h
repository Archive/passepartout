#ifndef GROUPMETA_H		// -*- c++ -*-
#define GROUPMETA_H
///
// Copyright (C) 2002 - 2004, Fredrik Arnerup & Rasmus Kaj, See COPYING
///
#include "metabase.h"
#include "util/xmlwrap.h"

class GroupMeta : public MetaBase {
public:
  Pagent* load(const ElementWrap& node, Group* parent);
  
  Viewent::Ref create_viewent(View& view, Pagent& node);
  
  PropBase* getProp();
};

#endif
