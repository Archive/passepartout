#ifndef TEXTMETA_H		// -*- c++ -*-
#define TEXTMETA_H
///
// Copyright (C) 2002 - 2004, Fredrik Arnerup & Rasmus Kaj, See COPYING
///
#include "metabase.h"
#include "util/xmlwrap.h"

class TextMeta : public MetaBase {
public:
  Pagent* load(const ElementWrap& xml, Group* parent);

  Viewent::Ref create_viewent(View& view, Pagent& node);
  
  virtual PropBase* getProp();
};

#endif
