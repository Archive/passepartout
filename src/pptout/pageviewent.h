#ifndef PPT_PAGEVIEWENT_H	// -*- C++ -*-
#define PPT_PAGEVIEWENT_H
///
// Copyright (C) 2003, 2004, Fredrik Arnerup & Rasmus Kaj, See COPYING
///
#include "groupviewent.h"
#include "document/page.h"
#include <memory> // auto_ptr

/**
 * {Viewent} for a {Page}.  Responsible for drawing guides etc, {GroupViewent}
 * draws the actual content.
 */
class PageViewent : public GroupViewent {
  typedef Page* FrameRef;
public:
  PageViewent(View& view, const FrameRef page);
  ~PageViewent();
  
protected:
  const Pagent* get_frame() const { return page; }
  
private:
  void on_size_changed();
  void on_selection_changed(DocRef doc);

  FrameRef page;
  std::auto_ptr<Gnome::Canvas::Rect> paper;
  Gnome::Canvas::Group guide_group;
};

#endif
