///
// Copyright (C) 2002 - 2004, Fredrik Arnerup & Rasmus Kaj, See COPYING
///
#include "rastermeta.h"
#include "document/rasterframe.h"
#include "rasterviewent.h"
#include "widget/spinner.h"
#include "widget/filesel.h"
#include "widget/subpanel.h"
#include "widget/wmisc.h"
#include <gtkmm/togglebutton.h>
#include <gtkmm/separator.h>
#include <gtkmm/sizegroup.h>
#include <util/warning.h>
#include <glib/gi18n.h>

Pagent* RasterMeta::load(const ElementWrap& xml, Group* parent)
{
  return new RasterFrame(xml, parent);
}

Viewent::Ref RasterMeta::create_viewent(View& view, Pagent& node) {
  Viewent::Ref result(new RasterViewent
		      (view, &dynamic_cast<RasterFrame&>(node)));
  result->reference();
  return result;
}

namespace {
  class PropRaster : public GenericProp<RasterFrame> {
  public:
    PropRaster();
    
    virtual void reconnectSignals();
    void update();
  
    enum ChangeId { FILENAME, HRES, VRES, ASPECT };
    void on_change(ChangeId what);
  private:
    float last_x, last_y;
    bool use_aspect;
    
    FileEntry e_fname;
    Spinner e_ppix, e_ppiy;
    Gtk::ToggleButton b_ppic;
    Gtk::HSeparator hsep;
    sigc::connection geometry_changed_connection, props_changed_connection;
  };
};

PropBase* RasterMeta::getProp() {
  return new PropRaster;
}

PropRaster::PropRaster()
  :GenericProp<RasterFrame>(_("_Raster")), use_aspect(true),
   e_fname(_("Associated File")),
   b_ppic("}", Gtk::JUSTIFY_RIGHT) // FIXME: use the locked chain symbol that gimp and co use
{
  Gtk::Label *label;
  Gtk::Box *line = manage(new Gtk::HBox(false, double_space));
  label = manage(new Gtk::Label(_("_Filename:"), 0.0, 0.5, true));
  label->set_mnemonic_widget(e_fname);
  line->pack_start(*label, Gtk::PACK_SHRINK);
  line->pack_start(e_fname);
  e_fname.entry.get_entry().signal_activate().connect
    (bind(sigc::mem_fun(*this, &PropRaster::on_change), FILENAME));
  e_fname.entry.get_entry().signal_focus_out_event().connect
    (sigc::hide(bind_return(bind(sigc::mem_fun(*this, 
                                      &PropRaster::on_change), 
                                 FILENAME), 
                            true)));
  pack_start(*line, Gtk::PACK_SHRINK);
  
  Glib::RefPtr<Gtk::SizeGroup>  sizegroup = 
    Gtk::SizeGroup::create(Gtk::SIZE_GROUP_HORIZONTAL);
  
  SubPanel* rezpanel = manage(new SubPanel(_("Raster resolution")));
  Gtk::Box *hbox = manage(new Gtk::HBox(false, single_space));
  Gtk::Box *vbox = manage(new Gtk::VBox(false, single_space));
  line = manage(new Gtk::HBox(false, double_space));
  line->pack_start(*(label = manage(new Gtk::Label(_("_Horizontal:"),
						   0.0, 0.5, true))),
		   Gtk::PACK_SHRINK);
  sizegroup->add_widget(*label);
  label->set_mnemonic_widget(e_ppix);
  line->pack_start(e_ppix, Gtk::PACK_SHRINK);
  line->pack_start(*(manage(new Gtk::Label(_("ppi")))),
		   Gtk::PACK_SHRINK);
  vbox->pack_start(*line, Gtk::PACK_SHRINK);
  
  line = manage(new Gtk::HBox(false, double_space));
  line->pack_start(*(label = manage(new Gtk::Label(_("_Vertical:"),
						   0.0, 0.5, true))),
		   Gtk::PACK_SHRINK);
  sizegroup->add_widget(*label);
  label->set_mnemonic_widget(e_ppiy);
  line->pack_start(e_ppiy, Gtk::PACK_SHRINK);
  line->pack_start(*(manage(new Gtk::Label(_("ppi")))),
		   Gtk::PACK_SHRINK);
  vbox->pack_start(*line, Gtk::PACK_SHRINK);


  hbox->pack_start(*vbox, Gtk::PACK_SHRINK);
  hbox->pack_start(b_ppic, Gtk::PACK_SHRINK);
  b_ppic.set_active(use_aspect);
  rezpanel->pack_start(*hbox, Gtk::PACK_SHRINK);

  pack_start(*rezpanel, Gtk::PACK_SHRINK);
  
  e_ppix.signal_value_changed().connect
    (bind(sigc::mem_fun(*this, &PropRaster::on_change), HRES));
  e_ppiy.signal_value_changed().connect
    (bind(sigc::mem_fun(*this, &PropRaster::on_change), VRES));
  b_ppic.signal_toggled().connect
    (bind(sigc::mem_fun(*this, &PropRaster::on_change), ASPECT));
  set_sensitive(false);
  update();
}

void PropRaster::reconnectSignals() {
  geometry_changed_connection.disconnect();
  props_changed_connection.disconnect();
  if(object) {
    // Switch raster resolution
    geometry_changed_connection = object->geometry_changed_signal.connect
      (sigc::mem_fun(*this, &PropRaster::update));
    // Switched file
    props_changed_connection = object->props_changed_signal.connect
      (sigc::mem_fun(*this, &PropRaster::update));
  }
}

void PropRaster::update() {
  if(object) {
    e_fname.entry.set_text(object->get_association(), true /* save history */);
    
    const Vector ppi = object->get_ppi();
    e_ppix.set(ppi.x);
    e_ppiy.set(ppi.y);
    
  } else {
    e_fname.entry.set_text("");
  }
}

void PropRaster::on_change(ChangeId what) {
  switch(what) {
  case FILENAME:
    if(object)
      object->set_association(e_fname.entry.get_text());
    break;
    
  case HRES: {
    const float x(e_ppix.get());
//     if(use_aspect && x != last_x) {
//       last_x = x; last_y = x; /// \todo * object->getAspectRatio()
//       e_ppiy.set(last_y);
//     }
    if(object) {
      Vector ppi = object->get_ppi();
      ppi.x = x;
      object->set_ppi(ppi);
    }
  } break;
    
  case VRES: {
    const float y(e_ppiy.get());
//     if(use_aspect && y != last_y) {
//       last_y = y; last_x = y; /// \todo / object->getAspectRatio()
//       e_ppix.set(last_x);
//     }
    if(object) {
      Vector ppi = object->get_ppi();
      ppi.y = y;
      object->set_ppi(ppi);
    }
  } break;
  
  case ASPECT:
    use_aspect = b_ppic.get_active();
    if(use_aspect) {
      e_ppiy.set(e_ppix.get());	/// \todo * object->getAspectRatio()
    }
    break;
  }
}
