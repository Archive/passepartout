#ifndef DOCVIEW_H		// -*- c++ -*-
#define DOCVIEW_H
///
// Copyright (C) 2002 - 2004, Fredrik Arnerup & Rasmus Kaj, See COPYING
///
#include "view.h"
#include "viewent.h"
#include "document/group.h"
#include "docmeta.h"
#include <gdkmm.h>
#include <libgnomecanvasmm.h>
#include <memory> // auto_ptr

class Page;
class TextStream;

namespace snap {
  enum Mode {GRID, GUIDE, NONE};
}

class DocumentView: public Gnome::Canvas::Canvas, public View {
public:
  sigc::signal<void> current_page_num_changed_signal;
  sigc::signal<void> document_changed_signal;
  sigc::signal<void> document_set_signal;

  Gdk::Color white, black, gray, red;

  DocumentView(DocMeta, float zoom_factor = 1.0);
  ~DocumentView();

  void set_document(DocMeta d);
  DocRef get_document() const { return document.get_document(); }
  DocMeta get_document_meta() const { return document; }
  void set_current_page_num(int);
  int get_current_page_num() const { return current_page_num; }
  Page* get_page();
  float get_zoom_factor() const { return zoom_factor; }
  void set_zoom_factor(float);

  void delete_page();

  void new_image_frame(std::string, float res = -1);

  Gnome::Canvas::Group& get_pagent_group() { return pagent_group; }
  Gnome::Canvas::Group& get_guide_group() { return guide_group; }

  const Gdk::Color& get_color(Color::Id color) const;

  void set_snap(snap::Mode mode) { snap_mode = mode; }
  snap::Mode get_snap() const { return snap_mode; }
		
  double get_scrres() const { return resolution * zoom_factor; }
  float pt2scr(float pt) const;
  float scr2pt(float scr) const;
  // Important! the scalar versions of pt2scr and scr2pt only perform
  // length conversions while the vector versions also convert between
  // right- and left-handed coordinate systems
  Gdk::Point pt2scr(const Vector& pt) const;
  Vector scr2pt(const Gdk::Point& scr) const;

  bool on_key_press_event(GdkEventKey*); // called from Window

private:
  // Undefined ctors, avoid defaults
  DocumentView(const DocumentView&);
  DocumentView();
  void operator = (const DocumentView&);

  sigc::signal<void, float> zoom_change_signal;
  Viewent::Ref pageview;
  Gnome::Canvas::Group no_pages_group, pagent_group, guide_group, handle_group;
  std::auto_ptr<Gnome::Canvas::Rect> no_pages_frame;
  std::auto_ptr<Gnome::Canvas::Text> no_pages_text;
  Gnome::Canvas::Rect *handles[8]; /// managed by the canvas

  /// a connection to the selected pagents geometry_changed_signal
  sigc::connection selected_geometry_connection;
  /// a connection to the selected pagents props_changed_signal
  sigc::connection selected_props_connection;
  /// keep track of which pagent the connection is to
  Pagent *last_selected;

  snap::Mode snap_mode;
  int margin_size;
  float resolution; //ppi
  float zoom_factor;
  Vector old_size;
  Matrix old_matrix;
  IVector offset;
  int reshape_box, current_page_num;
  bool moving, reshaping;
  DocMeta document;
  Page *old_page; //last page visited
  Gtk::Menu popup_menu;

  /// If there is a reshape box at pixel coordinates (x, y) return the
  /// index of the box, otherwise return -1.
  int hover_reshape(int x, int y);

  bool on_button_press_event(GdkEventButton*);
  bool on_button_release_event(GdkEventButton*);
  bool on_motion_notify_event(GdkEventMotion*);
  void on_drag_data_received(const Glib::RefPtr<Gdk::DragContext>& context,
			     int x, int y,
                             const Gtk::SelectionData &selection_data,
			     guint info, guint time);

  void update_handles();

  void begin_move(int x, int y);
  void end_move(bool revert);
  void begin_reshape(int x, int y, int box);
  void end_reshape(bool revert);
  void move_reshape_box(int x, int y);

  void act_on_document_change(DocRef document_);
  void act_on_selection_change(DocRef document_);
  void update_document_size();
  Gdk::CursorType get_cursor(int x, int y);
  void update_cursor(int x, int y);
  bool in_move_area(int x, int y);
  Pagent* in_select_area(int x, int y);

  sigc::connection connect_zoom_change(sigc::slot<void, float> slot) {
    return zoom_change_signal.connect(slot);
  }
};

#endif
