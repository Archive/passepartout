///
// Copyright (C) 2003, 2004, Fredrik Arnerup & Rasmus Kaj, See COPYING
///
#include "groupviewent.h"
#include "pptcore.h"
#include <gdkmm.h>
#include <functional>

class SubView : public View {
public:
  SubView(View& p, const Matrix& m, Gnome::Canvas::Group &group)
    : parent(p), matrix(m), pagent_group(group)
  {}
  
  Gnome::Canvas::Group& get_pagent_group() { return pagent_group; }
  Gnome::Canvas::Group& get_guide_group() { return parent.get_guide_group(); }
  
  gdouble get_scrres() const {
    const Matrix inv = matrix.inv();
    return dist(inv.transform(Vector(parent.get_scrres(), 0)),
		inv.transform(Vector(0,0)));
  }
  // transforming lengths only make sense if the axes are 
  // scaled by the same amount
  float pt2scr(float pt) const {
    return parent.pt2scr(dist(matrix.transform(Vector(pt, 0)),
			      matrix.transform(Vector(0,0))));
  }
  Gdk::Point pt2scr(const Vector& pt) const {
    return parent.pt2scr(matrix.transform(pt));
  }
  Vector scr2pt(const Gdk::Point& scr) const {
    return matrix.inv().transform(parent.scr2pt(scr));
  }
  float scr2pt(float scr) const { // untested code!
    return dist(scr2pt(Gdk::Point(int(scr + 0.5), 0)), 
		scr2pt(Gdk::Point(0, 0)));
  }
  const Gdk::Color &get_color(Color::Id color) const { 
    return parent.get_color(color); 
  }
  sigc::connection connect_zoom_change(sigc::slot<void, float> slot) {
    return parent.connect_zoom_change(slot);
  }
private:
  View& parent;
  const Matrix& matrix;
  Gnome::Canvas::Group &pagent_group;
};

GroupViewent::GroupViewent(View& view, const FrameRef groupframe)
  : Viewent(view, groupframe),
    subview(new SubView(view, groupframe->get_matrix(), *content_group)),
    frame(groupframe),
    groupcon(frame->group_changed_signal.connect
	     (sigc::mem_fun(*this, &GroupViewent::group_changed)))
{
  // Create the initial content, just as if it was added after this object.
  group_changed();
  
  on_geometry_changed();
  on_properties_changed();
}

GroupViewent::~GroupViewent() {
  /// \todo delete subview; but make sure its not used first
}

void GroupViewent::update_selection(const Document::Selection& selected) {
  for(ChildVec::iterator i = childvec.begin(); i != childvec.end(); i++) {
    const Pagent* pagent = (*i)->get_frame();
    (*i)->set_selected(std::find(selected.begin(), selected.end(), 
				 pagent) != selected.end());
  }
}

void GroupViewent::group_changed() {
  ChildVec newchilds;
  for(Group::ChildVec::const_reverse_iterator i = frame->prbegin();
      i != frame->prend();
      ++i) {
    ChildVec::const_iterator old;
    for(old = childvec.begin(); old != childvec.end(); ++old) {
      if((*old)->samePagent(*i))
	break;
    }
    if(old != childvec.end())
      newchilds.push_back(*old);
    else
      newchilds.push_back(core.getMeta((*i)->getTypeName())->
			  create_viewent(*subview, **i));
    newchilds.back()->raise_to_top();
  }
  swap(childvec, newchilds);
}
