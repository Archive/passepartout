#ifndef VIEW_H			// -*- c++ -*-
#define VIEW_H
///
// Copyright (C) 2002 - 2004, Fredrik Arnerup & Rasmus Kaj, See COPYING
///
#include "util/vector.h"
#include <gdkmm/drawable.h>
#include <libgnomecanvasmm.h>

namespace Color {
  typedef enum { bg, frame, locked, guide, empty } Id;
}

class View {
public:
  virtual ~View() {}

  virtual Gnome::Canvas::Group& get_pagent_group() = 0;
  virtual Gnome::Canvas::Group& get_guide_group() = 0;

  virtual double get_scrres() const = 0;
  virtual float pt2scr(float pt) const = 0;
  virtual Gdk::Point pt2scr(const Vector& pt) const = 0;
  virtual Vector scr2pt(const Gdk::Point& scr) const = 0;
  virtual float scr2pt(const float scr) const = 0;

  virtual const Gdk::Color& get_color(Color::Id color) const = 0;

  /** Register a callback for the signal raised on rescaling the view */
  virtual sigc::connection connect_zoom_change(sigc::slot<void, float>) = 0;
};

#endif
