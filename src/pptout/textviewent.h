#ifndef PPT_TEXTVIEWENT_H	// -*- C++ -*-
#define PPT_TEXTVIEWENT_H
///
// Copyright (C) 2003, 2004, Fredrik Arnerup & Rasmus Kaj, See COPYING
///
#include "postscriptviewent.h"

class TextFrame;

class TextViewent : public PostscriptViewent {
public:
  /**
   * Create a new TextViewent.
   * \param cachedframe
   * \param view
   * \param scale_factor Create bitmaps this many times bigger than
   *  necessary and scale them down, kind of a poor mans
   *  antialiasing. */
  TextViewent(View& view, const FrameRef cachedframe,
	      int scale_factor = 1);

private:
  TextFrame *textframe;
  std::auto_ptr<Gnome::Canvas::Item> trunc_ind;
  void on_geometry_changed();
  void on_properties_changed();
  void on_truncated_state_changed(bool);
};

#endif
