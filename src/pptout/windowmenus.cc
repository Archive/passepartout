///
// Copyright (C) 2002 - 2004, Fredrik Arnerup & Rasmus Kaj, See COPYING
///
#include "window.h"
#include <fstream>
#include <gtkmm/stock.h>
#include "widget/filesel.h"
#include "widget/programs.h"
#include "widget/usererror.h"
#include "config.h"
#include "printdialog.h"
#include "propertiesdialog.h"
#include "streamdialog.h"
#include "util/warning.h"
#include "util/tempfile.h"
#include "docpropsdialog.h"
#include "docview.h"
#include "document/page.h"
#include "templatepagedialog.h"
#include "clipboard.h"

#include <glib/gi18n.h>

namespace {
  template<class P> P nonull(P p) {
    if(!p)
      throw std::runtime_error("A pointer was null in " __FILE__);
    return p;
  }
  
  void duplicate_view(const FrameWindow *orig) { new FrameWindow(*orig); }
  
  void new_document(DocumentView *view) {
    DocPropsDialog::instance().create(view);
  }
  
  void print_to_viewer(const DocumentView *view) {
    const DocRef document = nonull(view->get_document());
    std::string tmp_file = Tempfile::find_new_name();
    std::ofstream out(tmp_file.c_str());
    if(!out)
      throw std::runtime_error("Could not open file for printing: "
                               + tmp_file);
    // print all pages:
    document->print
      (out, document->get_first_page_num(),
       document->get_first_page_num() + document->get_num_of_pages() - 1,
       false /* not eps */, true /* include fonts */);
    Programs::open_postscript_file(tmp_file, true /* delete file when done */);
  }
  
  void show_doc_props_dialog(DocumentView *view) {
    DocPropsDialog::instance().modify(view);
  }
  
  void copi(DocumentView *view) {
    Clipboard::copy(nonull(view->get_document()));
  }
  
  void cut(DocumentView *view) {
    Clipboard::cut(nonull(view->get_document()));
  }
  
  void paste(DocumentView *view) {
    Clipboard::paste(nonull(view->get_document()),
                     nonull(view->get_page()));
  }
  
  void delete_selected(DocumentView *view) {
    nonull(view->get_document())->delete_selected();
  }
  
  void group_selected(DocumentView *view) {
    nonull(view->get_page())->group_selected();
  }
  
  void ungroup_selected(DocumentView *view) {
    nonull(view->get_page())->ungroup_selected();
  }
  
  void select_all_frames(DocumentView *view, bool select_not_unselect) {
    nonull(view->get_page())->select_all(select_not_unselect);
  }
  
  void rearrange_selected(DocumentView *view, RearrangeTarget target) {
    nonull(view->get_page())->rearrange_selected(target);
  }

  void
  page_changed (DocumentView                  * view,
                Glib::RefPtr<Gtk::ActionGroup>  group)
  {
    DocRef document = view->get_document ();

    bool sensitive = document && view->get_current_page_num () > document->get_first_page_num ();
    group->get_action ("PageFirst")->set_sensitive (sensitive);
    group->get_action ("PagePrevious")->set_sensitive (sensitive);

    sensitive = document && view->get_current_page_num () < (document->get_first_page_num () + document->get_num_of_pages () - 1);
    group->get_action ("PageLast")->set_sensitive (sensitive);
    group->get_action ("PageNext")->set_sensitive (sensitive);
  }

  void
  page_first (DocumentView* view)
  {
    view->set_current_page_num (view->get_document ()->get_first_page_num ());
  }

  void
  page_last (DocumentView* view)
  {
    view->set_current_page_num (view->get_document ()->get_first_page_num () + view->get_document ()->get_num_of_pages () - 1);
  }

  void
  page_next (DocumentView* view)
  {
    view->set_current_page_num (view->get_current_page_num () + 1);
  }

  void
  page_previous (DocumentView* view)
  {
    view->set_current_page_num (view->get_current_page_num () - 1);
  }

  void insert_page(Gtk::Window *parent, DocumentView *view, bool after) {
    DocRef document = nonull(view->get_document());
    int page_num =
      document->get_num_of_pages()
      ? (view->get_current_page_num() + (after ? 1 : 0))
      : document->get_first_page_num();
    if(document->get_template_pages().empty()) {
      document->new_page(page_num);
      view->set_current_page_num(page_num);
    } else {
      new TemplatePageDialog(*parent, *view, page_num);
    }
  }
  
  void toggle_grid(Glib::RefPtr<Gtk::ToggleAction> action,
                   DocumentView *view) {
    view->set_snap(action->get_active() ? snap::GRID : snap::NONE);
  }
  
  void toggle_toolbar(Glib::RefPtr<Gtk::UIManager> uimanager) {
    Gtk::Widget *toolbar = nonull(uimanager->get_widget("/ToolBar"));
    if(toolbar->is_visible())
      toolbar->hide();
    else
      toolbar->show();
  }
  
  void toggle_fullscreen(Glib::RefPtr<Gtk::ToggleAction> action,
                         Gtk::Window *window) {
    if(action->get_active())
      window->fullscreen();
    else
      window->unfullscreen();
  }
  
  void zoom(Zoomer *zoomer, Gtk::BuiltinStockID id) {
    float f = zoomer->get_factor();
    using namespace Gtk;
    if(id == Stock::ZOOM_IN) f = 2.0 * f;
    else if(id == Stock::ZOOM_OUT) f = 0.5 * f;
    else if(id == Stock::ZOOM_100) f = 1.0;
    // rely on Zoomer to limit the values
    zoomer->set_factor(f);
  }
}

void
FrameWindow::create_menus (Gtk::RecentFilter& f)
{
  using namespace Gtk;
  using namespace sigc;
  
  /// *** file menu ***
  
  main_group->add(Action::create("FileMenu", _("_File")));
  
  main_group->add(Action::create("FileNew", Stock::NEW, _("_New ..."),
                                 _("Create a new document")),
                  bind(sigc::ptr_fun(&new_document), &document_view));
  
  doc_group->add(Action::create("FileNewView", _("New Vi_ew"),
                                _("Create a new window "
                                "for the current document")),
                 bind(sigc::ptr_fun(&duplicate_view), this));

  // FIXME: turn all mem_fun()'s into sigc::ptr_fun()'s
  main_group->add(Action::create("FileOpen", Stock::OPEN, _("_Open ..."),
                                 _("Open a document")),
                  mem_fun(*this, &FrameWindow::open_file));

  Glib::RefPtr<RecentAction> action = RecentAction::create ("FileOpenRecent",
					 _("Open _recent file"),
					 _("Open one of these recently opened files"));
  action->set_show_numbers (true);
  action->set_local_only ();
  action->set_sort_type (RECENT_SORT_MRU); /* most recently used on top */
  action->add_filter (f);
  main_group->add (action);
  action->signal_item_activated ().connect (bind (mem_fun (*this, &FrameWindow::on_recent_file),
						  action));

  doc_group->add(Action::create("FileSave", Stock::SAVE, _("_Save ..."),
                                _("Save document")),
                 mem_fun(*this, &FrameWindow::save));
  
  doc_group->add(Action::create("FileSaveAs", Stock::SAVE_AS),
                 AccelKey("<shift><control>S"),
                 mem_fun(*this, &FrameWindow::save_as));
  
  page_group->add(Action::create("FilePrint", Stock::PRINT, _("_Print ..."),
                                 _("Print document")),
                  AccelKey("<control>P"),
                  mem_fun(*this, &FrameWindow::print));
  
  page_group->add(Action::create("FilePrintPreview", Stock::PRINT_PREVIEW,
                                 _("Print Pre_view ..."),
                                 _("Print document to external viewer")),
                  AccelKey("<shift><control>P"),
                  bind(sigc::ptr_fun(&print_to_viewer), &document_view));
  
  doc_group->add(Action::create("FileDocProps", Stock::PROPERTIES,
				_("P_roperties ..."),
                                _("Document properties")),
		 AccelKey("<Mod1>Return"),
                 bind(sigc::ptr_fun(&show_doc_props_dialog), &document_view));

  main_group->add(Action::create("FileClose", Stock::CLOSE),
                  mem_fun(*this, &FrameWindow::close));
  
  main_group->add(Action::create("FileQuit", Stock::QUIT),
                  ptr_fun(&FrameWindow::quit));
  
  
  /// *** edit menu ***
  
  main_group->add(Action::create("EditMenu", _("_Edit")));
  
  selection_group->add(Action::create("EditCopy", Stock::COPY),
                       bind(sigc::ptr_fun(&copi), &document_view));
  
  selection_group->add(Action::create("EditCut", Stock::CUT),
                       bind(sigc::ptr_fun(&cut), &document_view));
  
  page_group->add(Action::create("EditPaste", Stock::PASTE),
                  bind(sigc::ptr_fun(&paste), &document_view));
  
  selection_group->add(Action::create("EditDelete", Stock::DELETE),
                       AccelKey("Delete"),
                       bind(sigc::ptr_fun(&delete_selected), &document_view));
  
  page_group->add(Action::create("EditSelectAll", Stock::SELECT_ALL),
                  AccelKey("<control>A"),
                  bind(sigc::ptr_fun(&select_all_frames), &document_view,
                       true /* select */));
  
  page_group->add(Action::create("EditUnselectAll", _("Dese_lect All")),
                  AccelKey("<shift><control>A"),
                  bind(sigc::ptr_fun(&select_all_frames), &document_view,
                       false /* deselect */));
  
  // *** arrange submenu ***
  
  main_group->add(Action::create("ArrangeMenu", _("A_rrange")));
  
  selection_group->add(Action::create("ArrangeGroup", _("_Group")),
                       AccelKey("<control>G"),
                       bind(sigc::ptr_fun(group_selected), &document_view));
  
  selection_group->add(Action::create("ArrangeUngroup", _("_Ungroup")),
                       AccelKey("<control>U"),
                       bind(sigc::ptr_fun(ungroup_selected), &document_view));
  
  selection_group->add(Action::create("ArrangeTop", Stock::GOTO_TOP,
                                      _("Move to _top"), _("Move to top")),
                       AccelKey("<control>Home"),
                       bind(sigc::ptr_fun(&rearrange_selected),
                            &document_view, TOP));
  
  
  selection_group->add(Action::create("ArrangeUp", Stock::GO_UP,
                                      _("Move _up"), _("Move up")),
                       AccelKey("<control>Page_Up"),
                       bind(sigc::ptr_fun(&rearrange_selected),
                            &document_view, UP));
  
  selection_group->add(Action::create("ArrangeDown", Stock::GO_DOWN,
                                      _("Move _down"), _("Move down")),
                       AccelKey("<control>Page_Down"),
                       bind(sigc::ptr_fun(&rearrange_selected),
                            &document_view, DOWN));
  
  selection_group->add(Action::create("ArrangeBottom", Stock::GOTO_BOTTOM,
                                      _("Move to _bottom"), _("Move to bottom")),
                       AccelKey("<control>End"),
                       bind(sigc::ptr_fun(&rearrange_selected),
                            &document_view, DOWN));
  
  page_group->add(Action::create("EditInsertText", StockID("newframe"),
                                 _("Insert Te_xt Frame ..."),
                                 _("Insert text frame")),
                  mem_fun(*this, &FrameWindow::insert_text));
  
  page_group->add(Action::create("EditInsertImage", StockID("moose"),
                                 _("Insert _Image ..."), _("Insert image")),
                  mem_fun(*this, &FrameWindow::insert_image));
  
  main_group->add(Action::create("EditPrefs", Stock::PREFERENCES),
                  mem_fun (*this, &FrameWindow::show_preferences));
  
  
  // *** page menu ***
  
  main_group->add(Action::create("PageMenu", _("_Page")));

  page_group->add (Action::create ("PageFirst", Stock::GOTO_FIRST, _("_First Page")),
                   AccelKey ("<Alt>Home"),
                   bind<DocumentView*> (sigc::ptr_fun (&page_first), &this->document_view));
  page_group->add (Action::create ("PagePrevious", Stock::GO_BACK, _("_Previous Page")),
                   AccelKey ("<Alt>Page_Up"),
                   bind<DocumentView*> (sigc::ptr_fun (&page_previous), &this->document_view));
  page_group->add (Action::create ("PageNext", Stock::GO_FORWARD, _("_Next Page")),
                   AccelKey ("<Alt>Page_Down"),
                   bind<DocumentView*> (sigc::ptr_fun (&page_next), &this->document_view));
  page_group->add (Action::create ("PageLast", Stock::GOTO_LAST, _("_Last Page")),
                   AccelKey ("<Alt>End"),
                   bind<DocumentView*> (sigc::ptr_fun (&page_last), &this->document_view));
  sigc::slot< void, DocumentView*, Glib::RefPtr< Gtk::ActionGroup > > one (sigc::ptr_fun (&page_changed));
  sigc::slot< void, DocumentView* > two (sigc::bind (one, page_group));
  sigc::slot< void > three (sigc::bind (two, &this->document_view));
  document_view.current_page_num_changed_signal.connect (three);
  document_view.document_changed_signal.connect (three);
  page_changed (&this->document_view, page_group);

  page_group->add (Action::create ("PageDelete", Stock::DELETE, _("_Delete")),
                   AccelKey ("<Ctrl>Delete"),
                   mem_fun (document_view, &DocumentView::delete_page));

  /* FIXME: as we display a dialog anyway, we can ask the user for the page's location there
   * => drop one of these two
   */
  doc_group->add(Action::create("PageInsertBefore", _("Insert _Before ...")),
                 bind(sigc::ptr_fun(&insert_page),
                      this, &document_view, false /* not after */));
  
  // we only need one of these when we don't have any pages
  page_group->add(Action::create("PageInsertAfter", _("Insert _After ...")),
                 bind(sigc::ptr_fun(&insert_page),
                      this, &document_view, true /* after */));
  
  // *** view menu ***
  
  main_group->add(Action::create("ViewMenu", _("_View")));
  
  main_group->add(ToggleAction::create("ViewToolbar", _("_Toolbar"), "",
                                       true /* active */),
                  bind(sigc::ptr_fun(toggle_toolbar), uimanager));
  {
    Glib::RefPtr<ToggleAction> action =
      ToggleAction::create("ViewFullScreen", Stock::FULLSCREEN, _("_Full screen"), "",
                           false /* not active */);
    main_group->add(action, AccelKey("F11"),
                    bind(sigc::ptr_fun(toggle_fullscreen), action, this));
  }
  
  main_group->add(Action::create("ZoomIn", Stock::ZOOM_IN), 
                  AccelKey("<ctrl>plus"),
                  bind(sigc::ptr_fun(&zoom), &zoom_factor, Stock::ZOOM_IN));
  main_group->add(Action::create("ZoomOut", Stock::ZOOM_OUT),
                  AccelKey("<ctrl>minus"),
                  bind(sigc::ptr_fun(&zoom), &zoom_factor, Stock::ZOOM_OUT));
  main_group->add(Action::create("Zoom100", Stock::ZOOM_100),
                  AccelKey("<ctrl>0"),
                  bind(sigc::ptr_fun(&zoom), &zoom_factor, Stock::ZOOM_100));
  
  doc_group->add(Action::create("ViewProps", Stock::PROPERTIES,
                                _("Show Properties window")),
                 mem_fun(*this, &FrameWindow::show_properties));
  
  doc_group->add(Action::create("ViewStreams", StockID("streams"),
                                _("_Streams"), _("Show Streams window")),
                 mem_fun(*this, &FrameWindow::show_streams));
  
  {
    Glib::RefPtr<Gtk::ToggleAction> action =
      ToggleAction::create("ViewSnapGrid", StockID("grid"),
                           _("Snap to _grid"), _("Toggle snap to grid"));
    doc_group->add(action,
                   bind(sigc::ptr_fun(&toggle_grid), action, &document_view));
  }
  
  // *** help menu ***
  main_group->add(Action::create("HelpMenu", _("_Help")));
  
  main_group->add(Action::create("HelpHelp", Stock::HELP, _("_Contents")),
                  AccelKey("F1"),
                  ptr_fun(&Programs::open_docs));
  
  main_group->add(Action::create("HelpAbout", Stock::ABOUT, _("_About")),
                  mem_fun(*this, &FrameWindow::show_about));
  
  main_group->add(Action::create("HelpInspiration", _("_Inspiration")),
                  mem_fun(*this, &FrameWindow::show_inspiration));
  
  // Layout the actions in a menubar and toolbar:
  try {
    const Glib::ustring ui_info = 
      "<ui>"
      "  <menubar name='MenuBar'>"
      "    <menu action='FileMenu'>"
      "      <menuitem action='FileNew'/>"
      "      <menuitem action='FileOpen'/>"
      "      <menuitem action='FileOpenRecent'/>"
      "      <separator/>"
      "      <menuitem action='FileSave'/>"
      "      <menuitem action='FileSaveAs'/>"
      "      <separator/>"
      "      <menuitem action='FilePrintPreview'/>"
      "      <menuitem action='FilePrint'/>"
      "      <separator/>"
      "      <menuitem action='FileDocProps'/>"
      "      <separator/>"
      "      <menuitem action='FileClose'/>"
      "      <menuitem action='FileQuit'/>"
      "    </menu>"
      
      "    <menu action='EditMenu'>"
      "      <menuitem action='EditCut'/>"
      "      <menuitem action='EditCopy'/>"
      "      <menuitem action='EditPaste'/>"
      "      <menuitem action='EditDelete'/>"
      "      <separator/>"
      "      <menuitem action='EditSelectAll'/>"
      "      <menuitem action='EditUnselectAll'/>"
      "      <separator/>"
      "      <menu action='ArrangeMenu'>"
      "        <menuitem action='ArrangeGroup'/>"
      "        <menuitem action='ArrangeUngroup'/>"
      "        <separator/>"
      "        <menuitem action='ArrangeTop'/>"
      "        <menuitem action='ArrangeUp'/>"
      "        <menuitem action='ArrangeDown'/>"
      "        <menuitem action='ArrangeBottom'/>"
      "      </menu>"
      "      <separator/>"
      "      <menuitem action='EditInsertText'/>"
      "      <menuitem action='EditInsertImage'/>"
      "      <separator/>"
      "      <menuitem action='EditPrefs'/>"
      "    </menu>"
      
      "    <menu action='ViewMenu'>"
      "      <menuitem action='ViewToolbar'/>"
      "      <menuitem action='ViewFullScreen'/>"
      "      <separator/>"
      "      <menuitem action='ZoomIn'/>"
      "      <menuitem action='ZoomOut'/>"
      "      <menuitem action='Zoom100'/>"
      // FIXME: best fit
      "      <separator/>"
      "      <menuitem action='ViewProps'/>"
      "      <menuitem action='ViewStreams'/>"
      "      <separator/>"
      "      <menuitem action='ViewSnapGrid'/>"
      "      <separator/>"
      "      <menuitem action='FileNewView'/>"
      "    </menu>"

      "    <menu action='PageMenu'>"
      "      <menuitem action='PageFirst'/>"
      "      <menuitem action='PagePrevious'/>"
      "      <menuitem action='PageNext'/>"
      "      <menuitem action='PageLast'/>"
      "      <separator/>"
      "      <menuitem action='PageDelete'/>"
      "      <separator/>"
      "      <menuitem action='PageInsertBefore'/>"
      "      <menuitem action='PageInsertAfter'/>"
      "    </menu>"

      "    <menu action='HelpMenu'>"
      "      <menuitem action='HelpHelp'/>"
      "      <separator/>"
      "      <menuitem action='HelpInspiration'/>"
      "      <separator/>"
      "      <menuitem action='HelpAbout'/>"
      "    </menu>"
      "  </menubar>"
      
      "  <toolbar name='ToolBar'>"
      "    <toolitem action='FileNew'/>"
      "    <separator/>"
      "    <toolitem action='FilePrintPreview'/>"
      "    <separator/>"
      "    <toolitem action='EditInsertText'/>"
      "    <toolitem action='EditInsertImage'/>"
      "    <separator/>"
      "    <toolitem action='ViewProps'/>"
      "    <toolitem action='ViewStreams'/>"
      "    <separator/>"
      "    <toolitem action='ViewSnapGrid'/>"
      "    <separator/>"
      "    <toolitem action='ZoomIn'/>"
      "    <toolitem action='ZoomOut'/>"
      "    <toolitem action='Zoom100'/>"
      "    <separator/>"
      "    <toolitem action='ArrangeTop'/>"
      "    <toolitem action='ArrangeUp'/>"
      "    <toolitem action='ArrangeDown'/>"
      "    <toolitem action='ArrangeBottom'/>"
      "  </toolbar>"
      "</ui>";
    
    uimanager->add_ui_from_string(ui_info);
  }
  catch(const Glib::Error& e) {
    warning << "failed to build menus: " <<  e.what() << std::endl;
  }
}
