#ifndef PAGESEL_H		// -*- c++ -*-
#define PAGESEL_H
///
// Copyright (C) 2002 - 2004, Fredrik Arnerup & Rasmus Kaj, See COPYING
///

#include "docview.h"
#include <gtkmm/box.h>
#include <gtkmm/button.h>
#include <gtkmm/comboboxtext.h>

class Pagesel: public Gtk::HBox {
public:
  Pagesel(DocumentView &docview);
  Gtk::Widget& get_menu() { return pages; }

private:
  sigc::connection changed_connection;
  DocumentView &document_view;
  Gtk::ComboBoxText pages;
  Gtk::Button left, right;
  void update();
};

#endif
