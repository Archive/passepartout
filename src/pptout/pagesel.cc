///
// Copyright (C) 2002 - 2004, Fredrik Arnerup & Rasmus Kaj, See COPYING
///
#include "pagesel.h"
#include "document/page.h"
#include "document/document.h"
#include <gtkmm/arrow.h>

namespace {
  void page_selected_action(DocumentView *view,
                            const Gtk::ComboBoxText *pages) {
    DocRef document = view->get_document();
    Page *page;
    if(document && (page = document->get_page(pages->get_active_text())))
      view->set_current_page_num(page->get_page_num());
  }

  void next(DocumentView *view) {
    view->set_current_page_num(view->get_current_page_num() + 1);
  }

  void prev(DocumentView *view) {
    view->set_current_page_num(view->get_current_page_num() - 1);
  }
}

void Pagesel::update() {
  // we don't want to act on signal_changed while we're updating
  struct ConnectionBlocker {
    sigc::connection &connection;
    ConnectionBlocker(sigc::connection &connection) : connection(connection) {
      connection.block();
    }
    ~ConnectionBlocker() { connection.unblock(); }
  } blocker(changed_connection);

  DocRef document = document_view.get_document();
  Glib::RefPtr<Gtk::ListStore>::cast_dynamic(pages.get_model())->clear();
  bool have_pages = document && document->get_num_of_pages();
  set_sensitive(have_pages);
  
  if(!have_pages){
    pages.append_text("-");
    pages.set_active(0);
    return;
  }
  
  int i = document->get_first_page_num();
  while(i < (document->get_first_page_num()
	     + int(document->get_num_of_pages()))) {
    Glib::ustring tmp;

    // The doublesided feature is useless until we can display 
    // two pages at once, so it is disabled
    if(false // document->is_doublesided() 
       && i == document->get_first_page_num() 
       && i % 2 != 0)
      tmp = "-";
    else {
      if(document->get_page(i))
	tmp = document->get_page(i)->get_name();
      i++;
    }
    if(false) // document->is_doublesided())
      {
        if(i < (document->get_first_page_num()
	        + int(document->get_num_of_pages()))) {
	  if(document->get_page(i))
	    tmp += ", " + document->get_page(i)->get_name();
	  i++;
        } else
	  tmp += ", -";
      }
    pages.append_text(tmp);
  }
  int index = document_view.get_current_page_num()
    - document->get_first_page_num();
  pages.set_active(index);
  left.set_sensitive(index);
  right.set_sensitive(index < (pages.get_model()->children().size()) - 1);
}

Pagesel::Pagesel(DocumentView &docview) :
  document_view(docview)
{
  using namespace sigc;

  document_view.current_page_num_changed_signal.connect
    (mem_fun(*this, &Pagesel::update));
  document_view.document_changed_signal.connect
    (mem_fun(*this, &Pagesel::update));

  left.add(*manage(new Gtk::Arrow(Gtk::ARROW_LEFT, Gtk::SHADOW_NONE)));
  right.add(*manage(new Gtk::Arrow(Gtk::ARROW_RIGHT, Gtk::SHADOW_NONE)));
  
  update();

  pack_start(left, Gtk::PACK_SHRINK);
  pack_start(pages, Gtk::PACK_SHRINK);
  pack_start(right, Gtk::PACK_SHRINK);

  left.signal_clicked().connect(bind(sigc::ptr_fun(&prev), &document_view));
  right.signal_clicked().connect(bind(sigc::ptr_fun(&next), &document_view));
  changed_connection = pages.signal_changed().connect
    (bind(sigc::ptr_fun(&page_selected_action), &document_view, &pages));

  show_all();
}
