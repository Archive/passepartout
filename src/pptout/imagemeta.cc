///
// Copyright (C) 2002 - 2004, Fredrik Arnerup & Rasmus Kaj, See COPYING
///
#include "imagemeta.h"
#include "document/imageframe.h"
#include "propbase.h"
#include "postscriptviewent.h"
#include "widget/filesel.h"
#include "widget/wmisc.h"
#include <gtkmm/label.h>
#include <glib/gi18n.h>

Pagent* ImageMeta::load(const ElementWrap& xml, Group* parent)
{
  return new ImageFrame(xml, parent);
}

Viewent::Ref ImageMeta::create_viewent(View& view, Pagent& node) {
  PostscriptViewent* rawresult
    = new PostscriptViewent(view, &dynamic_cast<ImageFrame&>(node));
  Viewent::Ref result(rawresult);
  result->reference();

  // Images should be re-rastered when geometry changes, but not text frames,
  // so connect this signal here rather than in the result.
  node.geometry_changed_signal.connect
    (sigc::mem_fun(*rawresult, &PostscriptViewent::regenerate));

  return result;
}

// Properties for a EPS image, as represented by ImageFrame.
// Currently just displays the filename.  It should be able to change the file
// name and possibly display some of the header info from the EPS (title,
// created by, etc).
class PropImage : public GenericProp<ImageFrame> {
public:
  PropImage()
    : GenericProp<ImageFrame>(_("_EPS")),
     e_fname(_("Associated File"))
    {
      Gtk::Box *line = manage(new Gtk::HBox(false, double_space));
      Gtk::Label *label = manage(new Gtk::Label(_("_Filename:"), 0.0, 0.5, true));
      label->set_mnemonic_widget(e_fname);
      line->pack_start(*label, Gtk::PACK_SHRINK);
      line->pack_start(e_fname);
      e_fname.entry.get_entry().signal_activate().connect
	(sigc::mem_fun(*this, &PropImage::on_change));
      e_fname.entry.get_entry().signal_focus_out_event().connect
	(sigc::hide(bind_return(sigc::mem_fun(*this, 
                                              &PropImage::on_change),
                                true)));
      pack_start(*line, Gtk::PACK_SHRINK);
      set_sensitive(false);
      update();
    }
  
  void update() {
    if(object) {
      e_fname.entry.set_text(object->get_association(),
                             true /* save history */);
    } else {
      e_fname.entry.set_text("");
    }
  }
  void on_change() {
    if(object)
      object->set_association(e_fname.entry.get_text());
  }
private:
  FileEntry e_fname;
};

PropBase* ImageMeta::getProp() {
  return new PropImage;
}
