#ifndef RASTERFRAME_H		// -*- c++ -*-
#define RASTERFRAME_H
///
// Copyright (C) 2002 - 2004, Fredrik Arnerup & Rasmus Kaj, See COPYING
///
#include "util/filewatcher.h"
#include "basicframe.h"
#include <gdkmm/pixbuf.h>

class RasterFrame: public BasicFrame {
public:
  RasterFrame(const ElementWrap& xml, Group *parent);
  RasterFrame(Group *parent, const std::string& filename, float res = -1);
  // negative res means use default
  virtual ~RasterFrame();
  
  virtual std::string getTypeName() const;
  
  virtual xmlpp::Element *save(xmlpp::Element& parent_node,
			       const FileContext &context) const;
  virtual void print(std::ostream &out, bool grayscale = false) const;
  virtual void print_pdf(PDF::Content::Ptr pdf) const;
  

  // *** undoable actions ***
  void set_association(const std::string &s);
  /** Scale the image by setting the nuber of Pixels Per Inch (on paper, the
   * screen resolution doesn't matter).
   */
  void set_ppi(const Vector& ppi);
  
  // *** probe state ***
  Boundary get_box() const;
  const std::string &get_association() const {return association;}
  Vector get_ppi() const;
  Vector get_default_ppi();
  /** Get the pixel size of the image (the size of the image in 72ppi). */
  Vector get_inherent_size() const;
  
  // *** non-undoable actions ***
  void set_default_ppi()
  {set_ppi(get_default_ppi());}
  
  Glib::RefPtr<Gdk::Pixbuf> get_filepixbuf() { return filepixbuf; }
  
private:
  std::string association;
  FileWatcher filewatcher;
  Gdk::Point unit_size;		// Number of pixels in image
  Glib::RefPtr<Gdk::Pixbuf> filepixbuf;
  
  void on_file_modified();
};

#endif
