#ifndef IMAGEFRAME_H		// -*- c++ -*-
#define IMAGEFRAME_H
///
// Copyright (C) 2002 - 2004, Fredrik Arnerup & Rasmus Kaj, See COPYING
///
#include "util/filewatcher.h"
#include "cachedframe.h"

class ImageFrame: public CachedFrame {
public:
  // try to determine if a file is a ps
  static bool is_postscript(std::string filename);

  ImageFrame(Group *parent, const std::string& assoc);
  ImageFrame(const ElementWrap& xml, Group *parent);
  virtual ~ImageFrame();

  virtual std::string getTypeName() const;
  
  virtual xmlpp::Element *save(xmlpp::Element& parent_node,
			       const FileContext &context) const ;
  virtual void print(std::ostream &out, bool grayscale = false) const;
  virtual void print_pdf(PDF::Content::Ptr pdf) const;

  void generate_picture(std::ostream& psstream) const;
  
  // *** undoable actions ***
  void set_association(const std::string &s);


  // *** probe state ***
  Boundary get_box() const;
  const std::string &get_association() const {return association;}
  Vector get_inherent_size() const { return inherentsize; }
  
protected:
  std::string association;
  FileWatcher filewatcher;

  void on_file_modified();
  void read_size();
private:
  Vector lowerleft, inherentsize;
};

#endif
