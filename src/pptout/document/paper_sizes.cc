///
// Copyright (C) 2002 - 2004, Fredrik Arnerup & Rasmus Kaj, See COPYING
///
#include "paper_sizes.h"

Papers papers;

Papers::Papers() {
  //  LengthUnits &l = length_units;

  // ISO golden section paper sizes 
  // - used in almost the entire civilised world
  sizes["A0"] = Size(2383.9, 3370.4); // 841 x 1189 mm
  sizes["A1"] = Size(1683.8, 2383.9); // 594 x 841 mm
  sizes["A2"] = Size(1190.6, 1683.8); // 420 x 594 mm
  sizes["A3"] = Size(841.9, 1190.6); // 297 x 420 mm
  sizes["A4"] = Size(595.3, 841.9); // 210 x 297 mm
  sizes["A5"] = Size(419.5, 595.3); // 148 x 210 mm
  sizes["A6"] = Size(297.6, 419.5); // 105 x 148 mm
//   sizes["A0"] = Size(l.to_base(841, "mm"), l.to_base(1189, "mm"));
//   sizes["A1"] = Size(l.to_base(594, "mm"), l.to_base(841, "mm"));
//   sizes["A2"] = Size(l.to_base(420, "mm"), l.to_base(594, "mm"));
//   sizes["A3"] = Size(l.to_base(297, "mm"), l.to_base(420, "mm"));
//   sizes["A4"] = Size(l.to_base(210, "mm"), l.to_base(297, "mm"));
//   sizes["A5"] = Size(l.to_base(148, "mm"), l.to_base(210, "mm"));
//   sizes["A6"] = Size(l.to_base(105, "mm"), l.to_base(148, "mm"));

  // paper sizes used in the U.S. and Canada
  // "Tabloid" and "Ledger" are two names for the same paper
  sizes["Letter"] = Size(612.3, 790.9); // 216 x 279 mm
  sizes["Legal"] = Size(612.3, 1009.1); // 216 x 356 mm
  sizes["Executive"] = Size(538.6, 720.0); // 190 x 254 mm
  sizes["Tabloid/Ledger"] = Size(790.9, 1224.6); // 279 x 432 mm
//   sizes["Letter"] = Size(l.to_base(216, "mm"), l.to_base(279, "mm"));
//   sizes["Legal"] = Size(l.to_base(216, "mm"), l.to_base(356, "mm"));
//   sizes["Executive"] = Size(l.to_base(190, "mm"), l.to_base(254, "mm"));
//   sizes["Tabloid/Ledger"] = Size(l.to_base(279, "mm"), l.to_base(432, "mm"));
}
