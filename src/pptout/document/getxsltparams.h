#ifndef GETXSLTPARAMS_H	      	// -*- c++ -*-
#define GETXSLTPARAMS_H
///
// Copyright (C) 2003, 2004, Fredrik Arnerup & Rasmus Kaj, See COPYING
///
#include <vector>
#include <string>

/**
 * Get a list of the parameters the xslt stylesheet {source} accepts.
 * \param source  a file name for an xslt stylesheet.
 * \result a vector of parameter names.
 */
std::vector<std::string> getXsltParams(const std::string& source);

#endif
