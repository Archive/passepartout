#ifndef GROUP_H			// -*- c++ -*-
#define GROUP_H
///
// Copyright (C) 2002 - 2004, Fredrik Arnerup & Rasmus Kaj, See COPYING
///
#include "pagent.h"
#include "util/xmlwrap.h"
#include <list>

enum RearrangeTarget { UP, DOWN, TOP, BOTTOM };

class Group : public Pagent {
public:
  typedef std::list<Pagent*> ChildVec;
  
  Group(Group *parent, const std::string& name);
  Group(Group *parent, const std::string& name, const Matrix& xform);
  Group(const ElementWrap& xml, Group *parent);
  virtual ~Group();
  
  virtual std::string getTypeName() const;
  
  xmlpp::Element *save(xmlpp::Element& parent_node,
		       const FileContext &context) const;
  void print(std::ostream& out, bool grayscale = false) const;
  void print_pdf(PDF::Content::Ptr pdf) const;
  
  Boundary get_box() const;
  Vector get_inherent_size() const;
  void set_flow_around(bool);
  bool get_flow_around() const;
  Boundary get_obstacle_boundary() const;
  BoundaryVect obstacle_list() const;
  
  void add(Pagent* obj);
  Pagent* ungroup(Pagent* obj);
  void rearrange_selected(RearrangeTarget target);
  void group_selected();
  void ungroup_selected();
  int count() const {return childs.size();}
  /// Returns true if pagent is a child of this group.
  bool has_child(const Pagent *pagent) const;

//protected:
  void save_childs(xmlpp::Element& node, const FileContext &context) const;
  ChildVec::const_iterator pbegin() const { return childs.begin(); }
  ChildVec::const_iterator pend() const { return childs.end(); }
  ChildVec::const_reverse_iterator prbegin() const { return childs.rbegin(); }
  ChildVec::const_reverse_iterator prend() const { return childs.rend(); }

  // A change in a child is a change in the group:
  virtual void child_props_changed();
  virtual void child_geometry_changed();
  
  /** Raised on direct changes to the group (i.e. new / ungrouped member) */
  sigc::signal<void> group_changed_signal; 
private:
  Boundary get_untransformed_box() const;
  
  ChildVec childs;
};

#endif
