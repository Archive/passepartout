///
// Copyright (C) 2003, 2004, Fredrik Arnerup & Rasmus Kaj, See COPYING
///
#include <libxml++/libxml++.h>
#include <fstream>
#include <algorithm>

class XsltParamsFinder : public xmlpp::SaxParser {
public:
  XsltParamsFinder() : done(false) {}
  const std::vector<std::string>& result() const { return found; }

protected:
  void on_start_element(const Glib::ustring& n,
			const xmlpp::SaxParser::AttributeList &p);

private:
  std::vector<std::string> found;
  std::string param_elem, template_elem;
  bool done;
};

void
XsltParamsFinder::on_start_element (const Glib::ustring& n,
                                    const xmlpp::SaxParser::AttributeList &p)
{
  if(done) return;
  
  if(param_elem.empty()) {
    // This is the first element in the stylesheet, get the prefix from this
    // element!
    std::string::size_type p = n.find(':');
    if(p == std::string::npos) {
	param_elem = "param";
	template_elem = "template";
    } else {
      param_elem = n.substr(0, p) + ":param";
      template_elem = n.substr(0, p) + ":template";
    }
  } else if(n == param_elem) {
    using namespace xmlpp;
    const SaxParser::AttributeList::const_iterator i = 
      std::find_if(p.begin(), p.end(), SaxParser::AttributeHasName("name"));
    if(i != p.end()) {
      found.push_back(i->value);
    }
  } else if(n == template_elem) {
    /// \todo actually abort the parsing, rather than just converting the
    /// callback to a noop.
    done = true;
  }
}

std::vector<std::string> getXsltParams(const std::string& source) {
  XsltParamsFinder finder;
  std::ifstream in(source.c_str());
  finder.parse_stream(in);
  return finder.result();
}

#ifdef STANDALONE
#include <iostream>

int
main (int   argc,
      char**argv)
{
  int file_argument = 1;
  bool verbose = false;

  for (int i = 1; i < argc; i++)
    {
      if (!strcmp ("-v", argv[i]) ||
          !strcmp ("--verbose", argv[i]))
        {
          verbose = true;

          if (i == file_argument)
            {
              file_argument++;
            }
        }
    }

  if (argc <= 1)
    {
      std::cerr << "Usage:" << std::endl
                << "\t" << argv[0] << " [-v|--verbose] <xslt-file>" << std::endl;
      return 1;
    }

  std::vector<std::string> params = getXsltParams (argv[file_argument]);

  if (params.size () <= 0)
    {
      std::cerr << "File \"" << argv[file_argument] << "\" didn't contain parameters" << std::endl;
      return 2;
    }

  if (verbose)
    {
      std::cout << "File \"" << argv[file_argument] << "\" contains "
                << params.size () << " parameter(s)." << std::endl;

      for (std::vector<std::string>::const_iterator i = params.begin();
           i != params.end();
           ++i)
        {
          std::cout << "Found parameter: " << *i << std::endl;
        }
    }
  return 0;
}
#endif
