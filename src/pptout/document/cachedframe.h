#ifndef CACHEDFRAME_H		// -*- c++ -*-
#define CACHEDFRAME_H
///
// Copyright (C) 2002 - 2004, Fredrik Arnerup & Rasmus Kaj, See COPYING
///
#include "basicframe.h"
#include <sigc++/slot.h>

/**
 * A frame that is represented in postscript.  This may be a referenced EPS
 * file, or a text frame (that is made into postscript code by TextStream and
 * xml2ps).  This interface is mainly used by PostscriptViewent.
 */
class CachedFrame: public BasicFrame {
public:
  CachedFrame(Group *parent, const std::string &name);
  CachedFrame(const ElementWrap& xml, Group *parent);
  
  virtual void generate_picture(std::ostream& psstream) const = 0;
};

#endif
