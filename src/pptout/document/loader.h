#ifndef LOADER_H			// -*- c++ -*-
#define LOADER_H
///
// Copyright (C) 2002 - 2004, Fredrik Arnerup & Rasmus Kaj, See COPYING
///
#include "group.h"

/**
 * Create the appropriate pagent from a \<frame\> node.
 * Throws Error::Read() on failure.
 */
Pagent *load(const ElementWrap& node, Group *parent);

#endif
