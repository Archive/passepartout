#ifndef TEXTFRAME_H		// -*- c++ -*-
#define TEXTFRAME_H
///
// Copyright (C) 2002 - 2004, Fredrik Arnerup & Rasmus Kaj, See COPYING
///
#include "cachedframe.h"
#include "textstream.h"

class TextFrame : public CachedFrame, public Pagent::Resizable {
public:
  TextFrame(Group *parent, TextStream *stream,
	     float w, float h);
  TextFrame(const ElementWrap& xml, Group *parent);
  virtual ~TextFrame();

  virtual std::string getTypeName() const;
  
  virtual xmlpp::Element *save(xmlpp::Element& parent_node,
			       const FileContext &context) const;
  virtual void print(std::ostream &out, bool grayscale = false) const;
  virtual void print_pdf(PDF::Content::Ptr pdf) const;


  // *** undoable actions ***
  void set_stream(TextStream *text_stream, 
		  bool remove_from_old = true);
  // the destructor of TextStream sets remove_from_old = false
  // in order not to get a segmentation violation
  void set_num_columns(unsigned int columns);
  void set_gutter(const float& gutter);
  
  void set_size(float w, float h);


  // *** probe state ***
  Boundary get_box() const;
  TextStream *get_stream() const {return text_stream;}
  Vector get_inherent_size() const { return Vector(width, height); }
  unsigned int get_num_columns() const { return num_columns; }
  float get_gutter() const { return gutter_width; }
  
  /** 
   * \return true if this {TextFrame} is the last frame for a truncated
   * {TextStream}. */
  bool is_truncated() const { return truncated; }
  
  /** Emitted when the frame is or stops being the last frame of a
   * truncated stream. */
  sigc::signal<void, bool> truncated_state_changed_signal;

  // the frame will not try to read the ps between calls to
  // begin- and end_write_ps
  void begin_write_ps(); 
  void end_write_ps(bool _ps_exists, bool truncated,
		    const std::string &error="");

  void generate_picture(std::ostream& psstream) const;
  
private:
  bool ps_exists, parsed_file_lock, truncated;
  std::string error;
  TextStream *text_stream;
  unsigned int num_columns;
  float gutter_width, width, height;
};

#endif
