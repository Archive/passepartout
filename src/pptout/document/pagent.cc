///
// Copyright (C) 2002 - 2004, Fredrik Arnerup & Rasmus Kaj, See COPYING
///
#include "pagent.h"
#include "group.h"
#include "util/stringutil.h"
#include <sigc++/sigc++.h>

Error::InvalidPageNum::InvalidPageNum(int num) 
  : logic_error("Invalid page number: " + tostr(num))
{}

/**
 * Create a new Pagent as a child of a specified parent.
 */
Pagent::Pagent(Group *_parent, const std::string& _name)
  : parent(_parent),
  locked(false), flow_around(false), name(_name),
  obstacle_margin(0)
{
  // a geometry change implies a properties change
  geometry_changed_signal.connect(props_changed_signal.make_slot());
  // a geometry change does NOT imply a redraw

  connect_to_parent();
}


Pagent::~Pagent() {}

void Pagent::set_translation(const Vector& v) {
  Matrix m = get_matrix(); m.set_tr(v);
  set_matrix(m);
}

void Pagent::set_shearing(float shear) {
  Matrix m = get_matrix(); m.set_sh(shear);
  set_matrix(m);
}

void Pagent::set_rotation(float angle) {
  Matrix m = get_matrix(); m.set_rot(angle);
  set_matrix(m);
}

void Pagent::set_scaling(float xfactor, float yfactor) {
  Matrix m = get_matrix(); m.set_scale(xfactor, yfactor);
  set_matrix(m);
}

void Pagent::set_lock(bool _locked) {
  if(_locked != locked) {
    locked = _locked;
    props_changed_signal();
  }
}

void Pagent::set_flow_around(bool _flow_around) {
  if(flow_around != _flow_around) {
    flow_around = _flow_around;
    props_changed_signal();
  }
}

void Pagent::set_obstacle_margin(float margin) {
  if(margin != obstacle_margin) {
    obstacle_margin = margin;
    props_changed_signal();
  }
}

void Pagent::set_name(const std::string &_name) {
  if(_name != name) {
    name = _name;
    props_changed_signal();
  }
}

int Pagent::get_page_num() const {
  if(parent)
    return parent->get_page_num();
  else
    throw Error::NoParent();
}

void Pagent::set_parent(Group *_parent) {
  if(parent) {
    draw_connection.disconnect();
    geometry_connection.disconnect();
    props_connection.disconnect();
  }
  parent = _parent;
  connect_to_parent();
}

void Pagent::set_matrix(Matrix m) {
  if(matrix == m)
    return;
  /// \todo This is an assert, it should be defined as an assert.
  if(m.sc_x() == 0 || m.sc_y() == 0 || m.det() == 0)
    throw std::runtime_error("Bad matrix in set_matrix: " + tostr(m));
  matrix = m;
  geometry_changed_signal();
}

Group& Pagent::get_parent() {
  if(!parent) 
    throw Error::NoParent(); 
  else
    return *parent;
}

const Group& Pagent::get_parent() const {
  if(!parent) 
    throw Error::NoParent(); 
  else
    return *parent;
}

void Pagent::connect_to_parent() {
  // Anything that happens to a pagent, happens to its parent.
  // It is simpler to put this in pagent than in group, because
  // a group would need to maintain a list of connections.
  // Note: we will get multible implicit signals.
  if(parent) {
    geometry_connection = geometry_changed_signal.connect
      (sigc::mem_fun(*parent, &Group::child_geometry_changed));
    
    props_connection = props_changed_signal.connect
      (sigc::mem_fun(*parent, &Group::child_props_changed));
  }
}

Pagent::Resizable::~Resizable() {}
