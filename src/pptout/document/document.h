#ifndef DOCUMENT_H		// -*- c++ -*-
#define DOCUMENT_H
///
// Copyright (C) 2002 - 2004, Fredrik Arnerup & Rasmus Kaj, See COPYING
///
#include <list>
#include "paper_sizes.h"
#include <sigc++/object.h>
#include <sigc++/signal.h>
#include <glibmm/refptr.h>
#include <string>
#include <stdexcept>
#include "util/refcount.h"
#include "util/xmlwrap.h"
#include "filecontext.h"
#include "ps/pdf.h"

class Pagent;
class TextStream;
class Page;

namespace Error {
  struct TextStreamName : public std::logic_error {
    TextStreamName(const std::string& msg) : logic_error(msg) {}
  };
}

class Document;
typedef Glib::RefPtr<Document> DocRef;

/**
 * This is it! The document class!
 * It is reference counted and has no public constructors, 
 * so you will have to use the create methods.
 */
class Document: public RefCounted {
public:
  typedef std::list<Pagent*> Selection;
  typedef std::list<TextStream*> StreamVec;

  static sigc::signal<void, DocRef> changed_signal;
  sigc::signal<void> size_changed_signal;
  static sigc::signal<void, DocRef> selection_changed_signal;
  static sigc::signal<void, DocRef> streams_changed_signal;

  static DocRef null() { return DocRef(); }

  static DocRef create();

  /** throws Error::Open */
  static DocRef create(const std::string &filename, 
		       bool is_template_);

  /** throws Error::Open */
  static DocRef create(const std::string &template_file_);

  void save(const std::string &filename);

  /** throws Error::Print */
  void print(std::ostream& out, int first_page, 
	     int last_page, 
	     bool eps = false, bool include_fonts = false, 
	     bool grayscale = false) const;

  void print_pdf(PDF::Document::Ptr pdf, int first_page, int last_page);

  float get_width() const {
    return papers.sizes[get_paper_name()].get_width(orientation); 
  }
 
 float get_height() const {
   return papers.sizes[get_paper_name()].get_height(orientation);
 }

  void set_template(const std::string &template_file_);

  const std::string &get_template_file() { return template_file; }

  DocRef get_template() {return the_template;}

  std::list<std::string> get_template_pages();

  void set_paper_name(const std::string &_paper_name);

  const std::string &get_paper_name() const { return paper_name; }

  void set_orientation(Papers::Orientation _orientation);

  Papers::Orientation get_orientation() const { return orientation; }

  void set_first_page_num(int num);

  int get_first_page_num() const { return first_page_num; }

  void set_doublesided(bool ds);

  bool is_doublesided() const { return doublesided; }

  unsigned int get_num_of_pages() const;

  /** A page does not know its own number. */
  int get_page_num_of_page(const Page *page) const;

  /** throws Error::InvalidPageNum */
  Page *get_page(int page_num);

  /** Returns NULL if page with name page_name does not exist. */
  Page *get_page(const std::string &page_name);

  /** throws Error::InvalidPageNum */
  void delete_page(int page_num); 

  /**
   * Inserts page at requested page number, possibly a copy of original.
   * If existing pages are e.g. 4, 5, 6 then valid numbers are 4, 5, 6, 7.
   * If no pages exist, then first_page_num is set to page_num.
   * throws Error::InvalidPageNum 
   */
  Page *new_page(int page_num, Page *original = 0);

  int count_selected() const;

  const Selection& selected() const;

  /**
   * @param select true to select all, false to select none
   */
  void select_all(bool select);

  /**
   * @param select true to select all, false to select none
   */
  void select_all_on_page(Page *page, bool select);

  void select(Pagent* obj, bool deselect_old = true);

  void deselect(Pagent* obj);

  void delete_selected();

  /** Returns sorted list. */
  StreamVec get_text_streams();

  std::string make_up_new_name();

  /**
   * Add a stream, while checking that it has a unique name (otherwise
   * Error::TextStreamName. If successful, a streams_changed_signal is
   * raised.
   */
  void add_text_stream(TextStream* new_stream);

  /* throws Error::TextStreamName */
  void rename_text_stream(const std::string &old_name, 
			  const std::string &new_name);

  TextStream* get_text_stream(const std::string &name);

  void remove_text_stream(const std::string &name);

  /// \todo these also break encapsulation
  /** Find the document containing a specific Pagent. */
  static Document &containing(Pagent& obj);

  /** Find the document containing a specific Pagent. */
  static const Document &containing(const Pagent& obj);
  
  /** Return a reference to this object. */
  DocRef self();

private:
  typedef std::list<Page*> PageVec;
  int first_page_num;
  PageVec pages;
  int stream_num;
  StreamVec text_streams;
  bool doublesided, is_template;
  std::string paper_name, template_file;
  Papers::Orientation orientation;
  DocRef the_template;
  Selection selection;

  Document();

  /** throws Error::Open */
  Document(const std::string &filename, 
	   bool is_template_);

  /** throws Error::Open */
  Document(const std::string &template_file_);

  // avoid defaults:
  Document(const Document&);
  void operator = (const Document&);

  ~Document();

  /** throws Error::Open */
  void open(const std::string &filename);

  void xml_open(const ElementWrap& root); // throw Error::Open
  xmlpp::Document *xml_save(const FileContext &context); 

  // these methods don't emit signals:
  void _add_text_stream(TextStream* new_stream);
};

#endif
