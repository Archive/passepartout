#ifndef FILECONTEXT_H		// -*- c++ -*-
#define FILECONTEXT_H
///
// Copyright (C) 2002 - 2004, Fredrik Arnerup & Rasmus Kaj, See COPYING
///
#include <string>
#include "util/filesys.h"

using namespace std;

/** Represents a path that files can be "relative" to. */
class FileContext {
public:
  FileContext() : context_path("") {}
  FileContext(const string &path) : context_path(path) {}

  /** convert to path relative to context path: */
  std::string to(const string &filename) const {
    return relative_path(path(context_path), filename);
  }

  /** convert from path relative to context path: */
  std::string from(const string &filename) const {
    if(filename.empty() || filename[0] == '/')
      return filename;
    else
      return path(context_path) + filename;
  }
  
private:
  string context_path;
};

#endif
