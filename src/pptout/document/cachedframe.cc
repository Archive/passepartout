///
// Copyright (C) 2002 - 2004, Fredrik Arnerup & Rasmus Kaj, See COPYING
///
#include "cachedframe.h"

CachedFrame::CachedFrame(Group *parent, const std::string &name)
  : BasicFrame(parent, name)
{}

CachedFrame::CachedFrame(const ElementWrap& xml, Group* parent)
  : BasicFrame(xml, parent)
{}
