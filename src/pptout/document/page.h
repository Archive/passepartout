#ifndef PAGE_H			// -*- c++ -*-
#define PAGE_H
///
// Copyright (C) 2002 - 2004, Fredrik Arnerup & Rasmus Kaj, See COPYING
///
#include "group.h"
#include "ps/pdf.h"
#include <list> 

struct Guide { // a guide-line
  
  enum Orientation { HORIZONTAL, VERTICAL };
  
  Orientation orientation;
  float position;
  Guide(Orientation orientation_, float position_)
    : orientation(orientation_), position( position_)
    {}
  Guide(const ElementWrap& node);
};

class Document;

class Page: public Group {
public:
  Page(Document&);
  Page(const ElementWrap& xml, Document&);
  ~Page();
  
  xmlpp::Element *save(xmlpp::Element& parent_node,
		       const FileContext &context);
  void print(std::ostream &out, bool grayscale = false);
  void print_pdf(PDF::Document::Ptr pdf) const;
  
  std::string get_name(); // return page number if no name
  float get_width() const;
  float get_height() const;
  float get_xpos() const;
  float get_ypos() const;

  int get_page_num() const; // ask document what the page number is
  
  void addObject(Pagent* obj);

  void select_all(bool select);	// false for select none

  /** Find the Page containing a specific Pagent. */
  static Page& containing(Pagent& obj);
  /** Find the Page containing a specific Pagent. */
  static const Page& containing(const Pagent& obj);
  
private:
  friend class Document;	/// \todo not like this, anyway ...
  Document &document;
  void child_geometry_changed(Pagent *pagent) {}

  friend class PageViewent;
  typedef std::list<Guide> Guides;
  Guides guides;

  // Undefined ctors, avoid defaults
  Page(const Page&);
  Page();
  void operator = (const Page&);
};

#endif
