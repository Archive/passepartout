#ifndef FILEERRORS_H		// -*- c++ -*-
#define FILEERRORS_H
///
// Copyright (C) 2002 - 2004, Fredrik Arnerup & Rasmus Kaj, See COPYING
///
#include <string>
#include <stdexcept>

namespace Error
{
  struct File : public std::runtime_error {
    typedef std::string string;
    string message;
    File(const string _message)
      : runtime_error("File error: " + _message), message(_message) {}
    ~File() throw() {}
  };
  struct Write: public File {
    Write(const string _message="Write error"): File(_message) {}
  };
  struct Print: public Write {
    Print(const string _message="Print error"): Write(_message) {}
  };
  struct Read: public File {
    Read(const string _message="Read error"): File(_message) {}
  };
}

#endif
