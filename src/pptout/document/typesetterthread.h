#ifndef TYPESETTERTHREAD_H		// -*- c++ -*-
#define TYPESETTERTHREAD_H
///
// Copyright (C) 2002 - 2004, Fredrik Arnerup & Rasmus Kaj, See COPYING
///

#include "textstream.h"
#include <glibmm/dispatcher.h>
#include "util/barrier.h"
#include "util/processman.h"
#include "textframe.h"

/// \todo move this some place else
Process runXsltProc(const std::string& transform,
                    const std::string& association,
                    const TextStream::ParamIter& begin,
                    const TextStream::ParamIter& end);

class TypesetterThread : public sigc::trackable {
public:
  struct FrameData {
    const Vector framesize;
    const int page_num, num_columns;
    const float gutter;
    const Matrix matrix;
    const Boundary boundary;
    BoundaryVect obstacles;
    FrameData(const TextFrame& frame,
              const BoundaryVect& obstacles)
      : framesize(frame.get_inherent_size()), page_num(frame.get_page_num()),
        num_columns(frame.get_num_columns()), gutter(frame.get_gutter()),
        matrix(frame.get_matrix()), boundary(frame.get_obstacle_boundary()),
        obstacles(obstacles)
    {
    }
  };
  typedef std::list<FrameData> FrameDataList;
  
  sigc::signal<void, Glib::ustring,
               Glib::RefPtr<xml2ps::PsCanvas>, bool> signal_done;
  
  static Glib::RefPtr<TypesetterThread>
  create(const std::string& association, const std::string& transform,
         const TextStream::ParamMap& parameters,
         const FrameDataList& sorted_frames) {
    Glib::RefPtr<TypesetterThread> tmp(new TypesetterThread(association,
                                                            transform,
                                                            parameters,
                                                            sorted_frames));
    tmp->reference();
    tmp->self = tmp;
    return tmp;
  }
  
  void reference() const { ++count_; }
  void unreference() const { if(--count_ == 0) delete this; }
  bool is_done();
  
private:
  TypesetterThread(const std::string& association,
                   const std::string& transform,
                   const TextStream::ParamMap& parameters,
                   const FrameDataList& sorted_frames);
  ~TypesetterThread();
  
  void run();
  void run_xml2ps(istream& source);

  void on_done();
  void on_error();
  
  void process_stopped(pid_t  pid, bool exited_normally, int exit_code);
  
  /// reference count
  mutable unsigned int count_;

  /// Keeps a reference to itself, so that the object is not deleted
  /// until self is set to null explicitly, which should happen in
  /// on_done() or on_error(). The idea is that the object should be
  /// kept from being deleted until we are sure the Dispatcher pipe is
  /// empty.
  Glib::RefPtr<TypesetterThread> self;

  Barrier done;
  const std::string association, transform;
  std::string error;
  const TextStream::ParamMap parameters;
  FrameDataList sorted_frames;
  bool truncated;
  Glib::Dispatcher signal_thread_done, signal_thread_error;
  sigc::connection proc_stop_connection;
  Process xformproc;
  Glib::RefPtr<xml2ps::PsCanvas> new_canvas;
};

#endif
