///
// Copyright (C) 2002 - 2004, Fredrik Arnerup & Rasmus Kaj, See COPYING
///
#include "loader.h"
#include "fileerrors.h"
#include "imageframe.h"
#include "rasterframe.h"
#include "textframe.h"

Pagent *load(const ElementWrap& xml, Group *parent)
{
  const std::string name = xml.get_element_name();
  if(name == "frame") { 
    const std::string type = xml.get_required_attribute<std::string>("type");

    // create according to type string:
    if(type == "group")
      return new Group(xml, parent);
    else if(type == "image")
      return new ImageFrame(xml, parent);
    else if(type == "raster")
      return new RasterFrame(xml, parent);
    else if(type == "text")
      return new TextFrame(xml, parent);
    else
      throw Error::Read("Unknown frame type: \"" + type + "\".");
  } else {
    throw Error::Read("Expected a <frame> but got a <" + name + ">.");
  }
}
