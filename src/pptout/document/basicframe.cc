///
// Copyright (C) 2002 - 2004, Fredrik Arnerup & Rasmus Kaj, See COPYING
///
#include "basicframe.h"
#include "util/rectboundary.h"
#include <util/stringutil.h>
#include <util/warning.h>
#include <sigc++/sigc++.h>
#include <libxml++/libxml++.h>
#include <gdkmm.h>
#include <fstream>
#include <algorithm>
#include <glib/gi18n.h>

BasicFrame::BasicFrame(Group *parent, const std::string& name)
  : Pagent(parent, name)
{}

BasicFrame::BasicFrame(const ElementWrap& xml, Group *parent)
  : Pagent(parent, xml.get_attribute<string>("name", _("unnamed")))
{
  locked = xml.get_attribute<bool>("lock", false);
  flow_around = xml.get_attribute<bool>("flowaround", false);
  obstacle_margin = xml.get_attribute<float>("obstaclemargin");
  
  matrix = xml.get_required_attribute<Matrix>("matrix");
}

xmlpp::Element *BasicFrame::save(xmlpp::Element& parent_node,
				 const FileContext &context) const 
{
  xmlpp::Element *node = parent_node.add_child("frame");
  if(!name.empty())
    node->set_attribute("name", name);

  node->set_attribute("matrix", tostr(get_matrix()));
  node->set_attribute("lock", tostr(get_lock()));
  node->set_attribute("flowaround", tostr(flow_around));
  node->set_attribute("obstaclemargin", tostr(obstacle_margin));

  return node;
}

BasicFrame::~BasicFrame() {
}

void BasicFrame::print(std::ostream &out, bool grayscale) const {
}

Boundary BasicFrame::get_obstacle_boundary() const {
  if(flow_around) {
    Boundary bound = get_box();
    bound->grow(obstacle_margin);
    return bound;
    
  } else
    return Boundary();
};
