#ifndef PAPER_SIZES_H		// -*- c++ -*-
#define PAPER_SIZES_H
///
// Copyright (C) 2002 - 2004, Fredrik Arnerup & Rasmus Kaj, See COPYING
///
#include <string>
#include <map>
#include <stdexcept>

namespace Error {
  struct PaperName : public std::invalid_argument {
    std::string name;
    PaperName(const std::string& _name) 
      :invalid_argument("No paper format named \"" + _name + "\""),
       name(_name) {}
    ~PaperName() throw() {}
  };
}

class Papers {
public:
  enum Orientation {PORTRAIT, LANDSCAPE};

  class Size {
    float width, height;
  public:
    Size(float w, float h): width(w), height(h) {}
    Size(): width(-1), height(-1) {}
    float get_width(Orientation o) const 
    {return o == PORTRAIT ? width : height;}
    float get_height(Orientation o) const 
    {return o == PORTRAIT ? height : width;}
  };

  std::map<std::string, Size> sizes;
  
  Papers();
};

extern Papers papers;

#endif
