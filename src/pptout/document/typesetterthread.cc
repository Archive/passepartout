///
// Copyright (C) 2002 - 2004, Fredrik Arnerup & Rasmus Kaj, See COPYING
///
#include <typesetterthread.h>
#include "util/warning.h"
#include "textframe.h"
#include "xml2ps/typesetter.hh"

// kill
#include <sys/types.h>
#include <signal.h>


/**
 * Start an external process running a XLST transform.
 * \return a Process handle
 */
Process runXsltProc(const std::string& transform,
                    const std::string& association,
                    const TextStream::ParamIter& begin,
                    const TextStream::ParamIter& end) {
  debug << "Applying XSL Transform" << std::endl;
  // Assume xsltproc is in the path
  std::vector<std::string> command;
  command.push_back("xsltproc");
  command.push_back("--nonet");
  // TODO: Make arugments to xsltproc (e.g. --catalogs) optional.
  
  for(TextStream::ParamIter i = begin; i != end; ++i)
    if(!i->second.empty()) {
      command.push_back("--stringparam");
      command.push_back(i->first);
      command.push_back(i->second);
    }
  command.push_back(transform);
  command.push_back(association);
    
  Process xformproc = ProcessManager::instance().run(command);
  if(debug) {
    debug << "Started " << xformproc->get_pid() << ':';
    for(std::vector<std::string>::const_iterator i = command.begin();
        i != command.end(); ++i)
      debug << ' ' << *i;
    debug << "" << std::endl;
  }
  return xformproc;
}

TypesetterThread::TypesetterThread(const std::string& association,
                                   const std::string& transform,
                                   const TextStream::ParamMap& parameters,
                                   const FrameDataList& sorted_frames)
  : count_(0), association(association), transform(transform),
    parameters(parameters),
    sorted_frames(sorted_frames), truncated(false),
    proc_stop_connection(ProcessManager::instance().process_stopped.connect
			 (sigc::mem_fun(*this,
                                        &TypesetterThread::process_stopped)))
{
  signal_thread_done.connect(sigc::mem_fun(*this, &TypesetterThread::on_done));
  signal_thread_error.connect
    (sigc::mem_fun(*this,  &TypesetterThread::on_error));
  
  Glib::Thread *thread =
    Glib::Thread::create(sigc::mem_fun(*this, &TypesetterThread::run), false);

  debug << "Started thread " << thread << " from " << Glib::Thread::self()
	<< std::endl;
}

TypesetterThread::~TypesetterThread() {
  proc_stop_connection.disconnect();
  if(xformproc)
    kill(xformproc->get_pid(), SIGTERM);
}

bool TypesetterThread::is_done() {
  return done.get_open();
}

void TypesetterThread::run() {
  // this method is supposed to run in a separate thread
  debug << "inside typesetter thread" << std::endl;
  try {
    if(transform.empty()) {
      // Run xml2ps directly on the source xml
      std::ifstream source(association.c_str());
      if(!source) {
	error = "Could not read from " + association;
	throw std::runtime_error(error);
      }
      run_xml2ps(source);
      
    } else {
      xformproc = runXsltProc(transform, association,
			      parameters.begin(), parameters.end());
      
      run_xml2ps(xformproc->get_cout());
    }
    debug << "Signalling done from " << Glib::Thread::self() << std::endl;
    done.open();
    signal_thread_done();
    debug << " signalled done from " << Glib::Thread::self() << std::endl;
  } catch (...) {
    if(error.empty()) {
      try { throw; }
      catch (const std::exception& err) {
	error = err.what();
      } catch(...) {
	error = "unknown error";
      }
    }
    debug << "typesetter error: " << error << std::endl;
    done.open();
    signal_thread_error();
  }
}

void TypesetterThread::run_xml2ps(std::istream& source) {
  xml2ps::Canvas::PageVec pages;
  for(FrameDataList::const_iterator
        i = sorted_frames.begin(); i != sorted_frames.end(); i++) {
    pages.push_back(xml2ps::PageBoundary(i->page_num,
					 i->framesize.x, i->framesize.y,
					 i->num_columns, i->gutter));
    
    // Transform from page coords to local textframe coords.
    const Matrix xform(i->matrix.inv());
    
    // List of boundaries the text should flow around
    for(BoundaryVect::const_iterator
          o = i->obstacles.begin(); o != i->obstacles.end(); o++) {
      if(i->boundary != *o)
    	pages.back().addObstacle(*o * xform);
    }
  }
  
  debug << "running internal xml2ps ..." << std::endl;
  xml2ps::PsCanvas *tmp =
    new xml2ps::PsCanvas(pages, false /* no extra pages */);
  tmp->reference(); // refcount starts at 0
  new_canvas = Glib::RefPtr<xml2ps::PsCanvas>(tmp);
  new_canvas->setSubstituteFontAliases(true);
  xml2ps::PsConverter parser(*new_canvas.operator->());
  try {
    parser.parse_stream(source);
  } catch(const xml2ps::OutOfPages&) {
    truncated = true;
  }
}

/**
 * This method should run in the main GUI thread when the worker is done.
 */
void TypesetterThread::on_done() {
  // make sure the the TypesetterThread won't keep any references to
  // itself after this method has returned.
  Glib::RefPtr<TypesetterThread> tmp = self;
  self.clear();

  debug << "internal xml2ps done" << std::endl;
  signal_done("", new_canvas, truncated);
}

/**
 * This method should run in the main GUI thread if the worker fails.
 */
void TypesetterThread::on_error() {
  // make sure the the TypesetterThread won't keep any references to
  // itself after this method has returned.
  Glib::RefPtr<TypesetterThread> tmp = self;
  self.clear();

  debug << "internal xml2ps failed" << std::endl;
  signal_done(error,
              /* the rest is ignored */
              Glib::RefPtr<xml2ps::PsCanvas>(), true);
}

void TypesetterThread::process_stopped(pid_t pid, 
                                       bool exited_normally, 
                                       int exit_code)
{
  if(!xformproc || pid != xformproc->get_pid())
    return;
  
  if(!exited_normally) {
    warning << "Process " << pid << " exited abnormally" << std::endl;
    return; /// \todo do something clever
  }

  if(exit_code != 0) {
    /// \todo check that the return code 127 is not unique to Bash.
    if(exit_code == 127) // the shell could not find xsltproc
      warning << "xsltproc is not in $PATH" << std::endl;
    else
      warning << "xsltproc process with pid " << pid
	      << " failed with exit code " << exit_code << std::endl;
    return;
  }
  
  debug << "Stylesheet done" << std::endl;
}
