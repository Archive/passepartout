#ifndef PAGENT_H		// -*- c++ -*-
#define PAGENT_H
///
// Copyright (C) 2002 - 2004, Fredrik Arnerup & Rasmus Kaj, See COPYING
///
#include "util/matrix.h"
#include "util/boundary.h"
#include "ps/pdf.h"
#include <sigc++/connection.h>
#include <sigc++/signal.h>
#include <libxml++/nodes/element.h>
#include <string>
#include <stdexcept>
#include "filecontext.h"

class Group;

namespace Error {
  struct NoParent : public std::logic_error {
    NoParent() : logic_error("Pagent has no parent") {}
  };
  struct InvalidPageNum : public std::logic_error {
    InvalidPageNum() : logic_error("Invalid page number") {}
    InvalidPageNum(int num);
  };
}

/**
 * The most general interface to anything on a Page.
 * A Pagent has a size and a transformation matrix that defines where on the
 * Page it is.
 */
class Pagent: public sigc::trackable {
public:
  Pagent(Group *_parent, const std::string& _name);
  virtual ~Pagent();

  /**
   * Save this Pagent to an xml Element to be included in a Passepartout save
   * file.
   * \param parent_node the xml node that the saved element should be in.
   * \param context a context for handling file names.
   */
  virtual xmlpp::Element *save(xmlpp::Element& parent_node,
			       const FileContext &context) const = 0;

  /**
   * Print this pagent as PostScript.
   * \param out the stream for the PostScript output.
   * \param grayscale if true, print grayscale only.
   */
  virtual void print(std::ostream &out, bool grayscale = false) const = 0; 

  /**
   * Print this pagent as PDF.
   */
  virtual void print_pdf(PDF::Content::Ptr pdf) const = 0;
  
  
  // *** undoable actions ***
  virtual void set_translation(const Vector& v);
  virtual void set_shearing(float shear);
  virtual void set_rotation(float angle);
  virtual void set_scaling(float xfactor, float yfactor);
  virtual void set_lock(bool _locked);
  // a locked pagent cannot be moved or reshaped with the mouse
  virtual void set_flow_around(bool _flow_around);
  virtual void set_obstacle_margin(float margin);
  virtual void set_name(const std::string &_name);
  

  // *** probe state ***
  virtual const std::string &get_name() const {return name;}
  /** The name of this frame type, e.g. image. */
  virtual std::string getTypeName() const = 0;

  virtual bool get_lock() const {return locked;}
  virtual Boundary get_box() const = 0;
  virtual const Matrix& get_matrix() const {return matrix;} 
  
  /**
   * Get the inherent size of this object.  The inherent size is the size as
   * the object sees it, in its own coordinate system (before applying the
   * matrix).
   */
  virtual Vector get_inherent_size() const = 0;
  
  virtual bool get_flow_around() const {return flow_around;}
  virtual Boundary get_obstacle_boundary() const = 0;
  virtual float get_obstacle_margin() const {return obstacle_margin;}

  virtual int get_page_num() const; // Ask the page what number it is
  // May throw Error::InvalidPageNum if there is a problem 
  // with the pages in the document


  // *** non-undoable actions ***
  virtual void set_parent(Group *parent_);
  virtual void set_matrix(Matrix m);


  // *** access parent objects ***
  const Group& get_parent() const;
  Group& get_parent();

  /** There was a change in the source object for this Pagent. */
  sigc::signal<void> object_changed_signal;
  
  /** Anything has changed, anything at all, including position and shape. */
  sigc::signal<void> props_changed_signal; 

  /** the pagent has changed shape / position */
  sigc::signal<void> geometry_changed_signal; 
  
  /**
   * Resizability contract for Pagents.  That a Pagent inherits Resizable
   * means that it has an intrinsic size that can be changed.  Other objects
   * have a fixed intrinsic size (e.g. a raster image) that can't be changed
   * from inside passepartout.
   */
  class Resizable {
  public:
    virtual ~Resizable();
    /** Set the intrinsic size of the pagent. */
    virtual void set_size(float width, float height) = 0;
  };
  
protected:
  Group *parent;
  bool locked, flow_around;
  Matrix matrix;
  std::string name;
  float obstacle_margin;
  
  // connections to parent
  sigc::connection draw_connection, geometry_connection, props_connection;
  void connect_to_parent();

private:  
  // Undefined ctors, avoid defaults
  Pagent(const Pagent&);
  Pagent();
  void operator = (const Pagent&);
};

#endif
