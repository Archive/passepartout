#ifndef BASICFRAME_H		// -*- c++ -*-
#define BASICFRAME_H
///
// Copyright (C) 2002 - 2004, Fredrik Arnerup & Rasmus Kaj, See COPYING
///
#include "pagent.h"
#include "fileerrors.h"
#include "util/xmlwrap.h"
#include <iostream>

namespace Error {
  struct FrameType: public File {
    FrameType(const string& msg) : File("Frame-type error: " + msg) {}
  };
}

class BasicFrame: public Pagent {
public:
  BasicFrame(Group *parent, const std::string& name);
  BasicFrame(const ElementWrap& xml, Group *parent);
  virtual ~BasicFrame();
  
  virtual xmlpp::Element *save(xmlpp::Element& parent_node,
			       const FileContext &context) const;
  virtual void print(std::ostream &out, bool grayscale = false) const;

  /**
   * This implementation of get_obstacle_boundary() returns the bounding box
   * for the frame, appropriatley grown.  Pagents that works as
   * non-rectangular boundaries should provide their own implementation.
   */
  Boundary get_obstacle_boundary() const;
  
  // Errors reported by generate_picture:
  // ASSOCIATION - failed to open associated file
  //  GENERATION - general error (missing program, corrupted file, et cetera)
  //    ZEROSIZE - picture has zero size
  enum GenPicErrorType { ASSOCIATION, GENERATION, ZEROSIZE, NOTREADY }; 
  struct GenPicError : public std::logic_error {
    GenPicErrorType type;
    GenPicError(GenPicErrorType t, std::string s)
      : logic_error(s), type(t) {}
    ~GenPicError() throw() {}
  };
};

#endif
