#ifndef PREFERENCESDIALOG_H		// -*- c++ -*-
#define PREFERENCESDIALOG_H
///
// Copyright (C) 2002 - 2004, Fredrik Arnerup & Rasmus Kaj, See COPYING
///
#include "widget/dialogwrap.h"
#include "widget/filesel.h"
#include "widget/spinner.h"

#include <gtkmm/label.h>
#include <gtkmm/notebook.h>

class PreferencesDialog : public DialogWrap {
public:
  PreferencesDialog();
protected:
  void dialog_cancel();
  void dialog_apply();
  bool dialog_delete(GdkEventAny*);

private:
  Gtk::Notebook *notebook;
  Glib::ustring file_str, prog_str, env_str, tab1_str, tab2_str, 
    close_button_str, apply_button_str;//, misc_str;

  FileEntry *xslt_entry, *print_entry, *gs_entry, 
    *xslt_file_entry;
  Spinner *resolution_spin, *zoom_spin;
};

#endif
