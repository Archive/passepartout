#ifndef PROPERTIESDIALOG_H	// -*- c++ -*-
#define PROPERTIESDIALOG_H
///
// Copyright (C) 2002 - 2004, Fredrik Arnerup & Rasmus Kaj, See COPYING
///
#include "document/document.h"
#include "widget/dialogwrap.h"
#include <gtkmm/notebook.h>
#include <gtkmm/button.h>
#include <vector>

class PropBase;

/**
 * A dialog displaying properties of the selected object.  It will listen
 * for selection signals, so when the user selects a new object, the existing
 * dialog will show properties for that object.
 * This is a Singleton.
 */
class PropertiesDialog : public UtilityWindow {
public:
  static PropertiesDialog &instance();
  
  void show_raise();		// Make sure this dialog is visible
  
  DocRef get_document() const { return document; }
  void set_document(DocRef document_);
  
private:
  static PropertiesDialog *_instance;
  void select_change(DocRef doc);		// signal listener
  void update();
  DocRef document;

  PropertiesDialog();
  // no default constructors:
  PropertiesDialog(const PropertiesDialog&);
  void operator = (const PropertiesDialog&);
  // no destructor
  ~PropertiesDialog();

  void show_page_contents(GtkNotebookPage*, guint index);

  Gtk::Notebook book;
  std::vector<PropBase*> pages;
};

#endif
