#ifndef PPT_POSTSCRIPTVIEWENT_H	// -*- C++ -*-
#define PPT_POSTSCRIPTVIEWENT_H
///
// Copyright (C) 2003, 2004, Fredrik Arnerup & Rasmus Kaj, See COPYING
///
#include "viewent.h"
#include "document/cachedframe.h"
#include <sys/types.h>		// pid_t

class PostscriptViewent : public Viewent {
protected:
  typedef CachedFrame* FrameRef;
  typedef Glib::RefPtr<Gdk::Pixbuf> PixbufRef;
public:
  /**
   * Create a new TextViewent.
   * The _scale_factor is ignored if the used ghostscript supports the
   * pngalpha device.
   * \param cachedframe
   * \param view
   * \param _scale_factor Create bitmaps this many times bigger than
   *  necessary and scale them down, kind of a poor mans
   *  antialiasing. */
  PostscriptViewent(View& view, const FrameRef cachedframe,
		    int _scale_factor = 1);
  ~PostscriptViewent();
  
  void set_image(const std::string& psfile);
  void regenerate();
  
protected:
  const Pagent* get_frame() const { return frame; }
  void set_state(State state);
private:
  void process_stopped(pid_t _pid, bool exited_normally, int exit_code);
  bool regenerate_bg();
  void stop_process();
  
  std::auto_ptr<Gnome::Canvas::Pixbuf> canvas_pixbuf;
  FrameRef frame;
  pid_t pid;
  std::string tmp_file;
  int scale_factor;
  bool redraw_requested;
};

#endif
