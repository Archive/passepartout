#ifndef PPT_RASTERVIEWENT_H	// -*- C++ -*-
#define PPT_RASTERVIEWENT_H
///
// Copyright (C) 2003, 2004, Fredrik Arnerup & Rasmus Kaj, See COPYING
///
#include "viewent.h"
#include "document/rasterframe.h"

class RasterViewent : public Viewent {
  typedef RasterFrame* FrameRef;
public:
  RasterViewent(View& view, const FrameRef rasterframe);
  
protected:
  const Pagent* get_frame() const { return frame; }
  
  void on_object_changed();
  void on_geometry_changed();
  
private:
  FrameRef frame;
  
  std::auto_ptr<Gnome::Canvas::Pixbuf> canvas_pixbuf;
};

#endif
