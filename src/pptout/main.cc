///
// Copyright (C) 2002 - 2004, Fredrik Arnerup & Rasmus Kaj, See COPYING
///

#include <gtkmm/main.h>

#include "util/warning.h"
#include "util/tempfile.h"
#include "util/cmdline.h"
#include "widget/errordialog.h"
#include "defines.h" // PACKAGE_VERSION

#ifdef HAVE_GNOME
#include <libgnomevfs/gnome-vfs.h>
#endif

#include "i18n.h"
#include "config.h"
#include "window.h"
#include "icons/icon_48.h"
#include "stockitems.h"

#include <glib/gi18n.h>

int main(int argc, char *argv[]) {
  try {
    init_exception_handler();
    int win_count = 0;

    // set prefix for temp files
    Tempfile::set_prefix("pptout");

    Passepartout::init_i18n ();

    // initialize threads
    Glib::thread_init();

    // initialise gtkmm
    Gtk::Main kit(argc, argv);

    Gnome::Canvas::init ();
#ifdef HAVE_GNOME
    gnome_vfs_init ();
#endif

    // register new stock items
    register_stock_items();

    // application icon
    std::vector<Glib::RefPtr<Gdk::Pixbuf> > icon_list;
    icon_list.push_back(Gdk::Pixbuf::create_from_inline(sizeof(icon_48),
                                                        icon_48));
    Gtk::Window::set_default_icon_list(icon_list);

    // parse command line
    CmdLine cmdline(argc, argv);
    const int o_version =
      cmdline.add_option(CmdLine::Option('v', "version",
					 CmdLine::Option::NO_PARAM, 
					 _("Show version number")));
    const int o_verbose = 
      cmdline.add_option(CmdLine::Option('V', "", CmdLine::Option::NO_PARAM, 
					 _("Be verbose")));
    const int o_debug = 
      cmdline.add_option(CmdLine::Option('d', "", CmdLine::Option::NO_PARAM, 
					 _("Show debugging messages")));
    const int o_help =
      cmdline.add_option(CmdLine::Option('h', "help",
					 CmdLine::Option::NO_PARAM,
					 _("Show this help message")));
    CmdLine::ParsedOptions opts = cmdline.parse();
    for(CmdLine::ParsedOptions::const_iterator
	  arg = opts.begin(); arg != opts.end(); arg++) {
      int option = arg->first;
      if(!option) {
	try {
	  /// \todo config.read()?
	  config.read(); // after the cerr aliases are set
	  new FrameWindow(arg->second);
	  win_count++;
	} catch(const std::exception& err) {
	  warning << "Error opening \"" << arg->second
		  << "\": " << err.what() << std::endl;
	}
      } else if(option == o_version) {
	warning << "Passepartout " << std::string(PACKAGE_VERSION) << std::endl;
	return 0;
      } else if(option == o_verbose) {
	verbose.active = true;
      } else if(option == o_debug) {
	verbose.active = true;
	debug.active = true;
	debug << "Debug mode active" << std::endl; // Why wait?
      } else if(option == o_help) {
	std::cout << _("Usage: passepartout [options] [files...]") << std::endl;
	cmdline.print_usage(std::cout);
	return 0;
      }
    }

    if(!win_count) { // open empty window
      config.read(); // after the cerr aliases are set
      new FrameWindow();
      win_count++;
    }

    // if no windows were opened, don't run the kit 
    if(win_count) {
      gdk_rgb_init();		// Needed before rendering pixbuf to screen.
      Gtk::Main::run();
    }

    return 0;
  }
  catch(const std::exception &err) {
    warning << err.what() << std::endl;
    return 1;
  }
  catch(...) {
    warning << "Caught an exception that wasn't a std::exception."
	    << std::endl;
    return 1;
  }
}
