///
// Copyright (C) 2003, 2004, Fredrik Arnerup & Rasmus Kaj, See COPYING
///
#include "rasterviewent.h"

RasterViewent::RasterViewent(View& view, const FrameRef rasterframe)
  : Viewent(view, rasterframe), frame(rasterframe)
{
  frame->props_changed_signal.connect
    (sigc::mem_fun(*this, &RasterViewent::on_geometry_changed));
  frame->object_changed_signal.connect
    (sigc::mem_fun(*this, &RasterViewent::on_object_changed));

  canvas_pixbuf.reset(new Gnome::Canvas::Pixbuf(*content_group));
  canvas_pixbuf->property_x() = 0; canvas_pixbuf->property_y() = 0;
  canvas_pixbuf->property_anchor() = Gtk::ANCHOR_SOUTH_WEST;
  on_properties_changed();
  on_object_changed();
}

void RasterViewent::on_object_changed() {
  Glib::RefPtr<Gdk::Pixbuf> pixbuf = frame->get_filepixbuf();
  set_state(pixbuf ? NORMAL : BROKEN);
  canvas_pixbuf->property_pixbuf() = pixbuf;
  on_geometry_changed();
}

void RasterViewent::on_geometry_changed() {
  Viewent::on_geometry_changed();

  Vector size = frame->get_inherent_size();

  canvas_pixbuf->property_width() = size.x;
  canvas_pixbuf->property_height() = size.y;
  canvas_pixbuf->property_width_set() = true;
  canvas_pixbuf->property_height_set() = true;
  canvas_pixbuf->property_width_in_pixels() = false;
  canvas_pixbuf->property_height_in_pixels() = false;
}
