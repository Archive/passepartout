///
// Copyright (C) 2003, 2004, Fredrik Arnerup & Rasmus Kaj, See COPYING
///
#include "pageviewent.h"
#include "document/document.h"

PageViewent::PageViewent(View& view, const FrameRef pageframe)
  : GroupViewent(view, pageframe), page(pageframe),
    guide_group(view.get_guide_group())
{
  Document::selection_changed_signal.connect
    (sigc::mem_fun(*this, &PageViewent::on_selection_changed));
  /// \todo maybe let Page throw a size_changed_signal instead?
  Document::containing(*page).size_changed_signal.connect
    (sigc::mem_fun(*this, &PageViewent::on_size_changed));
  
  using namespace Gnome::Canvas;

  paper.reset(new Rect(view.get_pagent_group()));
  paper->property_fill_color_gdk() = view.get_color(Color::bg);
  paper->lower_to_bottom();
  on_size_changed();

  float width = page->get_width();
  float height = page->get_height();

  /// \todo update guides when the page size changes
  const float extra = 10; // let the guides be slightly wider than the page
  for(Page::Guides::const_iterator i = page->guides.begin();
      i != page->guides.end();
      i++) {
    Points points;

    switch(i->orientation) {
    case Guide::HORIZONTAL:
      points.push_back(Gnome::Art::Point(-extra, -i->position));
      points.push_back(Gnome::Art::Point(width + extra, -i->position));
      break;
    case Guide::VERTICAL:
      points.push_back(Gnome::Art::Point(i->position, extra));
      points.push_back(Gnome::Art::Point(i->position, -(height + extra)));
      break;
    }

    Line *line = manage(new Line(guide_group, points));
    line->property_fill_color_gdk() = view.get_color(Color::guide);
    line->property_line_style() = Gdk::LINE_ON_OFF_DASH;
    line->property_width_pixels() = 1;
  }

  rect->hide(); // no frame please

  // if this is a new view, something might be selected already
  on_selection_changed(Document::containing(*page).self());
}

PageViewent::~PageViewent() {
}

void PageViewent::on_size_changed() {
  paper->property_x1() = 0; paper->property_y1() = 0;
  paper->property_x2() = page->get_width(); paper->property_y2() = -page->get_height();
}

void PageViewent::on_selection_changed(DocRef doc) {
  if(doc != Document::containing(*page).self())
    return;
  else
    update_selection(doc->selected());
}

#ifdef PNUM_HACK
// this is duplicated from page.cc
namespace {
  struct PNumHack { //page numbering hack
    static const int xpos=40; //points
    static const int ypos=40; //points
    static const int font_size=10;
  } pnum_hack;
}
#endif
