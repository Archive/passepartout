#ifndef PPT_GROUPVIEWENT_H	// -*- C++ -*-
#define PPT_GROUPVIEWENT_H
///
// Copyright (C) 2003, 2004, Fredrik Arnerup & Rasmus Kaj, See COPYING
///
#include "viewent.h"
#include "document/group.h"
#include "document/document.h"	// all this just for the selection ...
#include <vector>

class GroupViewent : public Viewent {
  typedef Group* FrameRef;
  typedef Viewent::Ref ChildRef;
  typedef std::vector<ChildRef> ChildVec;
public:
  GroupViewent(View& view, const FrameRef groupframe);
  ~GroupViewent();
  
protected:
  const Pagent* get_frame() const { return frame; }
  void update_selection(const Document::Selection& selected);
  
private:
  /** Callback for group_chagned_signal */
  void group_changed();

  View* subview;
  FrameRef frame;
  sigc::connection groupcon;
  ChildVec childvec;
};

#endif
