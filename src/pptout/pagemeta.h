#ifndef PAGEMETA_H		// -*- c++ -*-
#define PAGEMETA_H
///
// Copyright (C) 2002 - 2004, Fredrik Arnerup & Rasmus Kaj, See COPYING
///
#include "metabase.h"

class PageMeta : public MetaBase {
public:
  Pagent* load(const xmlpp::Element& node, Group* parent);

  Viewent::Ref create_viewent(View& view, Pagent& node);
  
  PropBase* getProp();
};

#endif
