#ifndef DOCPROPSDIALOG_H	// -*- c++ -*-
#define DOCPROPSDIALOG_H
///
// Copyright (C) 2002 - 2004, Fredrik Arnerup & Rasmus Kaj, See COPYING
///
#include "widget/dialogwrap.h"

class DocumentView;
namespace Gtk {
  class CheckButton;
  class RadioButton;
  class Button;
  class VBox;
  class ComboBoxText;
};
class FileEntry;
class Spinner;

/**
 * This is a Singleton.
 */
class DocPropsDialog : public DialogWrap {
public:
  static DocPropsDialog &instance();
  void modify(DocumentView *_view) {show_it(_view, false);}
  void create(DocumentView *_view) {show_it(_view, true);};

private:
  static DocPropsDialog *_instance;  
  Gtk::RadioButton *portrait_button, *landscape_button,
    *single_sided_button, *double_sided_button;
  Gtk::CheckButton *template_button;
  Gtk::Button *cancel_button, *ok_button, *create_button;
  Gtk::VBox *format_box;
  Gtk::ComboBoxText *paper_size;
  FileEntry *file_entry;
  Spinner *first_page;
  bool new_document;
  DocumentView *view;

  DocPropsDialog();
  // no default constructors:
  DocPropsDialog(const DocPropsDialog&);
  void operator = (const DocPropsDialog&);
  // no destructor
  ~DocPropsDialog();

  void show_raise() {}
  void update_mode();

  void show_it(DocumentView *_view, bool create_new);
  void on_response(int response_id);
};

#endif
