#ifndef RASTERMETA_H		// -*- c++ -*-
#define RASTERMETA_H
///
// Copyright (C) 2002 - 2004, Fredrik Arnerup & Rasmus Kaj, See COPYING
///
#include "metabase.h"
#include "util/xmlwrap.h"

/**
 * Meta functions for the raster image frame type.
 */
class RasterMeta : public MetaBase {
public:
  Pagent* load(const ElementWrap& xml, Group* parent);
  
  Viewent::Ref create_viewent(View& view, Pagent& node);
  
  PropBase* getProp();
};

#endif
