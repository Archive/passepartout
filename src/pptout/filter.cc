/* This file is part of passepartout
 *
 * AUTHORS
 *     Sven Herzberg
 *
 * Copyright (C) 2009  Sven Herzberg
 *
 * This work is provided "as is"; redistribution and modification
 * in whole or in part, in any medium, physical or electronic is
 * permitted without restriction.
 *
 * This work is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * In no event shall the authors or contributors be liable for any
 * direct, indirect, incidental, special, exemplary, or consequential
 * damages (including, but not limited to, procurement of substitute
 * goods or services; loss of use, data, or profits; or business
 * interruption) however caused and on any theory of liability, whether
 * in contract, strict liability, or tort (including negligence or
 * otherwise) arising in any way out of the use of this software, even
 * if advised of the possibility of such damage.
 */

#include "filter.h"

#include "defines.h"

#include <glib/gi18n.h>

namespace Passepartout
{

class Filter::Private
{
public:
  Gtk::RecentFilter recent;
};

Filter::Filter (void)
{
  priv = new Private ();
  set_name (_("Passepartout Document files (.pp)"));
  add_mime_type ("application/x-passepartout");
  add_pattern ("*.pp");
}

Filter::~Filter (void)
{
  delete priv;
}

void
Filter::add_mime_type (const Glib::ustring& s)
{
  Gtk::FileFilter::add_mime_type (s);
  priv->recent.add_mime_type (s);
}

void
Filter::add_pattern (const Glib::ustring& s)
{
  Gtk::FileFilter::add_pattern (s);
  priv->recent.add_pattern (s);
}

Gtk::RecentFilter&
Filter::recent ()
{
  return priv->recent;
}

void
Filter::set_name (const Glib::ustring& s)
{
  Gtk::FileFilter::set_name (s);
  priv->recent.set_name (s);
}

}
