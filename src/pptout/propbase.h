#ifndef PROPBASE_H		// -*- c++ -*-
#define PROPBASE_H
///
// Copyright (C) 2002 - 2004, Fredrik Arnerup & Rasmus Kaj, See COPYING
///
#include <gtkmm/box.h>
#include <gtkmm/label.h>
#include "document/pagent.h"

// Base class for the different property sets.
// PropertiesDialog displays one PropBase at a time, through a Gtk Notebook.
class PropBase : public Gtk::VBox {
public:
  typedef std::string string;
  
  explicit PropBase(const string& name);
  Widget& getLabel() { return label; }
  
  virtual void update() = 0;	// Changes from object to dialog
  
  virtual void setObject(Pagent* pagent) = 0;

  void set_sensitive(bool sensitive);
  void right_justify(Gtk::Label &label);
private:
  Gtk::Label label;
};

template <class FrameC>
class GenericProp : public PropBase {
public:
  explicit GenericProp(const string& label)
    :PropBase(label), object(0)
  {}
  void setObject(Pagent* pagent) {
    object = dynamic_cast<FrameC*>(pagent);
    set_sensitive(object);
    reconnectSignals();
    if(is_visible()) update();
  }
  /// Called by setObject, 
  virtual void reconnectSignals() {}
protected:
  FrameC* object;
};

#endif
