///
// Copyright (C) 2002 - 2004, Fredrik Arnerup & Rasmus Kaj, See COPYING
///
#include "docpropsdialog.h"
#include <list>

#include <gtkmm/box.h>
#include <gtkmm/label.h>
#include <gtkmm/separator.h>
#include <gtkmm/radiobutton.h>
#include <gtkmm/checkbutton.h>
#include <gtkmm/comboboxtext.h>
#include <gtkmm/stock.h>
#include <gtkmm/image.h>

#include "util/warning.h"

#include "document/paper_sizes.h"
#include "document/document.h"
#include "window.h"
#include "config.h"

#include "widget/filesel.h"
#include "widget/spinner.h"
#include "widget/wmisc.h"
#include "widget/usererror.h"

#include <glib/gi18n.h>

DocPropsDialog *DocPropsDialog::_instance = 0;

DocPropsDialog &DocPropsDialog::instance() {
  if(!_instance)
    _instance = new DocPropsDialog();
  return *_instance;
}

DocPropsDialog::DocPropsDialog():
  DialogWrap(""), // don't know title or parent
  view(0)
{
  template_button = manage(new Gtk::CheckButton(_("_Use template:"), true));

  /* FIXME: add stock icons from GTK+ for the orientation */
  portrait_button = manage(new Gtk::RadioButton(_("_Portrait"), true));
  landscape_button = manage(new Gtk::RadioButton(_("_Landscape"), true));

  single_sided_button = manage(new Gtk::RadioButton(_("_Single-sided"), true));
  double_sided_button = manage(new Gtk::RadioButton(_("_Double-sided"), true));

  paper_size = manage(new Gtk::ComboBoxText());
  file_entry = manage(new FileEntry(_("Select template")));
  first_page = manage(new Spinner(config.StartPage.values.front(), false));

  Gtk::HBox *template_box = manage(new Gtk::HBox(false, double_space));
  Gtk::VBox *sidedness_box = manage(new Gtk::VBox(false, single_space)),
    *format_box3 = manage(new Gtk::VBox(false, single_space));
  Gtk::HBox *first_page_box = manage(new Gtk::HBox(false, single_space));
  format_box = manage(new Gtk::VBox(false, double_space));
    
  Gtk::VBox *vbox = manage(new Gtk::VBox(false, triple_space));
  vbox->set_border_width(border_width);
  set_modal(true);
  set_resizable(false);

  {
    Gtk::RadioButton::Group group = landscape_button->get_group();
    portrait_button->set_group(group);
  }

  {
    Gtk::RadioButton::Group group = double_sided_button->get_group();
    single_sided_button->set_group(group);
  }

  template_box->pack_start(*template_button, Gtk::PACK_SHRINK);
  /* FIXME: set a proper filter for the selector (*.pp) */
  template_box->pack_start(*file_entry, Gtk::PACK_EXPAND_WIDGET);

  format_box3->pack_start(*paper_size, Gtk::PACK_SHRINK);
  format_box3->pack_start(*portrait_button, Gtk::PACK_SHRINK);
  format_box3->pack_start(*landscape_button, Gtk::PACK_SHRINK);

  sidedness_box->pack_start(*single_sided_button, Gtk::PACK_SHRINK);
  sidedness_box->pack_start(*double_sided_button, Gtk::PACK_SHRINK);

  Gtk::Label *format_label = 
    manage(new Gtk::Label(Glib::ustring::compose("<b>%1</b>", _("Document _format")),
						 0.0, 0.5, true));
  format_label->set_use_markup();
  format_label->set_mnemonic_widget(*paper_size);
  format_box->pack_start(*format_label);
  Gtk::HBox *format_box2 = manage(new Gtk::HBox());
  /* FIXME: don't use empty labels for spacing */
  format_box2->pack_start(*manage(new Gtk::Label("  ")));
  format_box2->pack_start(*format_box3);
  format_box2->pack_start(*manage(new Gtk::VSeparator()), Gtk::PACK_SHRINK,
			  double_space);
  format_box2->pack_start(*sidedness_box);
  format_box->pack_start(*format_box2);

  first_page_box->pack_end(*first_page, Gtk::PACK_SHRINK);
  Gtk::Label *first_page_label = 
    manage(new Gtk::Label(_("First page nu_mber:"), true));
  first_page_label->set_mnemonic_widget(first_page->get_spinbutton());
  first_page_box->pack_end(*first_page_label, 
			   Gtk::PACK_SHRINK);

  vbox->pack_start(*template_box);
  vbox->pack_start(*manage(new Gtk::HSeparator()));
  vbox->pack_start(*format_box);
  vbox->pack_start(*manage(new Gtk::HSeparator()));
  vbox->pack_start(*first_page_box);

  get_vbox()->pack_start(*vbox);

  template_button->set_active(true);
  file_entry->entry.set_text(config.DocTemplatePath.values.front());
  portrait_button->set_active(!config.Landscape.values.front());
  config.SingleSided.values.front() ?
    single_sided_button->set_active() :
    double_sided_button->set_active();

  cancel_button = add_button(Gtk::Stock::CANCEL, 0);
  ok_button = add_button(Gtk::Stock::OK, 1);
  create_button = add_button(Gtk::Stock::NEW, 1);

  template_button->signal_clicked().connect
    (sigc::mem_fun(*this, &DocPropsDialog::update_mode));
  update_mode();

  get_vbox()->show_all();
  get_action_area()->show_all();
}

DocPropsDialog::~DocPropsDialog() {}

void DocPropsDialog::update_mode() {
  bool active = template_button->get_active();
  format_box->set_sensitive(!active);
  file_entry->set_sensitive(active);
}

namespace {
  // build list of paper sizes
  void fill_paper_size(Gtk::ComboBoxText &paper_size,
                       std::string selected_paper) {
    if(papers.sizes.find(selected_paper) == papers.sizes.end()) {
      warning << "Paper format \"" << selected_paper 
	      << "\" is unknown." << std::endl;
      selected_paper = "A4";
    }
    int i = 0;
    for(std::map<std::string, Papers::Size>::const_iterator
	  size = papers.sizes.begin();
        size != papers.sizes.end();
        size++, i++) {
      paper_size.append_text(size->first);
      if(size->first == selected_paper)
        paper_size.set_active(i);
    }
  }
}

void DocPropsDialog::show_it(DocumentView *_view, bool create_new) {
  std::string selected_paper = config.PaperName.values.front();
  new_document = create_new;
  view = _view;
  if(!new_document) {
    DocRef document = view->get_document();
    if(!document)
      return;
    std::string template_file = document->get_template_file();
    template_button->set_active(!template_file.empty());
    first_page->set(document->get_first_page_num());
    portrait_button->set_active(document->get_orientation()
				== Papers::PORTRAIT);
    double_sided_button->set_active(document->is_doublesided());
    selected_paper = document->get_paper_name();

    create_button->hide(); ok_button->show();
    cancel_button->grab_default();
    show();
    // Gtk::Entry::set_position doesn't seem to work 
    // unless the entry is shown first
    file_entry->entry.set_text(template_file);
  } else { // New document
    create_button->show(); ok_button->hide();
    create_button->grab_default();
    template_button->set_active(true);
    first_page->set(config.StartPage.values.front()); //Restore default
    show();
  }

  Glib::RefPtr<Gtk::ListStore>::cast_dynamic(paper_size->get_model())->clear();
  fill_paper_size(*paper_size, selected_paper);

  // have to show() first
  set_title(new_document
	    ? _("Create new document")
	    : _("Document properties"));

  set_transient_for (*dynamic_cast<Gtk::Window*>(view->get_toplevel ()));
}


void DocPropsDialog::on_response(int response_id) {
  if(response_id == 0) {
    hide();
  } else if(response_id == 1) {
    Papers::Orientation orientation =
      portrait_button->get_active()
      ? Papers::PORTRAIT 
      : Papers::LANDSCAPE; 

    if(template_button->get_active() && file_entry->entry.get_text().empty()) {
	    /* FIXME: is this user-visible? */
      throw UserError("No template file was selected",
		      "Either select a template or uncheck"
		      " \"Use template\".");
    }

    if(new_document) { // Create new document
      DocRef document = Document::null();
      if(template_button->get_active()){ // Create from template
	document = Document::create(file_entry->entry.get_text());
	document->set_first_page_num(int(first_page->get()));
	//Save template path
	config.DocTemplatePath.values.front() = file_entry->entry.get_text();
      } else {
	document = Document::create();
	document->set_paper_name(paper_size->get_active_text());
	config.PaperName.values.front() = paper_size->get_active_text();
	document->set_orientation(orientation);
	config.Landscape.values.front() = !portrait_button->get_active();
	document->set_doublesided(double_sided_button->get_active());
	config.SingleSided.values.front() = single_sided_button->get_active();
	document->set_first_page_num(int(first_page->get()));
      }

      if(view && !view->get_document())
	view->set_document(DocMeta(document));
      else
	new FrameWindow("", document);

      hide();
    } else { // Modify existing document
	DocRef document = view->get_document();
	if(!document)
	  return;
	if(template_button->get_active()) { // create from template
	  document->set_template(file_entry->entry.get_text());
	  document->set_first_page_num(int(first_page->get()));
	  //Save template path
	  config.DocTemplatePath.values.front() = file_entry->entry.get_text();
	} else {
	  document->set_template("");
	  document->set_paper_name(paper_size->get_active_text());
	  config.PaperName.values.front()= paper_size->get_active_text();
	  document->set_orientation(orientation);
	  config.Landscape.values.front() = !portrait_button->get_active();
	  document->set_doublesided(double_sided_button->get_active());
	  config.SingleSided.values.front()= single_sided_button->get_active();
	  document->set_first_page_num(int(first_page->get()));
	}
	hide();
    }
  }
}
