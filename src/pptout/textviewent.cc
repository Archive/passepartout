///
// Copyright (C) 2003, 2004, Fredrik Arnerup & Rasmus Kaj, See COPYING
///
#include "textviewent.h"
#include "document/textframe.h"
#include "docview.h"
#include <gdkmm.h>
#include <gtkmm/drawingarea.h>
#include <libgnomecanvasmm/polygon.h>

TextViewent::TextViewent(View& view, const FrameRef cachedframe,
			 int scale_factor)
  : PostscriptViewent(view, cachedframe, scale_factor),
    textframe(dynamic_cast<TextFrame*>(cachedframe))
{
  Gnome::Canvas::Polygon *tri = new Gnome::Canvas::Polygon(*group);

  tri->property_fill_color_gdk() = view.get_color(Color::guide);
  trunc_ind.reset(tri);
  trunc_ind->hide();
  on_geometry_changed();
  textframe->truncated_state_changed_signal.connect
    (sigc::mem_fun(*this, &TextViewent::on_truncated_state_changed));
}

void TextViewent::on_truncated_state_changed(bool truncated) {
  if(truncated)
    trunc_ind->show();
  else
    trunc_ind->hide();
}

void TextViewent::on_geometry_changed() {
  PostscriptViewent::on_geometry_changed();

  Vector size = get_frame()->get_inherent_size();

  Gnome::Canvas::Polygon *tri =
    dynamic_cast<Gnome::Canvas::Polygon*>(trunc_ind.get());
  Gnome::Canvas::Points points;
  using namespace Gnome::Art;
  points.push_back(Point(size.x - 5, -15));
  points.push_back(Point(size.x - 25, -15));
  points.push_back(Point(size.x - 15, + 5));
  tri->property_points() = points;
}

void TextViewent::on_properties_changed() {
  TextStream *stream = textframe->get_stream();
  if(stream)
    stream->generate_ps_request(textframe);
}
