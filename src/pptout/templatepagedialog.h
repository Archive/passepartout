#ifndef TEMPLATEPAGEDIALOG_H		// -*- c++ -*-
#define TEMPLATEPAGEDIALOG_H
///
// Copyright (C) 2002 - 2004, Fredrik Arnerup & Rasmus Kaj, See COPYING
///

#include "docview.h"
#include "widget/dialogwrap.h"
#include <gtkmm/treeview.h>

class TemplatePageDialog: public DialogWrap {
public:
  TemplatePageDialog(Gtk::Window &parent, DocumentView &document_view,
                     int page_num);
  
private:
  DocumentView &document_view;
  Gtk::TreeView page_list;
  int page_num;
  
  void on_response(int response_id);
};

#endif
