///
// Copyright (C) 2002 - 2004, Fredrik Arnerup & Rasmus Kaj, See COPYING
///
#include "pagemeta.h"
#include "pageviewent.h"

Pagent* PageMeta::load(const xmlpp::Element& node, Group* parent){
  throw std::runtime_error("pagemeta can't load yet");
}

Viewent::Ref PageMeta::create_viewent(View& view, Pagent& node) {
  Viewent::Ref result(new PageViewent
		      (view, &dynamic_cast<Page&>(node)));
  result->reference();
  return result;
}

PropBase* PageMeta::getProp() {
  return 0;
}
