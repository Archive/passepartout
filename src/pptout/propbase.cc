///
// Copyright (C) 2002 - 2004, Fredrik Arnerup & Rasmus Kaj, See COPYING
///
#include "propbase.h"
#include "widget/wmisc.h"

PropBase::PropBase(const string& name)
  : Gtk::VBox(false, double_space), label(name, 0.0, 0.5, true) 
{
  set_border_width(border_width);
}

namespace {
  void hide_widget(Gtk::Widget& widget) { widget.hide(); }
};

void PropBase::set_sensitive(bool sensitive) {
  property_visible() = sensitive;
  // Hide all children, so they are not shown when we show the page
  // again. this is a workaround for a problem with Gtk: If a notebook
  // page has a Gtk::Entry and the page is hidden, then shown again,
  // the entry will be visible even if the page is not the active
  // one. I think this has to do with Gtk::Entry having its own
  // window.
  if(!sensitive)
    foreach(sigc::ptr_fun(hide_widget));
}

void PropBase::right_justify(Gtk::Label &label) {
  label.set_alignment(Gtk::ALIGN_RIGHT);
}
