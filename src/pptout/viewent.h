#ifndef PPT_VIEWENT_H		// -*- C++ -*-
#define PPT_VIEWENT_H
///
// Copyright (C) 2003, 2004, Fredrik Arnerup & Rasmus Kaj, See COPYING
///
#include "view.h"
#include "document/pagent.h"
#include "util/refcount.h"
#include <memory> // auto_ptr

/**
 * Base class for View adapters for Pagent s.  A Pagent can be visible
 * in multiple View s, then each of the View s has a Viewent for the
 * Pagent.  When a View is rescaled etc, all its Viewent s are dropped
 * and new ones created.
 */
class Viewent : public sigc::trackable, public RefCounted {
public:
  typedef Glib::RefPtr<const Gdk::Pixmap> PixmapRef;
  typedef Glib::RefPtr<Viewent> Ref;
  
  /** Returns true if this viewent is for viewing {ent} */
  bool samePagent(const Pagent* ent) { return ent == get_frame(); }
  
  virtual const Pagent* get_frame() const = 0;
  
  void set_selected(bool selected);

  /** Raise the canvas items of this Viewent to the top of the canvas
   * group of its parent view. */
  void raise_to_top();

protected:
  Viewent(View& view, Pagent* pagent);
  virtual ~Viewent();
  std::auto_ptr<Gnome::Canvas::Rect> rect; /// the frame
  std::auto_ptr<Gnome::Canvas::Group> group, content_group;
  std::auto_ptr<Gnome::Canvas::Pixbuf> state_pixbuf;

  View& view;

  enum State {NORMAL, BROKEN, MISSING, WAIT};
  /** Display the appropriate icon showing the state of the pagent
   * (or no icon if the state is NORMAL). */
  virtual void set_state(State state);

  virtual void on_geometry_changed();
  virtual void on_properties_changed();

private:

  // Undefined
  Viewent();
  Viewent(const Viewent&);
  Viewent& operator = (const Viewent&);
};

#endif
