#ifndef PRINTDIALOG_H		// -*- c++ -*-
#define PRINTDIALOG_H
///
// Copyright (C) 2002 - 2004, Fredrik Arnerup & Rasmus Kaj, See COPYING
///

#include "widget/dialogwrap.h"
#include "widget/filesel.h"
#include "widget/spinner.h"
#include <gtkmm/radiobutton.h>
#include <gtkmm/checkbutton.h>
#include <gtkmm/entry.h>
#include <gtkmm/comboboxtext.h>

class DocumentView;

class PrintDialog : public DialogWrap {
public:
  PrintDialog(Gtk::Window &parent, DocumentView &_document_view);
  void show_it();

private:
  Gtk::RadioButton using_button, file_button, 
    all_button, current_button, from_button;
  Gtk::CheckButton fonts_button, gray_button;
  Gtk::Entry using_entry;
  Gtk::ComboBoxText format_menu;
  Gtk::Widget *pages_box;
  Spinner from_spinner, to_spinner;
  DocumentView &document_view; // document_view keeps record of the document
  FileEntry file_entry;
  Filesel *filesel;

  std::string saved_file_text, saved_using_text;
  int saved_format;
  bool saved_file_not_using, saved_eps, saved_gray, saved_fonts;

  void on_response(int response_id);
  void save_state(); // in case cancel is pressed we will restore inital state
  void restore_state();
  void update(); // make sure everything that should be grayed out is
};

#endif




