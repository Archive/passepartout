#ifndef LENGTHUNITS_H		// -*- c++ -*-
#define LENGTHUNITS_H
///
// Copyright (C) 2002 - 2004, Fredrik Arnerup & Rasmus Kaj, See COPYING
///
#include "util/units.h"
#include <glibmm/ustring.h>

typedef Units<float, Glib::ustring> FUUnits;

class LengthUnits: public FUUnits {
public:
  LengthUnits();
};

// Have to rename this file ...
class AngleUnits: public FUUnits {
public:
  AngleUnits();
};

extern LengthUnits length_units;
extern AngleUnits angle_units;

#endif
