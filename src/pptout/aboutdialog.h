#ifndef ABOUTDIALOG_H		// -*- c++ -*-
#define ABOUTDIALOG_H
///
// Copyright (C) 2002 - 2004, Fredrik Arnerup & Rasmus Kaj, See COPYING
///
#include "widget/dialogwrap.h"
#include <gtkmm/aboutdialog.h>

/**
 * This is a singleton.
 */
class AboutDialog : public Gtk::AboutDialog {
public:
  static AboutDialog &instance();
private:
  AboutDialog();
  void on_response(int);
  static AboutDialog *_instance;
};

#endif
