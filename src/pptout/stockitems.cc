///
// Copyright (C) 2002 - 2004, Fredrik Arnerup & Rasmus Kaj, See COPYING
///
#include "stockitems.h"
#include <gtkmm/iconfactory.h>
#include <gtkmm/iconset.h>
#include <gtkmm/iconsource.h>
#include <gtkmm/stock.h>
#include <gtkmm/stockitem.h>
#include "icons/newframe_24.h"
#include "icons/streams_24.h"
#include "icons/moose_24.h"
#include "icons/grid_24.h"

namespace {
  void add(Glib::RefPtr<Gtk::IconFactory> factory, const guint8 *data,
           int data_size, const Glib::ustring &id) {
    Gtk::IconSource source;
    source.set_pixbuf(Gdk::Pixbuf::create_from_inline(data_size, data));
    source.set_size(Gtk::ICON_SIZE_SMALL_TOOLBAR);
    source.set_size_wildcarded();
    Gtk::IconSet set;
    set.add_source(source);
    const Gtk::StockID stock_id(id);
    factory->add(stock_id, set);
    Gtk::Stock::add(Gtk::StockItem(stock_id, ""));
  }
}

void register_stock_items() {
  const Glib::RefPtr<Gtk::IconFactory> factory = Gtk::IconFactory::create();
  add(factory, newframe_24, sizeof(newframe_24), "newframe");
  add(factory, streams_24, sizeof(streams_24), "streams");
  add(factory, moose_24, sizeof(moose_24), "moose");
  add(factory, grid_24, sizeof(grid_24), "grid");
  factory->add_default();
}
