// -*- c++ -*-
///
// Copyright (C) 2004, Fredrik Arnerup & Rasmus Kaj, See COPYING
///
#ifndef STOCKITEMS_H
#define STOCKITEMS_H

/// Register icons as stock items.
void register_stock_items();

#endif
