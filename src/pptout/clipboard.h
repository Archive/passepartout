#ifndef CLIPBOARD_H		// -*- c++ -*-
#define CLIPBOARD_H
///
// Copyright (C) 2002 - 2004, Fredrik Arnerup & Rasmus Kaj, See COPYING
///

#include "document/document.h"

/// Copy/paste selected Pagents from/to Document.
namespace Clipboard {
  void copy(DocRef document);
  void cut(DocRef document);
  void paste(DocRef document, Page *page);
}

#endif
