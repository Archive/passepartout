///
// Copyright (C) 2002 - 2004, Fredrik Arnerup & Rasmus Kaj, See COPYING
///
#include "defines.h"

#include "config.h"

#include "preferencesdialog.h"

#include <fstream>

#include "i18n.h"
#include "util/warning.h"
#include "util/filesys.h"
#include "propbase.h"
#include <glib/gi18n.h>

PFile PFile::instance = PFile();
PFile &config = PFile::instance;

PFile::PFile():
  i18n_init (Passepartout::init_i18n ()),
  ReshapeBoxSize("ReshapeBoxSize", _("Size of reshape handles")),
  DefaultResolution("DefaultResolution", _("Default image resolution")),
  ZoomLevel("ZoomLevel", _("Default page zoom level")),
  StartPage("StartPage", _("Default start page in a new document")),
  Landscape("Landscape", _("Landscape format is default")),
  FakeTransparency("FakeTransparency", _("White is transparent")),
  SingleSided("SingleSided", _("Pages are single-sided")),
  PaperName("PaperName", _("Default paper")),
  PrintCommand("PrintCommand", _("Printing command")),
  StylesheetPath("StylesheetPath", _("Path to look for XSLT files in")),
  DefaultStylesheet("DefaultStylesheet", _("Default text stylesheet")),
  DocTemplatePath("DocTemplatePath", _("Default document template")),
  PSInterpreter("PSInterpreter", _("Command to run Ghostscript")),
  LengthUnit("LengthUnit", _("Default unit for measuring lengths")),
  dialog (0),
  first_error(true)
{
  // set default values
  
  declare_var(&ReshapeBoxSize);
  ReshapeBoxSize.visible = false; //Don't show up in preferences dialog
  ReshapeBoxSize.values.push_back(7);

  declare_var(&DefaultResolution);
  DefaultResolution.values.push_back(72);

  declare_var(&ZoomLevel);
  ZoomLevel.visible = false;//Option not implemented yet=>nothing to control
  ZoomLevel.values.push_back(50);

  declare_var(&StartPage);
  StartPage.values.push_back(1);

  declare_var(&Landscape);
  Landscape.visible = false;
  Landscape.values.push_back(false);

  declare_var(&FakeTransparency);
  FakeTransparency.visible = false;
  FakeTransparency.values.push_back(true);

  declare_var(&SingleSided);
  SingleSided.visible = false;
  SingleSided.values.push_back(false);
    
  declare_var(&PaperName);
  PaperName.values.push_back("A4");

  declare_var(&PrintCommand);
  PrintCommand.values.push_back("lpr");

  declare_var(&StylesheetPath);
  StylesheetPath.values.push_back(XMLPATH);

  declare_var(&DefaultStylesheet);
  DefaultStylesheet.values.push_back("");
  declare_var(&DocTemplatePath);
  DocTemplatePath.values.push_back("");

  declare_var(&PSInterpreter);
  PSInterpreter.values.push_back("gs");

  declare_var(&LengthUnit);
  LengthUnit.values.push_back("pt");

  // Try to get the $HOME location
  std::string home = Glib::get_home_dir();
  user_file = home.empty() ? "" : home + "/.pptout";
  if(home.empty())
    verbose << "Couldn't find your home directory. "
      "Try changing the $HOME environment variable!" << std::endl;
  global_file = "/etc/pptout";
}

PFile::~PFile() {
  // Evil! All your prior settings are belongs to us ...
  if(!user_file.empty())
    write(user_file);
  if (dialog)
    delete dialog;
}

void PFile::read() {
  if(!user_file.empty() && access(user_file)) {
    debug << "Reading configuration from " << user_file << std::endl;
    Config::File::read(user_file);
  } 
  else {
    if(access(global_file)) {
      debug << "Reading configuration from " << global_file << std::endl;
      Config::File::read(global_file);
    }
    else
      debug << "No configuration file found" << std::endl;
    debug << "Creating configuration file " << user_file << std::endl;
    if(!user_file.empty())
      write(user_file);
  }
}

void PFile::write(const std::string &filename) {
  using std::endl;
  std::ofstream out(filename.c_str());
  if(!out) {
    error("Could not write to \"" + filename + '"');
    return;
  }
  for(Config::Vars::const_iterator i = vars.begin(); i != vars.end(); i++) {
    if(GuiVar *guivar = dynamic_cast<GuiVar*>(*i))
      out << "# " << guivar->desc << endl;
    out << (*i)->name << " = "; 
    (*i)->print(out);
    out << endl << endl;
  }
}

Gtk::Dialog &PFile::get_dialog() {
  if(dialog)
    return *dialog;

  dialog = new PreferencesDialog();
  // FIXME: just use Gtk::Dialog.run
  dialog->signal_hide().connect(sigc::mem_fun(*this, &PFile::dialog_done));
  return *dialog;
}

void PFile::dialog_show (Gtk::Window* const toplevel) {
  get_dialog(); // create it if necessary
  if(dialog)
    {
      dialog->set_transient_for (*toplevel);
      dialog->show_all();
    }
}


void PFile::dialog_done() {
  if(dialog) {
    delete dialog;
    dialog = 0;
  }
}

void PFile::error(const std::string &message) {
  if(first_error) {
    warning << "Errors in config file:" << std::endl;
    first_error = false;
  }
  warning << message << std::endl;
}
