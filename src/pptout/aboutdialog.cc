///
// Copyright (C) 2002 - 2004, Fredrik Arnerup & Rasmus Kaj, See COPYING
///
#include "aboutdialog.h"
#include "defines.h" // PACKAGE_VERSION

#include <glib/gi18n.h>
#include "widget/wmisc.h"
#include "widget/programs.h"
#include <gtkmm/label.h>
#include <gtkmm/image.h>
#include <gtkmm/box.h>
#include <gtkmm/stock.h>
#include <gtkmm/notebook.h>
#include <gtkmm/scrolledwindow.h>
#include "authors.h"
#include "icons/logo.h"

AboutDialog *AboutDialog::_instance = 0;

extern const char *builddate;  
// The date this program was linked.
// Defined in builddate.cc.

namespace {
  const std::string mail_address = "passepartout@stacken.kth.se";

#ifdef HAVE_GNOME
  bool have_gnome = true;
#else
  bool have_gnome = false;
#endif

}

namespace {
  void about_uri_cb (Gtk::AboutDialog& about,
		     const Glib::ustring& uri)
  {
    Programs::open_url (uri);
  }
};

AboutDialog::AboutDialog()
//  : DialogWrap("About")
{
  std::vector<Glib::ustring> documenters;
  documenters.push_back ("Fredrik Arnerup");
  set_authors (authors);
  set_comments (_("A DTP application for GNOME") + std::string("\n") +
		Glib::ustring::compose (_("GNOME support: %1"),
					have_gnome ? _("enabled") : _("disabled")));
  set_copyright (_("Copyright (c) 2002-2005,2007,2009"));
  set_documenters (documenters);
  // FIXME: the size looks fishy
  set_logo (Gdk::Pixbuf::create_from_inline(69624, logo));
  // FIXME: set_mailing_list (mail_address);
  set_name (_("Passepartout"));
  set_translator_credits (_("translator-credits"));
  set_version (PACKAGE_VERSION);
  set_url_hook (ptr_fun(about_uri_cb));
  set_website ("http://www.stacken.kth.se/project/pptout/");
}

AboutDialog &AboutDialog::instance() {
  if(!_instance)
    _instance = new AboutDialog(); 
  return *_instance;
}

void AboutDialog::on_response(int response_id) {
  hide();
}
