#ifndef DOCMETA_H		// -*- c++ -*-
#define DOCMETA_H
///
// Copyright (C) 2002 - 2004, Fredrik Arnerup & Rasmus Kaj, See COPYING
///
#include <glibmm/ustring.h>
#include "document/document.h"

/** Holds a Document together with data that is not part of the
 * Document.  The data is reference counted, so there is only one
 * instance of the data per Document. */
class DocMeta {
public:
  explicit DocMeta(DocRef _document,
		   const Glib::ustring &filename = Glib::ustring());

  DocRef get_document() const { return document; }
  Document* operator->() const { return document.operator->(); }

  /** Returns a signal that is emitted when the data has changed. */
  sigc::signal<void>& changed_signal();

  const Glib::ustring& get_filename() const { return data->filename; }
  void set_filename(const Glib::ustring &filename);

  operator bool() const { return document; }

private:
  class DocData: public RefCounted {
  public:
    sigc::signal<void> changed_signal;
    Glib::ustring filename;
  };
  Glib::RefPtr<DocData> data;
  DocRef document;

  // unused
  DocMeta();
};

#endif
