///
// Copyright (C) 2002 - 2004, Fredrik Arnerup & Rasmus Kaj, See COPYING
///
#include "textmeta.h"
#include "streamdialog.h"
#include "textviewent.h"
#include "widget/wmisc.h"
#include "widget/spinner.h"
#include "widget/subpanel.h"
#include "document/textframe.h"
#include "document/document.h"

#include <gtkmm/separator.h>
#include <gtkmm/sizegroup.h>
#include <sigc++/bind.h>
#include <glib/gi18n.h>

Pagent* TextMeta::load(const ElementWrap& xml, Group* parent) {
  return new TextFrame(xml, parent);
}

Viewent::Ref TextMeta::create_viewent(View& view, Pagent& node) {
  Viewent::Ref result(new TextViewent
		      (view, &dynamic_cast<TextFrame&>(node), 2));
  result->reference();
  return result;
}

class PropText : public GenericProp<TextFrame> {
public:
  PropText()
    : GenericProp<TextFrame>(_("Te_xt")),
     e_columns(1, false), e_gutter(0, true, &length_units)
  {
    Glib::RefPtr<Gtk::SizeGroup>  sizegroup = 
      Gtk::SizeGroup::create(Gtk::SIZE_GROUP_HORIZONTAL);
    
    Gtk::Box *line = manage(new Gtk::HBox(false, single_space));
    Gtk::Label *label = manage(new Gtk::Label(_("_Stream:"), 0.0, 0.5, true));
    line->pack_start(*label, Gtk::PACK_SHRINK);
    label->set_mnemonic_widget(e_stream);
    line->pack_start(e_stream, Gtk::PACK_EXPAND_WIDGET);
    e_stream.signal_changed().connect
      (bind(sigc::mem_fun(*this, &PropText::on_change), STREAM));
    Gtk::Button *b_streams = manage(new Gtk::Button(_("E_dit streams"), true));
    b_streams->signal_clicked().connect
      (sigc::mem_fun(StreamDialog::instance(), &StreamDialog::show_raise));
    line->pack_start(*b_streams, Gtk::PACK_SHRINK);
    pack_start(*line, Gtk::PACK_SHRINK);
    
    SubPanel *box = manage(new SubPanel(_("Columns")));
    line = manage(new Gtk::HBox(false, double_space));
    line->pack_start(*(label = manage(new Gtk::Label(_("Number of _columns:"),
						     0.0, 0.5, true))),
		     Gtk::PACK_SHRINK);
    sizegroup->add_widget(*label);
    label->set_mnemonic_widget(e_columns);
    e_columns.limits(1, 64);	// Note: The upper limit is rather arbitrary.
    e_columns.signal_value_changed().connect
      (bind(sigc::mem_fun(*this, &PropText::on_change), COLUMNS));
    line->pack_start(e_columns, Gtk::PACK_SHRINK);
    box->pack_start(*line);
    
    line = manage(new Gtk::HBox(false, double_space));
    line->pack_start(*(label = manage(new Gtk::Label(_("_Gutter width:"),
						     0.0, 0.5, true))),
		     Gtk::PACK_SHRINK);
    sizegroup->add_widget(*label);
    label->set_mnemonic_widget(e_gutter);
    line->pack_start(e_gutter, Gtk::PACK_SHRINK);
    e_gutter.signal_value_changed().connect
      (bind(sigc::mem_fun(*this, &PropText::on_change), GUTTER));
    box->pack_start(*line);
    pack_start(*box, Gtk::PACK_SHRINK);
    
    // perhaps this should go into the StreamMenu class
    Document::streams_changed_signal.connect
      (sigc::mem_fun(*this, &PropText::on_streams_changed));

    set_sensitive(false);
    update();
  }
  
  void update() {
    if(object) {
      //The text stream of a text frame may be null.
      //This will happen, for instance, when the stream 
      //associated with the frame is deleted.
      if(object->get_stream()) 
	e_stream.update(Document::containing(*object).self(),
			object->get_stream()->get_name());
      else
	e_stream.update(Document::containing(*object).self());
      e_columns.set(object->get_num_columns());
      e_gutter.set(object->get_gutter());
    } else {
      e_stream.update(Document::null());
    }
  }
  
  void on_streams_changed(DocRef doc) {
    if(object && doc == Document::containing(*object).self())
      update();
  }

  enum ChangeId { STREAM, COLUMNS, GUTTER, };

  void on_change(ChangeId what) {
    if(object) switch(what) {
    case STREAM:
      object->set_stream(Document::containing(*object)
			 .get_text_stream(e_stream.get_stream()));
      break;

    case COLUMNS:
      object->set_num_columns(int(e_columns.get()));
      break;
      
    case GUTTER:
      object->set_gutter(e_gutter.get());
      break;
    }
  }
private:
  Gtk::HSeparator hsep;
  StreamMenu e_stream;
  Spinner e_columns, e_gutter;
};

PropBase* TextMeta::getProp() {
  return new PropText;
}

