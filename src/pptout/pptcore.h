#ifndef PPTCORE_H		// -*- c++ -*-
#define PPTCORE_H
///
// Copyright (C) 2002 - 2004, Fredrik Arnerup & Rasmus Kaj, See COPYING
///
#include "metabase.h"
#include <map>

/**
 * The core of Passepartout.
 * The main goal of PptCore is to provide the meta classes as needed for
 * loading data, managing properties, etc.
 */
class PptCore {
public:
  typedef std::map<std::string, MetaBase*> MetaMap;
  
  PptCore();
  MetaBase* getMeta(const std::string& type);
  
  MetaMap::const_iterator m_begin() { return meta.begin(); }
  MetaMap::const_iterator m_end() { return meta.end(); }
  
private:
  MetaMap meta;
};

extern PptCore core;

#endif
