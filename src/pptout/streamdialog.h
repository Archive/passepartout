#ifndef STREAMDIALOG_H		// -*- c++ -*-
#define STREAMDIALOG_H
///
// Copyright (C) 2002 - 2004, Fredrik Arnerup & Rasmus Kaj, See COPYING
///
#include "widget/filesel.h"
#include "widget/dialogwrap.h"
#include <gtkmm/entry.h>
#include <gtkmm/comboboxtext.h>
#include <gtkmm/menu.h>
#include <gtkmm/treeview.h>
#include <gtkmm/liststore.h>
#include "document/document.h"

class TextStream;

/**
 * List of Streams and their properties.
 * This Dialog lets the user select what data file and which transform to
 * apply for a specific stream.
 * This is a Singleton.
 */
class StreamDialog: public UtilityWindow {
public:
  static StreamDialog &instance();
  void show_raise(); // Make sure this dialog is visible
  void set_document(DocRef document_);
  
private:
  static StreamDialog *_instance;
  Glib::RefPtr<Gtk::ListStore> main_model, param_model;
  Gtk::TreeView stream_list, param_list;
  
  Gtk::Widget *propsbox, *param_box;
  
  Gtk::Entry name_entry;
  FileEntry file_entry;
  FileEntry xfrm_entry;
  
  DocRef document;
  TextStream* current_stream;

  StreamDialog();
  // no default constructors:
  StreamDialog(const StreamDialog&);
  void operator = (const StreamDialog&);
  // no destructor
  ~StreamDialog();

  Glib::ustring current_selection;  // Last known stream selection.
  
  /// \todo get rid of this:
  void _update(DocRef document_); // called when the document streams 
                                     // have changed in any way
  void update();

  Gtk::Button* fix_button(Gtk::Button* button, int action_id);

  void on_response(int response_id);
  void update_params(TextStream*);
  void on_selection_changed();
  void on_param_row_changed(const Gtk::TreeModel::Path& path,
			    const Gtk::TreeModel::iterator& iter);
};

/**
 * An menu for selecting streams.
 */
class StreamMenu: public Gtk::ComboBoxText {
public:
  StreamMenu();
  
  // select_name must be an existing name in the document or an empty
  // string, which means no stream
  void update(DocRef document = Document::null(), 
	      const std::string &select_name = "");

  std::string get_stream() const;
};

class DocumentView;

/**
 * Dialog for creating a text frame using a new or existing stream.
 */
class TextFrameDialog: public DialogWrap {
public:
  TextFrameDialog(Gtk::Window &parent, DocumentView &_view);
  void show_raise();

private:
  DocumentView &view;
  StreamMenu streams;
  Gtk::RadioButton *old_button, *new_button;
  Gtk::Box *new_box;
  Gtk::Entry name_entry;
  FileEntry file_entry;
  FileEntry xfrm_entry;

  void on_response(int response_id);
};

#endif
