#ifndef ICONS_H
#define ICONS_H

#include <glib.h>

G_BEGIN_DECLS

extern char* broken_xpm[];
extern char* missing_xpm[];
extern char* wait_xpm[];

G_END_DECLS

#endif /* ICONS_H */
