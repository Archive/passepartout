#ifndef ERRORDIALOG_H		// -*- c++ -*-
#define ERRORDIALOG_H
///
// Copyright (C) 2002 - 2004, Fredrik Arnerup & Rasmus Kaj, See COPYING
///
#include <glibmm/ustring.h>

/// Initializes the global handler for exceptions that propagate into gtkmm
void init_exception_handler();

class MsgDialog;

/// This is a singleton
class ErrorDialog {
public:
  static ErrorDialog& instance();

  void show_error(const Glib::ustring& msg1, const Glib::ustring& msg2);

  void show_warning(const Glib::ustring& msg1, const Glib::ustring& msg2);
  
private:
  static ErrorDialog* _instance;
  MsgDialog *error_dialog, *warning_dialog;

  ErrorDialog();
  ErrorDialog(const ErrorDialog&);
  void operator = (const ErrorDialog &);
  void on_response(int response_id);
};

#endif
