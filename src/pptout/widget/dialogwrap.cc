///
// Copyright (C) 2003, 2004, Fredrik Arnerup & Rasmus Kaj, See COPYING
///
#include "dialogwrap.h"

DialogWrap::DialogWrap(const Glib::ustring& name)
  : Gtk::Dialog(name + " - Passepartout",
		false /* modal */, false /* separator */)
{}

DialogWrap::DialogWrap(const Glib::ustring& name, Gtk::Window& parent)
  : Gtk::Dialog(name + " - Passepartout",
		parent, true /* modal */, false /* separator */)
{}

void DialogWrap::show_raise() { present(); }

Gtk::Button* DialogWrap::fix_button(Gtk::Button* button, int action_id) {
  Gtk::Button *b = manage(button);
  // Note: It seems that activate means "in any way except a simple click".
  // So we have to add callbacks for both activate and clicked.
  b->signal_clicked().connect
    (bind(sigc::mem_fun(*this, &DialogWrap::on_response), action_id));
  b->signal_activate().connect
    (bind(sigc::mem_fun(*this, &DialogWrap::on_response), action_id));
  return b;
}

UtilityWindow::UtilityWindow(const Glib::ustring& name) {
  set_type_hint(Gdk::WINDOW_TYPE_HINT_UTILITY);
  set_title(name + " - Passepartout");
}

void UtilityWindow::show_raise() { present(); }
