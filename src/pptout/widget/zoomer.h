// -*- c++ -*-
///
// Copyright (C) 2004, Fredrik Arnerup & Rasmus Kaj, See COPYING
///
#ifndef ZOOMER_H
#define ZOOMER_H

#include <gtkmm/widget.h>

class ZoomerRep;

/// Zoom widget wrapper.
class Zoomer {
public:
  sigc::signal<void, float> signal_changed;

  Zoomer(float factor = 1.0);
  ~Zoomer();
  void set_factor(float factor);
  float get_factor() const;
  Gtk::Widget& get_widget();

private:
  ZoomerRep *rep;
};

#endif
