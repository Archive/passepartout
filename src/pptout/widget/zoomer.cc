///
// Copyright (C) 2004, Fredrik Arnerup & Rasmus Kaj, See COPYING
///
#include "zoomer.h"
#include "spinner.h"

struct ZoomerRep {
  Spinner spinner;
  
  ZoomerRep(float factor)
    : spinner(100.0 * factor, false, 0, "", 5, 10000)
  {}
};

namespace {
  void on_value_changed(Zoomer *zoomer) {
    zoomer->signal_changed(zoomer->get_factor());
  }
}

Zoomer::Zoomer(float factor) : rep(new ZoomerRep(factor)) {
  rep->spinner.signal_value_changed().connect
    (sigc::bind(sigc::ptr_fun(&on_value_changed), this));
}

Zoomer::~Zoomer() { delete rep; }

void Zoomer::set_factor(float factor) { rep->spinner.set(100.0 * factor); }

float Zoomer::get_factor() const { return rep->spinner.get() / 100.0; }

Gtk::Widget& Zoomer::get_widget() { return rep->spinner; }
