#ifndef PPT_W_MISC_H		// -*- c++ -*-
#define PPT_W_MISC_H
///
// Copyright (C) 2002 - 2004, Fredrik Arnerup & Rasmus Kaj, See COPYING
///
// Miscellaneous constats for widgets / HIG compliance

extern const unsigned int single_space;// = 6;
extern const unsigned int double_space;// = 2 * single_space;
extern const unsigned int triple_space;// = 3 * single_space;
extern const unsigned int border_width;// = double_space;

#endif

