#ifndef USERERROR_H		// -*- c++ -*-
#define USERERROR_H
///
// Copyright (C) 2002 - 2004, Fredrik Arnerup & Rasmus Kaj, See COPYING
///
#include <glibmm/ustring.h>
#include <stdexcept>

/**
 * An error that we can present to the user without beeing ashamed.
 * It might be caused by the user doing something strange, or by strange
 * contents in a file, etc.  Any exception that is not a UserError and reaches
 * the main exception handler is an internal error.
 * We should try to not only tell the user what happend, but why and / or how
 * to fix or avoid it.
 */
class UserError : public std::runtime_error {
public:
  typedef Glib::ustring ustring;
  /**
   * Create a new UserError.  The ustring parameters should accept pango
   * markup, while the exception parameter should protect special characters.
   * \param msg the main message for the exception.
   * \param cause might be a submessage about what caused the exception or a
   * helpfull message about how to avoid it.
   */
  UserError(const ustring& msg, const ustring& cause);
  UserError(const ustring& msg, const std::exception& cause);
  ~UserError() throw() {}
  
  const ustring& get_message() const { return msg_; }
  const ustring& get_cause() const { return cause_; }
private:
  ustring msg_, cause_;
};

#endif
