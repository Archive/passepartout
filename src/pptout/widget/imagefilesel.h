// -*- c++ -*-
///
// Copyright (C) 2002 - 2004, Fredrik Arnerup & Rasmus Kaj, See COPYING
///
#ifndef IMAGEFILESEL_H
#define IMAGEFILESEL_H

#include "filesel.h"
#include "spinner.h"
#include <gtkmm/image.h>

using Glib::ustring;

class ImageFilesel: public Filesel {
 public:
  /** A file selection dialog with a preview feature, and the ability
   * to specify an image resolution. 
   * \param default_res Default resolution.
   * \param unit_name Name of unit for giving image size in.
   * \param unit_factor Factor for converting to base unit.
   */
  ImageFilesel(Gtk::Window &parent, const ustring& title,
	       float default_res, 
	       const ustring& unit_name, float unit_factor);
  float get_res() const { return res.get(); }
  
 private:
  // maximum size of preview
  static const int maxwidth, maxheight;
  Spinner res; // resolution;
  struct { ustring name;  float factor; } unit; 
  Gtk::Image preview;
  struct { int width, height; } image_size;
  Gtk::Label info;
  Glib::RefPtr<Gdk::Pixbuf> pixbuf;
  Glib::Dispatcher signal_loaded;
  void start_loader();
  void loader_thread();
  void update_preview();
  void update_size();
};

#endif 
