///
// Copyright (C) 2002 - 2004, Fredrik Arnerup & Rasmus Kaj, See COPYING
///
#include "usererror.h"
#include "util/stringutil.h"
#include <sstream>

UserError::UserError(const ustring& msg, const ustring& cause)
  : runtime_error("User error: " + msg),
    msg_(msg), cause_(cause)
{}

UserError::UserError(const ustring& msg, const std::exception& err)
  : runtime_error("User error: " + msg),
    msg_(msg), cause_(to_xml(err.what()))
{}
