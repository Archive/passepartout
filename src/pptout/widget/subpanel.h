#ifndef PPT_W_SUBPANEL_H	// -*- c++ -*-
#define PPT_W_SUBPANEL_H
///
// Copyright (C) 2002 - 2004, Fredrik Arnerup & Rasmus Kaj, See COPYING
///
#include <gtkmm/box.h>

/**
 * A part of a panel (mainly in dialogs) conststing of a heading label and
 * some indented content.  Mainly so we don't have to create lots of boxes and
 * labels by hand when we do as suggested in the HIG.
 *
 * Note: Only some of the Box methods that probably should be overrided is
 * overrided, and even them are not virtual, so e.g. a Box pointer cant be
 * used to add subwidgets to a SubPanel.
 */
class SubPanel : public Gtk::VBox {
public:
  /** Create a new SubPanel with the specified label. */
  SubPanel(const Glib::ustring& name);
  
  void pack_start(Gtk::Widget& child, bool expand, bool fill, guint padding=0);
  void pack_start(Gtk::Widget& child,
		  Gtk::PackOptions options=Gtk::PACK_EXPAND_WIDGET,
		  guint padding=0);
  void pack_end(Gtk::Widget& child, bool expand, bool fill, guint padding=0);
  void pack_end(Gtk::Widget& child,
		Gtk::PackOptions options=Gtk::PACK_EXPAND_WIDGET,
		guint padding=0);
private:
  Gtk::VBox inner_box;
};

#endif

