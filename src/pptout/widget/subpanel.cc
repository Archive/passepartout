///
// Copyright (C) 2003, 2004, Fredrik Arnerup & Rasmus Kaj, See COPYING
///
#include "subpanel.h"
#include <gtkmm/label.h>

extern int double_space;

SubPanel::SubPanel(const Glib::ustring& title)
  : Gtk::VBox(false, double_space),
    inner_box(false, double_space)
{
  Gtk::Label *label = manage(new Gtk::Label("<b>" + title + "</b>", 
					    0.0, 0.5));
  label->set_use_markup();
  Box::pack_start(*label, Gtk::PACK_SHRINK);

  Gtk::Box *box2 = manage(new Gtk::HBox(false, double_space));
  box2->pack_start(*manage(new Gtk::Label("    ")), Gtk::PACK_SHRINK, 0);
  box2->pack_start(inner_box);
  Box::pack_start(*box2);
}

void SubPanel::pack_start(Gtk::Widget& child, bool expand, bool fill,
			  guint padding) {
  inner_box.pack_start(child, expand, fill, padding);
}
void SubPanel::pack_start(Gtk::Widget& child, Gtk::PackOptions options,
			  guint padding) {
  inner_box.pack_start(child, options, padding);
}

void SubPanel::pack_end(Gtk::Widget& child, bool expand, bool fill,
			guint padding) {
  inner_box.pack_end(child, expand, fill, padding);
}
void SubPanel::pack_end(Gtk::Widget& child, Gtk::PackOptions options,
			guint padding) {
  inner_box.pack_end(child, options, padding);
}
