///
// Copyright (C) 2004, Fredrik Arnerup & Rasmus Kaj, See COPYING
///
#include "programs.h"
#include "usererror.h"
#include "defines.h" // PACKAGE_VERSION, HAVE_GNOME
#include "util/warning.h"
#include "util/filesys.h"

#ifdef HAVE_GNOME
#include <libgnome/libgnome.h>
#include <libgnomevfs/gnome-vfs-mime-handlers.h>
#endif

namespace {
  const std::string browser = "xdg-open";
}

void Programs::open_url(const std::string &url) {
#ifdef HAVE_GNOME
  GError *error = 0;
  if(!gnome_url_show(url.c_str(), &error))
    throw UserError("Failed to run web browser \"" + browser + '"',
                    error->message);
#else
  try {
    Glib::spawn_command_line_async(browser + " \"" + url + "\"");
  } catch(const Glib::SpawnError& e) {
    throw UserError("Failed to run web browser \"" + browser + '"', e.what());
  }
#endif
}

void Programs::open_docs() {
  try
    {
      open_url ("ghelp:passepartout");
      return;
    }
  catch (UserError& e)
    {
      /* ignore and continue */
    }
  std::string url = DOCDIR "/passepartout.html";
  if(access(url))
    url = "file://" + url;
  else
    url = "http://www.stacken.kth.se/project/pptout/doc/" 
      + std::string(PACKAGE_VERSION)
      + "/passepartout.html";
  open_url(url);
}

namespace {
  void run_command(const std::string cmd, const std::string filename,
                   bool requires_terminal) {
    std::string full_cmd =
      (requires_terminal ? "xterm -e " : "")
      + cmd + " " + filename;
    verbose << "Running: " << full_cmd << std::endl;
    Glib::spawn_command_line_async(full_cmd);
  }
}

void Programs::populate_file_handler_menu(Gtk::Menu &menu,
                                          const std::string &filename,
                                          const std::string &mimetype) {
#ifdef HAVE_GNOME
  using namespace Gtk;
  using namespace Menu_Helpers;
  MenuList& items = menu.items();
  GList *list =
    gnome_vfs_mime_get_short_list_applications(mimetype.c_str());
  if(!list)
    throw std::runtime_error("No applications handling "  + mimetype);
  for(; list; list = list->next) {
    GnomeVFSMimeApplication *app =
      static_cast<GnomeVFSMimeApplication*>(list->data);
    items.push_back
      (MenuElem(app->name,
                sigc::bind(sigc::ptr_fun(&run_command),
                           app->command,
                           filename,
                           app->requires_terminal)));
  }
#else
  throw std::runtime_error("No Gnome support");
#endif
}

void Programs::open_postscript_file(const std::string &filename,
                                    bool delete_when_done) {
  // try to run gv (or whatever you want to use instead)
  std::string psviewer = "";
#ifdef HAVE_GNOME
  GnomeVFSMimeApplication *app =
    gnome_vfs_mime_get_default_application("application/postscript");
  psviewer = app->command;
#endif
  if(psviewer.empty())
    const std::string psviewer = "gv -safer "; //config.PSViewer.values.front();
  // Removing the file after the viewer has terminated. However,
  // nowadays many apps use ipc to prevent the user from starting
  // more than one instance of the program, so the program might
  // just tell the running instance to open the file and then exit
  // immediately. Perhaps each document should have its own tmp file
  // that lives as long as the document?
  std::string cmd = "sh -c '" + psviewer + " " + filename;
  if(delete_when_done)
    cmd += " ; rm " + filename;
  cmd += "'";
  verbose << cmd << std::endl;
  Glib::spawn_command_line_async(cmd);
}
