#ifndef PPT_W_DIALOGWRAP_H		// -*- c++ -*-
#define PPT_W_DIALOGWRAP_H
///
// Copyright (C) 2002 - 2004, Fredrik Arnerup & Rasmus Kaj, See COPYING
///
#include <gtkmm/dialog.h>

/** A base class for creating uniform dialog windows. */
class DialogWrap : public Gtk::Dialog {
protected:
  /** Create a non-modal dialog */
  DialogWrap(const Glib::ustring& name);

  /** Create a modal dialog */
  DialogWrap(const Glib::ustring& name, Gtk::Window& parent);

  /**
   * Show and raise the dialog, so it is surely visible.
   * Subclasses might override this to make sure the dialog is up to date.
   */
  virtual void show_raise();

  /**
   * Hook in a button in the same callback system as buttons created by
   * {add_button()}.
   */
  Gtk::Button* fix_button(Gtk::Button* button, int action_id);
};

/** A base class for creating uniform utility windows. The difference
 *  between a utility window and a dialog is that a utility window
 *  never is modal and doesn't have action buttons at the bottom. */
class UtilityWindow : public Gtk::Window {
protected:
  UtilityWindow(const Glib::ustring& name);

  /** Show and raise the window, so it is surely visible. */
  virtual void show_raise();
};

#endif
