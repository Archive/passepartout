// -*- c++ -*-
///
// Copyright (C) 2002 - 2004, Fredrik Arnerup & Rasmus Kaj, See COPYING
///
#ifndef W_SPINNER_H		
#define W_SPINNER_H
#include <gtkmm/box.h>
#include <gtkmm/spinbutton.h>
#include <gtkmm/comboboxtext.h>
#include <memory>
#include "pptout/lengthunits.h"

class UnitAdjustment;

/** A spinbutton with an optional unit menu. */
class Spinner : public Gtk::HBox {
public:
  Spinner(const float _value = 0, bool allow_float = true, 
	  const FUUnits *_units = 0, 
	  const Glib::ustring &default_unit = Glib::ustring(),
	  const float _lo_value = -8000, 
	  const float _hi_value = 8000);
  ~Spinner();
  
  void limits(float low, float high);

  void set(float _value);
  float get() const;

  Gtk::SpinButton &get_spinbutton() const { return *spinbutton; }

  Glib::SignalProxy0<void> signal_value_changed();

protected:
  /** Direct focus to the proper part of the Spinner. */
  bool on_mnemonic_activate(bool group_cycling);
  
private:
  Gtk::SpinButton *spinbutton;
  Gtk::ComboBoxText *unitmenu;
  std::auto_ptr<UnitAdjustment> value;
  const FUUnits *units;

  void on_unit_select();

  // Undefined
  Spinner(const Spinner&);
  Spinner& operator = (const Spinner&);
};

#endif
