#ifndef PROGRAMS_H		// -*- c++ -*-
#define PROGRAMS_H
///
// Copyright (C) 2004, Fredrik Arnerup & Rasmus Kaj, See COPYING
///

#include <string>
#include <gtkmm/menu.h>

/// Calling other programs for handling URLs and mime types

extern const std::string homepage;

namespace Programs {
  void open_url(const std::string&);
  void open_docs();

  /// Add items for calling external programs (based on mime type) on
  /// a given file to an existing menu. Throws an exception if it
  /// fails to add any items.
  void populate_file_handler_menu(Gtk::Menu &menu, const std::string &filename,
                                  const std::string &mimetype);

  /// Open a postscript file in an external viewer.
  /// \param delete_when_done The file is deleted when the viewer terminates.
  void open_postscript_file(const std::string &filename,
                            bool delete_when_done);
}

#endif
