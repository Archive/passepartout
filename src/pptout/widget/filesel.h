// -*- c++ -*-
///
// Copyright (C) 2002 - 2004, Fredrik Arnerup & Rasmus Kaj, See COPYING
///
#ifndef FILESEL_H
#define FILESEL_H

#include <gtkmm/filechooserdialog.h>
#include <gtkmm/comboboxentrytext.h>
#include <gtkmm/entry.h>
#include <memory>

class Filesel;

/**
 * A combo box that adds what you type in the entry to a history list
 * in the pop-down menu. Also the cursor is moved to the end when the
 * text is set.
 */
class EndEntry: public Gtk::ComboBoxEntryText {
public:
  EndEntry(int _history_max = 5);
  void set_text(const Glib::ustring &text, bool remember_text = false);
  Glib::ustring get_text(bool remember_text = true);
  Gtk::Entry& get_entry();
  void remember(); // add current text to history
private:
  unsigned int history_max;
  typedef std::list<Glib::ustring> History;
  History history;
};

/**
 * A text entry with a button that shows a file selection dialog.
 */
class FileEntry: public Gtk::HBox {
public:
  EndEntry entry;
  FileEntry(const Glib::ustring& window_title_, 
	    const Glib::ustring& default_path_ = "", 
            Gtk::FileChooserAction action = Gtk::FILE_CHOOSER_ACTION_OPEN);

  const Glib::ustring& get_default_value(); 
  //set entry text to value. default_path is still going to be used 
  void set_default_value(const Glib::ustring &value);

protected:
  /** Direct focus to the proper part of the Spinner. */
  bool on_mnemonic_activate(bool group_cycling);

private:
  Glib::ustring window_title, default_path, maybe_default_value; 
  Glib::ustring *default_value;
  Gtk::Button button;
  std::auto_ptr<Filesel> filesel;
  Gtk::FileChooserAction action;

  void filesel_done();
  void show_filesel();
};

/**
 * A file selection dialog with response handlers connected to the
 * ok/cancel buttons.
 */
class Filesel: public Gtk::FileChooserDialog {
public:
  Filesel(Gtk::Window &parent, const Glib::ustring& title,
          Gtk::FileChooserAction action = Gtk::FILE_CHOOSER_ACTION_OPEN);
  bool was_cancelled() const { return cancelled; }
  
private:
  void on_response(int response_id);
  bool cancelled;
};

#endif
