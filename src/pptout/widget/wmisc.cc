///
// Copyright (C) 2003, 2004, Fredrik Arnerup & Rasmus Kaj, See COPYING
///
#include "wmisc.h"

const unsigned int single_space = 6;
const unsigned int double_space = 2 * single_space;
const unsigned int triple_space = 3 * single_space;
const unsigned int border_width = double_space;
