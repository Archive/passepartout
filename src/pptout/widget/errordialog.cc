///
// Copyright (C) 2002 - 2004, Fredrik Arnerup & Rasmus Kaj, See COPYING
///
#include "errordialog.h"
#include "usererror.h"
#include "util/stringutil.h"
#include <glibmm/exception.h>
#include <gtkmm/messagedialog.h>
#include <gtkmm/label.h>
#include <gtkmm/stock.h>
#include "wmisc.h" // double_space

namespace {
  void exception_handler() {
    try {
      throw;			// rethrow the causing exception
      
    } catch(const UserError& err) {
      // A UserError is any error the user should be able to handle.
      ErrorDialog::instance().show_error(err.get_message(), err.get_cause());
      
    } catch(const std::exception& err) {
      // All other exceptions that reaches this handler are internal errors.
      ErrorDialog::instance().show_error("Internal error", err.what());
      
    } catch(const Glib::Exception& err) {
      // Glib:.Exception is not a std::exception (why?)
      ErrorDialog::instance().show_error("Internal error (glib)", err.what());
      
    } catch(...) {
      ErrorDialog::instance().show_error("Unknown internal error", "Sorry.");
    }
  }
  
}

void init_exception_handler() {
  Glib::add_exception_handler(&exception_handler);
}

class MsgDialog : public Gtk::Dialog {
public:
  MsgDialog(Gtk::MessageType type)
    : Gtk::Dialog(std::string(type == Gtk::MESSAGE_ERROR ? "Error" :"Warning")
                  + " - Passepartout",
                  true /* modal */, false /* separator */)
  {
    set_resizable(false);
    Gtk::Box *box = manage(new Gtk::HBox(false, double_space));
    box->set_border_width(border_width);
    get_vbox()->pack_start(*box);
    
    box->pack_start(*manage(new Gtk::Image((type == Gtk::MESSAGE_ERROR)
                                           ? Gtk::Stock::DIALOG_ERROR
                                           : Gtk::Stock::DIALOG_WARNING,
                                           Gtk::ICON_SIZE_DIALOG)),
                    Gtk::PACK_SHRINK);
    label.set_selectable();
    box->pack_start(label, Gtk::PACK_SHRINK);
    add_button(Gtk::Stock::OK, 0);
  }
  void show_message(const Glib::ustring &msg1, const Glib::ustring &msg2) {
    label.set_markup("<b>" + to_xml(msg1) + "</b>\n\n" + to_xml(msg2));
    show_all();
  }
  
private:
  Gtk::Label label;
  void on_response(int /* response_id */) { hide(); }
};

ErrorDialog* ErrorDialog::_instance = 0;

ErrorDialog& ErrorDialog::instance() {
  return *(_instance = (_instance ? _instance : new ErrorDialog()));
}

ErrorDialog::ErrorDialog() : error_dialog(0), warning_dialog(0) {}

void ErrorDialog::show_error(const Glib::ustring& msg1,
                             const Glib::ustring& msg2) {
  if(!error_dialog)
    error_dialog = new MsgDialog(Gtk::MESSAGE_ERROR);
  error_dialog->show_message(msg1, msg2);
}

void ErrorDialog::show_warning(const Glib::ustring& msg1,
                               const Glib::ustring& msg2) {
  if(!warning_dialog)
    warning_dialog = new MsgDialog(Gtk::MESSAGE_WARNING);
  warning_dialog->show_message(msg1, msg2);
}
