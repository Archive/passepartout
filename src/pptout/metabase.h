#ifndef METABASE_H		// -*- c++ -*-
#define METABASE_H
///
// Copyright (C) 2002 - 2004, Fredrik Arnerup & Rasmus Kaj, See COPYING
///
#include "document/pagent.h"
#include "propbase.h"
#include "viewent.h"

/**
 * An interface to get information about frame kinds.  To invent a new kind of
 * frame, simply implement this interface and the objects it returns.
 */
class MetaBase {
public:
  virtual Viewent::Ref create_viewent(View& view, Pagent& node) = 0;
  
  // MAY return 0.
  virtual PropBase* getProp() = 0;
};

#endif
