#ifndef CONFIG_H		// -*- c++ -*-
#define CONFIG_H
///
// Copyright (C) 2002 - 2004, Fredrik Arnerup & Rasmus Kaj, See COPYING
///
#include "util/configfile.h"
#include <sigc++/object.h>
#include <gtkmm/dialog.h>


/** Base class for config variables with a user interface
 * representation. */
struct GuiVar {
  /// description
  std::string desc; 
  /// display in gui
  bool visible; 
  GuiVar(std::string desc_): desc(desc_), visible(true) {}
};

struct BVar: public Config::BoolVar, public GuiVar {
  BVar(std::string name_, std::string desc_): 
    BoolVar(name_), GuiVar(desc_) {}
};

struct SVar: public Config::StringVar, public GuiVar {
  SVar(std::string name_, std::string desc_): 
    StringVar(name_), GuiVar(desc_) {}
};

struct FVar: public Config::FloatVar, public GuiVar {
  FVar(std::string name_, std::string desc_): 
    FloatVar(name_), GuiVar(desc_) {}
};

/// Passepartout-specific config file reader.
class PFile: public Config::File, public sigc::trackable {
private:
  bool i18n_init;
public:

  /// The global instance.
  static PFile instance;

  FVar ReshapeBoxSize, DefaultResolution, ZoomLevel, StartPage;
  BVar Landscape, FakeTransparency, SingleSided;
  SVar PaperName, PrintCommand, StylesheetPath,
    DefaultStylesheet, DocTemplatePath, PSInterpreter,
    LengthUnit;

  PFile();
  ~PFile();
  Gtk::Dialog &get_dialog();
  void dialog_show(Gtk::Window* const toplevel);

  /// Read the config file.
  /** main() calls this after the cerr aliases are set. */
  void read();

  /** Overrides Config::File::write() */
  void write(const std::string &filename);

protected:
  void error(const std::string &message);
  void dialog_done();
  Gtk::Dialog *dialog;
  bool first_error;
  std::string user_file, global_file;
};

/** Global name for PFile::instance, for convenience. */
extern PFile &config;

#endif
