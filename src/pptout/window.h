#ifndef WINDOW_H		// -*- c++ -*-
#define WINDOW_H
///
// Copyright (C) 2002 - 2004, Fredrik Arnerup & Rasmus Kaj, See COPYING
///
#include "docview.h"
#include <gtkmm/window.h>
#include <memory> // auto_ptr
#include <gtkmm/uimanager.h>
#include "widget/zoomer.h"

class Pagesel;
class Filesel;
class PrintDialog;
class TextFrameDialog;

namespace Gtk {
  class ScrolledWindow;
  class OptionMenu;
  class Statusbar;
}

/**
 * The main window containing a view of a Document.
 */
class FrameWindow : public Gtk::Window {
public:
  FrameWindow(const Glib::ustring &filename = "", 
	      DocRef document = Document::null());

  /** Duplicate view */
  explicit FrameWindow(const FrameWindow&);
  ~FrameWindow();

private:
  class Private;
  Private* p;
  static FrameWindow *active_window; // pointer to the currently active window
  Gtk::Widget *toolbar;
  Gtk::ScrolledWindow *scroller;
  Gtk::Statusbar *cafe_opera;
  Zoomer zoom_factor;
  DocumentView document_view;
  Pagesel *pagesel;
  Glib::RefPtr<Gtk::UIManager> uimanager;
  Glib::RefPtr<Gtk::ActionGroup> main_group, doc_group, page_group,
    selection_group;

  bool on_delete_event(GdkEventAny*);
  bool on_focus_in_event(GdkEventFocus*);
  bool on_key_press_event(GdkEventKey*);

  void on_recent_file (const Glib::RefPtr<Gtk::RecentAction>& recent);

  void constructor_common();
  void set_filename(const Glib::ustring&);

  void on_document_updated(DocRef document_); // change IN document
  void on_document_changed(); // change OF document
  void on_document_filename_changed(); 

  static void quit();

  void zoom_factor_changed_action(float factor);

  void insert_image (void);
  void insert_text (void);
  void open_file (void);
  void print (void);
  void save (void);
  void save_as (void);

  void show_about (void);
  void show_inspiration (void);
  void show_properties (void);
  void show_streams (void);

  void create_menus (Gtk::RecentFilter& filter);
  void show_preferences ();
  void close();
};

#endif
