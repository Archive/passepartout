///
// Copyright (C) 2002 - 2004, Fredrik Arnerup & Rasmus Kaj, See COPYING
///
#include "preferencesdialog.h"

#include <sstream>

#include "config.h"
#include "widget/wmisc.h"
#include "widget/filesel.h"
#include "widget/usererror.h"
#include "widget/subpanel.h"
#include "widget/spinner.h"
#include "fonts/fontmanager.hh" // for reportFonts

#include <gtkmm/label.h>
#include <gtkmm/button.h>
#include <gtkmm/table.h>
#include <gtkmm/entry.h>
#include <gtkmm/stock.h>
#include <gtkmm/notebook.h>
#include <gtkmm/box.h>
#include <gtkmm/messagedialog.h>
#include <gtkmm/scrolledwindow.h>
#include <gtkmm/textview.h>

#include <glib/gi18n.h>

PreferencesDialog::PreferencesDialog() : 
  DialogWrap(_("Preferences")),
  file_str(_("File locations")),
  prog_str(_("Program locations")),
  env_str(_("Environment")),
  tab1_str(_("Files")),
  tab2_str(_("Environment")),
  close_button_str(_("_Close")),
  apply_button_str(_("_Save & Close"))
{
  try {
    //Declaration of variables
    //    const int max_res = 
    notebook = manage(new Gtk::Notebook());
    Gtk::Box *m_VBox = manage(new Gtk::VBox(false, double_space));

    //Buttons
    Gtk::Button *cancel_button = manage(new Gtk::Button());
    Gtk::Button *apply_button = manage(new Gtk::Button());
    Gtk::Box *cb_HBox = manage(new Gtk::HBox(false, single_space));
    Gtk::Box *ab_HBox = manage(new Gtk::HBox(false, single_space));
    cb_HBox->pack_start(*manage(new Gtk::Image(Gtk::Stock::CLOSE, 
					       Gtk::ICON_SIZE_BUTTON) ),
			Gtk::PACK_SHRINK);
    Gtk::Label *close_label = manage(new Gtk::Label(close_button_str, true));
    close_label->set_mnemonic_widget(*cancel_button);    
    cb_HBox->pack_start(*close_label, Gtk::PACK_EXPAND_WIDGET);

    ab_HBox->pack_start(*manage(new Gtk::Image(Gtk::Stock::APPLY, 
					       Gtk::ICON_SIZE_BUTTON) ),
			Gtk::PACK_SHRINK);
    Gtk::Label *apply_label = manage(new Gtk::Label(apply_button_str, true));
    apply_label->set_mnemonic_widget(*apply_button);
    ab_HBox->pack_start(*apply_label, Gtk::PACK_EXPAND_WIDGET);
    cancel_button->add(*cb_HBox);
    apply_button->add(*ab_HBox);
    
    //Tab 1
    Gtk::Box *tab1_box = manage(new Gtk::VBox(false, triple_space));
    tab1_box->set_border_width(border_width);

    Gtk::Table *tab1_table2 = manage(new Gtk::Table(3, 2));
    tab1_table2->set_spacings(single_space);
    Gtk::Table *tab1_table3 = manage(new Gtk::Table(3, 2));
    tab1_table3->set_spacings(single_space);

    //Tab 2
    Gtk::Box *tab2_box = manage(new Gtk::VBox(false, triple_space));
    tab2_box->set_border_width(border_width);

    Gtk::Table *tab2_table2 = manage(new Gtk::Table(4, 5));
    tab2_table2->set_spacings(double_space);

    //Tab 3
    Gtk::ScrolledWindow *tab3_scroller = manage(new Gtk::ScrolledWindow());
    tab3_scroller->set_policy(Gtk::POLICY_AUTOMATIC, Gtk::POLICY_AUTOMATIC);
    Glib::RefPtr<Gtk::TextBuffer> buffer = Gtk::TextBuffer::create();
    {
      Glib::RefPtr<Gtk::TextBuffer::TagTable> table = buffer->get_tag_table();
      Gtk::TextBuffer::iterator i = buffer->begin();
      font::FontManager::FontNames fontnames = 
        font::FontManager::instance().getAvailableFonts();
      for(font::FontManager::FontNames::const_iterator f = fontnames.begin();
          f != fontnames.end(); f++) {
        Glib::RefPtr<Gtk::TextTag> tag = Gtk::TextTag::create();
        const font::Metrics &metrics =
          font::FontManager::instance().getFont(*f);
        tag->property_font() = metrics.getName();
        tag->property_size_points() = 20;
        table->add(tag);
        i = buffer->insert_with_tag(i, metrics.getName() + "\n", tag);
        i = buffer->insert(i, Glib::ustring::compose (_("PostScript name: %1\n"), *f));
        i = buffer->insert(i, Glib::ustring::compose (_("File: %1\n\n"),
                           font::FontManager::instance().getFontFile(*f)));
      }
    }
    Gtk::TextView *text_view = manage(new Gtk::TextView(buffer));
    text_view->set_editable(false);
    tab3_scroller->add(*text_view);
    //Tab 1
    xslt_entry =
      manage(new FileEntry(config.StylesheetPath.desc,
                           _("Select default stylesheet folder"),
                           Gtk::FILE_CHOOSER_ACTION_SELECT_FOLDER));
    xslt_file_entry = manage(new FileEntry(config.DefaultStylesheet.desc,
                                           _("Select default stylesheet")));
    /// \todo filesel window titles
    print_entry = manage(new FileEntry(config.PrintCommand.desc, ""));
    gs_entry = manage(new FileEntry(config.PSInterpreter.desc, ""));
    SubPanel *filePanel = manage(new SubPanel(file_str)); 
    SubPanel *progPanel = manage(new SubPanel(prog_str)); 
    //Tab 2
    SubPanel *envPanel = manage(new SubPanel(env_str));
    resolution_spin = manage(new Spinner(config.DefaultResolution.
					 values.front(), false, 
					 0));
    zoom_spin = manage(new Spinner(config.ZoomLevel.values.front(), 
				   false, 0));
    //Add the main components
    get_vbox()->pack_start(*m_VBox, Gtk::PACK_EXPAND_WIDGET);
    set_resizable(true);

    notebook->set_border_width(10);
    m_VBox->pack_start(*notebook, Gtk::PACK_EXPAND_WIDGET);

    //Add the buttons
    get_action_area()->pack_end(*cancel_button, 
				Gtk::PACK_SHRINK);
    get_action_area()->pack_end(*apply_button, 
				Gtk::PACK_SHRINK);
    cancel_button->set_flags(Gtk::CAN_DEFAULT);
    cancel_button->grab_default();
    cancel_button->signal_clicked().
      connect(sigc::mem_fun(*this, &PreferencesDialog::dialog_cancel));
    apply_button->signal_clicked().
      connect(sigc::mem_fun(*this, &PreferencesDialog::dialog_apply));
    signal_delete_event().
      connect(sigc::mem_fun(*this, &PreferencesDialog::dialog_delete));

    //Add the notebook pages
    //---Page 1:
    //-----Main tab layout
    tab1_box->pack_start(*filePanel, Gtk::PACK_SHRINK);
    tab1_box->pack_start(*progPanel, Gtk::PACK_SHRINK);

    //-----File location group
    filePanel->pack_start(*tab1_table2, Gtk::PACK_SHRINK);

    if(config.StylesheetPath.visible) {
      tab1_table2->attach
	      // TRANSLATORS: this is for settings; "%1" is the setting description
	(*manage(new Gtk::Label(Glib::ustring::compose (_("%1: "), config.StylesheetPath.desc),
				Gtk::JUSTIFY_LEFT, 
				Gtk::ALIGN_BOTTOM)), 
	 0, 1, 1, 2);
      tab1_table2->attach(*xslt_entry, 1, 2, 1, 2);
      xslt_entry->entry.set_text(config.StylesheetPath.values.front());
      xslt_entry->show();
    }

    if(config.DefaultStylesheet.visible) {
      tab1_table2->attach
	(*manage(new Gtk::Label(Glib::ustring::compose (_("%1: "), config.DefaultStylesheet.desc),
				Gtk::JUSTIFY_LEFT, 
				Gtk::ALIGN_BOTTOM)), 
	 0, 1, 2, 3);
      tab1_table2->attach(*xslt_file_entry, 1, 2, 2, 3);
      xslt_file_entry->entry.set_text(config.DefaultStylesheet.values.front());
      xslt_file_entry->show();
    }


    //-----Program location group
    progPanel->pack_start(*tab1_table3, Gtk::PACK_SHRINK);

    if(config.PrintCommand.visible) {
      tab1_table3->attach
	(*manage(new Gtk::Label(Glib::ustring::compose (_("%1: "), config.PrintCommand.desc),
				Gtk::JUSTIFY_LEFT, 
				Gtk::ALIGN_BOTTOM)), 
	 0, 1, 0, 1);
      tab1_table3->attach(*print_entry, 1, 2, 0, 1);
      print_entry->entry.set_text(config.PrintCommand.values.front());
      print_entry->show();
    }

    if(config.PSInterpreter.visible) {
      tab1_table3->attach
	(*manage(new Gtk::Label(Glib::ustring::compose (_("%1: "), config.PSInterpreter.desc),
				Gtk::JUSTIFY_LEFT, 
				Gtk::ALIGN_BOTTOM)), 
	 0, 1, 1, 2);
      tab1_table3->attach(*gs_entry, 1, 2, 1, 2);
      gs_entry->entry.set_text(config.PSInterpreter.values.front());
      gs_entry->show();
    }

    notebook->append_page(*tab1_box, tab1_str);

    //---Page 2:
    tab2_box->pack_start(*envPanel, Gtk::PACK_SHRINK);
    envPanel->pack_start(*tab2_table2, Gtk::PACK_SHRINK);
    
    if(config.DefaultResolution.visible) {
      tab2_table2->attach
	(*manage(new Gtk::Label(Glib::ustring::compose (_("%1: "), config.DefaultResolution.desc),
				Gtk::JUSTIFY_LEFT, 
				Gtk::ALIGN_BOTTOM)), 
	 1, 2, 1, 2);
      tab2_table2->attach(*resolution_spin, 2, 3, 1, 2);
      tab2_table2->attach(*manage(new Gtk::Label(_("ppi"),
						 Gtk::JUSTIFY_LEFT, 
						 Gtk::ALIGN_BOTTOM)), 
			  3, 4, 1, 2);
    }

    if(config.ZoomLevel.visible) {      
      tab2_table2->attach
	(*manage(new Gtk::Label(Glib::ustring::compose (_("%1: "), config.ZoomLevel.desc),
				Gtk::JUSTIFY_LEFT, 
				Gtk::ALIGN_BOTTOM)), 
	 1, 2, 2, 3);
      tab2_table2->attach(*zoom_spin, 2, 3, 2, 3);
      tab2_table2->attach(*manage(new Gtk::Label(_("%"),
						 Gtk::JUSTIFY_LEFT, 
						 Gtk::ALIGN_BOTTOM)), 
			  3, 4, 2, 3);
    }

    notebook->append_page(*tab2_box, tab2_str);
    

    //---Page 3:
    notebook->append_page(*tab3_scroller, _("Font Info"));
  } catch(const std::out_of_range &error) {
    throw UserError("Settings not found", 
		    "Can't find one or more setting values."
		    "\nYou can still change your settings by editing the</i> "
		    "<tt>.pptout</tt> file in you home directory.");
  }
  catch (Glib::Exception &e)
    {
      g_warning ("%s", e.what ().c_str ());
    }

  show_all_children();
}


void PreferencesDialog::dialog_cancel() {
  hide();
}

void PreferencesDialog::dialog_apply() {
  /// \todo some kind of error check

  if(config.StylesheetPath.visible) 
    config.StylesheetPath.values.front() = xslt_entry->entry.get_text(); 

  if(config.DefaultStylesheet.visible) 
    config.DefaultStylesheet.values.front()=xslt_file_entry->entry.get_text(); 

  if(config.PrintCommand.visible)
    config.PrintCommand.values.front() = print_entry->entry.get_text(); 

  if(config.PSInterpreter.visible)
    config.PSInterpreter.values.front() = gs_entry->entry.get_text(); 
  
  if(config.DefaultResolution.visible)
    config.DefaultResolution.values.front() = resolution_spin->get();

  if(config.ZoomLevel.visible)
    config.ZoomLevel.values.front() = zoom_spin->get();

  Gtk::MessageDialog msg(*this, _("Note: you need to restart the program "
			    "for some changes to take effect"));
  msg.run();
  dialog_cancel();
}


bool PreferencesDialog::dialog_delete(GdkEventAny*) {
  dialog_cancel();
  return true;
}
