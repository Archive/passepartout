///
// Copyright (C) 2002 - 2004, Fredrik Arnerup & Rasmus Kaj, See COPYING
///
#include "pdfcanvas.hh"
#include <stdexcept>

xml2ps::PDFCanvas::PDFCanvas(PDF::Document::Ptr doc, TargetType target,
			     const xml2ps::Canvas::PageVec& pages,
			     bool allow_extra_pages)
  : Canvas(pages, allow_extra_pages), document(doc), content(0), 
    target_(target), currentfont(0), cur_rise(0)
{
  newPage();
}

xml2ps::PDFCanvas::~PDFCanvas() {
  if(content) content->endText();
}

void xml2ps::PDFCanvas::newPage() {
  if(content) content->endText();
  Canvas::newPage();
  if(target_ == Pages)
    content = document->add_page(int(cur_page_->width() + 0.5), 
				 int(cur_page_->height() + 0.5));
  else {
    content = PDF::Content::create(document, 
				   int(cur_page_->width()+0.5),
				   int(cur_page_->height()+0.5));
    pageobjs.push_back(content);
  }
  content->beginText();
}

void xml2ps::PDFCanvas::closePage() {
  if(content) {
    content->endText();
    content = PDF::Content::Ptr();
  }
}

namespace {
  PDF::Content& nonull(PDF::Content::Ptr c) {
    if(!c) throw std::logic_error("Tried to paint on no page");
    return *c.operator->();
  }
}

void xml2ps::PDFCanvas::setgray(float gray) {
  nonull(content).setgray(gray);
}

void xml2ps::PDFCanvas::setfont(const font::FontInfo& font) {
  currentfont = &font;
  Canvas::setfont(font);
  nonull(content).selectfont(font);
  // The letter spacing is scaled by the font size, we need to
  // compensate for that.
  nonull(content).setCharSpace(font.getLetterSpacing() / font.getSize());
}

void xml2ps::PDFCanvas::moveto(float hpos, float vpos) {
  nonull(content).moveto(hpos, vpos);
}

float xml2ps::PDFCanvas::textRise(float rise) {
  float oldrise = cur_rise;
  nonull(content).textRise(cur_rise = rise);
  return oldrise;
}

float xml2ps::PDFCanvas::getRise() const {
  return cur_rise;
}

void xml2ps::PDFCanvas::setWordSpace(const float& space) {
  // the space parameter is in "times normal width of whitespace"
  // the content parameter is in "text units extra".
  if(!currentfont)
    throw std::runtime_error("Tried to set wordspace w/o current font");
  // Content::setWordSpace wants the length to add to a normal space
  nonull(content).setWordSpace(currentfont->getMetrics().getWidth(" ")
                               * (space - 1));
}

void xml2ps::PDFCanvas::show(const Glib::ustring& text) {
  //  const float ls = font_->getLetterSpacing();
  nonull(content).show(text);
}

void xml2ps::PDFCanvas::whitespace() {
  nonull(content).whitespace();
}

void xml2ps::PDFCanvas::whitespace(const float& space) {
  // The text space is scaled by the font size, we need to compensate
  // for that.
  nonull(content).whitespace(space / currentfont->getSize());
}

void xml2ps::PDFCanvas::underlineFrom(const float& below) {
  // who needs it anyway?
}

void
xml2ps::PDFCanvas::underlineTo(const float& below, const float& thickness) {
  // who needs it anyway?
}

void
xml2ps::PDFCanvas::line(float x1, float y1, float x2, float y2) {
  /// \todo implement drawing a simple line.
}

PDF::Object::Ptr xml2ps::PDFCanvas::getPageObject(int pageno) {
  return pageobjs[pageno - 1];
}
