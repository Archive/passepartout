///
// Copyright (C) 2002 - 2004, Fredrik Arnerup & Rasmus Kaj, See COPYING
///
#ifndef X2P_XCANVAS
#define X2P_XCANVAS
// Canvas that renders text directly to the screen
// using pango

#include "canvas.hh"
#include <vector>
#include <sigc++/object.h>
#include <gtkmm/main.h>
#include <gtkmm/window.h>
#include <libgnomecanvasmm.h>

namespace xml2ps {

struct Part {
  Part(Glib::ustring _text, Glib::ustring _fontname, float _fontsize,
       float _x, float _y, 
       Gdk::Color &_color)
    :text(_text), fontname(_fontname), fontsize(_fontsize),
     x(_x), y(_y), color(_color)
  {}
  Glib::ustring text, fontname;
  float fontsize, x, y;
  Gdk::Color color;
  float width;
};
typedef std::vector<Part> Parts;

class XCanvas: public Canvas, public sigc::trackable {
public:
  explicit XCanvas(const PageVec& pages, bool allow_extra_pages);
  virtual ~XCanvas();
  
  virtual void newPage();
  virtual void closePage() {}
  
  virtual void setgray(float gray);
  virtual void setfont(const font::FontInfo& font);
  virtual void moveto(float hpos, float vpos);

  virtual float textRise(float rise);
  virtual float getRise() const;
    
  virtual void setWordSpace(const float& space);

  virtual void show(const Glib::ustring& text);
  virtual void whitespace();
  virtual void whitespace(const float& space);
  
  virtual void underlineFrom(const float& below);
  virtual void underlineTo(const float& below, const float& thickness);

  virtual void line(float x1, float y1, float x2, float y2);

  float p2s(float length); // paper length to screen length

protected:
  float res; // screen resolution in pixels per inch
  Parts parts;
  Gtk::Window *window;
  Gtk::Main *kit;
  Gnome::Canvas::Canvas *canvas;
  //  Gnome::Canvas::Group canvasgroup;
  Gdk::Color color; 
  float x, y;
  const font::FontInfo *current_font;

  bool draw(GdkEventExpose*);
};

};

#endif
