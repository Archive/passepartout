///
// Copyright (C) 2002 - 2004, Fredrik Arnerup & Rasmus Kaj, See COPYING
///
#include "typesetter.hh"
#include "fonts/fontinfo.hh"
#include "fonts/fontmanager.hh" // for reportFonts
#include "util/stringutil.h"
#include "util/cmdline.h"
#include <stdexcept>
#include "defines.h" // PACKAGE_VERSION

static const std::string w_hdr("xml2ps Warning: "),
  e_hdr("xml2ps ERROR: ");

int main(int argc, char* argv[]) {
  int pagenum = 1;
  bool print_font_info = false;

  try {
    xml2ps::Typesetter typesetter;

    CmdLine cmdline(argc, argv);
    const int o_format =
      cmdline.add_option(CmdLine::Option(0, "output-format",
					 CmdLine::Option::REQ_PARAM, 
					 "Output format (ps, pdf)", "FORMAT"));
    const int o_fontinfo =
      cmdline.add_option(CmdLine::Option(0, "fontinfo",
					 CmdLine::Option::NO_PARAM, 
					 "Print font info"));
    const int o_fontsubst =
      cmdline.add_option(CmdLine::Option('s', "subst-fonst",
					 CmdLine::Option::NO_PARAM,
					 "Substitute font aliases"));
    const int o_page =
      cmdline.add_option(CmdLine::Option('p', "",
					 CmdLine::Option::REQ_PARAM, 
					 "Add page",
					 "WxH[/COLS[+GTR]]"));
    const int o_block =
      cmdline.add_option(CmdLine::Option('b', "",
					 CmdLine::Option::REQ_PARAM, 
					 "Add obstacle",
					 "WxH[+/-LEFT+/-BOTTOM]"));
    const int o_noextrapages =
      cmdline.add_option(CmdLine::Option(0, "no-extra-pages",
					 CmdLine::Option::NO_PARAM, 
					 "Complain if running out of pages"));
    const int o_version =
      cmdline.add_option(CmdLine::Option('v', "version",
					 CmdLine::Option::NO_PARAM, 
					 "Display version number"));
    const int o_help =
      cmdline.add_option(CmdLine::Option('h', "help",
					 CmdLine::Option::NO_PARAM, 
					 "Display this help message"));
    

    CmdLine::ParsedOptions opts = cmdline.parse();
    for(CmdLine::ParsedOptions::const_iterator
	  arg = opts.begin(); arg != opts.end(); arg++) {
      int option = arg->first;
      const std::string &param = arg->second;
      if(!option) {
	std::cerr << w_hdr << "Filename args (" << param
		  << ") not yet supported. Sorry." << std::endl;
      } else if(option == o_format) {
	if(param == "ps")
	  typesetter.setOutputFormat(xml2ps::FORMAT_PS);
	else if(param == "pdf")
	  typesetter.setOutputFormat(xml2ps::FORMAT_PDF);
	else if(param == "x")
	  typesetter.setOutputFormat(xml2ps::FORMAT_X);
	else
	  throw std::runtime_error("Unknown output format: \""
				   + param + "\"");
      } else if(option == o_page) {
	typesetter.addPage(xml2ps::PageBoundary(pagenum++, param));
      } else if(option == o_fontinfo) {
	print_font_info = true;
      } else if(option == o_fontsubst) {
	 typesetter.setSubstFontAliases(true);
      } else if(option == o_block) {
	typesetter.addObstacle(to<Boundary>(param));
      } else if(option == o_noextrapages) {
	typesetter.setExtraPages(false);
      } else if(option == o_help) {
	std::cout << "Usage: xml2ps [options]" << std::endl;
	cmdline.print_usage(std::cout);
	return 0;
      } else if(option == o_version) {
	std::cout << "xml2ps " << std::string(PACKAGE_VERSION) << std::endl;
	return 0;
      }
    }
    
    if(!print_font_info)
      typesetter.run();
    else
      font::FontManager::instance().reportFonts(std::cout);
//     std::cerr << used_fonts.size() << std::endl;
//     for(font::Fonts::const_iterator i = used_fonts.begin();
// 	i != used_fonts.end();
// 	i++) {
//       std::cerr << "% " << *i << std::endl;
//     }

    /// \todo warn about this
    //   } catch (const xml2ps::out_of_pages&) {
    //     std::cerr << w_hdr << "Out of pages, document truncated" << std::endl;
  } catch (const std::exception& err) {
    std::cerr << e_hdr << err.what() << std::endl;
  } catch (...) {
    std::cerr << e_hdr << "Main caught something that "
	      << "wasn't even an exception!"
	      << std::endl;
  }
}
