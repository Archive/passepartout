///
// Copyright (C) 2003, 2004, Fredrik Arnerup & Rasmus Kaj, See COPYING
///
#include "pagedstream.hh"
#include "util/filesys.h"
#include <fstream>
#include <sstream>
#include <iostream>
#include <vector>
#include <exception>
#include <memory>

xml2ps::PagedStream::PagedStream() 
  : current_part(0)
{
  dirname = mkdtemp(std::string("xml2psXXXXXX"));
  std::string file = filename(0);
  open(file.c_str());
}

xml2ps::PagedStream::~PagedStream() {
  try {
    for(unsigned int i = 0; i <= current_part; ++i) {
      unlink(filename(i));
    }
    rmdir(dirname);
  } catch(std::exception& err) {
    std::cerr << "Failed to cleanup after PagedStream: " << err.what()
	      << std::endl;
  }
}

void xml2ps::PagedStream::split() {
  close();
  std::string file = filename(++current_part);
  open(file.c_str());
}

void xml2ps::PagedStream::appendPage(unsigned int number, std::ostream& out)
  const
{
  const std::string inname = filename(number);
  std::ifstream in(inname.c_str());
  out << in.rdbuf();
}

std::string xml2ps::PagedStream::filename(unsigned int number) const {
  std::ostringstream filename;
  filename << dirname << "/part" << number;
  return filename.str();
}
