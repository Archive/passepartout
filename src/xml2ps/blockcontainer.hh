///
// Copyright (C) 2002 - 2004, Fredrik Arnerup & Rasmus Kaj, See COPYING
///
#ifndef X2P_BLOCKCONTAINER_HH
#define X2P_BLOCKCONTAINER_HH
#include "xly.hh"
#include "canvas.hh"
#include "line.hh"

namespace xml2ps {

  class BlockContainer : public Element {
  public:
    BlockContainer(Canvas& out, Element& parent,
		   const Glib::ustring& n, const Attributes attr);
    virtual void close();
    virtual Line* getLine(const Element& lineroot, const float& line_height,
			  const float& m_left, const float& m_right,
			  bool span);
  protected:
    BlockContainer(Canvas& out, Element& parent, const Glib::ustring& n,
		   const font::FontInfo& font);
    Canvas &out_;
    float margin_top, margin_bottom, margin_left, margin_right;
    bool spancolumns;
  };

  class RootNode : public BlockContainer {
  public:
    RootNode(Canvas& out);
    virtual void debug(std::ostream& out, bool nl = true);
    virtual Line* getLine(const Element& lineroot, const float& line_height,
			  const float& m_left, const float& m_right,
			  bool span);
  };
  
}

#endif
