#ifndef X2P_PAGEDSTREAM
#define X2P_PAGEDSTREAM
///
// Copyright (C) 2003, 2004, Fredrik Arnerup & Rasmus Kaj, See COPYING
///
#include <fstream>
#include <string>

namespace xml2ps {
  
  /**
   * Basically an output stream that goes to a set of "pages".  Page number 0
   * is allways a header.  One page is active at a time, and can be appended
   * like any stream.
   */
  class PagedStream : public std::ofstream {
  public:
    /** Create a new set with a temporary directory */
    PagedStream();
    virtual ~PagedStream();
    
    /** Create a new file.  Noop if working in single-sink mode. */
    void split();
    
    /**
     * Append the contents of a specific page to a specified stream.
     * No header is included here, just the raw content.
     */
    void appendPage(unsigned int number, std::ostream& out) const;
    
    /** Get the filename for one of the destination.  
     * \throws range_error if single-sink or number out of range.
     */
    std::string filename(unsigned int number) const;

  private:
    std::string dirname;
    unsigned int current_part;
  };
}

#endif
