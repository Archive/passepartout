///
// Copyright (C) 2002 - 2004, Fredrik Arnerup & Rasmus Kaj, See COPYING
///
#include "boundaries.hh"
#include <stdexcept>
#include <sstream>

xml2ps::PageSize::PageSize(int num, const std::string& s)
  : gutter_(10), columns_(1), num_(num)
{
  using std::runtime_error;
  std::istringstream in(s);
  if(!(in >> width_)) throw runtime_error("Bad format: \"" + s + "\"");
  char c;
  if(!((in >> c) && (c == 'x')))
    throw runtime_error("Bad format: \"" + s + "\"");
  if(!(in >> height_)) throw runtime_error("Bad format: \"" + s + "\"");
  if(in >> c >> columns_ >> gutter_) {
    if(c != '/') throw runtime_error("Bad format: \"" + s + "\"");
  } else {
    if(!in.eof()) throw runtime_error("Bad format: \"" + s + "\"");
  }
}

xml2ps::PageSize::PageSize(int num, float width, float height, 
			   int columns, float gutter) :
  width_(width), height_(height), gutter_(gutter), columns_(columns), num_(num)
{}


xml2ps::PageBoundary::~PageBoundary() {}
