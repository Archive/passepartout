///
// Copyright (C) 2002 - 2004, Fredrik Arnerup & Rasmus Kaj, See COPYING
///
#include <iostream>
#include "blockcontainer.hh"

xml2ps::BlockContainer::BlockContainer(Canvas& out, Element& parent,
				       const Glib::ustring& n, 
				       const Attributes attr)
  : Element(parent, n, attr), out_(out),
    margin_top(attr.get("margin-top", 0, getFont().getSize())),
    margin_bottom(attr.get("margin-bottom", 0, getFont().getSize())),
    margin_left(attr.get("margin-left", 0, getFont().getSize())),
    margin_right(attr.get("margin-right", 0, getFont().getSize())),
    spancolumns(attr.get("span-columns", false))
{}

xml2ps::BlockContainer::BlockContainer(Canvas& out, Element& parent,
				       const Glib::ustring& n, 
				       const font::FontInfo& font)
  : Element(parent, n, font), out_(out),
    margin_top(0), margin_bottom(0), margin_left(0), margin_right(0),
    spancolumns(false)
{}

void xml2ps::BlockContainer::close() {
  /// \todo:  Should to stuff about vertical margins here!
}

xml2ps::Line*
xml2ps::BlockContainer::getLine(const Element& lineroot, 
				const float& line_height,
				const float& m_left, const float& m_right,
				bool span)
{
  BlockContainer *parent=dynamic_cast<BlockContainer*>(&getParent());
  if(!parent)
    throw std::runtime_error("Parent of <block-container> "
			     "must be <block-container>");
  return parent->getLine(lineroot, line_height,
			 m_left + margin_left, m_right + margin_right,
			 span||spancolumns);
}

// - - RootNode - -

xml2ps::RootNode::RootNode(Canvas& out)
  // Note; The parameters to the FontInfo is the default root font.  They
  // should not matter in reality.
  : BlockContainer(out, *this, "root", font::FontInfo("Times-Roman", 10))
{}

void xml2ps::RootNode::debug(std::ostream& out, bool nl) {
  out << '<';
  if(nl) out << '>' << std::endl;
}

xml2ps::Line*
xml2ps::RootNode::getLine(const Element& lineroot, 
				const float& line_height,
				const float& m_left, const float& m_right,
				bool span)
{
  return new Line(lineroot, out_, line_height, span||spancolumns,
		  m_left + margin_left, m_right + margin_right);
}

