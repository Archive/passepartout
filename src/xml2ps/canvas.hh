#ifndef X2P_CANVAS
#define X2P_CANVAS
///
// Copyright (C) 2002 - 2004, Fredrik Arnerup & Rasmus Kaj, See COPYING
///
#include "boundaries.hh"
#include <fonts/fontinfo.hh>
#include <libxml++/exceptions/exception.h>
#include <glibmm/ustring.h>
#include <vector>

namespace xml2ps {
  
  /**
   * An exception to throw when xml2ps has reached the end of the last "page"
   * but there is more text to set.
   * It inherits xmlpp::exception so it can be thrown through the libxmlpp
   * callback layer.
   */
  class OutOfPages : public xmlpp::exception {
  public:
    OutOfPages() : xmlpp::exception("Out of pages") {}
    
    /** Required by xmlpp::exception */
    virtual void Raise() const {throw *this;}
    /** Required by xmlpp::exception */
    virtual OutOfPages * Clone() const {return new OutOfPages(*this);}
  };
  
  /**
   * An abstract class for a canvas.  For instantiation, there is a subclass
   * PsCanvas that writes to a PsStream.  later on, there might be an XCanvas
   * or something that draws on a window for fast interaction with pptout.
   *
   * The Canvas has knowledge about columns, and about the "current" column.
   * When the current column is filled, the next is started.
   */
  class Canvas {
  public:
    typedef std::vector<PageBoundary> PageVec;
    
    explicit Canvas(const PageVec& pages, bool allow_extra_pages);
    virtual ~Canvas();
    
    // if true, use the real name of a font instead of an alias
    virtual void setSubstituteFontAliases(bool subst) 
      {substitute_font_aliases = subst;}
    virtual bool getSubstituteFontAliases() const 
      {return substitute_font_aliases;}
    
    // return the x coordinates of the current hbox.
    HBox hbox(bool span, float margin_left, float margin_right, 
	      float ascender, float descender);
    
    virtual void addRelObstacle
    (const float left, const float bottom, const float right, const float top);
    
    // Check where the current column starts.
    virtual void addMargin(const float& m);
    
    // Make sure there is hspace in current column, or move on to the next.
    virtual void reserveHeight(const float& hspace, bool span = false);
    // Move the cursor down in the current column.
    virtual const float& down(const float& step);
    
    virtual void newPage();
    virtual void closePage() = 0;
    
    const PageBoundary &getCurrentPage() const {return *cur_page_;}
    
    virtual void setgray(float gray) = 0;
    virtual void setfont(const font::FontInfo& font);
    
    virtual void moveto(float hpos, float vpos) = 0;
    
    /**
     * Set the text rise about the baseline.  The new value is absolute
     * from the baseline, not accumuled with previous values.  Positive values
     * gives superscript, negative gives subscript.
     * \return the previous text rise value.
     */
    virtual float textRise(float rise) = 0;
    virtual float getRise() const = 0;
    
    /** Set the width of a wordspace, as a factor to the default word space
     * width of the font.
     */
    virtual void setWordSpace(const float& space) = 0;
    
    virtual void show(const Glib::ustring& text) = 0;
    /** A normal whitespace of the current default width. */
    virtual void whitespace() = 0;
    /** An exactly specified whitespace
     * /param space length of whitespace, in points.
     */
    virtual void whitespace(const float& space) = 0;
    virtual void underlineFrom(const float& below) = 0;
    virtual void underlineTo(const float& below, const float& thickness) = 0;
    
    virtual void line(float x1, float y1, float x2, float y2) = 0;
    
    const font::Fonts &getUsedFonts() const {return used_fonts;}
  protected:
    PageVec pages_;
    PageVec::iterator cur_page_;
    
  private:
    bool allow_extra_pages_, substitute_font_aliases;
    float height_;		// height _below_ spanning stuff
    float vpos, margin;
    int cur_column;
    font::Fonts used_fonts;
  };
};

#endif
