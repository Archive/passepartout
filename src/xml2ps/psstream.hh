#ifndef X2P_PSSTR
#define X2P_PSSTR
///
// Copyright (C) 2002 - 2004, Fredrik Arnerup & Rasmus Kaj, See COPYING
///
#include "pagedstream.hh"
#include <string>
#include <iostream>
#include <fstream>
#include <vector>
#include <set>
#include <fonts/fontinfo.hh>

namespace xml2ps {
  class PsStream : public PagedStream {
  public:
    explicit PsStream(const int& out_width = 0,
		      const int& out_height = 0);
    ~PsStream();
    
    void newPage(const int& width, const int& height);
    
    void require_font(const std::string &fontname);
    void include_font(const std::string &fontname); // can't include yet
    
    /**
     * Write the postscript prolog block for xml2ps.  Note that the Begin- /
     * EndProlog comments is not included here, since the prolog might be
     * concatenated with other prolog parts.
     */
    static void psProlog(std::ostream& out);
    
    /**
     * Merge all headers and pages to one complete postscript file written to
     * a stream.
     * \param out the stream to write to.
     */
    void merge(std::ostream& out) const;
    
    /**
     * Generate a stand-alone postscript file for a single page.
     * \param page_no number of the page to write.
     * \param out the stream to write to.
     */
    void pageEps(unsigned page_no, std::ostream& out);
    
  private:
    typedef std::set<std::string> FontSet;
    FontSet require_fonts, include_fonts;
    std::vector<std::pair<int, int> > page_size;
    int page_no_, width_max, height_max;
  };
};

#endif
