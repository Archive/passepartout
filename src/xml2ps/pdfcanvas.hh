///
// Copyright (C) 2002 - 2004, Fredrik Arnerup & Rasmus Kaj, See COPYING
///
#ifndef X2P_PDFCANVAS
#define X2P_PDFCANVAS
// See canvas.hh for discussion
#include "canvas.hh"
#include <ps/pdf.h>
#include <vector>

namespace xml2ps {
class PDFCanvas : public Canvas {
public:
  enum TargetType { Pages, XObjs };
  
  /**
   * \param doc The PDF Document to write to.  Kept as a reference, so the
   * Document object must exist at least as long as the PDFCanvas.
   * \param target wheter to create the resulting content as Pages or as Form
   * XObjects (XObjs) in the PDF Document.
   * \param pages Description of the pages (bounding box and any forbidden
   * areas) to fill with text.
   * \param allow_extra_pages wether to create copies of the last page to make
   * room for all text (true) of cut the text after the last page (false).
   */
  PDFCanvas(PDF::Document::Ptr doc, TargetType target,
	    const PageVec& pages,
	    bool allow_extra_pages);
  virtual ~PDFCanvas();
  
  virtual void newPage();
  void closePage();

  void setgray(float gray);
  void setfont(const font::FontInfo& font);
  void moveto(float hpos, float vpos);
  float textRise(float rise);
  float getRise() const;
  void setWordSpace(const float& space);
  
  void show(const Glib::ustring& text);
  void whitespace();
  void whitespace(const float& space);
  
  void underlineFrom(const float& below);
  void underlineTo(const float& below, const float& thickness);
  
  void line(float x1, float y1, float x2, float y2);

  PDF::Object::Ptr getPageObject(int pageno);
  
private:
  PDF::Document::Ptr document;
  PDF::Content::Ptr content;
  const TargetType target_;
  const font::FontInfo* currentfont;
  float cur_rise;
  std::vector<PDF::Object::Ptr> pageobjs;
};
};
#endif
