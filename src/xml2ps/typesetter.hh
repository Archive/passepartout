///
// Copyright (C) 2002 - 2004, Fredrik Arnerup & Rasmus Kaj, See COPYING
///
#ifndef X2P_TYPESETTER
#define X2P_TYPESETTER
#include "boundaries.hh"
#include "canvas.hh"
#include "blockcontainer.hh"
#include <fonts/fontinfo.hh>
#include <libxml++/libxml++.h>
#include <iostream>

// External interface to the typesetter

namespace xml2ps {
  enum OutputFormat {FORMAT_PS, FORMAT_PDF, FORMAT_X};
  
  /**
   */
  class PsConverter : public xmlpp::SaxParser {
  public:
    PsConverter(xml2ps::Canvas& output);
    virtual ~PsConverter();

    void on_start_element(const Glib::ustring& n,
			  const xmlpp::SaxParser::AttributeList &p);
    void on_end_element(const Glib::ustring &n);
    void on_characters(const Glib::ustring& s);
    void on_cdata_block(const Glib::ustring& s);
    void on_warning(const Glib::ustring &s);
    void on_error(const Glib::ustring &s);
    void on_fatal_error(const Glib::ustring &s);
  private:
    xml2ps::Canvas& out;
    xml2ps::RootNode root;
    xml2ps::Element * node;
    std::vector<std::string> element_stack; 
  };

  
  /**
   * Main interface to the xml2ps Typesetter.
   */
  class Typesetter {
  public:
    Typesetter(OutputFormat _output_format = FORMAT_PS,
	       bool _extra_pages = true) :
      extra_pages(_extra_pages),
      subst_font_aliases(false),
      output_format(_output_format)
    {}
    
    ~Typesetter();


    void setOutputFormat(OutputFormat _output_format = FORMAT_PS) 
    {output_format = _output_format;}
    void addPage(const PageBoundary &page_boundary);
    // Fire and forget: create the obstacle with new
    void addObstacle(Boundary obstacle);
    void setExtraPages(bool _extra_pages = true) 
    {extra_pages = _extra_pages;}
    void setSubstFontAliases(bool _subst_font_aliases = false)
    {subst_font_aliases = _subst_font_aliases;}

    // Make sure you have initialized font::FontInfo before you run
    // the typesetter
    void run(std::istream &input = std::cin, 
	     std::ostream &output = std::cout); 

    // These methods can be used after the typesetter has been run:
    const font::Fonts &getUsedFonts() const {return used_fonts;}
    // TODO:
    // const Warnings &getWarnings() const;
    // bool enoughPages() const;
  private:
    typedef Canvas::PageVec Pages;
    Pages pages;
    bool extra_pages, subst_font_aliases;
    OutputFormat output_format;
    font::Fonts used_fonts;
  };
};

#endif
