#ifndef X2P_PSCANVAS
#define X2P_PSCANVAS
///
// Copyright (C) 2002 - 2004, Fredrik Arnerup & Rasmus Kaj, See COPYING
///
#include "canvas.hh"
#include "psstream.hh"
#include "util/refcount.h"
#include <memory>

namespace xml2ps {

  /**
   * A Canvas which output is a PostScript file (or a set of PostScript
   * files).  It inherits Canvas for the drawing interface and PsStream to
   * manage the PostScript data.
   */
  class PsCanvas : public Canvas, public PsStream, public RefCounted {
  public:
    PsCanvas(const PageVec& pages, bool allow_extra_pages);
    virtual ~PsCanvas();
    
    virtual void newPage();
    virtual void closePage();
    
    void setgray(float gray);
    void setfont(const font::FontInfo& font);
    void moveto(float hpos, float vpos);
    void moverel(float dx, float dy);
    float textRise(float rise);
    float getRise() const;
    void setWordSpace(const float& space);
    
    void show(const Glib::ustring& text);
    void whitespace();
    void whitespace(const float& space);
    
    void underlineFrom(const float& below);
    void underlineTo(const float& below, const float& thickness);
    
    void line(float x1, float y1, float x2, float y2);
    
  private:
    std::auto_ptr<font::FontInfo> font_;
    float cur_rise, word_space;
  };
};
#endif
