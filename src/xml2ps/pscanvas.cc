///
// Copyright (C) 2002 - 2004, Fredrik Arnerup & Rasmus Kaj, See COPYING
///
#include "pscanvas.hh"
#include <algorithm>                // max?
#include <iomanip>
#include <stdexcept>
#include <util/stringutil.h>

xml2ps::PsCanvas::PsCanvas(const PageVec& pages,
			   bool allow_extra_pages)
  : Canvas(pages, allow_extra_pages), cur_rise(0)
{
  newPage();
}

xml2ps::PsCanvas::~PsCanvas() {}

void xml2ps::PsCanvas::newPage() {
  Canvas::newPage();
  PsStream::newPage(int(cur_page_->width()+0.5), int(cur_page_->height()+0.5));
}

void xml2ps::PsCanvas::closePage() {
  flush();
}

void xml2ps::PsCanvas::setgray(float gray) {
  (*this) << gray << " setgray\n";
}

void xml2ps::PsCanvas::setfont(const font::FontInfo& font) {
  Canvas::setfont(font);
  font_.reset(new font::FontInfo(font));
  std::string name=
    getSubstituteFontAliases()
    ? font_->getRealName() 
    : font_->getName();
  require_font(name);
  (*this) << font_->getSize() << " (" << name << ") F\n";
}

void xml2ps::PsCanvas::moveto(float hpos, float vpos) {
  (*this) << hpos << ' ' << vpos << " M\n";
}

void xml2ps::PsCanvas::moverel(float dx, float dy) {
  (*this) << dx << ' ' << dy << " rmoveto\n";
}

float xml2ps::PsCanvas::textRise(float rise) {
  float oldrise = cur_rise;
  float change = rise - cur_rise;
  cur_rise = rise;
  moverel(0, change);
  return oldrise;
}

void xml2ps::PsCanvas::setWordSpace(const float& space) {
  // the space parameter is in "times normal width of whitespace"
  // word_space is in points.
  word_space = font_->getWidth(" ") * space;
}

float xml2ps::PsCanvas::getRise() const {
  return cur_rise;
}

void xml2ps::PsCanvas::show(const Glib::ustring& text) {
  const float ls = font_->getLetterSpacing();

  std::string show_cmd;
  if(ls) show_cmd = tostr(ls) + " 0 3 -1 roll ashow";
  else show_cmd = "S";

  std::string ascii_part;
  for(Glib::ustring::const_iterator i = text.begin(); 
      i != text.end(); 
      ++i) {
    if(*i == '(' || *i == ')' || *i == '\\') { 
      // escape parentheses and backslash
      ascii_part += '\\';
    } 
    if(*i < 128) {// ASCII
      ascii_part += *i;
    } else if(*i == 160) { // nbsp
      ascii_part += ' ';
    } else {
      if(!ascii_part.empty()) { // flush ascii_part
	(*this) << "(" << ascii_part << ") " << show_cmd << '\n';
	ascii_part = "";
      }

      std::string glyphname = 
	font_->getGlyphName(Glib::ustring(Glib::ustring::size_type(1), *i));
      if(!glyphname.empty()) {
	(*this) << "/" << glyphname << " GS "; 
	if(ls) (*this) << ls << " WS";
	(*this) << '\n';
      } else {
	(*this) << "(?) " << show_cmd 
		<< " % " << *i << " " << glyphname << '\n';
      }
    }

  }
  if(!ascii_part.empty()) { // flush ascii_part
    (*this) << "(" << ascii_part << ") " << show_cmd << '\n';
  }
}

void xml2ps::PsCanvas::whitespace() {
  (*this) << word_space << " WS ";
}

void xml2ps::PsCanvas::whitespace(const float& space) {
  (*this) << space << " WS ";
}

void xml2ps::PsCanvas::underlineFrom(const float& below) {
  (*this) << below << " UFROM ";
}

void
xml2ps::PsCanvas::underlineTo(const float& below, const float& thickness) {
  (*this) << thickness << ' ' << below << " UTO\n";
}

void
xml2ps::PsCanvas::line(float x1, float y1, float x2, float y2) {
  (*this) << "newpath " << x1 << ' ' << y1 << " moveto "
	  << x2 << ' ' << y2 << " lineto stroke\n";
}
