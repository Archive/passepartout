<?xml version="1.0" encoding="iso-8859-1" ?>
<!DOCTYPE artikel SYSTEM "emission.dtd">
<artikel>
  <!-- sidrubrik>Bokrecensioner:</sidrubrik-->
  
  <rubrik>Kanonerna p� Navarone</rubrik>
  
  <ingress>Emissionen recenserar Alistair MacLeans klassiker om den mindre
    av omv�lvningarna vid nitton<bp/>hundra<bp/>talets b�rjan.</ingress>
  
  <para><start>Kriget har g�tt in</start> i en ny fas och Tyskland har
    potential f�r att ta led<bp/>ningen i kampen om jorden.
    Navarone sitter som en propp och isolerar de alli<bp/>erade.
    En laddning kan bli sista st�ten.</para>
  
  <para>En grupp batteri<bp/>eliminatorer drar ut i f�lt och st�ter p�
    motst�nds<bp/>r�relsen.
    Det �r en ganska sluten krets, men de lyckas f� kontakt med
    ledaren.
    Tyv�rr visar sig motst�ndet inneh�lla diskreta element med tveksamma
    kopplingar.</para>

  <para>Effektiv sp�nning p� h�g niv�.</para>

  <del>
    <rubrik>Experimentjunk ...</rubrik>
    
    <para>Ett stycke utan de speciella prickar och ringar som visar att en
      text har skrivits i Sverige.</para>
    
    <para>��� ������� ��� ��� � � � �� � �� �� �������� ���� ����� ��� �� ��
      ���� ��� ���� ��� ��� � ���� ��� �� � ������� �� �� �������� ��� ��
      ���� ������ ���������</para>
    
    <para>R�k<bp/>sm�rg�s<bp/>kompatibelt och
      ombuds<bp/>manna<bp/>spr�k<bp/>bruks<bp/>passande.
      R�k<bp/>sm�rg�s<bp/>kompatibelt och
      ombuds<bp/>manna<bp/>spr�k<bp/>bruks<bp/>passande.
      R�k<bp/>sm�rg�s<bp/>kompatibelt och
      ombuds<bp/>manna<bp/>spr�k<bp/>bruks<bp/>passande.
      R�k<bp/>sm�rg�s<bp/>kompatibelt och
      ombuds<bp/>manna<bp/>spr�k<bp/>bruks<bp/>passande.
    </para>
    
    <para>Och s� skriver vi lite mer text h�r, bara f�r att det ska bli
      lite l�ngre.
      Inget inneh�ll, <em>men inget kan ocks� vara viktigt</em>, och
      det �r i alla fall n�gra rader vanlig text som kan fylla ut lite
      snyggt.</para>
    <para><!-- Todo: Do not fake! --></para>
  </del>
  <del>
    <para>Vi vill ha mera text, vi vill ha flera ord, ett litet typsatt,
      ob�jt, tryckbart vanligt ord.
      Och vi ska s�tta dem, och vi ska trycka dem, s� att vi har en tidning
      f�re deadlinen&#160;...
    </para>
    <para>Det kommer texter h�r, det st�r en dator d�r, och pengar kanske
      om man s�ljer annonser.
      F�rst TeX och decwrite h�ll, sen vi f�r frame corp f�ll, men nu
      kanske man ska hacka rubbet sj�lv&#160;...</para>
  </del>
  
  <signatur>Fredrik &amp; Rasmus</signatur>
</artikel>
