///
// Copyright (C) 2002 - 2004, Fredrik Arnerup & Rasmus Kaj, See COPYING
///
#include <util/stringutil.h>
#include "paragraph.hh"
#include "blockcontainer.hh"
#include <stdexcept>
#include <memory>

xml2ps::Paragraph::Paragraph(Canvas& out, Element& parent,
			     const Glib::ustring& n, const Attributes attr)
  : TextContainer(parent, n, attr), out_(out),
    margin_top(attr.get("margin-top", 0, getFontSize())),
    margin_bottom(attr.get("margin-bottom", 0, getFontSize())),
    margin_left(attr.get("margin-left", 0, getFontSize())),
    margin_right(attr.get("margin-right", 0, getFontSize())),
    line_height(attr.get("line-height", 1, getFontSize())),
    spancolumns(attr.get("span-columns", false))
{}

void xml2ps::Paragraph::close() {
  TextContainer::close();
  
  out_.reserveHeight(margin_top + margin_bottom, spancolumns);
  out_.setfont(getFont());
  out_.addMargin(margin_top);
  BlockContainer *parent=dynamic_cast<BlockContainer*>(&getParent());
  if(!parent){
    if(dynamic_cast<Paragraph*>(&getParent()))
      throw std::runtime_error("Parent of <para> cannot be <para>");
    throw std::runtime_error("Parent of <para> is not <block-container>");
  }
  std::auto_ptr<Line> line(parent->getLine(*this, line_height,
					   margin_left, margin_right, 
					   spancolumns));
  flushLines(nodes.begin(), nodes.end(), *line);
  line->flush(true);
  out_.addMargin(margin_bottom);
}

void xml2ps::Paragraph::flushLines(NodeVect::const_iterator begin,
				   NodeVect::const_iterator end,
				   Line& line) {
  for(NodeVect::const_iterator i = begin; i != end; ++i) {
    if(!line.add(*i)) {
      if(const Element* elem = dynamic_cast<const Element*>(*i))
	flushLines(elem->begin(), elem->end(), line);
      else {
	line.flush();
	line.add(*i);
      }
    }
  }
}
