///
// Copyright (C) 2003, 2004, Fredrik Arnerup & Rasmus Kaj, See COPYING
///
#include "xcanvas.hh"
#include <iostream>

namespace {

// can use CanvasAA to, but the fonts get a different size (???)
class TextCanvas: public Gnome::Canvas::Canvas {
public:
  TextCanvas()
    : canvasgroup(*(root()), 0, 0)
  {
//     using namespace Gnome::Canvas;
//     text = new Gnome::Canvas::Text(canvasgroup, 0, 0, "Origo");
//     *text << Properties::font("-Adobe-Helvetica-Medium-R-Normal--*-100-*-*-*-*-*-*")
// 	  << Properties::fill_color("blue"); //Changes the color of the text.
  }
  ~TextCanvas() {
    delete text;
  }

  Gnome::Canvas::Group canvasgroup;
private:
  Gnome::Canvas::Text *text;
};

};

namespace xml2ps {

XCanvas::XCanvas(const PageVec& pages, bool allow_extra_pages)  
  : Canvas(pages, allow_extra_pages), x(0), y(0), current_font(0)
{
  res = 72;
  kit = new Gtk::Main(0, 0);
  window = new Gtk::Window();
  window->set_title("xml2ps");
  window->set_resizable(false);
  Gnome::Canvas::init();
  canvas = manage(new TextCanvas());
  window->add(*canvas);
  window->show_all();
  newPage();
}

XCanvas::~XCanvas() {
  draw(0);
  Gtk::Main::run(*window); // display it here
  delete window;
  delete kit;
}

void XCanvas::newPage(){
  Canvas::newPage();
  /// \todo clear drawingarea
  window->set_size_request(int(cur_page_->width() * 72 / res + 0.5), 
  			   int(cur_page_->height() * 72 / res + 0.5));
  canvas->set_scroll_region(0, 
			    0,
			    cur_page_->width() * 72 / res + 0.5, 
			    cur_page_->height() * 72 / res + 0.5);
}

void XCanvas::setgray(float gray){
  color.set_grey(int(gray * 255));
}

void XCanvas::setfont(const font::FontInfo& font) {
  Canvas::setfont(font);
  current_font = &font;
}

void XCanvas::moveto(float hpos, float vpos) {
  x = hpos; y = vpos;
}

float XCanvas::textRise(float rise) { return 0; } /// \todo implement
float XCanvas::getRise() const { return 0; } /// \todo implement
void XCanvas::setWordSpace(const float& space) {} /// \todo implement

void XCanvas::whitespace() {
  whitespace(17); /// \todo do something good
}

void XCanvas::whitespace(const float& space) {
  x += space;
}

void XCanvas::show(const Glib::ustring& text) {
  Glib::ustring fontname = current_font->getName();
  float width = current_font->getWidth(text);

  unsigned int index = fontname.find_first_of('-');
  if(index < fontname.length())
    fontname = fontname.substr(0, index);

  // It seems that X defines font size as the height of a capital letter,
  // while postscript defines it as the recommended minimum line height
  // or something like that.
  // It doesn't seem consistent in CanvasAA, though.
  parts.push_back(Part(text, fontname, current_font->getAscender(),
		       x, y, 
		       color));
  x += int(width + 0.5);
  parts.back().width = width;
}	    

float XCanvas::p2s(float length) {
  return length * 72 / res;
}

bool XCanvas::draw(GdkEventExpose*) {
  for(Parts::const_iterator 
	i = parts.begin(); i != parts.end(); i++) {

    Pango::FontDescription font;
    //    font.set_family(i->fontname);
    //    std::cerr << i->fontname << std::endl;
    if(i->fontname == "Helvetica")
      //      font.set_family("helvetica");
      font.set_family("nimbus sans l");
    else if(i->fontname == "Times")
      //      font.set_family("times");
      font.set_family("nimbus roman no9 l");

    //      font.set_family("nimbus sans l");
    font.set_size(long(p2s(i->fontsize) * Pango::SCALE + 0.5));
    //   font.set_stretch(Pango::STRETCH_CONDENSED);

    // create a layout, just so that we can measure it
    Glib::RefPtr<Pango::Context> context = window->get_pango_context();
    Glib::RefPtr<Pango::Layout> layout = Pango::Layout::create(context);
    layout->set_text(i->text);
    layout->set_font_description(font);
    int iw, ih;
    layout->get_size(iw, ih);
//     float w = iw / Pango::SCALE;
//     float h = ih / Pango::SCALE;
    layout->get_pixel_size(iw, ih);

    Gnome::Canvas::Rect *rect = 
      new Gnome::Canvas::Rect(dynamic_cast<TextCanvas*>(canvas)->canvasgroup, 
  			      p2s(i->x), 
  			      p2s(cur_page_->height() - i->y - i->fontsize),
			      p2s(i->x + i->width),
  			      p2s(cur_page_->height() - i->y));
    using namespace Gnome::Canvas::Properties;
    *rect << width_pixels(1)
	  << fill_color("white")
	  << outline_color("red");
    //    rect->affine_relative(Gnome::Art::AffineTrans::rotation(2));

//     rect = 
//       new Gnome::Canvas::Rect(dynamic_cast<TextCanvas*>(canvas)->canvasgroup, 
//   			      p2s(i->x), 
//   			      p2s(cur_page_->height() - i->y) - ih,
// 			      p2s(i->x) + iw,
//   			      p2s(cur_page_->height() - i->y));
//     using namespace Gnome::Canvas::Properties;
//     *rect << width_pixels(1)
// 	  << outline_color("blue");

    /// \todo delete text objects
    Gnome::Canvas::Text *text = 
      new Gnome::Canvas::Text(dynamic_cast<TextCanvas*>(canvas)->canvasgroup,
  			      p2s(i->x), // + iw / 2, 
  			      p2s(cur_page_->height() 
				  - i->y - i->fontsize / 2),
  			      i->text);

    //    Glib::ustring tmp = font.to_string();
    //    std::cerr << tmp << std::endl;
    //  Glib::free(tmp) //?

    using namespace Gnome::Canvas::Properties;

    // text->affine_relative(Gnome::Art::AffineTrans::scaling(0.5, 1));
    //    text->affine_relative(Gnome::Art::AffineTrans::rotation(2));
    *text << Property<Pango::FontDescription>("font_desc", font);
    text->property_anchor() = Gtk::ANCHOR_W;
    text->property_fill_color_rgba() = 0x000000ff; // for CanvasAA
    //    *text << Property<double>("size_points", p2s(i->fontsize));
      //      	  << Property<Gtk::Justification>("justification", Gtk::JUSTIFY_LEFT);
  }
  canvas->set_center_scroll_region(false);
  canvas->scroll_to(0,0);  
  return false;
}

void XCanvas::underlineFrom(const float& below) {
  // Todo
}

void XCanvas::underlineTo(const float& below, const float& thickness) {
  // Todo
}

void XCanvas::line(float x1, float y1, float x2, float y2) {
  // Todo
}

};
