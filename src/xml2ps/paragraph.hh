///
// Copyright (C) 2002 - 2004, Fredrik Arnerup & Rasmus Kaj, See COPYING
///
#ifndef X2P_PARAGRAPH_HH
#define X2P_PARAGRAPH_HH
#include "xly.hh"
#include "canvas.hh"
#include "line.hh"

namespace xml2ps {

  // A Paragrah is a block level Element that only contains inline elements.
  class Paragraph : public TextContainer {
  public:
    Paragraph(Canvas& out, Element& parent,
	      const Glib::ustring& n, const Attributes attr);
    
    virtual void close();
  private:
    Canvas &out_;
    const Glib::ustring align;
    float margin_top, margin_bottom, margin_left, margin_right, line_height;
    bool spancolumns;
    
    void flushLines(NodeVect::const_iterator begin,
		    NodeVect::const_iterator end,
		    Line& line);
  };

}

#endif
