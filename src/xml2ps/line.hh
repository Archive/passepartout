///
// Copyright (C) 2002 - 2004, Fredrik Arnerup & Rasmus Kaj, See COPYING
///
#ifndef X2P_LINE
#define X2P_LINE
#include <fonts/fontinfo.hh>
#include "canvas.hh"
#include "xly.hh"

namespace xml2ps {
  class Line {
  public:
    Line(const Element& elem, Canvas& o, float line_h, bool span,
	 float margin_left, float margin_right);
    ~Line();
    
    bool add(Node* node);
    
    // when the line is_last, no new hbox is created, and text is not
    // justified.
    void flush(bool is_last = false);
    
  private:
    Canvas& out;
    HBox hbox;
    float text_width, space_width;
    const Element& element;
    const Node *first_node, *last_node;

    float line_height;
    bool spancolumns;
    // Ok, I could ask the hbox when I need this, but then the hbox would need
    // to contain both unmodified and modified margins ...
    float m_left, m_right;
  };

}

#endif
