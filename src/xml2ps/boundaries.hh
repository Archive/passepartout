///
// Copyright (C) 2002 - 2004, Fredrik Arnerup & Rasmus Kaj, See COPYING
///
#ifndef X2P_BOUNDARIES
#define X2P_BOUNDARIES
#include "util/boundary.h"
#include <string>
#include <vector>

namespace xml2ps {
  
  class PageSize {
  public:
    PageSize() {}
    explicit PageSize(int num, const std::string& s);
    explicit PageSize(int num, float width, float height, 
		      int columns = 1, float gutter = 10);
    
    float width()  const { return width_; }
    float height() const { return height_; }
    int  columns() const { return columns_; }
    float gutter() const { return gutter_; }
    int num() const { return num_; }
    
    float left(const int column) const { 
      return column * (columnWidth() + gutter_);
    }
    float columnWidth(bool span = false) const {
      if (span || columns_ == 1)
	return width_;
      else
	return ((width_ - gutter_*(columns_-1)) / columns_);
    }
  private:
    float width_, height_, gutter_;
    int columns_, num_;
  };
  
  class HBox {
  public:
    HBox() : baseline_(0), ascender_(0), descender_(0),
	     left_(0), left_out_(0),	right_(0), right_out_(0) {}
    HBox(const float& baseline, const float& ascender, const float& descender,
	 const float& left, const float& right,
	 const float& l_margin, const float& r_margin)
      : baseline_(baseline), ascender_(ascender), descender_(descender),
	left_(left+l_margin), left_out_(left),
	right_(right-r_margin), right_out_(right)
    {}
    
    float room()  const { return right_ - left_; }
    // Inner positions (inside margins).
    float left()  const { return left_; }
    float right() const { return right_; }

    void inLeft(const float& in) { left_ += in; }
    void inRight(const float& in) { right_ -= in; }
    
    float baseline() const { return baseline_; }
    float top() const { return baseline_ + ascender_; }
    float bottom() const { return baseline_ - descender_; }
    
  private:
    float baseline_, ascender_, descender_;
    // *out is outside the margins, the others are inside margins.
    // obstacles makes margins wider.
    float left_, left_out_, right_, right_out_;
  };
  
  class PageBoundary : public PageSize {
  public:
    typedef std::vector<Boundary> ObVec;
    
    explicit PageBoundary(int num, const std::string& init) 
      : PageSize(num, init) {}
    explicit PageBoundary(int num, float width, float height, 
			  int columns = 1, float gutter = 10) :
      PageSize(num, width, height, columns, gutter)
    {}
    ~PageBoundary();
    
    void addObstacle(Boundary obstacle) { obstacles.push_back(obstacle); }
    const ObVec::const_iterator obegin() const { return obstacles.begin(); }
    const ObVec::const_iterator oend()   const { return obstacles.end(); }
  private:
    ObVec obstacles;
  };

};

#endif
