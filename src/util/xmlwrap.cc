#include "xmlwrap.h"
#include "filesys.h"

std::string ElementWrap::get_filename(const std::string& attrname) const {
  if(const xmlpp::Attribute* file = element_.get_attribute(attrname)) {
    const std::string value = file->get_value();
    if(value.empty() || value[0] == '/')
      return value;
    else
      return path(filename_) + value;
  } else
    return std::string();
}
