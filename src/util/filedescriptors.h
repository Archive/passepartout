#ifndef INCU_FILEDESC
#define INCU_FILEDESC
///
// Copyright (C) 2003, 2004, Fredrik Arnerup & Rasmus Kaj, See COPYING
///
// Strange stuff that works directly on filedescriptors.  The implementation
// of other parts of incu use this, but you are not encoraged to use this
// directly ...
#include <iostream>

namespace filedesc {

  int move(int to, int from);

  class BufBase : public std::streambuf {
  public:
    void close();
    
  protected:
    BufBase(int fd, bool should_close, int bufsize);
    virtual ~BufBase();
    
    int fd_;
    bool should_close_;
    int bufsize_;
  };
  
  class OutBuf : virtual public BufBase {
  public:
    OutBuf(int fd, bool should_close = false, int bufsize = 1024);
    virtual ~OutBuf();
    
  protected:
    virtual int sync();
    virtual int overflow (int c);
    
  private:
    char *outbuffer_;
  };
  
  class InBuf : virtual public BufBase {
  public:
    InBuf(int fd, bool should_close = false, int bufsize = 1024);
    virtual ~InBuf();
    
  protected:
    virtual int underflow ();
    
  private:
    char *inbuffer_;
  };

  class IoBuf : public OutBuf, public InBuf {
  public:
    IoBuf(int fd, bool should_close = false, int bufsize = 1024);
  };
};

#endif
