#ifndef OS_H			// -*- c++ -*-
#define OS_H
///
// Copyright (C) 2002 - 2004, Fredrik Arnerup & Rasmus Kaj, See COPYING
///
#include <string>

/**
 * User-friendly C++-style portable OS interface.
 * Philosophy: POSIX is da thing
 */
namespace os {
  /// username of user
  /** Returns an empty string on failure. */
  std::string username();  

  /// full name of user
  /** Returns an empty string on failure. */
  std::string fullname();

  /// name of host
  /** Returns an empty string on failure. */
  std::string hostname();

  /// operating system of host
  /** Returns an empty string on failure. */
  std::string sysname();   

  /// operating system release number of host
  /** Returns an empty string on failure. */
  std::string release();   

  /// host architecture
  /** Returns an empty string on failure. */
  std::string machine();

  /// read environment variable
  /** Returns an empty string if the variable name does not exist. */
  std::string get_env(const std::string &name);
}

#endif
