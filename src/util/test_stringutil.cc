///
// Copyright (C) 2003, 2004, Fredrik Arnerup & Rasmus Kaj, See COPYING
///
#include "stringutil.h"
#include "testbed/testbed.hh"

template<class Value>
class TestStringConvert : public TestCase {
public:
  TestStringConvert(std::string input, Value value)
    : TestCase("StringConvert " + input),
      input_(input), value_(value)
    {}
  
  void test() {
    ASSERT(value_ == to<Value>(input_));
    ASSERT(input_ == tostr(value_));
  }
  
private:
  const std::string input_;
  const Value value_;
};

namespace {
  TestStringConvert<float> t1("14", 14);
  TestStringConvert<float> t3("-14.2", -14.2);
  TestStringConvert<int>   t4("4711", 4711);
  TestStringConvert<int>   t5("-17", -17);
  TestStringConvert<bool>  t6("true", true);
  TestStringConvert<bool>  t7("false", false);
}

class TestRoman : public TestCase {
public:
  TestRoman(int num, std::string roman, bool expect_failure = false)
    : TestCase("Roman " + tostr(num)),
      num_(num), roman_(roman), expect_failure_(expect_failure)
  {}

  void test() {
    if(expect_failure_) {
      bool ok = false;
      try {
	to_roman(num_);
	ok = true;
      } catch(...) {}
      if(ok) 
	throw std::logic_error("Expected an exception to be thrown.");
    } else ASSERT(roman_ == to_roman(num_));
  }

private:
  const int num_;
  const std::string roman_;
  const bool expect_failure_;
};

namespace {
  TestRoman trm1(-1, "-i", true); // should fail
  TestRoman tr0(0, "", true); // should fail
  TestRoman tr1(1, "i");
  TestRoman tr2(17, "xvii");
  TestRoman tr3(1066, "mlxvi");
  TestRoman tr4(1976, "mcmlxxvi");
  TestRoman tr5(3999, "mmmcmxcix");
  TestRoman tr6(4000, "mmmm", true); // should fail
}
