///
// Copyright (C) 2002 - 2004, Fredrik Arnerup & Rasmus Kaj, See COPYING
///
#include "os.h"

#include <cstdlib> // getenv(3)
#include <unistd.h> // unlink(2)
#include <sys/utsname.h>
#include <pwd.h>
#include <sys/types.h>

#include "warning.h"

namespace os{
  std::string username(){
    struct passwd *pw = getpwuid(getuid()); // getuid and getpwuid are POSIX
    return pw && pw->pw_name ? pw->pw_name : "";
  }

  std::string fullname(){
    struct passwd *pw = getpwuid(getuid()); // getuid and getpwuid are POSIX
    std::string pw_s = pw && pw->pw_gecos ? pw->pw_gecos : "";
    const std::string::size_type pos = pw_s.find_first_of(',');
    return pw_s.substr(0, pos);
  }
  
  enum uname_field {uf_sysname, uf_nodename, uf_release, 
		    uf_version, uf_machine};

  static std::string uname_helper(uname_field field){
    static struct utsname buf;
    static bool uname_called = false; // we need only call uname once
    if(!uname_called && (uname(&buf)!=0)) // uname is POSIX
      return ""; // error
    uname_called=true;
    switch(field){
    case uf_sysname: return buf.sysname;
    case uf_nodename: return buf.nodename;
    case uf_release: return buf.release;
    case uf_version: return buf.version;
    case uf_machine: return buf.machine;
    default: 
      warning << "default reached in uname_helper" << std::endl;
      return "";
    }
  }
  
  std::string hostname() {return uname_helper(uf_nodename);}
  std::string sysname() {return uname_helper(uf_sysname);}
  std::string release() {return uname_helper(uf_release);}
  std::string machine() {return uname_helper(uf_machine);}

  std::string get_env(const std::string &name)
  {
    char *var = getenv(name.c_str()); // getenv is POSIX & K&R 
    return var ? var : "";
  }
}
