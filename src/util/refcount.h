#ifndef REFCOUNT_H		// -*- c++ -*-
#define REFCOUNT_H
///
// Copyright (C) 2002 - 2004, Fredrik Arnerup & Rasmus Kaj, See COPYING
///

/**
 * A simple base for a referece counted object.  Glib::ObjectBase can be used
 * for this purpose, but this class is lighter since it doesn't support
 * properties and events.
 */
class RefCounted {
protected:
  RefCounted() : count_(0) {}
  virtual ~RefCounted() {}
public:
  /** 
   * Increse the reference count for this object.
   */
  void reference() const { ++count_; }
  
  /**
   * Decrease the reference count for this object.  If the reference count
   * reaches zero, the object is deleted.
   */
  void unreference() const { if(--count_ == 0) delete this; }

private:
  mutable unsigned int count_;
  
  /** undefined */
  RefCounted(const RefCounted&);
  /** undefined */
  RefCounted operator = (const RefCounted&);
};

#endif
