#ifndef VECTOR_H		// -*- c++ -*-
#define VECTOR_H
///
// Copyright (C) 2002 - 2004, Fredrik Arnerup & Rasmus Kaj, See COPYING
///
#include <algorithm> // max
#include <cmath> // sqrt
#include <string>

template<class C> C sqr(const C& c) { return c*c; }

template <class C>
struct VectorOf
{
  C x, y;
  VectorOf(): x(C()), y(C()) {} // the zero vector
  VectorOf(C xx, C yy): x(xx), y(yy) {}

  VectorOf& operator += (const VectorOf& d)
    { x += d.x; y += d.y; return *this; }
  VectorOf& operator /= (const C& d)
    { x /= d; y /= d; return *this; }
};
typedef VectorOf<double> Vector;
typedef VectorOf<int> IVector;

template<class C>
inline VectorOf<C> operator - (const VectorOf<C>& a, const VectorOf<C> b) {
  return VectorOf<C>(a.x - b.x, a.y - b.y);
}
template<class C>
inline VectorOf<C> operator + (const VectorOf<C>& a, const VectorOf<C> b) {
  return VectorOf<C>(a) += b;
}
template<class C>
inline VectorOf<C> operator / (const VectorOf<C>& a, const C& b) {
  return VectorOf<C>(a) /= b;
}

template<class C>
inline std::ostream& operator << (std::ostream& out, const VectorOf<C> v) {
  return out << v.x << ',' << v.y;
}

template<class C>
inline C dist(const VectorOf<C>& a, const VectorOf<C>& b) {
  return C(sqrt(sqr(a.x - b.x) + sqr(a.y - b.y)));
}

template<class C>
inline C chessboard_dist(const VectorOf<C>& a, const VectorOf<C>& b) {
  return C(std::max(fabs(a.x - b.x), fabs(a.y - b.y)));
}
#endif
