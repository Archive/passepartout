#ifndef STDERR_H		// -*- c++ -*-
#define STDERR_H
///
// Copyright (C) 2002 - 2004, Fredrik Arnerup & Rasmus Kaj, See COPYING
///
#include <iostream>

// Aliases for std::cerr that can be turned off.

struct cerrAlias
{
  explicit cerrAlias(bool a) : active(a) {}

  bool active;

  template<class A>
  std::ostream &operator<<(const A &a) {
    if(active) { 
      try {
	return std::cerr << a; 
      } catch(...) { // Glib::ustring might throw a Glib::ConvertError
	return std::cerr << "[error in << operator]";
	// We might want to create a smarter fallback
      }
    } else return null;}
  
  operator bool () const { return active && std::cerr; }
  
private:
  static std::ostream null;
};

// serious stuff, should not be turned off
extern cerrAlias warning;

// for people who want to know what happens behind the scenes
extern cerrAlias verbose;

// debug stuff that means nothing to the user
extern cerrAlias debug;

#endif
