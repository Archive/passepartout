///
// Copyright (C) 2003, 2004, Fredrik Arnerup & Rasmus Kaj, See COPYING
///
#include "boundary.h"
#include "testbed/testbed.hh"
#include "stringutil.h"

void testBoundaryRefs() {
  Boundary b1 = to<Boundary>("100x20+0+302");
  Boundary b2 = b1; // actually same object
  Boundary b3 = b1->clone();	// Other object, same value
  Boundary b4 = b1 * Matrix::scaling(2, 2);

  ASSERT(b1 == b2);
  ASSERT(b1 == b3);
  ASSERT(b1 != b4);
  ASSERT(b4 != b3);
  ASSERT(b3 * Matrix::scaling(2, 2) == b4);
}

namespace {
  TestFunction t("Boundary refs", &testBoundaryRefs);
}
