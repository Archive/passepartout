///
// Copyright (C) 2002 - 2004, Fredrik Arnerup & Rasmus Kaj, See COPYING
///
#include "boundary.h"
#include "rectboundary.h"	// The input operator creates specifiks
#include "typeinfo.h"
#include <iostream>

template<> std::string TypeInfo<Boundary>::name() { return "boundary"; }

std::istream& operator >> (std::istream& in, Boundary& b) {
  float w, h, left, bottom;
  char x;
  if((in >> w >> x) && (x == 'x') && (in >> h >> left >> bottom))
    b = RectBoundary::create(Matrix::translation(left, bottom), w, h);
  
  return in;
}

Boundary operator * (const Matrix& m, const Boundary& b) {
  if(!b) return Boundary();
  Boundary result = b->clone();
  result->transformLeft(m);
  return result;
}

Boundary operator * (const Boundary& b, const Matrix& m) {
  if(!b) return Boundary();
  Boundary result = b->clone();
  result->transform(m);
  return result;
}

