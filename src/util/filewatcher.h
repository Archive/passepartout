#ifndef FILEWATCHER_H		// -*- c++ -*-
#define FILEWATCHER_H
///
// Copyright (C) 2002 - 2004, Fredrik Arnerup & Rasmus Kaj, See COPYING
///
#include <sigc++/sigc++.h>
#include <time.h>
#include <string>
#include <memory>

namespace FAM { class File; }

/// Monitor a file for changes.
/** Uses the File Alteration Monitor from SGI if available, otherwise
 * it calls stat(2) on the file at regular intervals. */
class FileWatcher: public sigc::trackable {
public:
  /**
   * \param filename File to watch. Must be an absolute path.
   */
  explicit FileWatcher(const std::string &filename);
  virtual ~FileWatcher();
  void set_file(const std::string &filename);

  /// Signal emitted each time a change is detected.
  sigc::signal<void> modified_signal;

protected:
  std::string file;
  time_t last_time;
  std::auto_ptr<FAM::File> famfile;
  sigc::connection connection;
  bool check_file();

private:
  // Undefined ctors, avoid defaults
  FileWatcher(const FileWatcher&);
  FileWatcher();
  void operator = (const FileWatcher&);
};

#endif
