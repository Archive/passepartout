///
// Copyright (C) 2003, 2004, Fredrik Arnerup & Rasmus Kaj, See COPYING
///
#include "cmdline.h"
#include "testbed/testbed.hh"

std::string join_strings(char const **argv) {
  std::string tmp;
  for(int i = 1; argv[i]; i++)
    tmp += std::string(" ") + argv[i];
  return tmp;
}

struct TestCmdLine: public TestCase {
  TestCmdLine(char const **argv, bool expect_failure = false)
    : TestCase("CmdLine '" + join_strings(argv) + "'"),
      _argv(argv), _expect_failure(expect_failure)
  {}
  
  void test() {
    int argc = 0;
    for(; _argv[argc]; argc++);
    //    std::cerr << argc << std::endl;
    CmdLine cmdline(argc, const_cast<char**>(_argv));
    cmdline.add_option(CmdLine::Option('p'));
    cmdline.add_option(CmdLine::Option('f', "foo",
				       CmdLine::Option::REQ_PARAM));
    cmdline.add_option(CmdLine::Option('b', "bar",
				       CmdLine::Option::OPT_PARAM));
    cmdline.add_option(CmdLine::Option('n', "nisse"));
    cmdline.add_option(CmdLine::Option(0, "lisa"));
//     using namespace std;
    try {
      CmdLine::ParsedOptions opts = cmdline.parse();
//       for(CmdLine::ParsedOptions::const_iterator
// 	    arg = opts.begin(); arg != opts.end(); arg++) {
// 	if(!arg->first)
// 	  cerr << arg->second << endl;
// 	else 
// 	  cerr << arg->first->short_opt << "/" << arg->first->long_opt << ": "
// 	       << arg->second << endl;
//       }
      if(_expect_failure)
	throw std::logic_error("Expected an exception to be thrown.");
    }
    catch(const CmdLine::Error &e) {
      if(!_expect_failure)
	throw;
    }
  }

  char const **_argv;
  bool _expect_failure;
};

namespace {
  char const *a1[] = {"fooprog", "-pbn", "file", "-ffoo",
		"-f", "foo", "--foo=foo",
	       "--", "-x", "--junk", 0};
  TestCmdLine t1(a1);

  char const *a2[] = {"fooprog", "-x", 0};
  TestCmdLine t2(a2, true);

  char const *a3[] = {"fooprog", "--nisse", "--lisa", "--bar=bar", "--bar", 0};
  TestCmdLine t3(a3);

  char const *a4[] = {"fooprog", "--lisa=foo", 0};
  TestCmdLine t4(a4, true);

  char const *a5[] = {"fooprog", "--kurt", 0};
  TestCmdLine t5(a5, true);
}
