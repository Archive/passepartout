#ifndef BOUNDARY_H		// -*- c++ -*-
#define BOUNDARY_H
///
// Copyright (C) 2002 - 2004, Fredrik Arnerup & Rasmus Kaj, See COPYING
///
#include "refcount.h"
#include "matrix.h"
#include <glibmm/refptr.h>
#include <vector>

/**
 * A Polygon is a c++ vector of geometric points, described by Vector's.
 */
typedef std::vector<Vector> Polygon;

namespace Corner {
  enum Id { LL, LR, UL, UR, };
}

class Boundary;			// defined below

/**
 * A boundary is a limited area, rectangular or something else.  It might be
 * used as an obstacle or as a 1clipping path, etc.  Borders might be drawn
 * along a boundary (but not /by/ it).
 */
class BoundaryBase : public RefCounted {
public:
  /** Create a distinct Boundary identical to this one. */
  virtual Boundary clone() const = 0;
  
  /** Just for the outuput operator */
  virtual std::ostream& print(std::ostream&) const = 0;
  
  /**
   * Get the coordinates for a basic corner of this boundary.
   */
  virtual Vector getCorner(Corner::Id corner) const = 0;
  
  /**
   * Check if v is inside or within distance dist from this boundary.
   */
  virtual bool isInsideOrClose(const Vector& v, const float& dist = 0) = 0;

  /**
   * Grow this boundrary {amount} units in all directions.
   * \return this Boundary.
   */
  virtual void grow(const float& amount) = 0;
  
  /**
   * Get the polygon limiting this boundary.  The polygon should allways go
   * clock-wise around the bounary.  The polygon is implicitly closed (i.e, a
   * line from the last point to the first is assumed.  In case of "smooth"
   * boundaries, an approximation is returned.
   */
  virtual Polygon get_polygon() = 0;

  friend Boundary operator * (const Matrix&, const Boundary&);
  friend Boundary operator * (const Boundary&, const Matrix&);
  friend bool operator == (const Boundary&, const Boundary&);
  friend bool operator != (const Boundary&, const Boundary&);
  
  virtual float get_width() const = 0;
  virtual float get_height() const = 0;
protected:
  // Methods only for use by the Boundary operators.
  virtual void transform(const Matrix& m) = 0;
  virtual void transformLeft(const Matrix& m) = 0;
  virtual bool equals(const BoundaryBase* m) const = 0;
};

/**
 * Boundary is a true by-name object.  There is no need to create a pointer
 * to a Boundary, since Boundary is by itself a pointer.
 * Note: This not just a typedef, sine I define some operators below which
 * should take precedence over those given by RefPtr.
 */
class Boundary : public Glib::RefPtr<BoundaryBase> {
public:
  Boundary() {}
  Boundary(BoundaryBase* ptr) : Glib::RefPtr<BoundaryBase>(ptr) {}
};

/**
 * Output a boundaray to a stream.  BoundaryBase::print is used to
 * "virtualize" the opeartor.
 */
inline std::ostream& operator << (std::ostream& out, const Boundary& b) {
  return b->print(out);
}

/**
 * Read a boundary from an input stream (use with stringutils to create a
 * boundary from a string, "Boundary foo = to<Boundary>(stringdata)").
 */
std::istream& operator >> (std::istream& in, Boundary& b);

/** Multiply a boundary by a matrix.  Note that this is non-commutative. */
Boundary operator * (const Matrix& m, const Boundary& b);

/** Multiply a boundary by a matrix.  Note that this is non-commutative. */
Boundary operator * (const Boundary& b, const Matrix& m);

/** Check if two boundaries are equal */
inline bool operator == (const Boundary& a, const Boundary& b) {
  return ((!a && !b) || (a && a->equals(b.operator->())));
}

/** Check if two boundaries are not equal */
inline bool operator != (const Boundary& a, const Boundary& b) {
  return !(a == b);
}

typedef std::vector<Boundary> BoundaryVect;

#endif
