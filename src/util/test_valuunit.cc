///
// Copyright (C) 2003, 2004, Fredrik Arnerup & Rasmus Kaj, See COPYING
///
#include "valueunit.h"
#include "testbed/testbed.hh"

template<class Value>
class TestValue : public TestCase {
public:
  TestValue(std::string input, Value value, std::string unit)
    : TestCase("ValueUnit " + input),
      input_(input), value_(value), unit_(unit)
    {}
  
  void test() {
    ValueUnit<Value> vu = to<ValueUnit<Value> >(input_);
    
    if(vu.value() != value_)
      throw std::logic_error("Unexpeted value");
    
    if(vu.unit() != unit_)
      throw std::logic_error("Unexpected unit");
  }
private:
  std::string input_;
  Value value_;
  std::string unit_;
};

namespace {
  TestValue<float> t1("14em", 14, "em");
  TestValue<float> t2("14.0em", 14, "em");
  TestValue<float> t3("14.000em", 14, "em");
  TestValue<float> t4("14.2pt", 14.2, "pt");
  TestValue<float> t7("14.2", 14.2, "");
  TestValue<int>   t5("4711pt", 4711, "pt");
  TestValue<int>   t6("4711h", 4711, "h");
  TestValue<int>   t8("17", 17, "");
}
