///
// Copyright (C) 2002 - 2004, Fredrik Arnerup & Rasmus Kaj, See COPYING
///
#include "barrier.h"

Barrier::Barrier()
  : is_open(false)
{}

Barrier::~Barrier() {}

void Barrier::wait() {
  Glib::Mutex::Lock lock(mutex);
  while (!is_open)
    cond.wait(mutex);
}

void Barrier::open() {
  Glib::Mutex::Lock lock(mutex);
  if(!is_open) {
    is_open = true;
    cond.broadcast();
  }
}

void Barrier::reclose() {
  Glib::Mutex::Lock lock(mutex);
  is_open = false;
}
