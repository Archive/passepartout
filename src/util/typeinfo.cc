///
// Copyright (C) 2003, 2004, Fredrik Arnerup & Rasmus Kaj, See COPYING
///
#include "typeinfo.h"
#include <glibmm/ustring.h>

// Instantiations of TypeInfo for standard types

template<> std::string TypeInfo<int>::name() { return "integer"; }
template<> std::string TypeInfo<double>::name() { return "double float"; }
template<> std::string TypeInfo<float>::name() { return "float"; }
template<> std::string TypeInfo<bool>::name() { return "boolean"; }
template<> std::string TypeInfo<Glib::ustring>::name() { return "UTF8 string";}
