#ifndef CONFIGFILE_H		// -*- c++ -*-
#define CONFIGFILE_H
///
// Copyright (C) 2002 - 2004, Fredrik Arnerup & Rasmus Kaj, See COPYING
///
#include <string>
#include <vector>
#include <iostream>

namespace Config {

struct Var {
  std::string name;
  Var(std::string name_): name(name_) {}
  virtual ~Var() {}
  virtual void print(std::ostream &out) const = 0;
  virtual void parse(const std::string &input) = 0;
};

typedef std::vector<bool> Bools;

struct BoolVar: public Var {
  Bools values;
  BoolVar(std::string name_): Var(name_) {}
  virtual ~BoolVar() {}
  void print(std::ostream &out) const;
  void parse(const std::string &input);
};

typedef std::vector<std::string> Strings;

struct StringVar: public Var {
  Strings values;
  StringVar(std::string name_): Var(name_) {}
  virtual ~StringVar() {}
  void print(std::ostream &out) const;
  void parse(const std::string &input);
};

typedef std::vector<float> Floats;

struct FloatVar: public Var {
  Floats values;
  FloatVar(std::string name_): Var(name_) {}
  virtual ~FloatVar() {}
  void print(std::ostream &out) const;
  void parse(const std::string &input);
};

typedef std::vector<Var*> Vars;

/**
 * A configuration file parser.  Each statement can only be one line
 * of the format: name = value The variable name may contain any
 * character except '=' and newline.  All variables must be declared,
 * so that the parser knows what format to expect the value to be
 * in. Actually, the parsing of the value is handled by the declared
 * variable object itself, making it easy to add new types.
 *
 * Existing types are lists of booleans, strings and floats. The
 * values in a list are separated by whitespace. Strings may be quoted
 * with '"'. The '"' character may be escaped (in strings only) with
 * '\"'. 
 *
 * Comments start with '#' and may only appear on lines of their own.*/
class File {
public:
  File(): ignore_unknown(false) {}
  virtual ~File() {}
  void set_filename(std::string filename);
  void declare_var(Var *var);
  void read(const std::string &filename);
  virtual void write(const std::string &filename);

protected:
  bool ignore_unknown; // don't complain about unknown variables
  /** Report error. Feel free to override this method. */
  virtual void error(const std::string &message) {
    std::cerr << message << std::endl;
  }
  Vars vars;
};

}

#endif
