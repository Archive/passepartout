///
// Copyright (C) 2003, 2004, Fredrik Arnerup & Rasmus Kaj, See COPYING
///
#include "testbed/testbed.hh"
#include "processman.h"
#include <iostream>

/**
 * Put some output to an external wc command, check that we can read back
 * correct measurments.
 */
void testProcessIO() {
  std::vector<std::string> cmd;
  cmd.push_back("wc");
  Process proc = ProcessManager::instance().run(cmd);
  proc->get_cin() << "Foo bar" << std::endl
		  << "baz" << std::endl;
  proc->close_cin();
  
  int lines, words, chars;
  ASSERT(proc->get_cout() >> lines >> words >> chars);
//   std::cerr << "Lines: " << lines << ", words: " << words
// 	    << ", chars:" << chars << std::endl;
  ASSERT((lines == 2) && (words == 3) && (chars == 12))
}

namespace {
  TestFunction t("Process IO", &testProcessIO);
}
