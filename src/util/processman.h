#ifndef PROCESSMAN_H		// -*- c++ -*-
#define PROCESSMAN_H
///
// Copyright (C) 2002 - 2004, Fredrik Arnerup & Rasmus Kaj, See COPYING
///
#include "refcount.h"
#include <map>
#include <string>
#include <sigc++/object.h>
#include <sys/types.h>
#include <gtkmm/main.h>
#include <memory>

// Just forward declarations
namespace filedesc { class InBuf; class OutBuf; }

class ProcessRecord : public RefCounted {
public:
  // No public constructor -- Should only be created by ProcessManager
  
  /**
   * Wait for completition of this Process.
   * \return the exit code of the external process.
   */
  int wait();
  
  /**
   * An ostream writing from the host program to the standard input stream of
   * the external process.
   * \note: Do <em>not</em> keep references to this ostream
   */
  std::ostream& get_cin();

  /**
   * Close the data source to the standard input stream of the external
   * process, telling it that there is no more input.
   */
  void close_cin();
  
  /**
   * An istream reading to the host program from the standard output stream of
   * the external process.
   * \note: Do <em>not</em> keep references to this istream
   */
  std::istream& get_cout();

  /** Get the process id of the external process. */
  pid_t get_pid() const { return pid; }

private:
  /**
   * Actually start an external process.
   */
  ProcessRecord(const std::string& line);
  ProcessRecord(const std::vector<std::string>& line);
  
  friend class ProcessManager;

  std::string command_line;
  pid_t pid;
  std::auto_ptr<filedesc::InBuf> outbuf;
  std::auto_ptr<filedesc::OutBuf> inbuf;
  std::auto_ptr<std::ostream> in;
  std::auto_ptr<std::istream> out;
};

typedef Glib::RefPtr<ProcessRecord> Process;

//This is a singleton
class ProcessManager: public sigc::trackable {
public:
  // Signal that a process has stopped,
  // attach process id, a bool to signify normal termination
  // and an exit code that is only meaningful if the bool is true:
  sigc::signal<void, pid_t, bool, int> process_stopped;

  static ProcessManager &instance();

  Process run(const std::string& command);
  Process run(const std::vector<std::string>& command);
  bool stop(pid_t pid);
  Process *get_process(pid_t pid);
  bool stop_all();
  bool run_check();
  int system(std::string);
  int system(std::vector<std::string>& command);
  
private:
  static ProcessManager *_instance;
  ProcessManager();
  ProcessManager(const ProcessManager&);
  void operator = (const ProcessManager&);
  ~ProcessManager();
  typedef std::map<pid_t, Process> Processes;
  Processes processes;
};

#endif
