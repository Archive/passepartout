#ifndef PUTIL_TYPEINFO_H	// -*- c++ -*-
#define PUTIL_TYPEINFO_H
///
// Copyright (C) 2002 - 2004, Fredrik Arnerup & Rasmus Kaj, See COPYING
///
#include <string>

/**
 * Get information about specific types.  The template must be explicitly
 * instantiated for each type that is converted using to to<Tp> template.
 */
template <typename Tp>
class TypeInfo {
public:
  /** Get the (user friendly) name of a type. */
  static std::string name();
};

#endif
