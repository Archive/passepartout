#ifndef TMPFILE_H		// -*- c++ -*-
#define TMPFILE_H
///
// Copyright (C) 2002 - 2004, Fredrik Arnerup & Rasmus Kaj, See COPYING
///
#include <string>

// Creates a temp file and deletes it in the destructor
class Tempfile {
public:
  static std::string find_new_name();
  static void set_prefix(const std::string &prefix);
  Tempfile();
  const std::string &get_filename() const {return filename;}
  ~Tempfile();

private:
  static std::string tmplate;
  std::string filename;
  Tempfile(const Tempfile&);
  void operator = (const Tempfile&);
};

#endif
