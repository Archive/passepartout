#ifndef PUTIL_BARRIER_H		// -*- c++ -*-
#define PUTIL_BARRIER_H
///
// Copyright (C) 2002 - 2004, Fredrik Arnerup & Rasmus Kaj, See COPYING
///
#include <glibmm/thread.h>

/**
 * A simple Barrier impelemented with a condition variable.  The Barrier is
 * initially closed, any number of threads can {wait} for it, and when the
 * barrier is {open}ed all waiting threads are free to continue.
 * Calling {wait} on a Barrier that is allready {open} returns imediatley.
 */
class Barrier {
public:
  Barrier();
  ~Barrier();
  
  /** Wait for the barrier to become open. */
  void wait();
  
  /** 
   * Open the barrier, all current and future {wait}s are
   * returned. Opening a Barrier that is already open is a no-op.
   */
  void open();

  /** Get current state of the {Barrier}.  This method is non-blocking. */
  bool get_open() { return is_open; }
  
  /**
   * Close the barrier so it can be used again.
   * Caution: If a barrier is {open}ed and then {reclose}d, it can't be
   * garanteed that all {wait}ing threads is continued before the {reclose}.
   */
  void reclose();
  
private:
  Glib::Mutex mutex;
  Glib::Cond cond;
  volatile bool is_open;

  // Undefined - forbidden
  Barrier(const Barrier&);
  Barrier& operator = (const Barrier&);
};

#endif
