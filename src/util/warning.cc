///
// Copyright (C) 2002 - 2004, Fredrik Arnerup & Rasmus Kaj, See COPYING
///
#include "warning.h"

#include <cstdio>

class NullFdBuf : public std::streambuf {
public:
  NullFdBuf() { setp(0, 0); }
protected:
  int sync() { return EOF; }
  int overflow(int c) { return EOF; }
};

std::ostream cerrAlias::null(new NullFdBuf());

// The actual alias streams.  The constructor arg is default visibility.
cerrAlias warning(true), verbose(false), debug(false);
