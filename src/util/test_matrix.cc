///
// Copyright (C) 2003, 2004, Fredrik Arnerup & Rasmus Kaj, See COPYING
///
#include "matrix.h"
#include "testbed/testbed.hh"

void testMatrix() {
  // Just some bloody simple tests here yet ...
  
  const Matrix e = Matrix::rotation(0.57) * Matrix::rotation(-0.57);
  ASSERT(e.is_plain());
  
  ASSERT(e.inv() == e);
  
  const Matrix e2 = Matrix::rotation(0.57) * Matrix::rotation(0.57).inv();
  ASSERT(e2 == e);
  
  ASSERT(Matrix() == Matrix::scaling(1, 1));
  ASSERT(Matrix() == Matrix::rotation(0));
  ASSERT(Matrix() == Matrix::translation(0, 0));
  ASSERT(Matrix::translation(1, 1) * Matrix::translation(-1, -1) == Matrix());
}

namespace {
  TestFunction t("Matrix", &testMatrix);
}
