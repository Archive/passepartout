#ifndef RECTBOUNDARY_H		// -*- c++ -*-
#define RECTBOUNDARY_H
///
// Copyright (C) 2002 - 2004, Fredrik Arnerup & Rasmus Kaj, See COPYING
///
#include "boundary.h"

/**
 * A RectBoundary is a unit square boundary, transformed by an arbitrary
 * transform matrix, yielding an arbitrary rectangle.
 */
class RectBoundary : public BoundaryBase {
public:
  static Boundary create(const Matrix& m,
			 const double width = 1, const double height = 1);
  
  /** Create a distinct Boundary identical to this one. */
  Boundary clone() const;
  
  std::ostream& print(std::ostream&) const;

  /**
   * Get the coordinates for a basic corner of this boundary.
   */
  virtual Vector getCorner(Corner::Id corner) const;
  
  /**
   * Check if v is inside or within distance dist from this boundary.
   */
  virtual bool isInsideOrClose(const Vector& v, const float& dist = 0);
  
  void grow(const float& amount);
  
  /**
   * Get the polygon limiting this boundary.
   */
  virtual Polygon get_polygon();
  
  /**
   * Transform this boundary by a matrix m.
   * Semantically "this = this * m".
   */
  virtual void transform(const Matrix& m);
  
  /**
   * Transform this boundary by a matrix m.
   * Semantically "this = m * this".
   */
  virtual void transformLeft(const Matrix& m);
  
  virtual bool equals(const BoundaryBase* m) const;
  
  float get_width() const;
  float get_height() const;
private:
  Matrix matrix;
  
  RectBoundary(const Matrix& m, const double width, const double height);
  
  // Undefined ctors, avoid defaults
  RectBoundary(const RectBoundary&);
  RectBoundary();
  void operator = (const RectBoundary&);
};

#endif
