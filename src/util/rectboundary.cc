///
// Copyright (C) 2002 - 2004, Fredrik Arnerup & Rasmus Kaj, See COPYING
///
#include "rectboundary.h"
#include <iostream>

RectBoundary::RectBoundary(const Matrix& m,
			   const double width, const double height)
  : matrix(Matrix::scaling(width, height) * m)
{}

Boundary RectBoundary::create(const Matrix& m,
			      const double width, 
			      const double height) {
  Boundary result(new RectBoundary(m, width, height));
  result->reference();
  return result;
}

Boundary RectBoundary::clone() const {
  return create(matrix);
}

std::ostream& RectBoundary::print(std::ostream& out) const {
  Vector origo = matrix.transform(Vector(0, 0));
  Vector size = matrix.transform(Vector(1, 1)) - origo;

  out << size.x << "x" << size.y
      << (origo.x>=0? "+": "") << origo.x
      << (origo.y>=0? "+": "") << origo.y;
  
  return out;
}

Vector RectBoundary::getCorner(Corner::Id corner) const {
  Vector basic;
  switch(corner) {
  case Corner::LL:
    basic = Vector(0, 0);
    break;
  case Corner::LR:
    basic = Vector(1, 0);
    break;
  case Corner::UL:
    basic = Vector(0, 1);
    break;
  case Corner::UR:
    basic = Vector(1, 1);
    break;
  }
  return matrix.transform(basic);
}

bool RectBoundary::isInsideOrClose(const Vector& v, const float& dist) {
  const Matrix inv = matrix.inv();
  const Vector i = inv.transform(v);
  const float d2 = chessboard_dist(inv.transform(Vector(dist, 0)), 
				   inv.transform(Vector(0, 0)));
  return ((i.x >= -d2 && i.x <= 1+d2 && i.y >= -d2 && i.y <= 1+d2));
}

void RectBoundary::grow(const float& amount) {
  Matrix m2(matrix);
  m2.set_scale(1,1);
  Vector size(matrix.sc_x(), matrix.sc_y());
  size += Vector(2*amount, 2*amount);
  Vector origo = m2.transform(Vector(-amount, -amount));
  m2.set_tr(origo);
  m2.set_scale(size.x, size.y);
  matrix = m2;
}

bool RectBoundary::equals(const BoundaryBase* other) const {
  const RectBoundary* other_r = dynamic_cast<const RectBoundary*>(other);
  return other_r && matrix == other_r->matrix;
}

void RectBoundary::transform(const Matrix& m) {
  matrix = matrix * m;
}

void RectBoundary::transformLeft(const Matrix& m) {
  matrix = m * matrix;
}

Polygon RectBoundary::get_polygon() {
  Polygon result;
  result.push_back(getCorner(Corner::LL));
  result.push_back(getCorner(Corner::UL));
  result.push_back(getCorner(Corner::UR));
  result.push_back(getCorner(Corner::LR));
  return result;
}

float RectBoundary::get_width()  const { return matrix.sc_x(); }
float RectBoundary::get_height() const { return matrix.sc_y(); }
