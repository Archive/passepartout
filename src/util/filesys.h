#ifndef UTIL_FILESYS_H		// -*- c++ -*-
#define UTIL_FILESYS_H
///
// Copyright (C) 2002 - 2004, Fredrik Arnerup & Rasmus Kaj, See COPYING
///
#include <time.h>
#include <string>
#include <ios>			// mode for access
#include <stdexcept>

/// An exception to use when a libc call fails and sets errno.
class ClibException : public std::runtime_error {
public:
  explicit ClibException(const std::string& msg);
};

/// Check if the specified file exists
bool exists(const std::string& filename);

/// Check if the specified file can be accessed
bool access(const std::string& filename, int mode = std::ios::in);

/// Check if the file is executable
bool executable(const std::string& filename);

/// Get date of last modification.
time_t modified(const std::string& filename);

/// Get the part after the last dot.
std::string suffix(const std::string& filename);

/// Get the part before the last dot.
std::string no_suffix(const std::string& filename);

/// Get the part after the last slash (filename without directory path)
std::string basename(const std::string& filename);

/// Get everything up to and and including the last slash
std::string path(const std::string& filename);

/// Expand all /./, /../, symbolic links and stuff
/** Does not work with "~" (tilde)!  If filename is empty then current
 * path is assumed. Only works on existing files / paths. */
std::string expand_path(const std::string& filename);

/// Express filename assuming current path is start_path
std::string relative_path(const std::string& start_path,
			  const std::string& filename);

/// unlink(2) the specified filename, throw an exception on failure.
void unlink(const std::string& filename);

/// rmdir(2) the specified dirname, throw an exception on failure.
void rmdir(const std::string& dirname);

/// Create a temporary directory, as mkdtemp(3).
/** If {tmpl_name} is relative, it is put in $TMPDIR. */
std::string mkdtemp(const std::string& tmpl_name);

#endif
